﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.Wcf;
using StoreProvisioning.Services;
using StoreProvisioning.Services.Common;
using StoreProvisioning.Services.Proxy.ProductService;
using StoreProvisioning.Services.Strategies;
using StoreProvisioning.Services.Strategies.Contracts;

namespace StoreProvisioning.Web
{
    public class DependencyConfig
    {
        public static void RegisterDependencies()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<TaskStrategy>().As<ITaskStrategy>();
            containerBuilder.RegisterType<HttpCache>().As<HttpCache>();
            containerBuilder.RegisterType<DataFacade>().As<IDataFacade>();
            containerBuilder.RegisterType<OrdersBackendServiceClient>()
                .As<IOrdersBackendService>();
            containerBuilder.RegisterType<AuthStrategy>().As<IAuthStrategy>();
            containerBuilder.RegisterControllers(Assembly.GetExecutingAssembly());
            var container = containerBuilder.Build();

            AutofacHostFactory.Container = container;
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}