﻿using System.Web.Optimization;
using StoreProvisioning.Web.Helpers;

namespace StoreProvisioning.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Manager
            
            var mangerCssBundle = new StyleBundle("~/Content/managerCss").Include(
                "~/Content/bootstrap-theme.css",
                "~/Content/bootstrap.css",
                "~/Content/bootstrapValidator.min.css",
                "~/Content/main.css");

            mangerCssBundle.Orderer = new CoherentlyBundleOrderer(); 
            bundles.Add(mangerCssBundle);

            var managerJsBundle = new ScriptBundle("~/Scripts/managerScripts").Include(
                "~/Scripts/jquery-1.11.1.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/bootstrapValidator.min.js",
                "~/Scripts/main.js",
                "~/Scripts/validationModalRules.js",
                "~/Scripts/jquery.validate.js");
            
            managerJsBundle.Orderer = new CoherentlyBundleOrderer();
            bundles.Add(managerJsBundle);
            #endregion
        }
    }
}