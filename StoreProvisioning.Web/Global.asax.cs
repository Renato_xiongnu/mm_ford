﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using log4net.Config;
using StoreProvisioning.Services.Model;
using StoreProvisioning.Web.Helpers;

namespace StoreProvisioning.Web
{
    public class MvcApplication : HttpApplication
    {
        private static ILog Log = LogManager.GetLogger("ServerErrors");

        protected void Application_Start()
        {
            XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyConfig.RegisterDependencies();
            ModelBinders.Binders.Add(typeof (OrderViewModel), new OrderModelBinder());
        }

        protected void Application_Error()
        {
            var exception = Server.GetLastError();
            Log.Fatal(exception);
        }
    }
}