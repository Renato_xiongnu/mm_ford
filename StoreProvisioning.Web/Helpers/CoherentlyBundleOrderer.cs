﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace StoreProvisioning.Web.Helpers
{
    public class CoherentlyBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}