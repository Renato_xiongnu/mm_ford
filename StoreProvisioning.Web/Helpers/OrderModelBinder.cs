﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Mvc;
using StoreProvisioning.DAL.Models;
using StoreProvisioning.DAL.Models.Content;
using StoreProvisioning.Services.Model;

namespace StoreProvisioning.Web.Helpers
{
    public class OrderModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var form = controllerContext.HttpContext.Request.Form;

            var order = new OrderViewModel
            {
                Task = new TaskItem
                {
                    TaskId = Convert.ToInt32(form["Task.TaskId"])
                },
                //MoveItemsCommand = new MoveItemsCommand
                //{
                //    Outcome = form["MoveItemsCommand.Outcome"],
                //    RequestId = form["MoveItemsCommand.RequestId"],
                //    AdditionalContent = GetAdditionalContent(form, form["MoveItemsCommand.AdditionalContentType"]),
                //    OrderLines = GetOrderLines(form)
                //}
            };
            
            return order;
        }

        private IList<OrderLine> GetOrderLines(NameValueCollection form)
        {
            var result = new List<OrderLine>();
            int i = 0;
            while (true)
            {
                bool hasFirstValue = form.Get(string.Format("[{0}].ArticleId", i)) != null;
                if (!hasFirstValue) return result;

                var additionalComment = string.IsNullOrEmpty(form.Get(string.Format("[{0}].AdditionalComment", i)))
                                        ? string.Empty
                                        : string.Format(" :: {0}", form.Get(string.Format("[{0}].AdditionalComment", i)));

                var orderLine = new OrderLine
                {
                    ArticleId = Convert.ToInt64(form.Get(string.Format("[{0}].ArticleId", i))),
                    OrderLineId = Convert.ToInt32(form.Get(string.Format("[{0}].OrderLineId", i))),
                    LineComment = form.Get(string.Format("[{0}].LineComment", i)) + additionalComment
                };

                var reviewStatus = form.Get(string.Format("[{0}].OrderLineResult.ReviewStatus", i));

                orderLine.OrderLineResult = new OrderLineResult
                {
                    SuccesfullyFulfilled = string.Equals("Отложен",reviewStatus,StringComparison.InvariantCultureIgnoreCase),
                    ReviewStatus = reviewStatus,
                    ItemStateDescription = form.Get(string.Format("[{0}].OrderLineResult.ItemStateDescription", i))
                };

                result.Add(orderLine);

                i++;
            }
        }

        private AdditionalContent GetAdditionalContent(NameValueCollection form, string type)
        {
            switch (type)
            {
                case CommandNames.AssembleItemsContent:
                    return new AssembleItemsContent
                    {
                        Comments = form["Comments"]
                    };

                case CommandNames.ReturnItemsContent:
                    return new ReturnItemsContent
                    {
                        Comments = form["Comments"]
                    };

                default:
                    return new AdditionalContent
                    {
                        Comments = form["Comments"]
                    };
            }
        }
    }
}