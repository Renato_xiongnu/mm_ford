﻿using System;
using System.Web.Mvc;
using log4net;

namespace StoreProvisioning.Web.Controllers
{
    public class ErrorsController : Controller
    {
        private static ILog Logger = LogManager.GetLogger(typeof (ErrorsController).Name);

        public ActionResult Error()
        {
            var message = Convert.ToString(TempData["ErrorMessage"]);
            ResaveMessage(message);
            
            Logger.Error(message);

            return View("Error", new Exception(message));
        }

        private void ResaveMessage(string message)
        {
            TempData["ErrorMessage"] = message;
        }
    }
}
