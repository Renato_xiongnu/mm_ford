﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StoreProvisioning.DAL.Enums;
using StoreProvisioning.DAL.Models.Content;
using StoreProvisioning.Services.Model;
using StoreProvisioning.Services.Strategies.Contracts;

namespace StoreProvisioning.Web.Controllers
{
    public class ManagerTaskController : Controller
    {
        private readonly ITaskStrategy _taskStrategy;
        private readonly IAuthStrategy _authStrategy;
        
        public ManagerTaskController(
            ITaskStrategy taskFacade,
            IAuthStrategy authStrategy)
        {
            _taskStrategy = taskFacade;
            _authStrategy = authStrategy;
        }

        public ActionResult Index(string taskId, string m, string u, string key)
        {
            SetCache();

            var authResult = _authStrategy.LoginByToken(u, m, taskId, key);
            if (authResult.HasErrors) return RedirectToErrorPage(authResult.Message);

            var taskResult = _taskStrategy.AssignTaskForMe(taskId);
            if (taskResult.HasErrors || (taskResult.Item != null && taskResult.Item.HasError))
                return RedirectToErrorPage(taskResult.Message);

            var taskItem = _taskStrategy.GetTask(taskId);
            if (taskItem.HasErrors) return RedirectToErrorPage(taskItem.Message);

            var commandItem = _commandStrategy.GetCommandItem(taskItem.Item.WorkItemId);
            if (commandItem.HasErrors) return RedirectToErrorPage(commandItem.Message);

            commandItem.Item.OrderLines.ToList().ForEach(
                x => { x.OrderLineAnswers = GetOrderLineAnswers(commandItem.Item.AdditionalContentType); });

            return View(new OrderViewModel
            {
                Task = taskItem.Item,
                MoveItemsCommand = commandItem.Item
            });
        }

        [HttpPost]
        public ActionResult Complete(OrderViewModel orderModel)
        {
            var taskData = new TaskExecutionResult
            {
                TaskId = orderModel.Task.TaskId.ToString(),
                //Outcome = orderModel.MoveItemsCommand.Outcome,
                //Comments = orderModel.MoveItemsCommand.AdditionalContent.Comments
            };
            
            var saveTaskResult = _taskStrategy.SaveTaskOutcome(taskData);
            if (!saveTaskResult.HasErrors)
            {
                return View("Success");
            }

            return RedirectToErrorPage();
        }

        public ActionResult RedirectToErrorPage(string message = "Ошибка при завершении задачи")
        {
            TempData["ErrorMessage"] = message;
            return RedirectToAction("Error", "Errors");
        }
        
        [ChildActionOnly]
        public PartialViewResult ItemContent(string modelTypeName, AdditionalContent content)
        {
            switch (modelTypeName)
            {
                case CommandNames.AssembleItemsContent:
                    return PartialView("Partials/_AssembleItemsContent", content);

                case CommandNames.ReturnItemsContent:
                    return PartialView("Partials/_ReturnItemsContent", content);

                default:
                    return PartialView("Error");
            }
        }

        public ActionResult FinishLater(string taskId)
        {
            var result = _taskStrategy.FinishTaskLater(taskId);
            if (result.HasErrors)
            {
                return RedirectToErrorPage();
            }

            return View("FinishLater");
        }

        private IList<SelectListItem> GetOrderLineAnswers(string modelTypeName)
        {
            var orderLines = new List<SelectListItem>();

            switch (modelTypeName)
            {
                case CommandNames.AssembleItemsContent:
                    orderLines.Add(new SelectListItem
                    {
                        Text = GetReviewItemStateDisplayName(ReviewItemState.Reserved),
                        Value = "0",
                    });
                    orderLines.Add(new SelectListItem
                    {
                        Text = GetReviewItemStateDisplayName(ReviewItemState.ReservedPart),
                        Value = "1",
                    });
                    orderLines.Add(new SelectListItem
                    {
                        Text = GetReviewItemStateDisplayName(ReviewItemState.ReservedDisplayItem),
                        Value = "2",
                    });
                    orderLines.Add(new SelectListItem
                    {
                        Text = GetReviewItemStateDisplayName(ReviewItemState.NotFound),
                        Value = "3",
                    });
                    orderLines.Add(new SelectListItem
                    {
                        Text = GetReviewItemStateDisplayName(ReviewItemState.IncorrectPrice),
                        Value = "4",
                    });
                    break;
            }

            return orderLines;
        }

        private string GetReviewItemStateDisplayName(ReviewItemState? state)
        {
            switch (state)
            {
                case ReviewItemState.Empty:
                    return "";
                case ReviewItemState.IncorrectPrice:
                    return "Неправильная цена";
                case ReviewItemState.NotFound:
                    return "Не найден";
                case ReviewItemState.Reserved:
                    return "Отложен";
                case ReviewItemState.ReservedDisplayItem:
                    return "Отложен витринный экземпляр";
                case ReviewItemState.ReservedPart:
                    return "Отложен частично";
                case ReviewItemState.DisplayItem:
                    return "Витрина";
                case ReviewItemState.Transfer:
                    return "Трансфер";
                default:
                    return "";
            }
        }

        private void SetCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();
        }
    }
}