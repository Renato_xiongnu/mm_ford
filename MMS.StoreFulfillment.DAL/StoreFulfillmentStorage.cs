﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;

namespace MMS.StoreFulfillment.DAL
{
    public class StoreFulfillmentStorage:IStoreFulfillmentStorage, IDisposable
    {
        private readonly StoreFulfillmentContext _storeFulfillmentContext;

        public StoreFulfillmentStorage(StoreFulfillmentContext storeFulfillmentContext)
        {
            _storeFulfillmentContext = storeFulfillmentContext;
        }

        public ItemsMovementRequest GetById(string requestId,
            Expression<Func<ItemsMovementRequest, object>>[] includes=null)
        {
            var query = PrepareItemsQuery(
                _storeFulfillmentContext.Requests.AsQueryable(), includes);
            return query
                    .FirstOrDefault(t => t.RequestId == requestId);
        }

        public IQueryable<ItemsMovementRequest> Find(Expression<Func<ItemsMovementRequest, bool>> query,
            Expression<Func<ItemsMovementRequest, object>>[] includes = null)
        {
            return PrepareItemsQuery(_storeFulfillmentContext.Requests.AsQueryable(), includes).Where(query);
        }

        public PlanStep GetPlanStepByTaskId(string taskId)
        {
            return
                _storeFulfillmentContext.PlanSteps.Include(t=>t.TicketTask).FirstOrDefault(
                    t => t.TicketTask != null && t.TicketTask.TaskId == taskId);
        }

        public PlanStep GetPlanStepById(int planStepId)
        {
            return _storeFulfillmentContext.PlanSteps.Include(t => t.TicketTask).FirstOrDefault(t => t.Id == planStepId);
        }

        public void Save(ItemsMovementRequest request)
        {
            if (_storeFulfillmentContext.Entry(request).State == EntityState.Detached)
            {
                _storeFulfillmentContext.Requests.Add(request);
            }
            else
            {
                request.UpdatedDate = DateTime.UtcNow;
            }
            _storeFulfillmentContext.SaveChanges();
        }

        public IList<ItemsMovementRequest> GetByCustomerOrderId(string customerOrderId, Expression<Func<ItemsMovementRequest, object>>[] expressions)
        {
            return
                PrepareItemsQuery(_storeFulfillmentContext.Requests.AsQueryable(), expressions)
                    .Where(el => el.CustomerOrder.CustomerOrderId == customerOrderId).ToList();
        }

        public IList<PlanStep> GetUnprocessedPlanSteps(int count = 100)
        {
            return
                _storeFulfillmentContext.PlanSteps.Where(el => el.Status == PlanStepStatus.Pending).Take(count).ToList();
        }

        public void Save(PlanStep requestBase)
        {
            _storeFulfillmentContext.Entry(requestBase).State = EntityState.Modified;
            _storeFulfillmentContext.SaveChanges();
        }

        public ItemsMovementRequest GetByTicketId(string ticketId)
        {
            return
                PrepareItemsQuery(_storeFulfillmentContext.Requests.AsQueryable(),
                    new Expression<Func<ItemsMovementRequest, object>>[]
                    {t => t.PlanSteps, t => t.PlanSteps.Select(t1 => t1.TicketTask), t => t.Items})
                    .First(t => t.TicketId == ticketId);
        }

        private IQueryable<ItemsMovementRequest> PrepareItemsQuery(IQueryable<ItemsMovementRequest> queryable,
            IEnumerable<Expression<Func<ItemsMovementRequest, object>>> includeExpressions)
        {
            if (includeExpressions == null)
                return
                    queryable;
            return includeExpressions.Aggregate(queryable, (current, includeExpression) => current.Include(includeExpression));
        }

        public void Dispose()
        {
            _storeFulfillmentContext.Dispose();
        }
    }
}
