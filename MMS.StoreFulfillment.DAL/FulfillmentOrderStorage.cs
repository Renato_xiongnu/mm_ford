﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MMS.StoreFulfillment.Entities.FORD;

namespace MMS.StoreFulfillment.DAL
{
    public class FulfillmentOrderStorage : IFulfillmentOrderRepository, IDisposable
    {
        private readonly StoreFulfillmentContext _storeFulfillmentContext;

        public FulfillmentOrderStorage(StoreFulfillmentContext storeFulfillmentContext)
        {
            _storeFulfillmentContext = storeFulfillmentContext;
        }

        public void Save(FulfillmentOrder fulfillmentOrder)
        {
            if (_storeFulfillmentContext.Entry(fulfillmentOrder).State == EntityState.Detached)
            {
                _storeFulfillmentContext.FulfillmentOrders.Add(fulfillmentOrder);
                fulfillmentOrder.UpdatedDate = DateTime.UtcNow;
            }
            else
            {
                fulfillmentOrder.UpdatedDate = DateTime.UtcNow;
            }
            _storeFulfillmentContext.SaveChanges();
        }

        public FulfillmentOrder GetById(string fulfillmentOrderId)
        {
            return _storeFulfillmentContext.FulfillmentOrders
                .Include(t => t.FulfillmentPlans)
                .Include(t => t.FulfillmentPlans.Select(p => p.FulfillmentPlanSteps))
                .Include(t => t.FulfillmentItems)
                .FirstOrDefault(t => t.Id == fulfillmentOrderId);
        }

        public IList<FulfillmentOrder> GetForCustomerOrderId(string customerOrderId)
        {
            return _storeFulfillmentContext.FulfillmentOrders
                .Include(t => t.FulfillmentPlans)
                .Include(t => t.FulfillmentPlans.Select(p => p.FulfillmentPlanSteps))
                .Include(t => t.FulfillmentItems)
                .Where(t => t.SourceOrder.CustomerOrderId == customerOrderId)
                .ToList();
        }

        public IList<FulfillmentPlanStep> GetUprocessedPlanSteps()
        {
            return _storeFulfillmentContext
                .FulfillmentPlanSteps
                .Where(t => t.FulfillmentPlanStepStatus == FulfillmentPlanStepStatus.Pending)
                .ToList();
        }

        public void Save(FulfillmentPlanStep unprocessedPlanStep)
        {
            if (_storeFulfillmentContext.Entry(unprocessedPlanStep).State == EntityState.Detached)
            {
                _storeFulfillmentContext.FulfillmentPlanSteps.Add(unprocessedPlanStep);
            }
            else
            {
                //fulfillmentOrder.UpdatedDate = DateTime.UtcNow;
            }
            _storeFulfillmentContext.SaveChanges();
        }

        public void Dispose()
        {
            _storeFulfillmentContext.Dispose();
        }
    }
}
