﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;

namespace MMS.StoreFulfillment.DAL
{
    public class StoreFulfillmentContext:DbContext
    {
        public DbSet<ItemsMovementRequest> Requests { get; set; }
        public DbSet<PlanStep> PlanSteps { get; set; }
        public DbSet<FulfillmentOrder> FulfillmentOrders { get; set; }
        public DbSet<FulfillmentPlanStep> FulfillmentPlanSteps { get; set; }

        public StoreFulfillmentContext()
            : base("StoreFulfillmentContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<string>().Configure(t => t.HasMaxLength(500));

            modelBuilder.Configurations.Add(new ItemsMovementRequestMap());
            modelBuilder.Configurations.Add(new FulfillmentOrderMap());

            base.OnModelCreating(modelBuilder);
        }
    }

    public class ItemsMovementRequestMap : EntityTypeConfiguration<ItemsMovementRequest>
    {
        public ItemsMovementRequestMap()
        {
            HasKey(t => t.RequestId);
            Property(t => t.RequestId).HasMaxLength(100);
            Property(t => t.TimeStamp).IsRowVersion();
            Property(t => t.BasedOnCommandId).HasMaxLength(300);
            Property(t => t.BasedOnRequestId).HasMaxLength(100);
            Property(t => t.Comments).HasMaxLength(500);
            Property(t => t.SapCode).HasMaxLength(4);
            Property(t => t.TicketId).HasMaxLength(100);
            Property(t => t.WwsOrderId).HasMaxLength(15);
            Property(t => t.CustomerOrder.CustomerOrderId).HasMaxLength(50);
            Property(t => t.CustomerOrder.OrderSource).HasMaxLength(25);
            Property(t => t.CustomerOrder.ShippingMethod).HasMaxLength(25);
            Property(t => t.CustomerOrder.PaymentType).HasMaxLength(50);
            Property(t => t.Contact.RequestedDeliveryServiceOption).HasMaxLength(100);
            Property(t => t.Contact.RequestedDeliveryServiceOption).HasMaxLength(50);
        }
    }

    public class FulfillmentOrderMap : EntityTypeConfiguration<FulfillmentOrder>
    {
        public FulfillmentOrderMap()
        {
            Property(t => t.Id).HasMaxLength(100);
        }
    }
}
