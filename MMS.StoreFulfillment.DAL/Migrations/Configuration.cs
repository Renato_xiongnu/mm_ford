using System.Data.Entity.Migrations;

namespace MMS.StoreFulfillment.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<StoreFulfillmentContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StoreFulfillmentContext context)
        {
        }
    }
}
