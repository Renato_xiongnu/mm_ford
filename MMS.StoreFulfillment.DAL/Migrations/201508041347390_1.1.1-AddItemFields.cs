namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _111AddItemFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "HasShippedFromStock", c => c.Boolean(nullable: false));
            AddColumn("dbo.Items", "BrandTitle", c => c.String(maxLength: 500));
            AddColumn("dbo.Items", "StorageCell", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "StorageCell");
            DropColumn("dbo.Items", "BrandTitle");
            DropColumn("dbo.Items", "HasShippedFromStock");
        }
    }
}
