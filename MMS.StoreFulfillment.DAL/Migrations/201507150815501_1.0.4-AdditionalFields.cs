namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _104AdditionalFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemsMovementRequests", "Contact_RequestedDeliveryDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemsMovementRequests", "Contact_RequestedDeliveryDate");
        }
    }
}
