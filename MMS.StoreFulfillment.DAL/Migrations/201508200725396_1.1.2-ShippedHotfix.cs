namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _112ShippedHotfix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemsMovementRequests", "IsShipped", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemsMovementRequests", "IsShipped");
        }
    }
}
