namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlanStep_AddColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanSteps", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.PlanSteps", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.PlanSteps", "FinishedDate", c => c.DateTime());
            AddColumn("dbo.TicketTasks", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.TicketTasks", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.TicketTasks", "FinishedDate", c => c.DateTime());
            AddColumn("dbo.TicketTasks", "RequiredFiled", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketTasks", "RequiredFiled");
            DropColumn("dbo.TicketTasks", "FinishedDate");
            DropColumn("dbo.TicketTasks", "UpdatedDate");
            DropColumn("dbo.TicketTasks", "CreatedDate");
            DropColumn("dbo.PlanSteps", "FinishedDate");
            DropColumn("dbo.PlanSteps", "UpdatedDate");
            DropColumn("dbo.PlanSteps", "CreatedDate");
        }
    }
}
