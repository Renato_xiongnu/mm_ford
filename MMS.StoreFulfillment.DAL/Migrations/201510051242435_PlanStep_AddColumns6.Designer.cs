// <auto-generated />
namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PlanStep_AddColumns6 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PlanStep_AddColumns6));
        
        string IMigrationMetadata.Id
        {
            get { return "201510051242435_PlanStep_AddColumns6"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
