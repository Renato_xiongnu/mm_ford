namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _103 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemsMovementRequests", "BasedOnCommand", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemsMovementRequests", "BasedOnCommand");
        }
    }
}
