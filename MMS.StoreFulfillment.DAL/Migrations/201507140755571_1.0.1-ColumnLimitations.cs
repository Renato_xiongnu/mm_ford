namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class _101ColumnLimitations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests");
            DropForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests");
            DropIndex("dbo.PlanSteps", new[] { "RequestId" });
            DropIndex("dbo.Items", new[] { "ItemsMovementRequest_RequestId" });
            DropPrimaryKey("dbo.ItemsMovementRequests");
            AlterColumn("dbo.PlanSteps", "RequestId", c => c.String(maxLength: 500));
            AlterColumn("dbo.PlanSteps", "CommandType", c => c.String(maxLength: 500));
            AlterColumn("dbo.PlanSteps", "TaskType", c => c.String(maxLength: 500));
            AlterColumn("dbo.TicketTasks", "TaskId", c => c.String(maxLength: 500));
            AlterColumn("dbo.TicketTasks", "Outcome", c => c.String(maxLength: 500));
            AlterColumn("dbo.TicketTasks", "TaskType", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "RequestId", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "BasedOnRequestId", c => c.String(maxLength: 100));
            AlterColumn("dbo.ItemsMovementRequests", "BasedOnCommandId", c => c.String(maxLength: 300));
            AlterColumn("dbo.ItemsMovementRequests", "SapCode", c => c.String(maxLength: 4));
            AlterColumn("dbo.ItemsMovementRequests", "LastStatusReason", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "WwsOrderId", c => c.String(maxLength: 15));
            AlterColumn("dbo.ItemsMovementRequests", "Comments", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "TicketId", c => c.String(maxLength: 100));
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_CustomerOrderId", c => c.String(maxLength: 50));
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_OrderSource", c => c.String(maxLength: 25));
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_PaymentType", c => c.String(maxLength: 50));
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_SocialCardNumber", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_ShippingMethod", c => c.String(maxLength: 25));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_FirstName", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_LastName", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_Phone", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_AdditionalPhone", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_Email", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_Address", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_City", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_ZIPCode", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_RequestedDeliveryServiceOption", c => c.String(maxLength: 50));
            AlterColumn("dbo.ItemsMovementRequests", "Contact_RequestedDeliveryTimeslot", c => c.String(maxLength: 500));
            AlterColumn("dbo.Items", "Title", c => c.String(maxLength: 500));
            AlterColumn("dbo.Items", "SerialNumber", c => c.String(maxLength: 500));
            AlterColumn("dbo.Items", "ReserveStatus", c => c.String(maxLength: 500));
            AlterColumn("dbo.Items", "ArticleProductType", c => c.String(maxLength: 500));
            AlterColumn("dbo.Items", "ProductGroupName", c => c.String(maxLength: 500));
            AlterColumn("dbo.Items", "ItemsMovementRequest_RequestId", c => c.String(maxLength: 500));
            AddPrimaryKey("dbo.ItemsMovementRequests", "RequestId");
            CreateIndex("dbo.PlanSteps", "RequestId");
            CreateIndex("dbo.Items", "ItemsMovementRequest_RequestId");
            AddForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests", "RequestId");
            AddForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests", "RequestId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests");
            DropForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests");
            DropIndex("dbo.Items", new[] { "ItemsMovementRequest_RequestId" });
            DropIndex("dbo.PlanSteps", new[] { "RequestId" });
            DropPrimaryKey("dbo.ItemsMovementRequests");
            AlterColumn("dbo.Items", "ItemsMovementRequest_RequestId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Items", "ProductGroupName", c => c.String());
            AlterColumn("dbo.Items", "ArticleProductType", c => c.String());
            AlterColumn("dbo.Items", "ReserveStatus", c => c.String());
            AlterColumn("dbo.Items", "SerialNumber", c => c.String());
            AlterColumn("dbo.Items", "Title", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_RequestedDeliveryTimeslot", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_RequestedDeliveryServiceOption", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_ZIPCode", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_City", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_Address", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_Email", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_AdditionalPhone", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_Phone", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_LastName", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Contact_FirstName", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_ShippingMethod", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_SocialCardNumber", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_PaymentType", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_OrderSource", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "CustomerOrder_CustomerOrderId", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "TicketId", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "Comments", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "WwsOrderId", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "LastStatusReason", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "SapCode", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "BasedOnCommandId", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "BasedOnRequestId", c => c.String());
            AlterColumn("dbo.ItemsMovementRequests", "RequestId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.TicketTasks", "TaskType", c => c.String());
            AlterColumn("dbo.TicketTasks", "Outcome", c => c.String());
            AlterColumn("dbo.TicketTasks", "TaskId", c => c.String());
            AlterColumn("dbo.PlanSteps", "TaskType", c => c.String());
            AlterColumn("dbo.PlanSteps", "CommandType", c => c.String());
            AlterColumn("dbo.PlanSteps", "RequestId", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.ItemsMovementRequests", "RequestId");
            CreateIndex("dbo.Items", "ItemsMovementRequest_RequestId");
            CreateIndex("dbo.PlanSteps", "RequestId");
            AddForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests", "RequestId");
            AddForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests", "RequestId");
        }
    }
}
