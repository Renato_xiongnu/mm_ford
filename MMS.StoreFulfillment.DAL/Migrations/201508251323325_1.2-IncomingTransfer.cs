namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12IncomingTransfer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "IncomingTransfer", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "IncomingTransfer");
        }
    }
}
