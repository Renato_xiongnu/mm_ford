namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _121FORDEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FulfillmentOrders",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 100),
                        SourceOrder_CustomerOrderId = c.String(maxLength: 500),
                        SourceOrder_OrderSource = c.String(maxLength: 500),
                        SourceOrder_IsOnlineOrder = c.Boolean(nullable: false),
                        SourceOrder_PaymentType = c.String(maxLength: 500),
                        SourceOrder_Downpayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SourceOrder_SocialCardNumber = c.String(maxLength: 500),
                        SourceOrder_ShippingMethod = c.String(maxLength: 500),
                        FulfillmentStatus = c.Short(nullable: false),
                        SapCode = c.String(maxLength: 500),
                        SaleLocationSapCode = c.String(maxLength: 500),
                        BasedOnCommand = c.String(maxLength: 500),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(nullable: false),
                        FinishedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FulfillmentPlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BasedOnTemplate = c.String(maxLength: 500),
                        FulfillmentOrder_Id = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FulfillmentOrders", t => t.FulfillmentOrder_Id)
                .Index(t => t.FulfillmentOrder_Id);
            
            CreateTable(
                "dbo.FulfillmentPlanSteps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlanStepType = c.String(maxLength: 500),
                        FulfillmentOrderId = c.String(maxLength: 500),
                        FulfillmentPlanStepStatus = c.Int(nullable: false),
                        PlanStepContext = c.String(maxLength: 500),
                        PlanStepContextTypeName = c.String(maxLength: 500),
                        Responsible = c.String(maxLength: 500),
                        CreatedDate = c.DateTime(nullable: false),
                        FinishedDate = c.DateTime(),
                        FulfillmentPlan_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FulfillmentPlans", t => t.FulfillmentPlan_Id)
                .Index(t => t.FulfillmentPlan_Id);
            
            AddColumn("dbo.ItemsMovementRequests", "FulfillmentOrderId", c => c.String(maxLength: 500));
            AddColumn("dbo.Items", "FulfillmentOrder_Id", c => c.String(maxLength: 100));
            CreateIndex("dbo.Items", "FulfillmentOrder_Id");
            AddForeignKey("dbo.Items", "FulfillmentOrder_Id", "dbo.FulfillmentOrders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FulfillmentPlanSteps", "FulfillmentPlan_Id", "dbo.FulfillmentPlans");
            DropForeignKey("dbo.FulfillmentPlans", "FulfillmentOrder_Id", "dbo.FulfillmentOrders");
            DropForeignKey("dbo.Items", "FulfillmentOrder_Id", "dbo.FulfillmentOrders");
            DropIndex("dbo.FulfillmentPlanSteps", new[] { "FulfillmentPlan_Id" });
            DropIndex("dbo.FulfillmentPlans", new[] { "FulfillmentOrder_Id" });
            DropIndex("dbo.Items", new[] { "FulfillmentOrder_Id" });
            DropColumn("dbo.Items", "FulfillmentOrder_Id");
            DropColumn("dbo.ItemsMovementRequests", "FulfillmentOrderId");
            DropTable("dbo.FulfillmentPlanSteps");
            DropTable("dbo.FulfillmentPlans");
            DropTable("dbo.FulfillmentOrders");
        }
    }
}
