namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomerOrder_DeliveryDate_v1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FulfillmentOrders", "SourceOrder_RequestedDeliveryDate", c => c.DateTime());
            AddColumn("dbo.ItemsMovementRequests", "CustomerOrder_RequestedDeliveryDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemsMovementRequests", "CustomerOrder_RequestedDeliveryDate");
            DropColumn("dbo.FulfillmentOrders", "SourceOrder_RequestedDeliveryDate");
        }
    }
}
