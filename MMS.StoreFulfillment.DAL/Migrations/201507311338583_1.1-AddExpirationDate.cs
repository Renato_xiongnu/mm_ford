namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _11AddExpirationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemsMovementRequests", "ExpirationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemsMovementRequests", "ExpirationDate");
        }
    }
}
