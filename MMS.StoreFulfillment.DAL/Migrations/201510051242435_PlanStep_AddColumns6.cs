namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlanStep_AddColumns6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketTasks", "RequiredFields", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TicketTasks", "RequiredFields");
        }
    }
}
