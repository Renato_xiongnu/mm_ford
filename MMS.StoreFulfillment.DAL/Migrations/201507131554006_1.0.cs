namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlanSteps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlanStepType = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        RequestId = c.String(maxLength: 128),
                        CommandType = c.String(),
                        TaskType = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        TicketTask_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TicketTasks", t => t.TicketTask_Id)
                .ForeignKey("dbo.ItemsMovementRequests", t => t.RequestId)
                .Index(t => t.RequestId)
                .Index(t => t.TicketTask_Id);
            
            CreateTable(
                "dbo.TicketTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.String(),
                        PlanStepId = c.Int(nullable: false),
                        Outcome = c.String(),
                        Completed = c.Boolean(nullable: false),
                        TaskType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemsMovementRequests",
                c => new
                    {
                        RequestId = c.String(nullable: false, maxLength: 128),
                        BasedOnRequestId = c.String(),
                        BasedOnCommandId = c.String(),
                        SapCode = c.String(),
                        Status = c.Short(nullable: false),
                        LastStatusReason = c.String(),
                        RequestType = c.Short(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(nullable: false),
                        FinishedDate = c.DateTime(),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        WwsOrderId = c.String(),
                        Comments = c.String(),
                        TicketId = c.String(),
                        CustomerOrder_CustomerOrderId = c.String(),
                        CustomerOrder_OrderSource = c.String(),
                        CustomerOrder_IsOnlineOrder = c.Boolean(nullable: false),
                        CustomerOrder_PaymentType = c.String(),
                        CustomerOrder_Downpayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CustomerOrder_SocialCardNumber = c.String(),
                        CustomerOrder_ShippingMethod = c.String(),
                        Contact_FirstName = c.String(),
                        Contact_LastName = c.String(),
                        Contact_Phone = c.String(),
                        Contact_AdditionalPhone = c.String(),
                        Contact_Email = c.String(),
                        Contact_Address = c.String(),
                        Contact_City = c.String(),
                        Contact_ZIPCode = c.String(),
                        Contact_Floor = c.Int(nullable: false),
                        Contact_RequestedDeliveryServiceOption = c.String(),
                        Contact_RequestedDeliveryTimeslot = c.String(),
                    })
                .PrimaryKey(t => t.RequestId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemExternalId = c.Long(nullable: false),
                        Title = c.String(),
                        Article = c.Long(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Qty = c.Int(nullable: false),
                        SerialNumber = c.String(),
                        ReserveStatus = c.String(),
                        ArticleProductType = c.String(),
                        ProductGroupName = c.String(),
                        ProductGroupNo = c.Int(nullable: false),
                        DepartmentNumber = c.Int(nullable: false),
                        Item_Id = c.Int(),
                        ItemsMovementRequest_RequestId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.Item_Id)
                .ForeignKey("dbo.ItemsMovementRequests", t => t.ItemsMovementRequest_RequestId)
                .Index(t => t.Item_Id)
                .Index(t => t.ItemsMovementRequest_RequestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests");
            DropForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests");
            DropForeignKey("dbo.Items", "Item_Id", "dbo.Items");
            DropForeignKey("dbo.PlanSteps", "TicketTask_Id", "dbo.TicketTasks");
            DropIndex("dbo.Items", new[] { "ItemsMovementRequest_RequestId" });
            DropIndex("dbo.Items", new[] { "Item_Id" });
            DropIndex("dbo.PlanSteps", new[] { "TicketTask_Id" });
            DropIndex("dbo.PlanSteps", new[] { "RequestId" });
            DropTable("dbo.Items");
            DropTable("dbo.ItemsMovementRequests");
            DropTable("dbo.TicketTasks");
            DropTable("dbo.PlanSteps");
        }
    }
}
