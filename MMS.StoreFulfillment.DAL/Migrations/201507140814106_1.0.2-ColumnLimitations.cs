namespace MMS.StoreFulfillment.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _102ColumnLimitations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests");
            DropForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests");
            DropIndex("dbo.PlanSteps", new[] { "RequestId" });
            DropIndex("dbo.Items", new[] { "ItemsMovementRequest_RequestId" });
            DropPrimaryKey("dbo.ItemsMovementRequests");
            AlterColumn("dbo.PlanSteps", "RequestId", c => c.String(maxLength: 100));
            AlterColumn("dbo.ItemsMovementRequests", "RequestId", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Items", "ItemsMovementRequest_RequestId", c => c.String(maxLength: 100));
            AddPrimaryKey("dbo.ItemsMovementRequests", "RequestId");
            CreateIndex("dbo.PlanSteps", "RequestId");
            CreateIndex("dbo.Items", "ItemsMovementRequest_RequestId");
            AddForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests", "RequestId");
            AddForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests", "RequestId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests");
            DropForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests");
            DropIndex("dbo.Items", new[] { "ItemsMovementRequest_RequestId" });
            DropIndex("dbo.PlanSteps", new[] { "RequestId" });
            DropPrimaryKey("dbo.ItemsMovementRequests");
            AlterColumn("dbo.Items", "ItemsMovementRequest_RequestId", c => c.String(maxLength: 500));
            AlterColumn("dbo.ItemsMovementRequests", "RequestId", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.PlanSteps", "RequestId", c => c.String(maxLength: 500));
            AddPrimaryKey("dbo.ItemsMovementRequests", "RequestId");
            CreateIndex("dbo.Items", "ItemsMovementRequest_RequestId");
            CreateIndex("dbo.PlanSteps", "RequestId");
            AddForeignKey("dbo.PlanSteps", "RequestId", "dbo.ItemsMovementRequests", "RequestId");
            AddForeignKey("dbo.Items", "ItemsMovementRequest_RequestId", "dbo.ItemsMovementRequests", "RequestId");
        }
    }
}
