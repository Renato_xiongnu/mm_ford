﻿using System;
using System.Linq;
using MMS.Cloud.Commands.StoreProvisioning;
using MMS.Cloud.Shared.Commands;
using StoreProvisioning.CloudService.Proxy.Sp;
using StoreProvisioning.Common;
using OrderLine = StoreProvisioning.CloudService.Proxy.Sp.OrderLine;

namespace StoreProvisioning.CloudService
{
    public static class Mapping
    {
        public static CreateMoveItemCommandRequest ToCreateMoveItemCommandRequest(this AssembleItems assembleItems, Key commandKey)
        {
            var createMoveItemCommandRequest = new CreateMoveItemCommandRequest
            {
                RequestId = KeySerializer.Serialize(commandKey),
                CustomerOrderId = assembleItems.CustomerOrderId,
                WwsOrderId = assembleItems.WwsOrderId,
                AdditionalContent = new AssembleItemsContent
                {
                    Comments = assembleItems.Comments,
                    CustomerEmail = assembleItems.CustomerEmail,
                    CustomerFullName = assembleItems.CustomerFullName,
                    DeliveryAddress = assembleItems.DeliveryAddress,
                    DeliveryCity = assembleItems.DeliveryCity,
                    PaymentType = assembleItems.PaymentType
                },
                OrderLines = assembleItems.OrderLines.Select(
                    el => new OrderLine
                    {
                        AppliedCampaigns = el.AppliedCampaigns,
                        ArticleId = el.ArticleId,
                        LineComment = el.LineComment,
                        Price = el.Price,
                        ProductGroup = el.ProductGroup,
                        ProductGroupNo = el.ProductGroupNo,
                        Qty = el.Qty,
                        Title = el.Title,
                        WWSFreeQty = el.FreeQty,
                        WWSPriceOrig = Convert.ToString(el.PriceOrig),
                        WWSStockNumber = el.StockNumber,
                        WwsDepartmentNo = el.DepartmentNo,
                    }).ToArray(),
            };

            return createMoveItemCommandRequest;
        }

        public static CreateMoveItemCommandRequest ToCreateMoveItemCommandRequest(this ReturnItems returnItems, Key commandKey)
        {
            var createMoveItemCommandRequest = new CreateMoveItemCommandRequest
            {
                RequestId = KeySerializer.Serialize(commandKey),
                CustomerOrderId = returnItems.CustomerOrderId,
                WwsOrderId = returnItems.WwsOrderId,
                AdditionalContent = new ReturnItemsContent
                {
                },
                OrderLines = returnItems.OrderLines.Select(
                    el => new OrderLine
                    {
                        AppliedCampaigns = el.AppliedCampaigns,
                        ArticleId = el.ArticleId,
                        LineComment = el.LineComment,
                        Price = el.Price,
                        ProductGroup = el.ProductGroup,
                        ProductGroupNo = el.ProductGroupNo,
                        Qty = el.Qty,
                        Title = el.Title,
                        WWSFreeQty = el.FreeQty,
                        WWSPriceOrig = Convert.ToString(el.PriceOrig),
                        WWSStockNumber = el.StockNumber,
                        WwsDepartmentNo = el.DepartmentNo,
                    }).ToArray(),
            };

            return createMoveItemCommandRequest;
        }
    }
}
