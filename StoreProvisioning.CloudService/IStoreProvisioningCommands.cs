﻿using System.ServiceModel;
using MMS.Cloud.Commands.StoreProvisioning;
using MMS.Cloud.Shared.Commands;

namespace StoreProvisioning.CloudService
{
    [ServiceContract]
    public interface IStoreProvisioningCommands
    {
        [OperationContract]
        [ServiceKnownType(typeof(AssembleItemsResponse))]
        [ServiceKnownType(typeof(ReturnItemsResponse))]
        void SaveResponse(Key commandKey, object senderCommandResponse);
    }
}
