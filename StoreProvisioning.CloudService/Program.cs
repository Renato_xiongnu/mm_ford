﻿using MMS.Cloud.Impl.Clients.Commands.WcfService;
using MMS.Shared;

namespace StoreProvisioning.CloudService
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = ClientCommandServiceFactory.CreateFromConfiguration<CloudIntegrationService>();
            DebugHelper.RunServiceAsWinForm(service);
        }
    }
}
