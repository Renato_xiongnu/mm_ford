﻿using System;
using System.Data.SQLite;
using MMS.Cloud.Commands.StoreProvisioning;
using MMS.Cloud.Impl.Clients.Commands.WcfService;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.Cloud.Shared.Dto.Commands;
using MMS.Cloud.Shared.Serialization;
using StoreProvisioning.CloudService.Proxy.Sp;
using StoreProvisioning.Common;

namespace StoreProvisioning.CloudService
{
    public class CloudIntegrationService : SimplifiedClientCommandService, IStoreProvisioningCommands
    {
        protected override ReceivedObjectProcessingResult ProcessExternalCommand(ReceiverCommand command)
        {
            var client = new Proxy.SpWf.SPClient();

            var result = CreateMoveItemCommand(command);

            if (!result.IsOk)
                throw new Exception(string.Format("Fail to create command {0}", command.CommandKey.Id));

            var ticketId = client.StartProcess(KeySerializer.Serialize(command.CommandKey),
                GetParameter(command).Substring(0, 4));
            CheckTicket(ticketId);

            return ReceivedObjectProcessingResult.InProcess;
        }
        
        private CreateMoveItemCommandResult CreateMoveItemCommand(ReceiverCommand command)
        {
            var storeClient = new StoreProvisioningServiceClient();

            switch (command.TypeName)
            {
                case CommandNames.AssembleItems:
                    {
                        var assembleItemsCommand =
                            ObjectSerializator.DeserialazeObject<AssembleItems>(command.SerializedData);
                        var createMoveItemCommandRequest =
                            assembleItemsCommand.ToCreateMoveItemCommandRequest(command.CommandKey);

                        var result = storeClient.CreateMoveItemCommand(createMoveItemCommandRequest);

                        return result;
                    }
                case CommandNames.ReturnItems:
                    {
                        var returnItemsCommand =
                            ObjectSerializator.DeserialazeObject<ReturnItems>(command.SerializedData);

                        var createMoveItemCommandRequest =
                            returnItemsCommand.ToCreateMoveItemCommandRequest(command.CommandKey);

                        var result = storeClient.CreateMoveItemCommand(createMoveItemCommandRequest);
                        return result;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException();
                    }
            }
        }

        private void CheckTicket(string ticketId)
        {
            Guid.Parse(ticketId);
        }

        private string GetParameter(ReceiverCommand command)
        {
            var commandData = ObjectSerializator.DeserialazeObject(typeof (object), command.SerializedData);
            dynamic commandDataDynamic = commandData;

            return new StockToSapCodeMapping().ToSapCode(commandDataDynamic.SapCode);
        }

        protected override ReceivedObjectProcessingResult ProcessExternalResponse(ReceiverCommandResponse response)
        {
            return ReceivedObjectProcessingResult.Failure("External responses not supported");
        }

        protected override ReceivedObjectProcessingResult ValidateExternalCommandInProcess(ReceiverCommand command)
        {
            return ReceivedObjectProcessingResult.InProcess;
        }

        protected override ReceivedObjectProcessingResult ValidateExternalResponseInProcess(ReceiverCommandResponse response)
        {
            return ReceivedObjectProcessingResult.InProcess;
        }

        protected override void ProcessOwnCommandError(SenderCommand targetCommand, CommandErrorData commandErrorData)
        {
        }

        protected override void ProcessOwnResponseError(SenderCommandResponse targetResponse, ResponseErrorData responseErrorData)
        {
        }

        public void SaveResponse(Key commandKey, object senderCommandResponse)
        {
            try
            {
                var response = ClientCommandManager.CommandFactory.NewResponse(Requisites.SystemId, commandKey,
                    senderCommandResponse,
                    string.Empty);
                ClientCommandManager.CreateResponse(response);
            }
            catch (SQLiteException e)
            {
                ServiceLogger.Error("Catch error: ", e);
            }
        }
    }
}
