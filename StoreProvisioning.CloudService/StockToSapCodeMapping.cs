﻿using System;

namespace StoreProvisioning.CloudService
{
    class StockToSapCodeMapping
    {
        public string ToSapCode(string stockLocationId)
        {
            var sapCode = stockLocationId.Replace("MM_shop_", string.Empty);
            if (sapCode.Length != 4)
            {
                throw new ArgumentOutOfRangeException(stockLocationId);
            }

            return sapCode;
        }
    }
}
