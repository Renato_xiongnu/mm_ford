﻿using System;
using Autofac;
using Hangfire;
using Microsoft.Practices.ServiceLocation;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Impl.Clients.Commands.WcfService;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.Cloud.Shared.Dto.Commands;
using MMS.Cloud.Shared.Serialization;
using MMS.Json;
using MMS.StoreFulfillment.Application;
using MMS.StoreFulfillment.Application.FORD.Execution;
using MMS.StoreFulfillment.Application.SF;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;

namespace MMS.StoreFulfillment.Host
{
    class StoreFulfillmentCloudIntegrationService:SimplifiedClientCommandService
    {
        private readonly ILifetimeScope _container;
        private BackgroundJobServer _backgroundJobServer;
        
        public StoreFulfillmentCloudIntegrationService()
        {
            _container = ServiceLocator.Current.GetInstance<ILifetimeScope>();
        }

        protected override ReceivedObjectProcessingResult ProcessExternalCommand(ReceiverCommand command, int attempts)
        {
            using (var scope = _container.BeginLifetimeScope())
            {
                var messageBus = scope.Resolve<IMessageBus>();

                switch (command.CommandName)
                {
                    case "FulfillOrder":
                    {
                        messageBus.SendCommand(
                            new CloudCommandWrapper<FulfillOrder>(command.SerializedData.Deserialize<FulfillOrder>(),
                                command.CommandKey, command.CommandName));
                        return ReceivedObjectProcessingResult.InProcess;
                    }
                    case "CancelFulfillOrder":
                    {
                        var cancelReserve = command.SerializedData.Deserialize<CancelFulfillOrder>();
                        messageBus.SendCommand(new CloudCommandWrapper<CancelFulfillOrder>(cancelReserve,
                            command.CommandKey, command.CommandName));
                        return ReceivedObjectProcessingResult.Success(new CancelFulfillOrderResponse());
                    }
                    case "PostponeExpirationDate":
                    {
                        var postponeExpiration = command.SerializedData.Deserialize<PostponeExpirationDate>();
                        messageBus.SendCommand(new CloudCommandWrapper<PostponeExpirationDate>(postponeExpiration,
                            command.CommandKey, command.CommandName));
                        return
                            ReceivedObjectProcessingResult.Success(new PostponeExpirationDateResponse
                            {
                                Result = Result.Done,
                                NewExpirationDate = postponeExpiration.NewExpirationDate
                            });
                    }
                    default:
                    {
                        throw new NotSupportedException(string.Format("Command {0} not supported", command.CommandName));
                    }
                }
            }
        }

        protected override ReceivedObjectProcessingResult ProcessExternalResponse(ReceiverCommandResponse response, int attempts)
        {
            ServiceLogger.InfoFormat("Received response {0}. Response will be ignored",
                JsonConvert.SerializeObject(response));

            return ReceivedObjectProcessingResult.Success();
        }

        protected override ReceivedObjectProcessingResult ValidateExternalCommandInProcess(ReceiverCommand command)
        {
            return ReceivedObjectProcessingResult.InProcess;
        }

        protected override ReceivedObjectProcessingResult ValidateExternalResponseInProcess(ReceiverCommandResponse response)
        {
            return ReceivedObjectProcessingResult.InProcess;
        }

        protected override void ProcessOwnCommandError(SenderCommand targetCommand, CommandErrorData commandErrorData)
        {
            ServiceLogger.ErrorFormat("ProcessOwnCommandError: {0}, {1}", JsonConvert.SerializeObject(targetCommand),
                JsonConvert.SerializeObject(commandErrorData));
        }

        protected override void ProcessOwnResponseError(SenderCommandResponse targetResponse, ResponseErrorData responseErrorData)
        {
            ServiceLogger.ErrorFormat("ProcessOwnResponseError: {0}, {1}", JsonConvert.SerializeObject(targetResponse),
                JsonConvert.SerializeObject(responseErrorData));
        }

        protected override ReceivedObjectProcessingResult ProcessAutoResponse(ReceiverCommandResponse response, AutoResponseReason reason,
            int attempts)
        {
            return ReceivedObjectProcessingResult.Failure("Auto responses not supported");
        }

        protected override void OnAfterStart()
        {
            _backgroundJobServer=new BackgroundJobServer(new BackgroundJobServerOptions{WorkerCount = Configurations.HangfireWorkersCount});
            RecurringJob.AddOrUpdate<ProcessFulfillmentStepJob>("ProcessFulfillmentOrderSteps", el=>el.Execute(),Cron.Minutely);
            base.OnAfterStart();
        }

        protected override void OnBeforeStop()
        {
            _backgroundJobServer.Dispose();
            base.OnBeforeStop();
        }
    }
}
