﻿using System;
using System.ServiceModel;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using Autofac.Integration.Wcf;
using MMS.Cloud.Impl.Clients.Commands;
using MMS.Cloud.Impl.Clients.Commands.Storage;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Interfaces.Clients.Commands.Storage;
using MMS.StoreFulfillment.Application;
using MMS.StoreFulfillment.Application.Common;
using MMS.StoreFulfillment.Application.FORD;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.Application.WWS;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Execution;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.SF.Compensation;

namespace MMS.StoreFulfillment.Host.Configuration
{
    public class ServicesStartup
    {
        public static void InitWcf()
        {
            var containerBuider = new ContainerBuilder();
            containerBuider.RegisterType<SalesServiceClient>().As<ISalesService>();
            containerBuider.RegisterAssemblyTypes(typeof (CreateReserveCommandHandler).Assembly)
                .AsClosedTypesOf(typeof (ICommandHandler<>));
            containerBuider.RegisterType<CreateTaskCommandHandler>()
                .As<ICommandHandler<CreateTaskCommand>>()
                .EnableClassInterceptors();
            containerBuider.RegisterType<CloseTicketCommandHandler>()
                .As<ICommandHandler<CloseTicketCommand>>()
                .EnableClassInterceptors();
            containerBuider.RegisterType<CompensateTicketCreationCommandHandler>()
                .As<ICommandHandler<CompensateTicketCreationCommand>>()
                .EnableClassInterceptors();
            containerBuider.RegisterType<StoreFulfillmentStorage>().As<IStoreFulfillmentStorage>();
            containerBuider.RegisterType<TicketToolServiceClient>().As<ITicketToolService>();
            containerBuider.RegisterType<StoreFulfillmentContext>()
                .OnActivated(t => t.Instance.Database.Log = Console.WriteLine);
            containerBuider.RegisterType<MessageBus>().As<IMessageBus>();
            containerBuider.RegisterType<CallbackTtService>().SingleInstance();
            containerBuider.Register(c => new CommandHandlerLogger());
            containerBuider.RegisterType<ExecutionEngine>().AsSelf();
            containerBuider.RegisterType<CallbackTtService>().As<ICallbackTtService>();
            containerBuider.RegisterType<CompensationEngine>().As<ICompensationEngine>();
            containerBuider.RegisterType<SqlServerSerializedCommandStorage>()
                .As<IClientCommandStorage>()
                .WithParameter("systemId", Configurations.SystemName);
            containerBuider.RegisterType<ClientCommandManager>()
                .As<IClientCommandManager>()
                .WithParameter("systemId", Configurations.SystemName);
            containerBuider.RegisterType<RecreateReserveService>().As<IRecreateReserveService>();
            containerBuider.RegisterType<DeliveryStoresProvider>().AsImplementedInterfaces();

            AutofacConfig.RegisterTicketTasks(containerBuider);
            AutofacConfig.RegisterFordServices(containerBuider);
            AutofacConfig.RegisterExternalServices(containerBuider);
            var container = containerBuider.Build();

            var host = new ServiceHost(typeof (CallbackTtService));
            host.AddDependencyInjectionBehavior<ICallbackTtService>(container);
            host.Open();
            
            var recreateReserveHost = new ServiceHost(typeof(RecreateReserveService));
            recreateReserveHost.AddDependencyInjectionBehavior<IRecreateReserveService>(container);
            recreateReserveHost.Open();
        }
    }
}
