﻿using System;
using System.Linq;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using Autofac.Extras.DynamicProxy2;
using Hangfire;
using Microsoft.Practices.ServiceLocation;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Impl.Clients.Commands;
using MMS.Cloud.Impl.Clients.Commands.Storage;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Interfaces.Clients.Commands.Storage;
using MMS.StoreFulfillment.Application;
using MMS.StoreFulfillment.Application.Common;
using MMS.StoreFulfillment.Application.FORD;
using MMS.StoreFulfillment.Application.FORD.Execution;
using MMS.StoreFulfillment.Application.Proxy.Auth;
using MMS.StoreFulfillment.Application.Proxy.ProductBackend;
using MMS.StoreFulfillment.Application.SF;
using MMS.StoreFulfillment.Application.SF.TaskHandlers;
using MMS.StoreFulfillment.Application.StockSelector;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.Application.WWS;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.Execution;
using MMS.StoreFulfillment.Entities.FORD.Planning;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Compensation;
using MMS.StoreFulfillment.Entities.SF.Execution;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using FulfillOrderCommandHandler = MMS.StoreFulfillment.Application.FORD.FulfillOrderCommandHandler;

namespace MMS.StoreFulfillment.Host.Configuration
{
    public class AutofacConfig
    {
        public static void RegisterContainer()
        {
            var containerBuider = new ContainerBuilder();
            containerBuider.RegisterType<SalesServiceClient>().As<ISalesService>();

            RegisterCommandHandlers(containerBuider);

            containerBuider.RegisterType<StoreFulfillmentStorage>().As<IStoreFulfillmentStorage>();
            containerBuider.RegisterType<TicketToolServiceClient>().As<ITicketToolService>();
            containerBuider.RegisterType<AuthenticationServiceClient>().As<IAuthenticationService>();
            containerBuider.RegisterType<StoreFulfillmentContext>()
                .OnActivated(c => c.Instance.Database.Log = Console.WriteLine);
            containerBuider.RegisterType<MessageBus>().As<IMessageBus>();
            containerBuider.RegisterType<CallbackTtService>().SingleInstance();
            containerBuider.Register(c => new CommandHandlerLogger());
            containerBuider.RegisterType<ExecutionEngine>()
                .AsSelf()
                .EnableClassInterceptors();
            containerBuider.RegisterType<SqlServerSerializedCommandStorage>()
                .As<IClientCommandStorage>()
                .WithParameter("systemId", Configurations.SystemName);
            containerBuider.RegisterType<ClientCommandManager>()
                .As<IClientCommandManager>()
                .WithParameter("systemId", Configurations.SystemName);
            containerBuider.RegisterType<CompensationEngine>().As<ICompensationEngine>();
            containerBuider.RegisterType<ExecutionPlanner>().As<IExecutionPlanner>();
            containerBuider.RegisterType<CompleteTaskService>();
            containerBuider.RegisterType<CheckPaymentJob>();
            containerBuider.RegisterType<RecreateReserveService>();
            containerBuider.RegisterType<CancelReserveJob>();
            containerBuider.RegisterType<DeliveryStoresProvider>().AsImplementedInterfaces();

            RegisterTicketTasks(containerBuider);

            RegisterFordServices(containerBuider);
            RegisterExternalServices(containerBuider);
            var container = containerBuider.Build();

            DomainEvents.Initialize(container.Resolve<IMessageBus>());
            OrderByRequestLookup.Initialize(container.Resolve<IStoreFulfillmentStorage>());
            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));

            GlobalConfiguration.Configuration.UseSqlServerStorage("StoreFulfillmentContext");
            GlobalConfiguration.Configuration.UseAutofacActivator(container);
        }

        internal static void RegisterTicketTasks(ContainerBuilder containerBuider)
        {
            var assembly = typeof(TaskHandlerBase).Assembly;

            var taskHandlerTypes =
                assembly.GetTypes().Where(t => t.GetInterfaces().Any(type => type == typeof(ITicketTaskHandler)) && !t.IsAbstract);

            var allMissed =
                TicketTaskTypes.GetAllTaskTypes()
                    .Where(t => taskHandlerTypes.All(handler => handler.Name != t + "TaskHandler")).ToList();
            if (allMissed.Any())
            {
                throw new Exception(string.Format("TaskHandler for task {0} missed", allMissed.First()));
            }

            foreach (var taskHandlerType in taskHandlerTypes)
            {
                var type = taskHandlerType.Name.Replace("TaskHandler", string.Empty);
                if (!TicketTaskTypes.GetAllTaskTypes().Contains(type))
                {
                    throw new Exception(string.Format("Task handler {0} dont have appropriate task", taskHandlerType));
                }
                containerBuider.RegisterType(taskHandlerType).Named<ITicketTaskHandler>(type)
                    .Named<ITicketTaskCreator>(type);
            }
        }

        private static void RegisterCommandHandlers(ContainerBuilder containerBuider)
        {
            var appAssembly = typeof(CreateTaskCommandHandler).Assembly;
            var commandHandlerTypes =
                appAssembly.GetTypes()
                    .Where(
                        t =>
                            t.GetInterfaces()
                                .Any(
                                    type =>
                                        type.IsGenericType &&
                                        type.GetGenericTypeDefinition() == typeof(ICommandHandler<>)));
            foreach (var commandHandlerType in commandHandlerTypes)
            {
                containerBuider.RegisterType(commandHandlerType)
                    .AsImplementedInterfaces()
                    .EnableInterfaceInterceptors()
                    .InterceptedBy(typeof(CommandHandlerLogger));
            }
        }

        internal static void RegisterFordServices(ContainerBuilder containerBuider)
        {
            containerBuider.RegisterType<ReserveAndPickupStepHandler>()
                .As<IPlanStepHandler>()
                .Named<IPlanStepHandler>(FulfillmentPlanStepType.ReserveAndPickup.ToString());
            containerBuider.RegisterType<ReserveAndDeliverStepHandler>()
                .As<IPlanStepHandler>()
                .Named<IPlanStepHandler>(FulfillmentPlanStepType.ReserveAndDeliver.ToString());
            containerBuider.RegisterType<TransferGoodsStepHandler>()
                .As<IPlanStepHandler>()
                .Named<IPlanStepHandler>(FulfillmentPlanStepType.TransferGoods.ToString());
            containerBuider.RegisterType<ConfirmTransferAndPickupStepHandler>()
                .As<IPlanStepHandler>()
                .Named<IPlanStepHandler>(FulfillmentPlanStepType.ConfirmTransferAndPickup.ToString());

            containerBuider.RegisterType<ProductBackendAvailabilityProvider>()
                .AsImplementedInterfaces();
            containerBuider.RegisterType<FulfillmentPlanner>();
            containerBuider.RegisterType<FulfillOrderCommandHandler>()
                .As<ICommandHandler<CloudCommandWrapper<FulfillOrder>>>();
            containerBuider.RegisterType<ProcessItemsMovementCompletionCommandHandler>()
                .As<ICommandHandler<ProcessItemsMovementCompletionCommand>>();
            containerBuider.RegisterType<ProcessFulfillmentOrderCompletionCommandHandler>()
                .As<ICommandHandler<ProcessFulfillmentOrderCompletionCommand>>();
            containerBuider.RegisterType<CancelItemMovementRequestHandler>()
                .As<ICommandHandler<CloudCommandWrapper<CancelItemMovementRequest>>>();
            containerBuider.RegisterType<CancelFulfillOrderCommandHandler>()
                .As<ICommandHandler<CloudCommandWrapper<CancelFulfillOrder>>>();
            containerBuider.RegisterType<FulfillmentOrderStorage>()
                .As<IFulfillmentOrderRepository>();
            containerBuider.RegisterType<ProcessFulfillmentStepJob>();            
        }

        internal static void RegisterExternalServices(ContainerBuilder containerBuider)
        {
            containerBuider.RegisterType<SaleLocationServiceClient>().AsImplementedInterfaces();
            containerBuider.RegisterType<OrdersBackendServiceClient>().AsImplementedInterfaces();
        }
    }
}