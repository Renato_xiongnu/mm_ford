﻿using StackExchange.Profiling;

namespace MMS.StoreFulfillment.Host.Configuration
{
    public class ProfilerConfig
    {
        public static void ConfigureProfiling()
        {
            MiniProfiler.Settings.ProfilerProvider = new SingletonProfilerProvider();
            MiniProfiler.Start();
        }
    }
}
