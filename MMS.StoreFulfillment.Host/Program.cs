﻿using System;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using MMS.Cloud.Impl.Clients.Commands.Storage;
using MMS.Cloud.Impl.Clients.Commands.WcfService;
using MMS.Shared;
using MMS.StoreFulfillment.Entities.SF.Execution;
using MMS.StoreFulfillment.Host.Configuration;
using NLog;

namespace MMS.StoreFulfillment.Host
{
    class Program
    {
        private static readonly ILogger Log = LogManager.GetLogger("MMS.StoreFulfillment.Host");

        private static void Main(string[] args)
        {
            Log.Info("Start application");
            Log.Info("Start configuring profiler");
            ProfilerConfig.ConfigureProfiling();
            Log.Info("Finish configuring profiler");
            
            Log.Info("Start init WCF services");
            ServicesStartup.InitWcf();
            Log.Info("Finish init WCF services");

            Log.Info("Start init execution engine");
            AutofacConfig.RegisterContainer();
            var timer = new System.Timers.Timer();
            timer.Elapsed += timer_Elapsed;
            timer.Interval = 5000;
            timer.Enabled = true;
            timer.AutoReset = true;
            Log.Info("Finish init execution engine");
            
            Log.Info("Start init cloud integration service");
            
            var clientSender =
                ClientCommandServiceFactory.CreateFromConfiguration<StoreFulfillmentCloudIntegrationService>(
                    new SqlServerSerializedCommandStorage());
            clientSender.ProcessingInterval = TimeSpan.FromSeconds(20);
            clientSender.Requisites.AttempRetryCount = 10;
            if (Environment.UserInteractive)
            {
                Task.Factory.StartNew(() => DebugHelper.RunServiceAsWinForm(clientSender), TaskCreationOptions.LongRunning);
            }
            else
            {
                DebugHelper.RunService(clientSender);
            }
            
            Log.Info("Finish init cloud integration service");
           
            Console.ReadLine();
        }

        static void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var executionEngine = ServiceLocator.Current.GetInstance<ExecutionEngine>();
            executionEngine.MakeIteration();
        }
    }
}
