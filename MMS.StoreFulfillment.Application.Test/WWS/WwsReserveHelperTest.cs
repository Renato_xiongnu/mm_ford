﻿
using NUnit.Framework;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class WwsReserveHelperTest
    {
        [Test]
        [TestCase(SalesOrderStatus.Paid, "Delivery", Result = true)]
        [TestCase(SalesOrderStatus.Prepaid, "Delivery", Result = false)]
        [TestCase(SalesOrderStatus.ToPay, "Delivery", Result = false)]
        [TestCase(SalesOrderStatus.Prepaid, "PickupShop", Result = false)]
        [TestCase(SalesOrderStatus.Paid, "PickupShop", Result = true)]
        [TestCase(SalesOrderStatus.Unknown, "PickupShop", Result = false)]
        public bool IsPaid_ShouldBeMapped(SalesOrderStatus salesDocStatus, string shippingMethod)
        {
            return WwsReserveHelper.IsPaid(salesDocStatus, shippingMethod);
        }
    }
}
