﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.WWS;
using Moq;
using NUnit.Framework;

namespace MMS.StoreFulfillment.Application.WWS
{
    [TestFixture]
    public class CancelReserveCommandHandlerTest
    {
        private string _requestId;
        private Mock<IStoreFulfillmentStorage> _storeFulfillmentStorageMock;
        private List<PlanStep> _planSteps;
        private Mock<ISalesService> _salesServiceMock;

        [SetUp]
        public void TestInit()
        {
            _requestId = "001-213424";
            _storeFulfillmentStorageMock = new Mock<IStoreFulfillmentStorage>();
            _planSteps = new List<PlanStep> { new PlanStep(PlanStepType.CancelReserve) };
            _storeFulfillmentStorageMock.Setup(el => el.GetById(_requestId, It.IsAny<Expression<Func<ItemsMovementRequest, object>>[]>()))
                .Returns(new ItemsMovementRequest("R202", RequestType.ReserveProducts,"002-1234567-1")
                {
                    CustomerOrder = new CustomerOrderInfo
                    {
                        CustomerOrderId = "1234567",
                    },
                    WwsOrderId = "41234567",
                    PlanSteps = _planSteps
                });
            _salesServiceMock = new Mock<ISalesService>();
            _salesServiceMock.Setup(el => el.CancelSalesOrder(It.IsAny<OrderNumberInfo>()))
                .Returns(new CancelSalesOrderResult { OperationCode = new OperationCode { Code = ReturnCode.Ok } });

            var messageBusMock = new Mock<IMessageBus>();
            DomainEvents.Initialize(messageBusMock.Object);
        }

        [Test]
        public void Handle_ShouldCancelReserve()
        {
            var cancelReserveComamndHandler = new CancelReserveCommandHandler(_storeFulfillmentStorageMock.Object,
                _salesServiceMock.Object);

            cancelReserveComamndHandler.Handle(new CancelReserveCommand(_requestId, 0));

            Assert.AreEqual(PlanStepStatus.Finished, _planSteps[0].Status);
            _salesServiceMock.Verify(
                t =>
                    t.CancelSalesOrder(
                        It.Is((OrderNumberInfo el) => el.OrderNumber == "41234567" && el.SapCode == "R202")),
                Times.Once);
        }

        [Test]
        public void Handle_ShouldThrowIfReserveCancellationFailed()
        {
            _salesServiceMock.Setup(el => el.CancelSalesOrder(It.IsAny<OrderNumberInfo>()))
                .Returns(new CancelSalesOrderResult { OperationCode = new OperationCode { Code = ReturnCode.Exception } });
            var cancelReserveComamndHandler = new CancelReserveCommandHandler(_storeFulfillmentStorageMock.Object,
                _salesServiceMock.Object);

            Assert.Throws<Exception>(()=> cancelReserveComamndHandler.Handle(new CancelReserveCommand(_requestId, 0)));
        }
    }
}
