﻿using System;
using System.Linq;
using MMS.StoreFulfillment.Application.SF.TaskHandlers;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.FORD.Planning;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using Moq;
using NUnit.Framework;
using MMS.StoreFulfillment.Entities.SF.Compensation;

namespace MMS.StoreFulfillment.Entities.Tasks
{
    [TestFixture]
    public class TicketTaskFactoryTest
    {
        private IStoreFulfillmentStorage _storeFulfillmentStorage = new Mock<IStoreFulfillmentStorage>().Object;

        private IMessageBus _messageBus = new Mock<IMessageBus>().Object;

        private ICompensationEngine _compensationEngine = new Mock<ICompensationEngine>().Object;



        [Test]
        public void ResolveTaskForHandoverToCustomer_ShoudReturnHandoverTaskDescription()
        {
            var taskType = TicketTaskTypes.HandOverToCustomer;
            
            var taskDescription = new HandOverToCustomerTaskHandler(_storeFulfillmentStorage,_messageBus).CreateDescription(null);
            
            Assert.AreEqual("Выдай товар покупателю", taskDescription.Name);
            Assert.AreEqual(2, taskDescription.Outcomes.Count());
            Assert.AreEqual("Выдан", taskDescription.Outcomes.First().OutcomeName);
            Assert.AreEqual("Отменен", taskDescription.Outcomes.Skip(1).First().OutcomeName);
            Assert.AreEqual(taskType, taskDescription.Type);
        }

        [Test]
        public void ResolveTaskForPutOnDelivery_ShoudReturnPutOnDeliveryTaskDescription()
        {
            var taskType = TicketTaskTypes.PutOnDelivery;

            var taskDescription =
                new PutOnDeliveryTaskHandler(_storeFulfillmentStorage, _messageBus).CreateDescription(null);
            var outcomes = taskDescription.Outcomes.ToArray();

            Assert.AreEqual("Запиши на доставку", taskDescription.Name);
            Assert.AreEqual(3, outcomes.Length);
            Assert.AreEqual("Записал", outcomes[0].OutcomeName);
            Assert.AreEqual("Новая дата доставки", outcomes[1].OutcomeName);
            Assert.AreEqual(1, outcomes[1].RequiredFields.Length);
            Assert.AreEqual("Новая дата доставки", outcomes[1].RequiredFields[0].FieldName);
            Assert.AreEqual(typeof(DateTime), outcomes[1].RequiredFields[0].FieldType);
            Assert.AreEqual("Доставка невозможна", outcomes[2].OutcomeName);
            Assert.AreEqual(taskType, taskDescription.Type);
        }

        [Test]
        public void ResolveTaskDeliverOrder_ShouldReturnDeliverOrderTaskDescription()
        {
            var taskType = TicketTaskTypes.DeliverOrder;

            var taskDescription = new DeliverOrderTaskHandler(_storeFulfillmentStorage,_messageBus).CreateDescription(null);

            Assert.AreEqual("Доставь заказ по адресу", taskDescription.Name);
            Assert.AreEqual(2, taskDescription.Outcomes.Count());
            Assert.AreEqual("Доставлено", taskDescription.Outcomes.First().OutcomeName);
            Assert.AreEqual("Отменен", taskDescription.Outcomes.Skip(1).First().OutcomeName);
            Assert.AreEqual(taskType, taskDescription.Type);
        }

        [Test]
        public void ResolveTaskCreateTransfer_ShoudReturnCreateTransferTaskDescription()
        {
            var taskType = TicketTaskTypes.CreateTransfer;

            var tansferStoresDataProviderMock = new Mock<ITransferStoresDataProvider>();
            tansferStoresDataProviderMock
                .Setup(m => m.GetAvailableStoresForTransfer(It.Is<string>(s => s == "R002")))
                .Returns(new[] { new Stock { SapCode = "R202", Rating = 1 } }.OrderBy(s => s.Rating));

            var request = new ItemsMovementRequest("R002", RequestType.TransferGoods, Guid.Empty.ToString());

            var taskDescription =
                new CreateTransferTaskHandler(_storeFulfillmentStorage, _messageBus, tansferStoresDataProviderMock.Object).CreateDescription(request);

            Assert.AreEqual("Создай трансфер", taskDescription.Name);
            Assert.AreEqual(3, taskDescription.Outcomes.Length);
            Assert.AreEqual("Сделал трансфер", taskDescription.Outcomes.First().OutcomeName);
            Assert.AreEqual("Магазин (SapCode)", taskDescription.Outcomes.First().RequiredFields.Skip(1).First().FieldName);
            Assert.AreEqual("R202", taskDescription.Outcomes.First().RequiredFields.Skip(1).First().ExtendedValues.First());
            Assert.AreEqual("Невозможно создать трансфер", taskDescription.Outcomes.Skip(1).First().OutcomeName);
            Assert.AreEqual("Перезвонить позже", taskDescription.Outcomes.Skip(2).First().OutcomeName);
            //Assert.AreEqual("Отклонено покупателем", taskDescription.Outcomes.Skip(3).First().OutcomeName);
            Assert.AreEqual(taskType, taskDescription.Type);
        }

        [Test]
        public void ResolveTaskCollectTransfer_ShoudReturnCollectTransferTaskDescription()
        {
            var taskType = TicketTaskTypes.CollectTransfer;

            var taskDescription =
                new CollectTransferTaskHandler(_storeFulfillmentStorage, _messageBus, _compensationEngine).CreateDescription(null);

            Assert.AreEqual("Собери трансфер", taskDescription.Name);
            Assert.AreEqual(2, taskDescription.Outcomes.Count());
            Assert.AreEqual("Собрал", taskDescription.Outcomes.First().OutcomeName);
            Assert.AreEqual("Невозможно собрать трансфер", taskDescription.Outcomes.Skip(1).First().OutcomeName);
            Assert.AreEqual(taskType, taskDescription.Type);
        }

        [Test]
        public void ResolveTaskConfirmTransfer_ShoudReturnCollectTransferTaskDescription()
        {
            var taskType = TicketTaskTypes.ConfirmTransfer;

            var taskDescription = new ConfirmTransferTaskHandler(_storeFulfillmentStorage,_messageBus).CreateDescription(null);

            Assert.AreEqual("Прими трансфер", taskDescription.Name);
            Assert.AreEqual(2, taskDescription.Outcomes.Count());
            Assert.AreEqual("Трансфер получен", taskDescription.Outcomes.First().OutcomeName);
            Assert.AreEqual("Трансфер не получен", taskDescription.Outcomes.Skip(1).First().OutcomeName);
            Assert.AreEqual(taskType, taskDescription.Type);
        }
    }
}
