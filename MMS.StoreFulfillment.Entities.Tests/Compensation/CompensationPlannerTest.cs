﻿using System;
using System.Collections.Generic;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.SF.Compensation;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using NUnit.Framework;
using Item = MMS.StoreFulfillment.Entities.SF.Items.Item;

namespace MMS.StoreFulfillment.Entities.Compensation
{
    [TestFixture]
    public class CompensationPlannerTest
    {
        [Test]
        public void CompenationForEmptyPlan_ShouldBeEmpty()
        {
            var compensationPlanner = new CompensationPlanner();

            var compenationPlan = compensationPlanner.PlanCompensation("002-1234567-1", new List<PlanStep>(), new List<Item>());

            Assert.IsNotNull(compenationPlan);
            CollectionAssert.IsEmpty(compenationPlan);
        }

        [Test]
        public void CompenationForNull_ShouldThrowsException()
        {
            var compensationPlanner = new CompensationPlanner();

            Assert.Throws<ArgumentNullException>(() => compensationPlanner.PlanCompensation("002-1234567-1", null, null));
            Assert.Throws<ArgumentNullException>(() => compensationPlanner.PlanCompensation("002-1234567-1", null, new Item[0]));
            Assert.Throws<ArgumentNullException>(() => compensationPlanner.PlanCompensation("002-1234567-1", new List<PlanStep>(), null));
        }

        [Test]
        public void CompensationForExecutedReserveCreation_ShouldBeReserveCancellation()
        {
            var compensationPlanner = new CompensationPlanner();
            var plan = new List<PlanStep> {new PlanStep(PlanStepType.CreateReserve) {Status = PlanStepStatus.Finished}};

            var compensationPlan = compensationPlanner.PlanCompensation("002-1234567-1", plan, new Item[0]);

            Assert.IsNotNull(compensationPlan);
            Assert.AreEqual(1, compensationPlan.Count);
            Assert.AreEqual(PlanStepType.CancelReserve, compensationPlan[0].PlanStepType);
            Assert.AreEqual(PlanStepStatus.Pending, compensationPlan[0].Status);
        }

        [Test]
        public void CompensationForPendingReserveCreation_ShouldBeEmpty()
        {
            var compensationPlanner = new CompensationPlanner();
            var plan = new List<PlanStep> {new PlanStep(PlanStepType.CreateReserve) {Status = PlanStepStatus.Pending}};

            var compensationPlan = compensationPlanner.PlanCompensation("002-1234567-1", plan, new Item[0]);

            Assert.IsNotNull(compensationPlan);
            Assert.AreEqual(0, compensationPlan.Count);
        }

        [Test]
        public void CompensationForExecutedReservationWithAllItemsReservedTask_ShouldBeReturnItemsBackTastAndTickets()
        {
            var compensationPlanner = new CompensationPlanner();
            var plan = new List<PlanStep>
            {
                new TaskPlanStep(TicketTaskTypes.ReserveProducts) {Status = PlanStepStatus.Finished}
            };

            var compensationPlan = compensationPlanner.PlanCompensation("002-1234567-1",plan,
                new[] {new Item(111111, 10000, 2, "test", 1) {ReserveStatus = ReserveStatuses.Reserved}});

            Assert.IsNotNull(compensationPlan);
            Assert.AreEqual(3, compensationPlan.Count);
            Assert.AreEqual(PlanStepType.CreateTicket, compensationPlan[0].PlanStepType);
            Assert.AreEqual(PlanStepType.CreateTask, compensationPlan[1].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.CancelReserveProducts, ((TaskPlanStep) compensationPlan[1]).TaskType);
            Assert.AreEqual(PlanStepType.CloseTicket, compensationPlan[2].PlanStepType);
            Assert.AreEqual(PlanStepStatus.Pending, compensationPlan[0].Status);
        }
        
        [Test]
        public void CompensationForExecutedReservationWithNotAllItemsReservedTask_ShouldBeEmpty()
        {
            var compensationPlanner = new CompensationPlanner();
            var plan = new List<PlanStep>
            {
                new TaskPlanStep(TicketTaskTypes.ReserveProducts) {Status = PlanStepStatus.Finished}
            };

            var compensationPlan = compensationPlanner.PlanCompensation("002-1234567-1", plan,
                new[]
                {
                    new Item(111111, 10000, 2, "test", 1) {ReserveStatus = ReserveStatuses.DisplayItem},
                    new Item(111112, 10002, 2, "test", 2) {ReserveStatus = ReserveStatuses.Reserved}
                });

            Assert.IsNotNull(compensationPlan);
            Assert.AreEqual(0, compensationPlan.Count);
        }
    }
}
