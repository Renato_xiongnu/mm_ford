﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using NUnit.Framework;

namespace MMS.StoreFulfillment.Entities.Tasks
{
    [TestFixture]
    public class WorkitemIdTest
    {
        [Test]
        public void WorkitemId_FromGuid()
        {
            var guid = Guid.NewGuid();
            var id = new WorkitemId(guid.ToString());

            Assert.AreEqual(guid.ToString(), id.RequestId);
            Assert.AreEqual(guid.ToString(), id.ToString());
            Assert.IsNull(id.OrderId);
        }

        [Test]
        public void WorkitemId_FromOrderId()
        {
            var orderId = "002-12345678";
            var id = new WorkitemId(orderId);

            Assert.AreEqual(orderId, id.OrderId);
            Assert.AreEqual(orderId, id.ToString());
            Assert.IsNull(id.RequestId);
        }

        [Test]
        public void WorkitemId_FromOrderIdAndRequestId()
        {
            var guid = Guid.NewGuid();
            var orderId = "002-12345678";
            var id = new WorkitemId(orderId + "-" + guid.ToString());

            Assert.AreEqual(orderId, id.OrderId);
            Assert.AreEqual(guid.ToString(), id.RequestId);
            Assert.AreEqual(orderId + "-" + guid.ToString(), id.ToString());
        }

    }
}
