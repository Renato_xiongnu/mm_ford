﻿using System.Collections.Generic;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using NUnit.Framework;
using CustomerOrderInfo = MMS.StoreFulfillment.Entities.SF.Items.CustomerOrderInfo;
using Item = MMS.StoreFulfillment.Entities.SF.Items.Item;

namespace MMS.StoreFulfillment.Entities.Planning
{
    [TestFixture]
    public class ExecutionPlannerTest
    {
        [Test]
        public void PlanExecutionForReserveProductsOnHandover_ShouldBeGenerated()
        {
            var planningEngine = new ExecutionPlanner();
            var request = new ItemsMovementRequest("R002", RequestType.ReserveProducts, "002-12345678-1");
            request.CustomerOrder = new CustomerOrderInfo();
            request.CustomerOrder.ShippingMethod = ShippingMethods.PickupShop;
            request.Items = new List<Item>();

            var plan = planningEngine.PlanExecution(request);

            Assert.AreEqual(5, plan.Count);
            Assert.AreEqual(PlanStepType.CreateTicket, plan[0].PlanStepType);
            Assert.AreEqual(PlanStepType.CreateReserve, plan[1].PlanStepType);
            Assert.AreEqual(PlanStepType.CreateTask, plan[2].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.ReserveProducts, ((TaskPlanStep) plan[2]).TaskType);
            Assert.AreEqual(PlanStepType.CheckPayment, plan[3].PlanStepType);
            Assert.AreEqual(PlanStepType.CloseTicket, plan[4].PlanStepType);
        }

        [Test]
        public void PlanExecutionForReserveProductsOnDelievry_ShouldBeGenerated()
        {
            var planningEngine = new ExecutionPlanner();
            var request = new ItemsMovementRequest("R002", RequestType.ReserveProducts, "002-12345678-1");
            request.CustomerOrder = new CustomerOrderInfo();
            request.CustomerOrder.ShippingMethod = ShippingMethods.Delivery;
            request.Items=new List<Item>();

            var plan = planningEngine.PlanExecution(request);

            Assert.AreEqual(6, plan.Count);
            Assert.AreEqual(PlanStepType.CreateTicket, plan[0].PlanStepType);
            Assert.AreEqual(PlanStepType.CreateReserve, plan[1].PlanStepType);

            Assert.AreEqual(PlanStepType.CreateTask, plan[2].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.ReserveProducts, ((TaskPlanStep)plan[2]).TaskType);

            Assert.AreEqual(PlanStepType.CreateTask, plan[3].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.PutOnDelivery, ((TaskPlanStep)plan[3]).TaskType);

            Assert.AreEqual(PlanStepType.CheckPayment, plan[4].PlanStepType);

            Assert.AreEqual(PlanStepType.CloseTicket, plan[5].PlanStepType);
        }

        [Test]
        public void PlanExecutionForCollectTransferGoods_ShouldBeGenerated()
        {
            var planningEngine = new ExecutionPlanner();
            var request = new ItemsMovementRequest("R002", RequestType.TransferGoods, "002-12345678-1");
            request.CustomerOrder=new CustomerOrderInfo();
            request.CustomerOrder.ShippingMethod = ShippingMethods.PickupShop;

            var plan = planningEngine.PlanExecution(request);

            Assert.AreEqual(PlanStepType.CreateTicket, plan[0].PlanStepType);
            Assert.AreEqual(PlanStepType.CreateTask, plan[1].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.CreateTransfer, ((TaskPlanStep) plan[1]).TaskType);
            Assert.AreEqual(PlanStepType.CreateTask, plan[2].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.CollectTransfer, ((TaskPlanStep) plan[2]).TaskType);
            Assert.AreEqual(PlanStepType.CloseTicket, plan[3].PlanStepType);
        }

        [Test]
        public void PlanExecutionForReserveProductsFromTransfer_ShouldBeGenerated()
        {
            var planningEngine = new ExecutionPlanner();
            var request = new ItemsMovementRequest("R002", RequestType.ReserveProducts, "002-12345678-1")
            {
                CustomerOrder = new CustomerOrderInfo
                {
                    ShippingMethod = ShippingMethods.PickupShop
                },
                Items = new List<Item>
                {
                    new Item(1234567, 10000, 2, "test", 2) {IncomingTransfer = "1234234"}
                }
            };
            
            var plan = planningEngine.PlanExecution(request);

            Assert.AreEqual(PlanStepType.CreateTicket, plan[0].PlanStepType);
            Assert.AreEqual(PlanStepType.CreateReserve, plan[1].PlanStepType);

            Assert.AreEqual(PlanStepType.CreateTask, plan[2].PlanStepType);
            Assert.AreEqual(TicketTaskTypes.ConfirmTransfer, ((TaskPlanStep) plan[2]).TaskType);
            
            Assert.AreEqual(PlanStepType.CheckPayment, plan[3].PlanStepType);
            Assert.AreEqual(PlanStepType.CloseTicket, plan[4].PlanStepType);
        }
    }
}
