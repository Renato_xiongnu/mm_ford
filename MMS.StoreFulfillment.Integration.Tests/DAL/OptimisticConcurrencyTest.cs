﻿using System.Data.Entity.Infrastructure;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Items;
using NUnit.Framework;

namespace MMS.StoreFulfillment.Integration.Tests.DAL
{
    [TestFixture(Category = "IntegrationTests")]
    public class OptimisticConcurrencyTest
    {
        private ItemsMovementRequest _request;

        [SetUp]
        public void TestStartup()
        {
            var requestBuilder = new RequestBuilder("R202", RequestType.ReserveProducts, "002-12345678");
            requestBuilder.WithCustomerContact(new CustomerContact());
            requestBuilder.WithOrderInfo(new CustomerOrderInfo());
            _request = requestBuilder.Build();
        }

        [Test]
        public void SaveRetreivedRequestTwoTimes_DoesntThrow()
        {
            var preparingStorage = new StoreFulfillmentStorage(new StoreFulfillmentContext());

            preparingStorage.Save(_request);

            var storage = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var request = storage.GetById(_request.RequestId);
            storage.Save(request);

            storage.Save(request);
        }

        [Test]
        public void SaveRetreivedRequestTwoTimesInRightOrder_DoesntThrow()
        {
            var preparingStorage = new StoreFulfillmentStorage(new StoreFulfillmentContext());

            preparingStorage.Save(_request);

            var storage1 = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var request1 = storage1.GetById(_request.RequestId);
            storage1.Save(request1);

            var storage2 = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var request2 = storage2.GetById(_request.RequestId);
            storage2.Save(request2);
        }

        [Test]
        public void SaveRetreivedRequestTwoTimesInConcurrent_ThrowsOptimiscticExcpetion()
        {
            var preparingStorage = new StoreFulfillmentStorage(new StoreFulfillmentContext());

            preparingStorage.Save(_request);

            var storage1 = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var storage2 = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var request1 = storage1.GetById(_request.RequestId);
            var request2 = storage2.GetById(_request.RequestId);
            
            storage1.Save(request1);

            try
            {
                storage2.Save(request2);
                Assert.Fail();
            }
            catch (DbUpdateConcurrencyException)
            {
            }
        }
    }
}
