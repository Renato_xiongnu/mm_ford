﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Integration.Tests.Proxy.Cloud;
using MMS.StoreFulfillment.Integration.Tests.Proxy.StoreFulfillment;
using Newtonsoft.Json;
using NUnit.Framework;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Application.SF.TaskHandlers;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Integration.Tests;

namespace MMS.StoreFulfillment.Integration.Tests.CloudClient
{
    [TestFixture(Category = "IntegrationTests")]
    public class PickupShopTransferTest
    {
        [Test]
        [TestCase("PickupShop", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        public void CancelTransfer_BadCollectTransfer(string shippingMethod, string reservePaymentStatus)
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var timeStamp = cloudIntegration.GetResponseTimeStamp();
            var commandId = Guid.NewGuid().ToString();

            var fulfillOrder = CreateFulfillOrder(shippingMethod);

            var command = new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey = new Key { Id = commandId, CorrelationId = Guid.NewGuid().ToString() },
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            };
            cloudIntegration.PutCommands(command);

            Thread.Sleep(TimeSpan.FromSeconds(90));
 
            var request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.CreateTransfer);

            var callbackTtClient = new CallbackTtServiceClient();

            callbackTtClient.CreateTransferContinue(request.TicketId, CreateTransferTaskHandler.CreateTransferOutcome,
                new Dictionary<string, object> {
                    { string.Format("{0};#{1}", CreateTransferTaskHandler.CreateTransferOutcome, CreateTransferTaskHandler.TransferNumberField), "1234567" },
                    { string.Format("{0};#{1}", CreateTransferTaskHandler.CreateTransferOutcome, CreateTransferTaskHandler.SapCodeField), "R202" }
                }, "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            //-->закончили создание трансфера

            Thread.Sleep(TimeSpan.FromSeconds(80));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.CollectTransfer);  

            //--> закончили проверку собери трансфер


            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.CollectTransferContinue(request.TicketId, CollectTransferTaskHandler.FailOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));
            // --> завершаем задачу собери трансфер


            //after fail is received, cancel the transfer and check if status is canceled:

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled);

            Assert.AreEqual(RequestStatus.Canceled, request.Status);

            //Завершение задачи отмена трансфера

            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.CancelTransferContinue(request.TicketId, CancelTransferTaskHandler.CancelTransferOutcome , new Dictionary<string, object>(), "admin");


            Thread.Sleep(TimeSpan.FromSeconds(30));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing, 1);
        }

        [Test]
        [TestCase("PickupShop", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        [TestCase("PickupShop", "ОТМЕНЕННЫЙ РЕЗЕРВ")]
        public void PickupShop_Transfer_Success(string shippingMethod, string reservePaymentStatus)
        {
            Thread.Sleep(TimeSpan.FromSeconds(20));

            var cloudIntegration = new ClientCommandServiceClient();
            var timeStamp = cloudIntegration.GetResponseTimeStamp();
            var commandId = Guid.NewGuid().ToString();

            //говорю форду Fulfillorder
            var fulfillOrder = CreateFulfillOrder(shippingMethod);
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey = new Key { Id = commandId, CorrelationId = Guid.NewGuid().ToString() },
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(90));

            var request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.CreateTransfer);


            var callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.CreateTransferContinue(request.TicketId, CreateTransferTaskHandler.CreateTransferOutcome,
                new Dictionary<string, object> {
                    { string.Format("{0};#{1}", CreateTransferTaskHandler.CreateTransferOutcome, CreateTransferTaskHandler.TransferNumberField), "1234567" },
                    { string.Format("{0};#{1}", CreateTransferTaskHandler.CreateTransferOutcome, CreateTransferTaskHandler.SapCodeField), "R202" }
                }, "admin");

            //-->закончили создание трансфера

            Thread.Sleep(TimeSpan.FromSeconds(30));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.CollectTransfer);

            //--> закончили проверку собери трансфер


            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.CollectTransferContinue(request.TicketId, CollectTransferTaskHandler.DoneOutcome, new Dictionary<string, object>(), "admin");
            
            // --> завершаем задачу собери трансфер

            Thread.Sleep(TimeSpan.FromSeconds(90));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Finished);
            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing, 1);
            currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.ConfirmTransfer);

            using (var context = new StoreFulfillmentContext())
            {
                var reserveProductsRequest = context.Requests
                    .Include(t => t.Items)
                    .First(t => t.RequestId == request.RequestId);
                reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
                context.SaveChanges();
            }

            callbackTtClient.ConfirmTransferContinue(request.TicketId, ConfirmTransferTaskHandler.DoneOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing, 1);
            currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CheckPayment);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.HandOverToCustomer);

            //Check payment
            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                callbackTtClient.HandOverToCustomerContinue(request.TicketId, HandOverToCustomerTaskHandler.CancelOutcome, new Dictionary<string, object>(), "admin");
            }
            else
            {
                callbackTtClient.HandOverToCustomerContinue(request.TicketId, HandOverToCustomerTaskHandler.DoneOutcome, new Dictionary<string, object>(), "admin");
            }

            Thread.Sleep(TimeSpan.FromSeconds(20));

            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled, 1);
            }
            else
            {
                AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Finished, 1);
            }

            GetObjectsRequest objetcRequest = new GetObjectsRequest() { Limit = 100, TimeStamp = timeStamp };
            var responses = cloudIntegration.GetResponses(objetcRequest);
            var response = responses.Data.FirstOrDefault(p => p.RefCommandKey.Id == commandId);
            var fulfillOrderResponse = JsonConvert.DeserializeObject<FulfillOrderResponse>(response.SerializedData);

            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                Assert.AreEqual(Result.Failed, fulfillOrderResponse.Result);
            }
            else
            {
                Assert.AreEqual(Result.Done, fulfillOrderResponse.Result);
            }            
        }

        [Test]
        [TestCase("PickupShop")]
        public void PickupShop_Transfer_BadTransfer_NotAllItemsReserved(string shippingMethod)
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var timeStamp = cloudIntegration.GetResponseTimeStamp();
            var commandId = Guid.NewGuid().ToString();

            var fulfillOrder = CreateFulfillOrder(shippingMethod);

            var command = new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey = new Key { Id = commandId, CorrelationId = Guid.NewGuid().ToString() },
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            };
            cloudIntegration.PutCommands(command);

            Thread.Sleep(TimeSpan.FromSeconds(90));

            var request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.CreateTransfer);

            var callbackTtClient = new CallbackTtServiceClient();

            callbackTtClient.CreateTransferContinue(request.TicketId, CreateTransferTaskHandler.BadTransferOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled);

            GetObjectsRequest req = new GetObjectsRequest() { Limit = 100, TimeStamp = timeStamp };
            var responses = cloudIntegration.GetResponses(req);
            var response = responses.Data.FirstOrDefault(p => p.RefCommandKey.Id == commandId);
            var fulfillOrderResponse = JsonConvert.DeserializeObject<FulfillOrderResponse>(response.SerializedData);
            Assert.AreEqual(Result.Failed, fulfillOrderResponse.Result);
            Assert.AreEqual(ResultReasons.NotAllItemsReserved, fulfillOrderResponse.ResultReason);
        }


        public static FulfillOrder CreateFulfillOrder(string shippingMethod)
        {
            var fulfillOrder = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1234567,
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = "Product",
                        ItemId = 1
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1111111,
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = "Set",
                        ItemId = 10
                    }
                },
                SapCode = "R202",
                Contact = new Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                }
            };
            return fulfillOrder;
        }
    }
}
