﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Integration.Tests.Proxy.Cloud;
using MMS.StoreFulfillment.Integration.Tests.Proxy.StoreFulfillment;
using Newtonsoft.Json;
using NUnit.Framework;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Application.SF.TaskHandlers;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Integration.Tests;

namespace MMS.StoreFulfillment.Integration.Tests.CloudClient
{
    [TestFixture(Category = "IntegrationTests")]
    public class FulfillOrderCommandTest
    {

        [Test]
        [TestCase("Delivery")]
        public void Delivery_Carousel_CheckItemExistence_Test(string shippingMethod)
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var fulfillOrder = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000, //  Есть везде по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000201, // Есть в 201 по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                },
                SapCode = "R002",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                    RequestedDeliveryDate = DateTime.Now.AddDays(1),
                    DeliveryTimeslot = "12:00-14:00",
                    DeliveryService = "DHL",
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(80));

            var reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);

            Assert.AreEqual("R201", reserveProductsRequest.SapCode);
        }

        [Test]
        [TestCase("PickupShop", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        [TestCase("PickupShop", "ОТМЕНЕННЫЙ РЕЗЕРВ")]
        public void PickupTransferPlanningFailed(string shippingMethod, string reservePaymentStatus)
        {
            #region "Successfull part"

            var cloudIntegration = new ClientCommandServiceClient();

            var ts1 = cloudIntegration.GetResponseTimeStamp();


            var fulfillOrder = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1234567,
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = "Product",
                        ItemId = 1
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1111111,
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = "Set",
                        ItemId = 10
                    }
                },
                SapCode = "R202",
                Contact = new Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                }
            };

            var id = Guid.NewGuid();

            var cmd = new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = id.ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            };


            cloudIntegration.PutCommands(cmd);

            #endregion

            Thread.Sleep(TimeSpan.FromSeconds(90));
   
            GetObjectsRequest req = new GetObjectsRequest() { Limit = 100, TimeStamp = ts1 };

            var responses = cloudIntegration.GetResponses(req);

            //DOES NOT FIND THE PROPER RESPONSE! Why???
            var resp = responses.Data.FirstOrDefault(p => p.RefCommandKey.Id == id.ToString());


            //...


            //check 
            //FordConstants.NotAllItemsReserved, 
        }


        [Test]
        [TestCase("PickupShop", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        [TestCase("PickupShop", "ОТМЕНЕННЫЙ РЕЗЕРВ")]
        public void SendReserveProducts_HandoverCommandTest(string shippingMethod, string reservePaymentStatus)
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var reserveProducts = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000,
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = "Product"
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000202,
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = "Set"
                    }
                },
                SapCode = "R202",
                Contact = new MMS.Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(reserveProducts),
                        RelatedEntityId = reserveProducts.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(120));

            var context = new StoreFulfillmentContext();
            var reserveProductsRequest =
                context.Requests.Include(t => t.PlanSteps).Include(t => t.Items).First(
                    t => t.CustomerOrder.CustomerOrderId == reserveProducts.CustomerOrderId);
            reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
            context.SaveChanges();

            var callbackTtClient = new Proxy.StoreFulfillment.CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, ReserveProductsTaskHandler.ReserveCreatedOutcome,
                new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(80));

            //Check payment
            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                callbackTtClient.HandOverToCustomerContinue(reserveProductsRequest.TicketId, "Отменен",
                    new Dictionary<string, object>(), "admin");
            }
            else
            {
                callbackTtClient.HandOverToCustomerContinue(reserveProductsRequest.TicketId, "Выдан",
                    new Dictionary<string, object>(), "admin");
            }

            Thread.Sleep(TimeSpan.FromSeconds(20));

            reserveProductsRequest =
                new StoreFulfillmentContext().Requests.Include(t => t.PlanSteps).First(
                    t => t.CustomerOrder.CustomerOrderId == reserveProducts.CustomerOrderId);

            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                Assert.AreEqual(RequestStatus.Canceled, reserveProductsRequest.Status);
            }
            else
            {
                Assert.AreEqual(RequestStatus.Finished, reserveProductsRequest.Status);
            }
        }




        //[Test]
        //public void CreateTicketTest()
        //{

        //   ILifetimeScope _container;


        //    _container = ServiceLocator.Current.GetInstance<ILifetimeScope>();

        //    var storage = new StoreFulfillmentStorage(new StoreFulfillmentContext());

        //    //var planStep = storage.GetPlanStepById(901);
        //    var planStep = storage.GetPlanStepById(945);//create transfer plan

        //    var tts = new MMS.StoreFulfillment.Application.TicketTool.TicketToolServiceClient();

        //    string sapCode = "201";

        //    //throw new NotImplementedException(); //Я добавил зависимость от ILifeTimeScope
        //    var comamndHandler = new CreateTaskCommandHandler(storage, tts, _container );

        //    var cmd = new CreateTaskCommand(planStep.RequestId, planStep.Id,
        //                 planStep.TicketTask.TaskType,
        //                 new Dictionary<string, string>
        //                 {
        //                                        {
        //                                            "{IncomingSapCode}",
        //                                            sapCode
        //                                        }
        //                 });



        //    comamndHandler.Handle(cmd);

        //    //TODO: check status after calling Continue

        //}

        [Test]
        public void DelayedTaskTest()
        {
            string shippingMethod = "Delivery";
            //string key_callBackLater = "Перезвонить позже";
            string key_CallMeIn = "Перезвоните мне через";
            var dic = new Dictionary<string, object>();
            dic.Add(key_CallMeIn, TimeSpan.FromSeconds(90));

            var cloudIntegration = new ClientCommandServiceClient();

            var reserveProducts = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Title = "Товар 1",
                        Article = 1234567,
                        Price = 10000,
                        Qty = 2,
                        ArticleProductType = "Set"
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Title = "Товар 2",
                        Article = 1111111,
                        Price = 10000,
                        Qty = 2
                    }
                },
                SapCode = "R002",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                    RequestedDeliveryDate = DateTime.Now.AddDays(1),
                    DeliveryTimeslot = "12:00-14:00",
                    DeliveryService = "DHL",
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(reserveProducts),
                        RelatedEntityId = reserveProducts.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(60));

            var context = new StoreFulfillmentContext();
            var reserveProductsRequest =
                context.Requests.Include(t => t.PlanSteps).Include(t => t.Items).First(
                    t => t.CustomerOrder.CustomerOrderId == reserveProducts.CustomerOrderId);
            reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
            context.SaveChanges();

            var callbackTtClient = new Proxy.StoreFulfillment.CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, key_CallMeIn,
                dic, "admin");

            Thread.Sleep(TimeSpan.FromSeconds(100));

        }


        [Test]
        [TestCase("Delivery", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        [TestCase("Delivery", "ОТМЕНЕННЫЙ РЕЗЕРВ")]
        public void SendReserveProducts_Delivery_Test(string shippingMethod, string reservePaymentStatus)
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var fulfillOrder = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000, //  Есть везде по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000202, // Есть в 202 по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                },
                SapCode = "R002",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                    RequestedDeliveryDate = DateTime.Now.AddDays(1),
                    DeliveryTimeslot = "12:00-14:00",
                    DeliveryService = "DHL",
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(80));

            var reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.ReserveProducts);


            using (var context = new StoreFulfillmentContext())
            {
                reserveProductsRequest = context.Requests.Include(t => t.PlanSteps).Include(t => t.Items)
                    .First(t => t.CustomerOrder.CustomerOrderId == fulfillOrder.CustomerOrderId);
                reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
                context.SaveChanges();
            }

            var callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, ReserveProductsTaskHandler.ReserveCreatedOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.PutOnDelivery);

            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.PutOnDeliveryContinue(reserveProductsRequest.TicketId, PutOnDeliveryTaskHandler.PutOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(80));

            reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CheckPayment);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.DeliverOrder);

            //Check payment
            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                callbackTtClient.DeliverOrderContinue(reserveProductsRequest.TicketId, DeliverOrderTaskHandler.CancelOutcome, new Dictionary<string, object>(), "admin");
            }
            else
            {
                callbackTtClient.DeliverOrderContinue(reserveProductsRequest.TicketId, DeliverOrderTaskHandler.DoneOutcome, new Dictionary<string, object>(), "admin");
            }

            Thread.Sleep(TimeSpan.FromSeconds(30));


            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled);
            }
            else
            {
                reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Finished);
            }
        }

        [Test]
        //Этот тест нужно переписать
        public void SendReserveProductsCommandWithCanellationTest()
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var handOverToCustomerRequest = new ReserveProducts
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1234567,
                        Price = 10000,
                        Qty = 2,
                        ArticleProductType = "Product"
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1111111,
                        Price = 10000,
                        Qty = 2
                    }
                },
                SapCode = "R002",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "Name2",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = "Delivery",
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(handOverToCustomerRequest),
                        RelatedEntityId = handOverToCustomerRequest.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(80));

            var reserveProductsRequest =
                new StoreFulfillmentContext().Requests.Include(t => t.PlanSteps).First(
                    t => t.CustomerOrder.CustomerOrderId == handOverToCustomerRequest.CustomerOrderId);

            var cancelReserveProductsCommand = new CancelReserveProducts 
            //Вот здесь косяк - это старая команда, она больше не поддерживается
            //Нужно использовать CancelFulfillOrder
            {
                RequestId = reserveProductsRequest.RequestId,
                CustomerOrderId = reserveProductsRequest.CustomerOrder.CustomerOrderId
            };

            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "CancelFulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(cancelReserveProductsCommand),
                        RelatedEntityId = handOverToCustomerRequest.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(120));

            reserveProductsRequest = AssertExtensions.AssertRequest(handOverToCustomerRequest.CustomerOrderId, RequestStatus.Canceled);
            var cancelReserveProductsRequest = AssertExtensions.AssertRequest(handOverToCustomerRequest.CustomerOrderId, RequestStatus.Finished, 1);
            Assert.AreEqual(1, cancelReserveProductsRequest.PlanSteps.Count);

            var callbackTtClient = new Proxy.StoreFulfillment.CallbackTtServiceClient();

            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, "Готово",
    new Dictionary<string, object>(), "admin");

            //try
            //{
            //    reserveProductsRequest = AssertRequest(reserveProductsRequest.CustomerOrder.CustomerOrderId, RequestStatus.Finished);


            //    callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, "Готово",
            //        new Dictionary<string, object>(), "admin");
            //    Assert.Fail();
            //}
            //catch (AssertionException ex)
            //{
            //    throw ex;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //Thread.Sleep(TimeSpan.FromSeconds(20));

            //var newReserveProductsRequest =
            //    new StoreFulfillmentContext().Requests.Include(t => t.PlanSteps).OrderByDescending(t => t.CreatedDate).First(
            //        t => t.CustomerOrder.CustomerOrderId == handOverToCustomerRequest.CustomerOrderId);
            //var oldReserveProductsRequest =
            //    new StoreFulfillmentContext().Requests.Include(t => t.PlanSteps).OrderByDescending(t => t.CreatedDate).Skip(1).First(
            //        t => t.CustomerOrder.CustomerOrderId == handOverToCustomerRequest.CustomerOrderId);

            //Assert.AreEqual(RequestStatus.Canceled, oldReserveProductsRequest.Status);
            //Assert.AreEqual(1, newReserveProductsRequest.PlanSteps.Count);
            //Assert.AreEqual(RequestStatus.Finished, newReserveProductsRequest.Status); //cancel reserve
        }

        [Test]
        public void SendReserveProducts_FailedReserveCreation_FailedReservationTest()
        {
            var cloudIntegration = new ClientCommandServiceClient();
            var handOverToCustomerRequest = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000, //  Есть везде по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000202, // Есть в 202 по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Title = "IPhone 7",
                        Article = 3000000, // Не продается (ошибка создания WWS резерва)
                        Price = 4999,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    }                },
                SapCode = "R202",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "Name2",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = "PickupShop",
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(handOverToCustomerRequest),
                        RelatedEntityId = handOverToCustomerRequest.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(80));

            var reserveProductsRequest = AssertExtensions.AssertRequest(handOverToCustomerRequest.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateReserve);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, "WwsReserveCreationFailed");

            var callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, WwsReserveCreationFailedTaskHandler.FailedOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(20));

            reserveProductsRequest = AssertExtensions.AssertRequest(handOverToCustomerRequest.CustomerOrderId, RequestStatus.Canceled);
            Assert.AreEqual(6, reserveProductsRequest.PlanSteps.Count);
        }


        //Самовывоз из магазина
        [Test]
        public void SendReserveProducts_FailedReserveCreation_SucessfullReservationTest()
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var reserveProducts = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000, //  Есть везде по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000202, // Есть в 202 по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Title = "IPhone 7",
                        Article = 3000000, // Не продается (ошибка создания WWS резерва)
                        Price = 4999,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    }
                },
                SapCode = "R202",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "Name2",
                    Email = "test@test.ru",
                    Phone = "+7911111111111",
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = "PickupShop",
                    RequestedDeliveryDate = DateTime.Now.AddDays(1),
                    DeliveryTimeslot = "12:00-14:00",
                    DeliveryService = "DHL",
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey =
                            new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(reserveProducts),
                        RelatedEntityId = reserveProducts.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(80));

            // Check Request
            var reserveProductsRequest = AssertExtensions.AssertRequest(reserveProducts.CustomerOrderId, RequestStatus.Executing);
            // Check Plan Step
            var currentPlanStep = reserveProductsRequest.CurrentPlanStep();
            Assert.IsNotNull(currentPlanStep);
            currentPlanStep = AssertExtensions.AssertPlanStep(currentPlanStep.Id, PlanStepStatus.Processing, PlanStepType.CreateReserve);
            
            var ticketTask = currentPlanStep.TicketTask;
            Assert.IsNotNull(ticketTask);
            Assert.AreEqual("WwsReserveCreationFailed", ticketTask.TaskType);

            using (var fulfillmentStorage = new StoreFulfillmentStorage(new StoreFulfillmentContext()))
            {
                reserveProductsRequest = fulfillmentStorage.Find(
                        t => t.CustomerOrder.CustomerOrderId == reserveProducts.CustomerOrderId,
                        new Expression<Func<ItemsMovementRequest, object>>[]
                        {t => t.PlanSteps, t => t.Items}).FirstOrDefault();
                reserveProductsRequest.Items.Remove(reserveProductsRequest.Items.First(t => t.Article == 3000000));
                fulfillmentStorage.Save(reserveProductsRequest);
            }

            var callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.WwsReserveCreationFailedContinue(reserveProductsRequest.TicketId, WwsReserveCreationFailedTaskHandler.FixedOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            // Check Request
            reserveProductsRequest = AssertExtensions.AssertRequest(reserveProducts.CustomerOrderId, RequestStatus.Executing);
            // Check Plan Step
            currentPlanStep = reserveProductsRequest.CurrentPlanStep();
            Assert.IsNotNull(currentPlanStep);
            currentPlanStep = AssertExtensions.AssertPlanStep(currentPlanStep.Id, PlanStepStatus.Processing, PlanStepType.CreateTask);

            //Check task:
            ticketTask = currentPlanStep.TicketTask;
            Assert.IsNotNull(ticketTask);
            Assert.AreEqual("ReserveProducts", ticketTask.TaskType);

            using (var context = new StoreFulfillmentContext())
            {
                reserveProductsRequest = context.Requests.Include(t => t.PlanSteps).Include(t => t.Items).First(t => t.CustomerOrder.CustomerOrderId == reserveProducts.CustomerOrderId);
                reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
                context.SaveChanges();
            }

            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, ReserveProductsTaskHandler.ReserveCreatedOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(90));

            // Check Request
            reserveProductsRequest = AssertExtensions.AssertRequest(reserveProducts.CustomerOrderId, RequestStatus.Executing);
            // Check Plan Step
            currentPlanStep = reserveProductsRequest.CurrentPlanStep();
            Assert.IsNotNull(currentPlanStep);
            currentPlanStep = AssertExtensions.AssertPlanStep(currentPlanStep.Id, PlanStepStatus.Processing, PlanStepType.CheckPayment);

            //Check payment
            callbackTtClient.HandOverToCustomerContinue(reserveProductsRequest.TicketId, HandOverToCustomerTaskHandler.DoneOutcome, new Dictionary<string, object>(), "admin");
            Thread.Sleep(TimeSpan.FromSeconds(20));

            // Check Request
            reserveProductsRequest = AssertExtensions.AssertRequest(reserveProducts.CustomerOrderId, RequestStatus.Finished);
            Assert.AreEqual(5, reserveProductsRequest.PlanSteps.Count);
        }

        [Test]
        [TestCase("Delivery", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        [TestCase("Delivery", "ОТМЕНЕННЫЙ РЕЗЕРВ")]
        public void Delivery_FailedStore(string shippingMethod, string reservePaymentStatus)
        {
            var cloudIntegration = new ClientCommandServiceClient();

            var fulfillOrder = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000, //  Есть везде по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1201601, // Есть только в 201 и 601
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                },
                SapCode = "R002",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                }
            };
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey = new Key {Id = Guid.NewGuid().ToString(), CorrelationId = Guid.NewGuid().ToString()},
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(90));

            var reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.ReserveProducts);

            using (var context = new StoreFulfillmentContext())
            {
                reserveProductsRequest = context.Requests.Include(t => t.PlanSteps).Include(t => t.Items)
                    .First(t => t.CustomerOrder.CustomerOrderId == fulfillOrder.CustomerOrderId);
                reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.NotFound);
                context.SaveChanges();
            }

            var callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, ReserveProductsTaskHandler.ReserveCreatedOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(130));

            reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled, 0);
            reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing, 1);
            currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.ReserveProducts);

            using (var context = new StoreFulfillmentContext())
            {
                reserveProductsRequest = context.Requests.Include(t => t.PlanSteps).Include(t => t.Items)
                    .OrderByDescending(t => t.CreatedDate)
                    .First(t => t.CustomerOrder.CustomerOrderId == fulfillOrder.CustomerOrderId);
                reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
                context.SaveChanges();
            }

            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(reserveProductsRequest.TicketId, ReserveProductsTaskHandler.ReserveCreatedOutcome, new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(60));

            reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled, 0);
            reserveProductsRequest = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing, 1);
            currentPlanStep = AssertExtensions.AssertPlanStep(reserveProductsRequest.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.PutOnDelivery);


            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.PutOnDeliveryContinue(reserveProductsRequest.TicketId, "Записал", new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(70));

            //Check payment
            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                callbackTtClient.DeliverOrderContinue(reserveProductsRequest.TicketId, "Отменен", new Dictionary<string, object>(), "admin");
            }
            else
            {
                callbackTtClient.DeliverOrderContinue(reserveProductsRequest.TicketId, "Доставлено", new Dictionary<string, object>(), "admin");
            }

            Thread.Sleep(TimeSpan.FromSeconds(30));

            reserveProductsRequest = new StoreFulfillmentContext().Requests.Include(t => t.PlanSteps).OrderByDescending(t => t.CreatedDate)
                    .First(t => t.CustomerOrder.CustomerOrderId == fulfillOrder.CustomerOrderId);

            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                Assert.AreEqual(RequestStatus.Canceled, reserveProductsRequest.Status);
            }
            else
            {
                Assert.AreEqual(RequestStatus.Finished, reserveProductsRequest.Status);
            }
        }
    }
}
