﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Integration.Tests.Proxy.Cloud;
using MMS.StoreFulfillment.Integration.Tests.Proxy.StoreFulfillment;
using Newtonsoft.Json;
using NUnit.Framework;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Application.SF.TaskHandlers;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Integration.Tests;

namespace MMS.StoreFulfillment.Integration.Tests.CloudClient
{
    [TestFixture(Category = "IntegrationTests")]
    public class DeliveryTest
    {
        [Test]
        [TestCase("Delivery", "ОПЛАЧЕННЫЙ РЕЗЕРВ")]
        [TestCase("Delivery", "ОТМЕНЕННЫЙ РЕЗЕРВ")]
        public void Delivery_Success(string shippingMethod, string reservePaymentStatus)
        {
            var cloudIntegration = new ClientCommandServiceClient();
            var timeStamp = cloudIntegration.GetResponseTimeStamp();
            var commandId = Guid.NewGuid().ToString();

            var fulfillOrder = CreateFulfillOrder(shippingMethod);
            cloudIntegration.PutCommands(new PutReceiverObjectsRequestOfReceiverCommandzLKfs3NJ
            {
                Data = new[]
                {
                    new ReceiverCommand
                    {
                        CommandKey = new Key { Id = commandId, CorrelationId = Guid.NewGuid().ToString() },
                        ClientCreatedDate = DateTime.UtcNow,
                        CommandName = "FulfillOrder",
                        SerializedData = JsonConvert.SerializeObject(fulfillOrder),
                        RelatedEntityId = fulfillOrder.CustomerOrderId,
                        Version = "1.0.0.1"
                    }
                },
                NewTimeStamp = ""
            });

            Thread.Sleep(TimeSpan.FromSeconds(190));

            var request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            var currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.ReserveProducts);


            using (var context = new StoreFulfillmentContext())
            {
                var reserveProductsRequest = context.Requests
                    .Include(t => t.PlanSteps).Include(t => t.Items)
                    .First(t => t.CustomerOrder.CustomerOrderId == fulfillOrder.CustomerOrderId);
                reserveProductsRequest.Items.ToList().ForEach(t => t.ReserveStatus = ReserveStatuses.Reserved);
                context.SaveChanges();
            }

            var callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.ReserveProductsContinue(request.TicketId, "Готово", new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CreateTask);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.PutOnDelivery);

            callbackTtClient = new CallbackTtServiceClient();
            callbackTtClient.PutOnDeliveryContinue(request.TicketId, "Записал", new Dictionary<string, object>(), "admin");

            Thread.Sleep(TimeSpan.FromSeconds(70));

            request = AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Executing);
            currentPlanStep = AssertExtensions.AssertPlanStep(request.CurrentPlanStep().Id, PlanStepStatus.Processing, PlanStepType.CheckPayment);
            AssertExtensions.AssertTask(currentPlanStep.TicketTask, TicketTaskTypes.DeliverOrder);

            //Check payment
            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                callbackTtClient.DeliverOrderContinue(request.TicketId, "Отменен", new Dictionary<string, object>(), "admin");
            }
            else
            {
                callbackTtClient.DeliverOrderContinue(request.TicketId, "Доставлено", new Dictionary<string, object>(), "admin");
            }

            Thread.Sleep(TimeSpan.FromSeconds(30));

            if (reservePaymentStatus == "ОТМЕНЕННЫЙ РЕЗЕРВ")
            {
                AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Canceled);
                AssertExtensions.AssertResponse(timeStamp, commandId, Result.Failed);
            }
            else
            {
                AssertExtensions.AssertRequest(fulfillOrder.CustomerOrderId, RequestStatus.Finished);
                AssertExtensions.AssertResponse(timeStamp, commandId, Result.Done);
            }
        }

        public static FulfillOrder CreateFulfillOrder(string shippingMethod)
        {
            var fulfillOrder = new FulfillOrder
            {
                CustomerOrderId = AssertExtensions.GenerateOrderNumber(),
                Items = new[]
                {
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000000, //  Есть везде по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },
                    new Cloud.Commands.SF.Item
                    {
                        Article = 1000202, // Есть в 202 по OrdersBackendServiceMock
                        Price = 10000,
                        Qty = 1,
                        ArticleProductType = ArticleProductTypes.Product
                    },

                },
                SapCode = "R002",
                Contact = new Cloud.Commands.SF.Customer
                {
                    FirstName = "Name",
                    LastName = "ОТМЕНЕННЫЙ РЕЗЕРВ",
                    Email = "test@test.ru",
                    Phone = "+7911111111111"
                },
                CustomerOrder = new Cloud.Commands.SF.CustomerOrderInfo
                {
                    City = "Москва",
                    OrderSource = "WebSite",
                    PaymentType = "Cash",
                    ShippingMethod = shippingMethod,
                }
            };
            return fulfillOrder;
        }
    }
}
