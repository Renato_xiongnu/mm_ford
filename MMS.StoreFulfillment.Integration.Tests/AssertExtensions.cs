﻿using System;
using System.Data.Entity;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Integration.Tests.Proxy.Cloud;
using Newtonsoft.Json;
using NUnit.Framework;

namespace MMS.StoreFulfillment.Integration.Tests
{
    public static class AssertExtensions
    {
        public static string GenerateOrderNumber()
        {
            return "002-12345678" + new Random((int)DateTime.Now.Ticks & 0x0000FFFF).Next(9999);
        }


        public static ItemsMovementRequest AssertRequest(string orderId, RequestStatus status, int skip = 0)
        {
            using (var context = new StoreFulfillmentContext())
            {
                var reserveProductsRequest = context.Requests.Include(t => t.PlanSteps).Include(t => t.Items)
                    .Where(t => t.CustomerOrder.CustomerOrderId == orderId)
                    .OrderBy(t => t.CreatedDate)
                    .Skip(skip)
                    .FirstOrDefault();

                // Check Request
                Assert.IsNotNull(reserveProductsRequest);
                Assert.AreEqual(status, reserveProductsRequest.Status);
                return reserveProductsRequest;
            }
        }

        public static PlanStep AssertPlanStep(int planStepId, PlanStepStatus status, PlanStepType type)
        {
            using (var fulfillmentStorage = new StoreFulfillmentStorage(new StoreFulfillmentContext()))
            {
                var currentPlanStep = fulfillmentStorage.GetPlanStepById(planStepId);
                Assert.IsNotNull(currentPlanStep);
                Assert.AreEqual(status, currentPlanStep.Status);
                Assert.AreEqual(type, currentPlanStep.PlanStepType);
                return currentPlanStep;
            }
        }

        public static void AssertTask(TicketTask ticketTask, string type)
        {
            Assert.IsNotNull(ticketTask);
            Assert.AreEqual(type, ticketTask.TaskType);
        }

        public static FulfillOrderResponse AssertResponse(string timeStamp, string commandId, Result result)
        {
            using (var cloudIntegration = new ClientCommandServiceClient())
            {
                GetObjectsRequest req = new GetObjectsRequest() { Limit = 100, TimeStamp = timeStamp };
                var responses = cloudIntegration.GetResponses(req);
                var response = responses.Data.FirstOrDefault(p => p.RefCommandKey.Id == commandId);
                var fulfillOrderResponse = JsonConvert.DeserializeObject<FulfillOrderResponse>(response.SerializedData);
                Assert.AreEqual(result, fulfillOrderResponse.Result);
                return fulfillOrderResponse;
            }
        }
    }
}
