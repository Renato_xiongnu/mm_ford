﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.ManagerUI.Models.RequestView;

namespace MMS.StoreFulfillment.ManagerUI.Controllers
{
    public class RequestViewController : Controller
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;

        public RequestViewController(IStoreFulfillmentStorage storeFulfillmentStorage)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public ActionResult Index(string requestId)
        {
            var request = _storeFulfillmentStorage.GetById(requestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });

            var requestViewModel = request.ToRequestViewModel();

            return View(requestViewModel);
        }
    }
}