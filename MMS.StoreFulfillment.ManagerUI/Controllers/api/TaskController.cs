﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.ManagerUI.Models;
using Task = MMS.StoreFulfillment.ManagerUI.Models.Task;

namespace MMS.StoreFulfillment.ManagerUI.Controllers.api
{
    public class TaskController : ApiController
    {
        [Route("api/Tasks")]
        [ResponseType(typeof(IEnumerable<Task>))]
        public async Task<IHttpActionResult> Get()
        {
            var client = new TicketToolServiceClient();

            var parameters = await client.GetMyParametersAsync();
            var taskItems = await GetTaskItems(parameters.Select(t => t.ParameterName).ToArray());
            return Ok(taskItems);
        }

        [Route("api/Tasks/{sapCode}")]
        [ResponseType(typeof(IEnumerable<Task>))]
        public async Task<IHttpActionResult> Get(string sapCode)
        {
            var taskItems = await GetTaskItems(new[] { sapCode });
            return Ok(taskItems);
        }

        private async Task<List<Task>> GetTaskItems(string[] parameters)
        {
            var client = new TicketToolServiceClient();
            var tasksResult = await client.GetMyTasksInProgressAsync(parameters, null);

            var storage = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var requests = tasksResult
                .Where(m => !string.IsNullOrEmpty(m.WorkitemId))
                .Select(m => new WorkitemId(m.WorkitemId))
                .Select(w => w.RequestId)
                .ToArray();
            var requestResult = storage.Find(el => requests.Contains(el.RequestId))
                .Include(t => t.PlanSteps)
                .Include(p => p.PlanSteps.Select(planStep => planStep.TicketTask))
                .ToList();

            var taskItems = tasksResult
                .Select(t => new { Task = t, Request = requestResult.SingleOrDefault(z => z.RequestId == new WorkitemId(t.WorkitemId).RequestId) })
                .Where(x => x.Request != null)
                .Select(x => x.Task.ToTaskItem(x.Request))
                .OrderBy(t => t.CreatedDate)
                .ToList();
            return taskItems;
        }
    }
}