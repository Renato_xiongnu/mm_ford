﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.ManagerUI.Models;

namespace MMS.StoreFulfillment.ManagerUI.Controllers.api
{
    public class SapCodeController:ApiController
    {
        [Route("api/Stores")]
        [ResponseType(typeof(IEnumerable<Store>))]
        public async Task<IHttpActionResult> Get()
        {
            using (var client=new TicketToolServiceClient())
            {
                var stores = await client.GetMyParametersAsync();

                return Ok(stores.Select(t => new Store {SapCode = t.ParameterName, Name = t.Name}));
            }
        }
    }
}