﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;
using MMS.StoreFulfillment.ManagerUI.Models;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;
using MMS.StoreFulfillment.ManagerUI.Proxy.RecreateReserve;
using RequiredField = MMS.StoreFulfillment.Application.TicketTool.RequiredField;

namespace MMS.StoreFulfillment.ManagerUI.Controllers
{
    public class ManagerTaskController : Controller
    {
        private readonly ITaskStrategy _taskStrategy;
        private readonly IAuthStrategy _authStrategy;
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;
        private readonly IRecreateReserveService _recreateReserveService;
        private readonly TimeZoneInfo _moscow = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

        public ManagerTaskController(
            ITaskStrategy taskFacade,
            IAuthStrategy authStrategy,
            IStoreFulfillmentStorage storeFulfillmentStorage,
            IRecreateReserveService recreateReserveService)
        {
            _taskStrategy = taskFacade;
            _authStrategy = authStrategy;
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _recreateReserveService = recreateReserveService;
        }

        public ActionResult Index(string taskId, string m, string u, string key, short readOnly = 0)
        {
            SetCache();

            var authResult = _authStrategy.LoginByToken(u, m, taskId, key);
            if (!authResult) return RedirectToErrorPage("Invalid credentials");

            var taskResult = _taskStrategy.AssignTaskForMe(taskId);
            if (taskResult.HasError || taskResult.Result == null)
                return RedirectToErrorPage(taskResult.MessageText); //Какой текст?

            var commandItem = _storeFulfillmentStorage.GetById(new WorkitemId(taskResult.Result.WorkItemId).RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            var commandItemModel = commandItem.ToItemMovementRequestViewModel(readOnly == 1);
            var taskItem = taskResult.Result.ToTaskViewModel();

            return View(new ItemMovementViewModel
            {
                Task = taskItem,
                MoveItemsCommand = commandItemModel
            });
        }

        public ActionResult Handover(string taskId, string m, string u, string key)
        {
            SetCache();

            var authResult = _authStrategy.LoginByToken(u, m, taskId, key);
            if (!authResult) return RedirectToErrorPage("Invalid credentials");

            var task = _taskStrategy.GetTask(taskId);
            if (task == null)
                return RedirectToErrorPage("Task not found");

            var commandItem = _storeFulfillmentStorage.GetById(new WorkitemId(task.WorkItemId).RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            var commandItemModel = commandItem.ToItemMovementRequestViewModel(true, true);
            commandItemModel.Additionalnfo = new HandoverToCustomerAdditionalInfo
            {
                CustomerName = string.Format("{0} {1}", commandItem.Contact.LastName, commandItem.Contact.FirstName),
                ExpirationDate = commandItem.ExpirationDate != null ? TimeZoneInfo.ConvertTimeFromUtc(commandItem.ExpirationDate.Value, _moscow) : null as DateTime?,
                PaymentType = commandItem.CustomerOrder.PaymentType,
                Phone = commandItem.Contact.Phone
            };
            var taskItem = task.ToTaskViewModel();

            return View(new ItemMovementViewModel
            {
                Task = taskItem,
                MoveItemsCommand = commandItemModel
            });
        }

        public ActionResult DeliverOrder(string taskId, string m, string u, string key)
        {
            SetCache();

            var authResult = _authStrategy.LoginByToken(u, m, taskId, key);
            if (!authResult) return RedirectToErrorPage("Invalid credentials");

            var task = _taskStrategy.GetTask(taskId);
            if (task == null)
                return RedirectToErrorPage("Task not found");

            var commandItem = _storeFulfillmentStorage.GetById(new WorkitemId(task.WorkItemId).RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            var commandItemModel = commandItem.ToItemMovementRequestViewModel(true, true);
            commandItemModel.Additionalnfo = new DeliverOrderAdditionalInfo
            {
                DeliveryDate = commandItem.Contact.RequestedDeliveryDate != null ? TimeZoneInfo.ConvertTimeFromUtc(commandItem.Contact.RequestedDeliveryDate.Value, _moscow) : null as DateTime?,
                RequestedDeliveryService = commandItem.Contact.RequestedDeliveryServiceOption,
                RequestedDeliveryTimeInterval = commandItem.Contact.RequestedDeliveryTimeslot,
                Address = commandItem.Contact.Address,
                Floor = commandItem.Contact.Floor,
                CustomerName = string.Format("{0} {1}", commandItem.Contact.LastName, commandItem.Contact.FirstName),
                PaymentType = commandItem.CustomerOrder.PaymentType,
                CustomerPhone = commandItem.Contact.Phone,
            };
            var taskItem = task.ToTaskViewModel();

            return View(new ItemMovementViewModel
            {
                Task = taskItem,
                MoveItemsCommand = commandItemModel
            });
        }

        public ActionResult PutOnDelivery(string taskId, string m, string u, string key)
        {
            SetCache();

            var authResult = _authStrategy.LoginByToken(u, m, taskId, key);
            if (!authResult) return RedirectToErrorPage("Invalid credentials");

            var taskResult = _taskStrategy.AssignTaskForMe(taskId);
            if (taskResult.HasError || taskResult.Result == null)
                return RedirectToErrorPage(taskResult.MessageText); //Какой текст?

            var commandItem = _storeFulfillmentStorage.GetById(new WorkitemId(taskResult.Result.WorkItemId).RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            var commandItemModel = commandItem.ToItemMovementRequestViewModel(true, false);
            commandItemModel.Additionalnfo = new PutOnDeliveryAdditionalInfo
            {
                CustomerName = string.Format("{0} {1}", commandItem.Contact.LastName, commandItem.Contact.FirstName),
                CustomerPhone = commandItem.Contact.Phone,
                DeliveryDate = commandItem.Contact.RequestedDeliveryDate != null ? TimeZoneInfo.ConvertTimeFromUtc(commandItem.Contact.RequestedDeliveryDate.Value, _moscow) : null as DateTime?,
                RequestedDeliveryService = commandItem.Contact.RequestedDeliveryServiceOption,
                RequestedDeliveryTimeInterval = commandItem.Contact.RequestedDeliveryTimeslot,
                Address = commandItem.Contact.Address,
                Floor = commandItem.Contact.Floor
            };
            var taskItem = taskResult.Result.ToTaskViewModel();

            return View(new ItemMovementViewModel
            {
                Task = taskItem,
                MoveItemsCommand = commandItemModel
            });
        }

        [HttpPost]
        public ActionResult CompleteTask(string taskId, string requestId, Outcome outcome, string comments, ItemViewModel[] items)
        {
            var request = _storeFulfillmentStorage.GetById(requestId, new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            if (request.Status == RequestStatus.Canceled || request.Status == RequestStatus.Compensated)
            {
                return Json(new ResultViewModel { HasErrors = true, Message = "Заказ был отменен. Верни товар в зал." });
            }

            request.Comments = comments;
            if (items != null && items.Length > 0)
            {
                foreach (var itemViewModel in items)
                {
                    var targetItem = request.Items.First(el => el.Id == itemViewModel.Id);
                    targetItem.ReserveStatus = targetItem.ArticleProductType == ArticleProductTypes.Product || targetItem.ArticleProductType == ArticleProductTypes.Set ? itemViewModel.ReserveStatus : ReserveStatuses.Reserved;
                    targetItem.StorageCell = itemViewModel.StorageCell;
                }
            }
            _storeFulfillmentStorage.Save(request);

            var taskData = new TaskExecutionResult
            {
                TaskId = taskId.ToString(CultureInfo.InvariantCulture),
                Outcome = outcome.Value,
            };
            if (outcome.RequiredFields != null)
            {
                var reqFields = outcome.RequiredFields.Select(requiredField =>
                {
                    return new RequiredField
                    {
                        Name = requiredField.Name,
                        Value = requiredField.GetConvertedValue(),
                        Type = requiredField.StringType
                    };
                }).ToArray();
                _taskStrategy.SaveTaskOutcome(taskData, reqFields);
            }
            else
            {
                _taskStrategy.SaveTaskOutcome(taskData);
            }
            return Json(new ResultViewModel());
        }

        [HttpPost]
        public ActionResult UpdateOrder(string taskId, string requestId, ItemViewModel[] items)
        {
            var request = _storeFulfillmentStorage.GetById(requestId, new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            if (request.Status == RequestStatus.Canceled || request.Status == RequestStatus.Compensated)
            {
                return Json(new ResultViewModel { HasErrors = true, Message = "Заказ был отменен. Верни товар в зал." });
            }

            foreach (var targetItem in request.Items)
            {
                var itemViewModel = items.FirstOrDefault(el => el.Id == targetItem.Id);
                if (itemViewModel != null)
                {
                    targetItem.HasShippedFromStock = itemViewModel.HasShippedFromStock;
                    targetItem.SerialNumber = itemViewModel.SerialNumber;
                }
            }

            try
            {
                var reserveId = _recreateReserveService.RecreateReserve(requestId, request.Items.ToArray());
                return Json(new ResultViewModel { Message = "Новый номер резерва в WWS: " + reserveId });
            }
            catch (Exception ex)
            {
                return Json(new ResultViewModel { HasErrors = true, Message = ex.Message });
            }
        }


        [HttpPost]
        public ActionResult Complete(ItemMovementViewModel orderModel)
        {
            var request = _storeFulfillmentStorage.GetById(orderModel.MoveItemsCommand.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items, t => t.PlanSteps });
            if (request.Status == RequestStatus.Canceled || request.Status == RequestStatus.Compensated)
            {
                TempData["ErrorMessage"] = "Заказ был отменен. Верни товар в зал.";
                return RedirectToAction("RedirectToErrorPage");
            }

            var currentPlanStep = request.CurrentPlanStep() as TaskPlanStep;
            var currentTaskType = currentPlanStep != null ? currentPlanStep.TaskType : string.Empty;
            var requestContent = orderModel.MoveItemsCommand.Additionalnfo as ReserveProductsContent;
            if (requestContent != null && currentTaskType == TicketTaskTypes.ReserveProducts)
            {
                request.Comments = requestContent.Comments;

                foreach (var itemViewModel in orderModel.MoveItemsCommand.Items)
                {
                    var targetItem = request.Items.First(el => el.Id == itemViewModel.Id);
                    if (targetItem.ArticleProductType == ArticleProductTypes.Product ||
                        targetItem.ArticleProductType == ArticleProductTypes.Set)
                    {
                        targetItem.ReserveStatus = itemViewModel.ReserveStatus;
                    }
                    else
                    {
                        targetItem.ReserveStatus = ReserveStatuses.Reserved;
                    }
                }
            }

            var taskData = new TaskExecutionResult
            {
                TaskId = orderModel.Task.TaskId.ToString(CultureInfo.InvariantCulture),
                Outcome = orderModel.MoveItemsCommand.Outcome,
            };
            _storeFulfillmentStorage.Save(request);

            if (orderModel.MoveItemsCommand.RequiredFields != null)
            {
                var reqFields = new List<RequiredField>();
                foreach (var requiredField in orderModel.MoveItemsCommand.RequiredFields)
                {
                    reqFields.Add(new RequiredField
                    {
                        Name = requiredField.Name,
                        Value = requiredField.Value
                    });
                }
                _taskStrategy.SaveTaskOutcome(taskData, reqFields.ToArray());
            }
            else
            {
                _taskStrategy.SaveTaskOutcome(taskData);
            }

            return View("Success");
        }

        public ActionResult RedirectToErrorPage(string message = "Ошибка при завершении задачи")
        {
            TempData["ErrorMessage"] = TempData["ErrorMessage"] ?? message;
            return RedirectToAction("Error", "Errors");
        }

        public ActionResult FinishLater(string taskId)
        {
            try
            {
                _taskStrategy.FinishTaskLater(taskId);
                return View("FinishLater");
            }            
            catch(Exception ex)
            {
                TempData["ErrorMessage"] = "Что-то пошло не так...";
                return RedirectToAction("RedirectToErrorPage");
            }            
        }

        public ActionResult Success()
        {
            return View("Success");
        }

        private void SetCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();
        }
    }
}