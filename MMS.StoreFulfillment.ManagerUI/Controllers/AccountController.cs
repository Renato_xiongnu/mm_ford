﻿using System;
using System.Linq;
using System.Web.Mvc;
using MMS.StoreFulfillment.ManagerUI.AppServices;
using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;

namespace MMS.StoreFulfillment.ManagerUI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IDataFacade _dataFacade;

        public AccountController(IDataFacade dataFacade )
        {
            _dataFacade = dataFacade;
        }

        private IAuthStrategy Auth
        {
            get { return _dataFacade.Auth; }
        }

        [HttpPost]
        public ActionResult Login(string name,string password )
        {
            var result = Auth.Login(name, password, true);
            return result 
                ? RedirectToAction("CheckUser")
                : (ActionResult)Redirect(Request.UrlReferrer == null ? "/" : Request.UrlReferrer.AbsoluteUri);
        }

        public ActionResult CheckUser()
        {
            var result = Auth.GetCurrentUser();

            if (result.Roles.All(r => r == "Agent"))
                throw new InvalidOperationException("Воспользуйтесь другим интерфейсом получения задач");

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            Auth.Logout();
            return RedirectToAction("Index", "Home");
        }
    }
}
