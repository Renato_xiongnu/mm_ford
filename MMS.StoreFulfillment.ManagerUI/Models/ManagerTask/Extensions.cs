﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public static class Extensions
    {
        public static ItemMovementRequestViewModel ToItemMovementRequestViewModel(this ItemsMovementRequest request, bool readOnly = false, bool hideOutcomes = false)
        {
            var commandItemModel = new ItemMovementRequestViewModel
            {
                RequestId = request.RequestId,
                CustomerOrderId = request.CustomerOrder.CustomerOrderId,
                Additionalnfo = new ReserveProductsContent
                {
                    DeliveryType = request.CustomerOrder.DeliveryType == DeliveryType.Pickup ? "Самовывоз" : "Доставка",
                },
                HideOutcomes = hideOutcomes,
                Items =
                    request.Items.Select(
                        el =>
                        {
                            var item = new ItemViewModel(el);
                            item.ItemAnswers = GetOrderLineAnswers();
                            //item.ReserveStatus = GetReviewItemStateDisplayName(itemState);
                            item.IsReadOnly = readOnly;
                            return item;
                        })
                        .ToArray(),
                WwsOrderId = request.WwsOrderId
            };

            return commandItemModel;
        }

        public static TaskViewModel ToTaskViewModel(this TicketTask task)
        {
            return new TaskViewModel
            {
                TaskId = Convert.ToInt32(task.TaskId),
                Name = task.Name,
                CreationDate = task.Created,
                Escaleted = task.Escalated,
                Url = task.WorkitemPageUrl,
                WorkItemId = task.WorkItemId,
                Store = new StoreItem(),
                RequiredFields = task.RequiredFields.Select(x => new RequiredField
                {
                    Name = x.Name,
                    //Value = x.Value.ToString(),
                    DefaultValue = Convert.ToString(x.DefaultValue),
                    StringType = x.Type
                }).ToArray(),
                Outcomes = task.Outcomes.Select(x => new Outcome
                {
                    Value = x.Value,
                    RequiredFields = x.RequiredFields.Select(c => new RequiredField
                    {
                        Name = c.Name,
                        //Value = c.Value.ToString(),
                        DefaultValue = Convert.ToString(c.DefaultValue),
                        StringType = c.Type
                    }).ToArray()
                }).ToArray(),
                Description = task.Description
            };
        }

        private static IList<SelectListItem> GetOrderLineAnswers()
        {
            var lineStatuses = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.Reserved),
                    Value = ReserveStatuses.Reserved,
                },
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.ReservedPart),
                    Value = ReserveStatuses.ReservedPart,
                    Disabled = true
                },
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.ReservedDisplayItem),
                    Value = ReserveStatuses.ReservedDisplayItem,
                    Disabled = true
                },
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.DisplayItem),
                    Value = ReserveStatuses.DisplayItem,
                },
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.NotFound),
                    Value = ReserveStatuses.NotFound,
                },
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.IncorrectPrice),
                    Value = ReserveStatuses.IncorrectPrice,
                    Disabled = true
                },
                new SelectListItem
                {
                    Text = GetReviewItemStateDisplayName(ReserveStatuses.Transfer),
                    Value = ReserveStatuses.Transfer,
                    Disabled = true
                }
            };
            return lineStatuses;
        }

        private static string GetReviewItemStateDisplayName(string state)
        {
            switch (state)
            {
                case ReserveStatuses.Empty:
                    return "";
                case ReserveStatuses.IncorrectPrice:
                    return "Неправильная цена";
                case ReserveStatuses.NotFound:
                    return "Не найден";
                case ReserveStatuses.Reserved:
                    return "Отложен";
                case ReserveStatuses.ReservedDisplayItem:
                    return "Витрина";
                case ReserveStatuses.ReservedPart:
                    return "Отложен частично";
                case ReserveStatuses.DisplayItem:
                    return "Витрина";
                case ReserveStatuses.Transfer:
                    return "Трансфер";
                default:
                    return "";
            }
        }

        public static Outcome[] TestOutcomes()
        {
            return new[]
            {
                new Outcome
                {
                    Value = "String Value 1",
                    RequiredFields = new RequiredField[]
                    {
                        new RequiredField
                        {
                            Name = "Required Field 1",
                            StringType = "String",
                        },
                        new RequiredField
                        {
                            Name = "Required Field 2",
                            StringType = "Choice",
                            PredefinedValues = new[] {"Value 1", "Value 2", "Value 3"}
                        },
                        new RequiredField
                        {
                            Name = "Required Field 3",
                            StringType = "DateTime",
                        },
                        new RequiredField
                        {
                            Name = "Required Field 4",
                            StringType = "Boolean",
                        },
                        new RequiredField
                        {
                            Name = "TimeSpan Field 5",
                            StringType = "TimeSpan",
                        }
                    }
                },
                new Outcome
                {
                    Value = "Choice Value 1",
                    RequiredFields = new RequiredField[]
                    {
                        new RequiredField
                        {
                            Name = "Required Field 1",
                            StringType = "Choice",
                            DefaultValue = "Value 2",
                            PredefinedValues = new string[] {"Value 1", "Value 2", "Value 3"}
                        }
                    }
                },
                new Outcome
                {
                    Value = "DateTime Value 1",
                    RequiredFields = new RequiredField[]
                    {
                        new RequiredField
                        {
                            Name = "Required Field 1",
                            StringType = "DateTime",
                            DefaultValue = DateTime.Now.ToString()
                        }
                    }
                },
                new Outcome
                {
                    Value = "Boolean Value 1",
                    RequiredFields = new RequiredField[]
                    {
                        new RequiredField
                        {
                            Name = "Required Field 1",
                            StringType = "Boolean",
                            DefaultValue = true.ToString()
                        }
                    }
                },
                new Outcome
                {
                    Value = "TimeSpan Value 1",
                    RequiredFields = new RequiredField[]
                    {
                        new RequiredField
                        {
                            Name = "TimeSpan Field 1",
                            StringType = "TimeSpan",
                            DefaultValue = TimeSpan.FromHours(36.5).ToString()
                        }
                    }
                }
            };
        }
    }
}