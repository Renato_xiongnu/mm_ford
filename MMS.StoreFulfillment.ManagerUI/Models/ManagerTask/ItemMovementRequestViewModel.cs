﻿using System.Collections.Generic;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class ItemMovementRequestViewModel
    {
        public string RequestId { get; set; }
        public string WwsOrderId { get; set; }

        public string CustomerOrderId { get; set; }
        
        public string Outcome { get; set; }

        public IList<ItemViewModel> Items { get; set; }

        public IAdditionalnfo Additionalnfo { get; set; }

        public bool HideOutcomes { get; set; }

        public ICollection<RequiredField> RequiredFields { get; set; }

        public bool ShowPrice
        {
            get
            {
                return !(Additionalnfo is PutOnDeliveryAdditionalInfo) && !(Additionalnfo is DeliverOrderAdditionalInfo);
            }
        }

        public bool CanEdit
        {
            get
            {
                return (Additionalnfo is HandoverToCustomerAdditionalInfo);
            }
        }

        public bool CanEditStorageCell
        {
            get
            {
                return (Additionalnfo is ReserveProductsContent);
            }
        }
    }
}