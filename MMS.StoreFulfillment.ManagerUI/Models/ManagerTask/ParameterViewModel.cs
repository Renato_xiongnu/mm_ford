﻿namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class ParameterViewModel
    {        
        public string Name { get; set; }

        public string Title { get; set; }

        public string DisplayName
        {
            get { return string.Format("{0} - {1}", Name, Title); }
        }
    }
}