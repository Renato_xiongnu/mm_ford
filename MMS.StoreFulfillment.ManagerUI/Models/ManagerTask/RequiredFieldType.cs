﻿namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public enum RequiredFieldType
    {
        DateTime,
        TimeSpan,
        Boolean,
        String,
        Int32,
        Decimal,
        Double,
        Choice
    }
}