﻿namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class ItemMovementViewModel
    {
        public TaskViewModel Task { get; set; }

        public ItemMovementRequestViewModel MoveItemsCommand { get; set; }
    }
}