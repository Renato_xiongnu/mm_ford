﻿using System;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class DeliverOrderAdditionalInfo:IAdditionalnfo
    {
        public string PaymentType { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhone { get; set; }
        
        public DateTime? DeliveryDate { get; set; }

        public string Address { get; set; }

        public int Floor { get; set; }

        public string RequestedDeliveryTimeInterval { get; set; }

        public string RequestedDeliveryService { get; set; }
    }
}