﻿namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class ReserveProductsContent : IAdditionalnfo
    {
        public string Comments { get; set; }

        public string DeliveryType { get; set; }
    }
}