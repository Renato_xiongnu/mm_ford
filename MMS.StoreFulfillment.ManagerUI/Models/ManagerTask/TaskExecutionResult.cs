namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class TaskExecutionResult
    {
        public string TaskId { get; set; }

        public string Outcome { get; set; }
        public string Comments { get; set; }
    }
}