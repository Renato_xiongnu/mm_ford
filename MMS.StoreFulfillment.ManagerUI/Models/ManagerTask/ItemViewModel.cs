﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class ItemViewModel
    {
        public ItemViewModel()
        {
        }

        public ItemViewModel(Item item)
        {
            Id = item.Id;
            Title = item.Title;
            Article = item.Article;
            Price = item.Price;
            Qty = item.Qty;
            SerialNumber = item.SerialNumber;
            ArticleProductType = item.ArticleProductType;
            ReserveStatus = item.ReserveStatus;
            ProductGroupName = item.ProductGroupName;
            ProductGroupNo = item.ProductGroupNo;
            DepartmentNumber = item.DepartmentNumber;
            HasShippedFromStock = item.HasShippedFromStock;
            BrandTitle = item.BrandTitle;
            StorageCell = item.StorageCell;
        }

        public int Id { get; set; }

        public string Title { get; set; }
        public long Article { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public IList<ItemViewModel> SubItems { get; set; }
        public string SerialNumber { get; set; }
        public string ArticleProductType { get; set; }

        public IList<SelectListItem> ItemAnswers { get; set; }
        //public string ItemResult { get { return ItemAnswers.First(m => m.Selected).Value; } }
        public string ReserveStatus { get; set; }
        public string ProductGroupName { get; set; }
        public int ProductGroupNo { get; set; }
        public int DepartmentNumber { get; set; }

        public bool IsReadOnly { get; set; }

        public bool HasShippedFromStock { get; set; }

        public string BrandTitle { get; set; }

        public string StorageCell { get; set; }
    }
}