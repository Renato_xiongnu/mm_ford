using System.Collections.Generic;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class Outcome
    {
        public string Value { get; set; }
        public ICollection<RequiredField> RequiredFields { get; set; }
    }
}