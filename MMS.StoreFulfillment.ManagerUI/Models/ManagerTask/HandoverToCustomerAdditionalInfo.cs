﻿using System;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class HandoverToCustomerAdditionalInfo:IAdditionalnfo
    {
        public string PaymentType { get; set; }

        public string CustomerName { get; set; }

        public string Phone { get; set; }

        public DateTime? ExpirationDate { get; set; }
    }
}