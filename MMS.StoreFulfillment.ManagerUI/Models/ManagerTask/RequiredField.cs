using System;
using System.Collections.Generic;

namespace MMS.StoreFulfillment.ManagerUI.Models.ManagerTask
{
    public class RequiredField
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string StringType { get; set; }
        public string DefaultValue { get; set; }
        public ICollection<string> PredefinedValues { get; set; }

        public object GetConvertedValue()
        {
            if (string.IsNullOrEmpty(Value))
            {
                return null;
            }
            var actualType = Type.GetType(StringType);
            if (actualType == typeof(DateTime))
            {
                return DateTime.Parse(Value).ToUniversalTime();
            }
            if (actualType == typeof(TimeSpan))
            {
                return TimeSpan.Parse(Value);
            }
            if (actualType == typeof(Boolean))
            {
                return Boolean.Parse(Value);
            }
            return Value;

        }
    }
}