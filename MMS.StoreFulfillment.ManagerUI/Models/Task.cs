﻿using System;

namespace MMS.StoreFulfillment.ManagerUI.Models
{
    public class Task
    {
        public int TaskId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EscalatingAt { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsAssignedOnMe { get; set; }
        public string AssignedOn { get; set; }
        public string Description { get; set; }

        public string WorkItemId { get; set; }

        public string OrderId { get; set; }
        public string WWSOrderId { get; set; }
        public string SapCode { get; set; }
        public bool Escalated { get; set; }
        public TaskType TaskType { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string CustomerPhone { get; set; }
        public string FullName { get; set; }
    }

    public enum TaskType
    {
        Unfinished,
        Handover,
        Delivery
    }
}