﻿namespace MMS.StoreFulfillment.ManagerUI.Models
{
    public class Store
    {
        public string SapCode { get; set; }

        public string Name { get; set; }
    }
}