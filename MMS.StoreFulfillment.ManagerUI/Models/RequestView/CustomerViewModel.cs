﻿using System.ComponentModel.DataAnnotations;

namespace MMS.StoreFulfillment.ManagerUI.Models.RequestView
{
    public class CustomerViewModel
    {
        [Display(Name = "Фамилия")]
        public string CustomerSurname { get; set; }
        [Display(Name = "Имя")]
        public string CustomerName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Телефон")]
        public string Phone { get; set; }
    }
}