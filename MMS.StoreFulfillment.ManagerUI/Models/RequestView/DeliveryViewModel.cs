﻿using System.ComponentModel.DataAnnotations;

namespace MMS.StoreFulfillment.ManagerUI.Models.RequestView
{
    public class DeliveryViewModel
    {
        [Display(Name = "Тип доставки")]
        public bool HasDelivery { get; set; }
        [Display(Name="Магазин")]
        public string SapCode { get; set; }
    }
}