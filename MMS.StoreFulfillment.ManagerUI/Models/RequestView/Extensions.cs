﻿using System.Linq;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;

namespace MMS.StoreFulfillment.ManagerUI.Models.RequestView
{
    public static class Extensions
    {
        public static RequestViewModel ToRequestViewModel(this ItemsMovementRequest request)
        {
            return new RequestViewModel
            {
                OrderId = request.CustomerOrder.CustomerOrderId,
                Items = request.Items.Select(t => new ItemViewModel(t)).ToList(),
                CustomerInfo = new CustomerViewModel
                {
                    CustomerName = request.Contact.FirstName,
                    CustomerSurname = request.Contact.LastName,
                    Email = request.Contact.Email,
                    Phone = request.Contact.Phone
                },
                DeliveryInfo = new DeliveryViewModel
                {
                    HasDelivery = request.CustomerOrder.DeliveryType == DeliveryType.Delivery,
                    SapCode = request.SapCode,
                },
                WWSOrderId = request.WwsOrderId
            };
        }
    }
}