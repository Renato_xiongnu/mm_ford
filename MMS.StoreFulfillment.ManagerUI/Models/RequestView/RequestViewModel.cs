﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;

namespace MMS.StoreFulfillment.ManagerUI.Models.RequestView
{
    public class RequestViewModel
    {
        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }
        [Display(Name = "Номер резерва")]
        public string WWSOrderId { get; set; }

        [UIHint("ItemViewModel")]
        public List<ItemViewModel> Items { get; set; }

        [UIHint("CustomerViewModel")]
        public CustomerViewModel CustomerInfo { get; set; }

        [UIHint("DeliveryViewModel")]
        public DeliveryViewModel DeliveryInfo { get; set; }

        [Display(Name = "Суммарно")]
        public decimal TotalPrice
        {
            get { return Items.Sum(t => t.Price * t.Qty); }
        }
    }
}