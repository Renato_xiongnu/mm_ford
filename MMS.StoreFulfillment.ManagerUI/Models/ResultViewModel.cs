﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MMS.StoreFulfillment.ManagerUI.Models
{
    public class ResultViewModel
    {
        public bool HasErrors { get; set; }

        public string Message { get; set; }
    }
}