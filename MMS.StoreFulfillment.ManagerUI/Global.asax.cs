﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MMS.StoreFulfillment.ManagerUI.Common;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;

namespace MMS.StoreFulfillment.ManagerUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var binder = new DateTimeModelBinder("dd.MM.yyyy");
            ModelBinders.Binders.Add(typeof(DateTime), binder);
            ModelBinders.Binders.Add(typeof(DateTime?), binder);

            AutofacConfig.RegisterContainer();

            //ModelBinders.Binders.Add(typeof(ItemMovementViewModel), new OrderModelBinder());
        }
    }
}
