﻿using System;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.ManagerUI.Models;

namespace MMS.StoreFulfillment.ManagerUI
{
    public static class MappingExtensions
    {
        public static Task ToTaskItem(this TaskListItem item, ItemsMovementRequest request)
        {
            var moscow = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

            return new Task
            {
                Name = item.Name,
                CreatedDate = TimeZoneInfo.ConvertTimeFromUtc(item.CreationDate, moscow),
                EscalatingAt =
                    TimeZoneInfo.ConvertTimeFromUtc(item.CreationDate + (item.MaxRunTime ?? new TimeSpan()), moscow),
                Url = item.Url,
                IsAssignedOnMe = item.IsAssignedOnMe,
                AssignedOn = item.AssignedOn,
                Escalated = item.Escalated,
                OrderId = request.CustomerOrder.CustomerOrderId,
                WWSOrderId = request.WwsOrderId,
                SapCode = request.SapCode,
                TaskType = request.GetTaskType(),
                DeliveryDate = request.Contact.RequestedDeliveryDate != null
                    ? TimeZoneInfo.ConvertTimeFromUtc(request.Contact.RequestedDeliveryDate.Value, moscow)
                    : (DateTime?) null,
                ExpirationDate = request.ExpirationDate != null
                    ? TimeZoneInfo.ConvertTimeFromUtc(request.ExpirationDate.Value, moscow)
                    : (DateTime?) null,
                CustomerPhone = request.Contact.Phone,
                FullName = string.Format("{0} {1}", request.Contact.FirstName, request.Contact.LastName)
            };
        }

        public static TaskType GetTaskType(this ItemsMovementRequest request)
        {
            var planStep = request.CurrentPlanStep();
            if (planStep == null)
            {
                return TaskType.Unfinished;
            }
            var planStepType = planStep.TicketTask.TaskType;
            if (planStepType == TicketTaskTypes.HandOverToCustomer)
            {
                return TaskType.Handover;
            }
            if (planStepType == TicketTaskTypes.DeliverOrder)
            {
                return TaskType.Delivery;
            }
            return TaskType.Unfinished;
        }

    }
}