﻿interface JQuery {
    dropdown(): JQuery;
    dropdown(command: string): JQuery;

    modal(): JQuery;
    modal(command: string): JQuery;
}