﻿function notFoundValidationRule() {
    $('#notFoundForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            mnf_article: { validators: {} },
            mnf_qty: {
                validators: {
                    regexp: {
                        regexp: numberPattern,
                        message: numberMessage
                    }
                }
            },
            mnf_price: {
                validators: {
                    regexp: {
                        regexp: pricePattern,
                        message: numberMessage
                    }
                }
            },
            mnf_displayItem: { validators: {} },
            mnf_displayItemCondition: { validators: {} },
            mnf_comment: { validators: {} }
        }
    });
}

function resDisplayItemValidationRule() {
    $('#reservedDisplayItemForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            mrdi_conditionStateComment: {
                validators: {
                    notEmpty: {
                        message: requiredMessage
                    }
                }
            },
            mrdi_correctPrice: {
                validators: {
                    notEmpty: {
                        message: requiredMessage
                    },
                    regexp: {
                        regexp: pricePattern,
                        message: numberMessage
                    }
                }
            },
            mrdi_correctQty: {
                validators: {
                    notEmpty: {
                        message: requiredMessage
                    },
                    regexp: {
                        regexp: numberPattern,
                        message: numberMessage
                    }
                }
            },
            mrdi_article: { validators: {} },
            mrdi_qty: {
                validators: {
                    regexp: {
                        regexp: numberPattern,
                        message: numberMessage
                    }
                }
            },
            mrdi_price: {
                validators: {
                    regexp: {
                        regexp: pricePattern,
                        message: numberMessage
                    }
                }
            },
            mrdi_displayItem: { validators: {} },
            mrdi_displayItemCondition: { validators: {} },
            mrdi_comment: { validators: {} }
        }
    });

    return $('#reservedDisplayItemForm').data('bootstrapValidator');
}

function reservedPartValidationRule() {
    $('#reservedPartForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            mrp_correctqty: {
                validators: {
                    notEmpty: {
                        message: requiredMessage
                    },
                    regexp: {
                        regexp: numberPattern,
                        message: numberMessage
                    }
                }
            },
            mrp_article: { validators: {} },
            mrp_qty: {
                validators: {
                    regexp: {
                        regexp: numberPattern,
                        message: numberMessage
                    }
                }
            },
            mrp_price: {
                validators: {
                    regexp: {
                        regexp: pricePattern,
                        message: numberMessage
                    }
                }
            },
            mrp_displayItem: { validators: {} },
            mrp_displayItemCondition: { validators: {} },
            mrp_comment: { validators: {} }
        }
    });
}

function incorrectPriceValidationRule() {
    $('#incorrectPriceForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            mip_correctPrice: {
                validators: {
                    notEmpty: {
                        message: requiredMessage
                    },
                    regexp: {
                        regexp: pricePattern,
                        message: numberMessage
                    }
                }
            },
            mip_comment: { validators: {} }
        }
    });
}