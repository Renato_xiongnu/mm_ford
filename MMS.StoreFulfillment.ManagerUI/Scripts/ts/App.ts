﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="./TaskStore.ts" />
/// <reference path="./TaskActions.ts" />
/// <reference path="./TaskComponents.ts" />

module StoreFulfillment {
    tasksActions.subscribe(tasksStore);

    var app = React.createFactory(Layout);

    tasksStore.subscribe(
        value => {
            React.render(app(value), document.getElementById('app'));
        },
        error => {
            console.log(error);
        }
    );
    tasksActions.reload.onNext({tasks:[]});
}