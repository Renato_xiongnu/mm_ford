var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="../typings/_references.d.ts" />
/// <reference path="./TaskStore.ts" />
/// <reference path="./TaskActions.ts" />
var StoreFulfillment;
(function (StoreFulfillment) {
    var d = React.DOM;
    var Layout = (function (_super) {
        __extends(Layout, _super);
        function Layout() {
            _super.apply(this, arguments);
        }
        Layout.prototype.render = function () {
            var storeSelectionFactory = React.createFactory(StoreSelector);
            var storeFulfillmentTasks = React.createFactory(StoreFulfillmentTasks);
            var taskTypeFactory = React.createFactory(TaskTypeTab);
            var refreshTaskFactory = React.createFactory(RefreshTasks);
            var searchFactory = React.createFactory(SearchBox);
            return d.div({ className: "container-fluid" }, d.div({ className: "" }, refreshTaskFactory({})), d.form({ className: "form-horizontal" }, d.div({ className: "form-group" }, d.label({ className: "col-sm-2 control-label" }, "Выберите магазин "), d.div({ className: "col-sm-5" }, storeSelectionFactory(this.props))), d.div({ className: "form-group" }, d.label({ className: "col-sm-2 control-label" }, "Поиск"), d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "OrderId", placeholder: "Введите номер заказ", value: "" })), d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "WWSOrderId", placeholder: "Введите номер резерва", value: "" })), d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "FullName", placeholder: "Введите имя покупателя", value: "" })), d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "CustomerPhone", placeholder: "Введите номер телефона", value: "", datamask: "'mask': '+7 (999) 999 99 99'" })))), d.div({ className: "empty" }), taskTypeFactory({
                TaskType: this.props.selectedTaskType,
                UnfinishedCount: this.props.tasks.filter(function (t) { return t.TaskType === StoreFulfillment.TaskType.Unfinished; }).map(function (t) { return t.TaskId; }).length,
                HandoverCount: this.props.tasks.filter(function (t) { return t.TaskType === StoreFulfillment.TaskType.Handover; }).map(function (t) { return t.TaskId; }).length,
                DeliveryCount: this.props.tasks.filter(function (t) { return t.TaskType === StoreFulfillment.TaskType.Delivery; }).map(function (t) { return t.TaskId; }).length
            }), storeFulfillmentTasks(this.props));
        };
        return Layout;
    })(React.Component);
    StoreFulfillment.Layout = Layout;
    var StoreFulfillmentTasks = (function (_super) {
        __extends(StoreFulfillmentTasks, _super);
        function StoreFulfillmentTasks(props) {
            _super.call(this, props);
            moment.locale("ru");
        }
        StoreFulfillmentTasks.prototype.render = function () {
            var selectedType = this.props.selectedTaskType;
            var filteredTasks = this.props.tasks.filter(function (t) { return t.TaskType === selectedType; });
            var filters = this.props.filters;
            for (var i in filters) {
                if (filters.hasOwnProperty(i)) {
                    var filt = filters[i];
                    filteredTasks = filteredTasks.filter(function (t) { return (t[filt.fieldName]).toString().indexOf(filt.value) !== -1; });
                }
            }
            var cx = React.addons.classSet;
            var rows = filteredTasks.map(function (row, index) {
                var classes = cx({
                    'label': true,
                    'label-success': row.IsAssignedOnMe,
                    'label-info': (!row.IsAssignedOnMe && row.AssignedOn !== null),
                    'label-default': (!row.IsAssignedOnMe && row.AssignedOn == null)
                });
                var isEscalatedClasses = cx({
                    'label label-warning': row.Escalated
                });
                return d.tr({ key: row.OrderId + index }, selectedType === StoreFulfillment.TaskType.Unfinished ?
                    d.td({ style: { width: "50px", textAlign: "center" } }, d.span({ className: classes }, row.AssignedOn == null ? "Свободна" : row.AssignedOn))
                    : null, d.td({ style: { width: "115px" } }, row.SapCode), d.td({}, d.a({ href: row.Url }, row.Name)), d.td({ style: { width: "115px" } }, row.OrderId), d.td({ style: { width: "115px" } }, row.WWSOrderId), selectedType === StoreFulfillment.TaskType.Unfinished ? d.td({ style: { width: "180px" } }, moment(row.CreatedDate).format("DD.MM.YYYY HH:mm:ss")) : null, selectedType === StoreFulfillment.TaskType.Unfinished ? d.td({ style: { width: "180px" } }, d.span({ className: isEscalatedClasses }, row.EscalatingAt ? moment(row.EscalatingAt).format("DD.MM.YYYY HH:mm:ss") : null)) : null, (selectedType === StoreFulfillment.TaskType.Handover || selectedType === StoreFulfillment.TaskType.Delivery) ? d.td({ style: { width: "120px" } }, row.DeliveryDate ? moment(row.DeliveryDate).format("DD.MM.YYYY") : null) : null, selectedType === StoreFulfillment.TaskType.Handover ? d.td({ style: { width: "120px" } }, row.ExpirationDate ? moment(row.ExpirationDate).format("DD.MM.YYYY HH:mm:ss") : null) : null);
            });
            var th = d.tr(null, selectedType === StoreFulfillment.TaskType.Unfinished ? d.th({}, "Исполнитель") : null, d.th({}, "Место продажи"), d.th({}, "Название"), d.th({}, "Номер интернет заказа"), d.th({}, "Номер резерва"), selectedType === StoreFulfillment.TaskType.Unfinished ? d.th({}, "Дата выставления (MSK)") : null, selectedType === StoreFulfillment.TaskType.Unfinished ? d.th({}, "Дата эскалации (MSK)") : null, selectedType === StoreFulfillment.TaskType.Handover ? d.th({}, "Дата самовывоза") : null, selectedType === StoreFulfillment.TaskType.Handover ? d.th({}, "Резерв действителен до") : null, selectedType === StoreFulfillment.TaskType.Delivery ? d.th({}, "Дата доставки") : null);
            return d.table({ className: "table table-striped table-bordered" }, d.thead(null, th), d.tbody(null, rows));
        };
        return StoreFulfillmentTasks;
    })(React.Component);
    StoreFulfillment.StoreFulfillmentTasks = StoreFulfillmentTasks;
    var DataTable = (function (_super) {
        __extends(DataTable, _super);
        function DataTable() {
            _super.apply(this, arguments);
        }
        DataTable.prototype.render = function () {
            var _this = this;
            var table = React.createFactory(BootstrapTable);
            var data = this.props.data || [];
            var keySelector = this.props.keySelector || (function (item, rowIndex) { return rowIndex; });
            var rows = data.map(function (item, rowIndex) {
                var cells = React.Children.map(_this.props.children, function (column) {
                    if (column !== null) {
                        var header = column;
                        var valueRender = header.props.valueRender || (function (item, column, rowIndex) { return item[column.props.dataField]; });
                        return (d.td(header.props, valueRender(item)));
                    }
                });
                return (d.tr({ key: keySelector(item, rowIndex) }, cells));
            });
            return (table(this.props, d.thead({}, this.props.children), d.tbody({}, rows)));
        };
        return DataTable;
    })(React.Component);
    StoreFulfillment.DataTable = DataTable;
    var BootstrapTable = (function (_super) {
        __extends(BootstrapTable, _super);
        function BootstrapTable() {
            _super.apply(this, arguments);
        }
        BootstrapTable.prototype.render = function () {
            var classList = React.addons.classSet({
                "table": true,
                "table-striped": this.props.striped,
                "table-bordered": this.props.bordered,
                "table-hover": this.props.hover
            });
            return (d.table({ className: classList }, this.props.children));
        };
        return BootstrapTable;
    })(React.Component);
    StoreFulfillment.BootstrapTable = BootstrapTable;
    var BootstrapTableColumn = (function (_super) {
        __extends(BootstrapTableColumn, _super);
        function BootstrapTableColumn() {
            _super.apply(this, arguments);
        }
        BootstrapTableColumn.prototype.render = function () {
            var classList = React.addons.classSet({});
            return (d.th({ className: classList }, this.props.children));
        };
        BootstrapTableColumn.propTypes = {};
        return BootstrapTableColumn;
    })(React.Component);
    StoreFulfillment.BootstrapTableColumn = BootstrapTableColumn;
    var StoreSelector = (function (_super) {
        __extends(StoreSelector, _super);
        function StoreSelector() {
            _super.apply(this, arguments);
        }
        StoreSelector.prototype.change = function (event) {
            var selectedVal = event.target.value;
            StoreFulfillment.tasksActions.getByStore.onNext({ SapCode: selectedVal, Name: '' });
        };
        StoreSelector.prototype.render = function () {
            var avaliableStores = this.props.availableStores;
            var avaliableOptions = avaliableStores.map(function (t) {
                return d.option({ key: t.SapCode, value: t.SapCode }, t.SapCode + " " + t.Name);
            });
            avaliableOptions.splice(0, 0, d.option({ value: "" }, "Все магазины"));
            return d.select({ className: "form-control", onChange: this.change }, avaliableOptions);
        };
        return StoreSelector;
    })(React.Component);
    StoreFulfillment.StoreSelector = StoreSelector;
    var TaskTypeTab = (function (_super) {
        __extends(TaskTypeTab, _super);
        function TaskTypeTab() {
            _super.apply(this, arguments);
        }
        TaskTypeTab.prototype.render = function () {
            var cx = React.addons.classSet;
            var classesUnfinished = cx({
                'active': this.props.TaskType === StoreFulfillment.TaskType.Unfinished,
            });
            var classesHandover = cx({
                'active': this.props.TaskType === StoreFulfillment.TaskType.Handover,
            });
            var classesDelivery = cx({
                'active': this.props.TaskType === StoreFulfillment.TaskType.Delivery,
            });
            return d.ul({ className: "nav nav-tabs" }, d.li({ className: classesUnfinished, onClick: function () { return StoreFulfillment.tasksActions.selectTaskType.onNext(StoreFulfillment.TaskType.Unfinished); } }, d.a({ href: "#" }, "Незавершенные задачи (" + this.props.UnfinishedCount + ")")), d.li({ className: classesHandover, onClick: function () { return StoreFulfillment.tasksActions.selectTaskType.onNext(StoreFulfillment.TaskType.Handover); } }, d.a({ href: "#" }, "Заказы на выдачу (" + this.props.HandoverCount + ")")), d.li({ className: classesDelivery, onClick: function () { return StoreFulfillment.tasksActions.selectTaskType.onNext(StoreFulfillment.TaskType.Delivery); } }, d.a({ href: "#" }, "Заказы на доставку (" + this.props.DeliveryCount + ")")));
        };
        return TaskTypeTab;
    })(React.Component);
    StoreFulfillment.TaskTypeTab = TaskTypeTab;
    var RefreshTasks = (function (_super) {
        __extends(RefreshTasks, _super);
        function RefreshTasks() {
            _super.apply(this, arguments);
        }
        RefreshTasks.prototype.render = function () {
            return d.button({ className: "btn btn-default", onClick: function () { return StoreFulfillment.tasksActions.reload.onNext({}); } }, d.span({ className: "glyphicon glyphicon-refresh" }));
        };
        return RefreshTasks;
    })(React.Component);
    StoreFulfillment.RefreshTasks = RefreshTasks;
    var SearchBox = (function (_super) {
        __extends(SearchBox, _super);
        function SearchBox() {
            _super.apply(this, arguments);
        }
        SearchBox.prototype.onKeyPress = function (event) {
            var unmasked = event.target.value.toString().replace(/_/g, '').trim();
            if (unmasked.indexOf(')') === unmasked.length - 1) {
                unmasked = unmasked.toString().substring(0, unmasked.length - 1); //TODO
            }
            StoreFulfillment.tasksActions.search.onNext({ fieldName: this.props.fieldName, value: unmasked, placeholder: '' });
        };
        SearchBox.prototype.componentDidMount = function () {
            var search = React.findDOMNode(this);
            $(search).inputmask();
        };
        SearchBox.prototype.render = function () {
            var _this = this;
            return d.input({
                className: "form-control",
                placeholder: this.props.placeholder,
                onKeyUp: function (event) { _this.onKeyPress(event); },
                "data-inputmask": this.props.datamask
            });
        };
        return SearchBox;
    })(React.Component);
    StoreFulfillment.SearchBox = SearchBox;
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=TaskComponents.js.map