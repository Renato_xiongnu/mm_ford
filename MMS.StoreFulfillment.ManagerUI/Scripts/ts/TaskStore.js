/// <reference path="../typings/_references.d.ts" />
var StoreFulfillment;
(function (StoreFulfillment) {
    (function (TaskType) {
        TaskType[TaskType["Unfinished"] = 0] = "Unfinished";
        TaskType[TaskType["Handover"] = 1] = "Handover";
        TaskType[TaskType["Delivery"] = 2] = "Delivery";
    })(StoreFulfillment.TaskType || (StoreFulfillment.TaskType = {}));
    var TaskType = StoreFulfillment.TaskType;
    var data = {
        tasks: [],
        availableStores: [],
        selectedStore: null,
        selectedTaskType: TaskType.Unfinished,
        filters: []
    };
    StoreFulfillment.tasksStore = new Rx.BehaviorSubject(data);
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=TaskStore.js.map