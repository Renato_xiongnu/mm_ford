﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="./TaskStore.ts" />
/// <reference path="./TaskActions.ts" />
module StoreFulfillment {
    var d = React.DOM;

    export class Layout extends React.Component<ITasksStore, any> {
        render() {
            var storeSelectionFactory = React.createFactory(StoreSelector);
            var storeFulfillmentTasks = React.createFactory(StoreFulfillmentTasks);
            var taskTypeFactory = React.createFactory(TaskTypeTab);
            var refreshTaskFactory = React.createFactory(RefreshTasks);
            var searchFactory = React.createFactory(SearchBox);


            return d.div({ className: "container-fluid" },
                d.div({ className: "" }, refreshTaskFactory({})),
                d.form({className:"form-horizontal"},
                d.div({ className: "form-group" },
                    d.label({ className: "col-sm-2 control-label" }, "Выберите магазин "),
                    d.div({ className: "col-sm-5" }, storeSelectionFactory(this.props))),
                d.div({ className: "form-group" },
                    d.label({ className: "col-sm-2 control-label" }, "Поиск"),
                    d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "OrderId", placeholder: "Введите номер заказ", value: "" })),
                    d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "WWSOrderId", placeholder: "Введите номер резерва", value: "" })),
                    d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "FullName", placeholder: "Введите имя покупателя", value: "" })),
                    d.div({ className: "col-sm-2" }, searchFactory({ fieldName: "CustomerPhone", placeholder: "Введите номер телефона", value: "", datamask: "'mask': '+7 (999) 999 99 99'" })))),
                d.div({ className: "empty" }),
                taskTypeFactory({
                    TaskType: this.props.selectedTaskType,
                    UnfinishedCount: this.props.tasks.filter(t => t.TaskType === TaskType.Unfinished).map(t => t.TaskId).length,
                    HandoverCount: this.props.tasks.filter(t => t.TaskType === TaskType.Handover).map(t => t.TaskId).length,
                    DeliveryCount: this.props.tasks.filter(t => t.TaskType === TaskType.Delivery).map(t => t.TaskId).length
                }),
                storeFulfillmentTasks(this.props));
        }
    }

    export class StoreFulfillmentTasks extends React.Component<ITasksStore, any> {
        constructor(props) {
            super(props);
            moment.locale("ru");
        }

        render() {
            var selectedType = this.props.selectedTaskType;
            var filteredTasks = this.props.tasks.filter((t) => t.TaskType === selectedType);
            var filters = this.props.filters;
            for (var i in filters) {
                if (filters.hasOwnProperty(i)) {
                    var filt = filters[i];
                    filteredTasks = filteredTasks.filter((t) => (t[filt.fieldName]).toString().indexOf(filt.value) !== -1);
                }
            }
            var cx = React.addons.classSet;
            var rows = filteredTasks.map((row, index) => {
                var classes = cx({
                    'label': true,
                    'label-success': row.IsAssignedOnMe,
                    'label-info': (!row.IsAssignedOnMe && row.AssignedOn !== null),
                    'label-default': (!row.IsAssignedOnMe && row.AssignedOn == null)
                });
                var isEscalatedClasses = cx({
                    'label label-warning': row.Escalated
                });
                return d.tr({ key: row.OrderId + index },
                    selectedType === TaskType.Unfinished ?
                    d.td({ style: { width: "50px", textAlign: "center" } }, d.span({ className: classes }, row.AssignedOn == null ? "Свободна" : row.AssignedOn))
                    : null,
                    d.td({ style: { width: "115px" } }, row.SapCode),
                    d.td({}, d.a({ href: row.Url }, row.Name)),
                    d.td({ style: { width: "115px" } }, row.OrderId),
                    d.td({ style: { width: "115px" } }, row.WWSOrderId),
                    selectedType === TaskType.Unfinished ? d.td({ style: { width: "180px" } }, moment(row.CreatedDate).format("DD.MM.YYYY HH:mm:ss")) : null,
                    selectedType === TaskType.Unfinished ? d.td({ style: { width: "180px" } }, d.span({ className: isEscalatedClasses }, row.EscalatingAt ? moment(row.EscalatingAt).format("DD.MM.YYYY HH:mm:ss") : null)) : null,
                    (selectedType === TaskType.Handover || selectedType === TaskType.Delivery) ? d.td({ style: { width: "120px" } }, row.DeliveryDate ? moment(row.DeliveryDate).format("DD.MM.YYYY") : null) : null,
                    selectedType === TaskType.Handover ? d.td({ style: { width: "120px" } }, row.ExpirationDate ? moment(row.ExpirationDate).format("DD.MM.YYYY HH:mm:ss") : null) : null);
            });
            var th = d.tr(null,
                selectedType === TaskType.Unfinished ? d.th({}, "Исполнитель") : null,
                d.th({}, "Место продажи"),
                d.th({}, "Название"),
                d.th({}, "Номер интернет заказа"),
                d.th({}, "Номер резерва"),
                selectedType === TaskType.Unfinished ? d.th({}, "Дата выставления (MSK)") : null,
                selectedType === TaskType.Unfinished ? d.th({}, "Дата эскалации (MSK)") : null,
                selectedType === TaskType.Handover ? d.th({}, "Дата самовывоза") : null,
                selectedType === TaskType.Handover ? d.th({}, "Резерв действителен до") : null,
                selectedType === TaskType.Delivery ? d.th({}, "Дата доставки") : null);

            return d.table({ className: "table table-striped table-bordered" }, d.thead(null, th), d.tbody(null, rows));
        }
    }

    export interface IDataTableProps<TData> extends ITableProps {
        data?: Array<TData>;
        keySelector?: any;
    }

    export class DataTable<TData> extends React.Component<IDataTableProps<TData>, any> {
        render() {
            var table = React.createFactory(BootstrapTable);

            var data = this.props.data || [];
            var keySelector = this.props.keySelector || ((item, rowIndex) => rowIndex);

            var rows = data.map((item, rowIndex) => {
                var cells = React.Children.map(this.props.children, (column) => {
                    if (column !== null) {
                        var header = (<React.ReactElement<any>>column);
                        var valueRender = header.props.valueRender || ((item, column, rowIndex) => item[column.props.dataField]);
                        return (
                            d.td(header.props, valueRender(item))
                        );
                    }
                });
                return (
                    d.tr({ key: keySelector(item, rowIndex) }, cells)
                );
            });
            return (
                table(this.props,
                    d.thead({}, this.props.children),
                    d.tbody({}, rows)
                )
            );
        }
    }

    export interface ITableProps {
        children?: Array<any>;
        striped?: boolean;
        bordered?: boolean;
        hover?: boolean;
    }

    export class BootstrapTable extends React.Component<ITableProps, any> {
        render() {
            var classList = React.addons.classSet({
                "table": true,
                "table-striped": this.props.striped,
                "table-bordered": this.props.bordered,
                "table-hover": this.props.hover
            });

            return (
                d.table({ className: classList }, this.props.children)
            );
        }
    }

    export class BootstrapTableColumn extends React.Component<any, any> {
        static propTypes = {
        
        }

        render() {
            var classList = React.addons.classSet({
            
            });

            return (
                d.th({ className: classList }, this.props.children)
            );
        }
    }

    export class StoreSelector extends React.Component<ITasksStore, any> {
        change(event) {
            var selectedVal = event.target.value;
            tasksActions.getByStore.onNext({ SapCode: selectedVal, Name: '' });
        }

        render() {
            var avaliableStores = this.props.availableStores;
            var avaliableOptions = avaliableStores.map(t => {
                return d.option({ key: t.SapCode, value: t.SapCode }, t.SapCode + " " + t.Name);
            });
            avaliableOptions.splice(0, 0, d.option({ value: "" }, "Все магазины"));
            return d.select({ className: "form-control", onChange: this.change }, avaliableOptions);
        }
    }

    export class TaskTypeTab extends React.Component<ITaskTypeHolder, any> {
        render() {
            var cx = React.addons.classSet;
            var classesUnfinished = cx({
                'active': this.props.TaskType === TaskType.Unfinished,
            });
            var classesHandover = cx({
                'active': this.props.TaskType === TaskType.Handover,
            });
            var classesDelivery = cx({
                'active': this.props.TaskType === TaskType.Delivery,
            });
            return d.ul({ className: "nav nav-tabs" },
                d.li({ className: classesUnfinished, onClick: () => tasksActions.selectTaskType.onNext(TaskType.Unfinished) }, d.a({ href: "#" }, "Незавершенные задачи (" + this.props.UnfinishedCount + ")")),
                d.li({ className: classesHandover, onClick: () => tasksActions.selectTaskType.onNext(TaskType.Handover) }, d.a({ href: "#" }, "Заказы на выдачу (" + this.props.HandoverCount + ")")),
                d.li({ className: classesDelivery, onClick: () => tasksActions.selectTaskType.onNext(TaskType.Delivery) }, d.a({ href: "#" }, "Заказы на доставку (" + this.props.DeliveryCount + ")")));
        }
    }

    export class RefreshTasks extends React.Component<any, any> {
        render() {
            return d.button({ className: "btn btn-default", onClick: () => tasksActions.reload.onNext({}) },
                d.span({ className: "glyphicon glyphicon-refresh" }));
        }
    }

    export class SearchBox extends React.Component<IFilter, any> {
        onKeyPress(event) {
            var unmasked = event.target.value.toString().replace(/_/g, '').trim();
            if (unmasked.indexOf(')') === unmasked.length-1) {
                unmasked = unmasked.toString().substring(0, unmasked.length - 1); //TODO
            }
            tasksActions.search.onNext({ fieldName: this.props.fieldName, value: unmasked, placeholder:''});
        }

        componentDidMount() {
            var search = React.findDOMNode(this);
            $(search).inputmask();
        }

        render() {
            return d.input({
                className: "form-control",
                placeholder: this.props.placeholder,
                onKeyUp: (event) => { this.onKeyPress(event); },
                "data-inputmask": this.props.datamask
            });
        }
    }
}
 