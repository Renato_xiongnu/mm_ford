/// <reference path="../typings/_references.d.ts" />
/// <reference path="./TaskStore.ts" />
/// <reference path="./TaskActions.ts" />
/// <reference path="./TaskComponents.ts" />
var StoreFulfillment;
(function (StoreFulfillment) {
    StoreFulfillment.tasksActions.subscribe(StoreFulfillment.tasksStore);
    var app = React.createFactory(StoreFulfillment.Layout);
    StoreFulfillment.tasksStore.subscribe(function (value) {
        React.render(app(value), document.getElementById('app'));
    }, function (error) {
        console.log(error);
    });
    StoreFulfillment.tasksActions.reload.onNext({ tasks: [] });
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=App.js.map