﻿/// <reference path="../typings/_references.d.ts" />

module StoreFulfillment {
    export interface ITask {
        TaskId: number;
        AssignedOn: string;
        IsAssignedOnMe: boolean;
        Name: string;
        SapCode: string;
        Url: string;
        OrderId: string;
        WWSOrderId: string;
        CreatedDate: string;
        EscalatingAt: string;
        Escalated: boolean;
        TaskType: TaskType;
        DeliveryDate: string;
        ExpirationDate: string;
    }

    export interface IStore {
        SapCode: string;
        Name: string;
    }

    export interface ITaskTypeHolder {
        TaskType: TaskType;
        UnfinishedCount: number;
        HandoverCount: number;
        DeliveryCount: number;
    }

    export enum TaskType { Unfinished, Handover, Delivery }

    export interface IFilter {
        fieldName: string;
        value: string;
        placeholder: string;
        datamask?: string;
    }

    export interface ITasksStore {
        tasks: Array<ITask>;
        availableStores: Array<IStore>;
        selectedStore: IStore;
        selectedTaskType: TaskType;
        filters: Array<IFilter>;
    }

    var data: ITasksStore = {
        tasks: [],
        availableStores: [],
        selectedStore: null,
        selectedTaskType: TaskType.Unfinished,
        filters: []
    }

    export var tasksStore = new Rx.BehaviorSubject<ITasksStore>(data);
}