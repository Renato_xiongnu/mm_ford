﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="./TaskStore.ts" />

module StoreFulfillment {
    function getMyTasks(id: string) {
        return $.ajax(
        {
            url: "./api/Tasks/" + id,
            dataType: 'json'
        });
    }

    function getMyStores() {
        return $.ajax(
        {
            url: "./api/Stores/",
            dataType: 'json'
        });
    }

    export var tasksActions = {
        reload: new Rx.Subject<any>(),
        getByStore: new Rx.Subject<IStore>(),
        selectTaskType: new Rx.Subject<TaskType>(),
        search: new Rx.Subject<IFilter>(),
        subscribe(store: Rx.BehaviorSubject<ITasksStore>) {
            this.reload
                .flatMapLatest(() => {
                    return Rx.Observable.fromPromise(getMyStores()).catch(e => Rx.Observable.empty());
                }).
                map(stores => {
                    var value = store.getValue();
                    value.availableStores = stores;
                    return value;
                })
                .map(() => {
                    return store.getValue().selectedStore;
                })
                .subscribe(this.getByStore);

            this.getByStore
                .flatMapLatest((t) => {
                    var value = store.getValue();
                    value.selectedStore = t !== null ? t : { SapCode: '' };
                    return Rx.Observable.fromPromise(getMyTasks(value.selectedStore.SapCode)).catch(e => Rx.Observable.empty());
                })
                .map(tasks => {
                    var value = store.getValue();
                    value.tasks = tasks;
                    return value;
                })
                .subscribe(store);

            this.selectTaskType
                .map(
                    selectedTaskType => {
                        var value = store.getValue();
                        value.selectedTaskType = selectedTaskType;
                        return value;
                    })
                .subscribe(store);

            this.search
                .debounce(100)
                .map(selectedFilter => {
                    var value = store.getValue();
                    var currentFilters = value.filters.filter((t) => t.fieldName === selectedFilter.fieldName);
                    if (currentFilters.length !== 0) {
                        currentFilters[0] = selectedFilter;
                    } else {
                        currentFilters.push(selectedFilter);
                    }

                    value.filters = currentFilters;

                    return value;
                })
                .subscribe(store);
        }
    }
} 