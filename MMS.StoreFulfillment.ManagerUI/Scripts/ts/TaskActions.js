/// <reference path="../typings/_references.d.ts" />
/// <reference path="./TaskStore.ts" />
var StoreFulfillment;
(function (StoreFulfillment) {
    function getMyTasks(id) {
        return $.ajax({
            url: "./api/Tasks/" + id,
            dataType: 'json'
        });
    }
    function getMyStores() {
        return $.ajax({
            url: "./api/Stores/",
            dataType: 'json'
        });
    }
    StoreFulfillment.tasksActions = {
        reload: new Rx.Subject(),
        getByStore: new Rx.Subject(),
        selectTaskType: new Rx.Subject(),
        search: new Rx.Subject(),
        subscribe: function (store) {
            this.reload
                .flatMapLatest(function () {
                return Rx.Observable.fromPromise(getMyStores()).catch(function (e) { return Rx.Observable.empty(); });
            }).
                map(function (stores) {
                var value = store.getValue();
                value.availableStores = stores;
                return value;
            })
                .map(function () {
                return store.getValue().selectedStore;
            })
                .subscribe(this.getByStore);
            this.getByStore
                .flatMapLatest(function (t) {
                var value = store.getValue();
                value.selectedStore = t !== null ? t : { SapCode: '' };
                return Rx.Observable.fromPromise(getMyTasks(value.selectedStore.SapCode)).catch(function (e) { return Rx.Observable.empty(); });
            })
                .map(function (tasks) {
                var value = store.getValue();
                value.tasks = tasks;
                return value;
            })
                .subscribe(store);
            this.selectTaskType
                .map(function (selectedTaskType) {
                var value = store.getValue();
                value.selectedTaskType = selectedTaskType;
                return value;
            })
                .subscribe(store);
            this.search
                .debounce(100)
                .map(function (selectedFilter) {
                var value = store.getValue();
                var currentFilters = value.filters.filter(function (t) { return t.fieldName === selectedFilter.fieldName; });
                if (currentFilters.length !== 0) {
                    currentFilters[0] = selectedFilter;
                }
                else {
                    currentFilters.push(selectedFilter);
                }
                value.filters = currentFilters;
                return value;
            })
                .subscribe(store);
        }
    };
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=TaskActions.js.map