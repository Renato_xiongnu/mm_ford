/// <reference path="../typings/_references.d.ts" /> 
"use strict";
var StoreFulfillment;
(function (StoreFulfillment) {
    var data = {
        Task: StoreFulfillment.ViewModel.Task,
        Command: StoreFulfillment.ViewModel.MoveItemsCommand,
        OutcomeIndex: -1
    };
    StoreFulfillment.TaskStore = new Rx.BehaviorSubject(data);
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=OutcomeStore.js.map