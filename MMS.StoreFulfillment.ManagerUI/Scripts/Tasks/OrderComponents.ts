﻿/// <reference path="../typings/_references.d.ts" />
"use strict";

module StoreFulfillment {
    var d = React.DOM;

    export class OrderLinesTable extends React.Component<ICommandModel, any> {
        render() {
            const Line = React.createFactory(OrderLine);
            const showPrice = this.props.ShowPrice;
            const canEdit = this.props.CanEdit;
            const editMode = this.props.EditMode;

            var items = this.props.Items
                .filter(item => item.ArticleProductType === "Product" || item.ArticleProductType === "Set")
                .map((item, index) => Line({ key: item.Id, item: item, index: index, showPrice: showPrice, editMode: editMode, canEdit: canEdit }));

            var messageClass = React.addons.classSet({
                "alert": true,
                "alert-danger": this.props.HasErrors,
                "alert-success": !this.props.HasErrors
            });

            return (
                d.div({},
                    canEdit ? d.div({ className: "btn-group" },
                        this.props.EditMode ? d.button({
                            className: "btn btn-success", onClick: e => {
                                e.preventDefault();
                                OutcomeActions.updateOrder.onNext({});
                            }
                        }, "Сохранить") : null,
                        d.button({
                            className: "btn btn-primary", onClick: e => {
                                e.preventDefault();
                                OutcomeActions.editOrder.onNext(!this.props.EditMode);
                            }
                        }, this.props.EditMode ? "Отменить" : "Изменить заказ")
                        ) : null,
                    this.props.Message ? d.div({ className: messageClass }, this.props.Message) : null,
                    d.table({ className: "table table-bordered" },
                        d.thead({},
                            d.th({}, "Артикул"),
                            showPrice ? d.th({}, "Цена") : null,
                            d.th({}, "Кол-во")
                            ),
                        items
                        )
                    )
                );
        }
    }

    export interface IItemProps {
        index: number;
        item: IItemModel;
        showPrice?: boolean;
        editMode?: boolean;
        canEdit?: boolean;
    }

    export class OrderLine extends React.Component<IItemProps, any> {
        render() {
            const ReserveStatus = React.createFactory(ItemReserveStatusSelect);
            const Form = React.createFactory(SerialForm);

            var item = this.props.item;
            var index = this.props.index;
            const showPrice = this.props.showPrice;
            const editMode = this.props.editMode;
            const canEdit = this.props.canEdit;

            return d.tbody({},
                d.tr({},
                    d.td({ colSpan: 3 }, (item.BrandTitle || "") + " " + (item.Title || "[название не известно]"))
                    ),
                d.tr({},
                    d.td({}, item.Article),
                    showPrice ? d.td({}, item.Price.toFixed(2)) : null,
                    d.td({}, item.Qty)
                    ),
                d.tr({},
                    d.td({ colSpan: 3 }, `Отдел: ${item.DepartmentNumber}, ТГ ${item.ProductGroupName || ""}(${item.ProductGroupNo})`)
                    ),
                d.tr({},
                    d.td({ colSpan: 3 }, ReserveStatus({ index: index, item: item }))
                    ),
                canEdit ? Form({ index: index, item: item, editMode: editMode }) : null,
                d.tr({ className: "success" },
                    d.td({ colSpan: 3 }, "")
                    )
                );
        }
    }


    export class ItemReserveStatusSelect extends React.Component<IItemProps, any> {
        updateReserveStatus(e) {
            e.preventDefault();
            var item = this.props.item;
            item.ReserveStatus = (<HTMLInputElement>e.target).value;
            OutcomeActions.updateItem.onNext(item);
        }
        updateStorageCell(e) {
            if (e.keyCode === 10 || e.keyCode === 13) {
                e.preventDefault();
            }
            var item = this.props.item;
            item.StorageCell = (<HTMLInputElement>e.target).value;
            OutcomeActions.updateItem.onNext(item);
        }
        render() {
            var item = this.props.item;
            var currentAnswer = item.ItemAnswers.filter(a => a.Value === item.ReserveStatus)[0];

            var options = item.ItemAnswers
                .filter(a => !a.Disabled)
                .map(a => {
                return d.option({ key: a.Value, value: a.Value, disabled: a.Disabled }, a.Text);
            });
            options.splice(0, 0, d.option({ key: "NoValue", value: "" }, "Выберите ответ"));

            return (
                item.IsReadOnly || item.ItemAnswers.length === 0
                    ? d.span({}, `Cтатус резерва: ${currentAnswer ? currentAnswer.Text : item.ReserveStatus}; Ячейка: ${item.StorageCell || "[не задано]"}`)
                    : d.div({},
                        d.div({ className: "form-group" },
                            d.label({}, "Статус резерва "),
                            d.select({ className: "form-control required", onChange: this.updateReserveStatus.bind(this) }, options)
                            ),

                        d.div({ className: "form-group" },
                            d.label({}, "Ячейка "),
                            d.input({ type: "text", className: "form-control", onInput: this.updateStorageCell.bind(this), onChange: this.updateStorageCell.bind(this), onKeyDown: this.updateStorageCell.bind(this) })
                            )
                        )
                );
        }
    }

    export class SerialForm extends React.Component<IItemProps, any> {
        updateSerialNumber(e) {
            var item = this.props.item;
            item.SerialNumber = (<HTMLInputElement>e.target).value;
            OutcomeActions.updateItem.onNext(item);
        }
        updateHasShippedFromStock(e) {
            var item = this.props.item;
            item.HasShippedFromStock = (<HTMLInputElement>e.target).checked;
            OutcomeActions.updateItem.onNext(item);
        }
        render() {
            var item = this.props.item;
            var editMode = this.props.editMode;

            return (
                d.tr({},
                    editMode && item.Qty === 1
                        ? d.td({ colSpan: 3 },
                            d.div({ className: "form-group" },
                                d.label({}, "Серийный номер "),
                                d.input({ type: "text", className: "form-control required", value: item.SerialNumber, onInput: this.updateSerialNumber.bind(this) })
                                ),
                            d.div({ className: "checkbox" },
                                d.label({},
                                    d.input({ type: "checkbox", className: "required", checked: item.HasShippedFromStock, onChange: this.updateHasShippedFromStock.bind(this) }),
                                    "Выдача со склада"
                                    )
                                )
                            )
                        : d.td({ colSpan: 3 }, `Серийный номер: ${item.SerialNumber || "[не задано]"}; Выдача со склада: ${item.HasShippedFromStock ? "да" : "нет" }`)
                    )
                );
        }
    }
}