﻿/// <reference path="../typings/_references.d.ts" /> 
"use strict";

module StoreFulfillment {
    export interface ITaskStore {
        Task: ITaskModel;
        Command: ICommandModel;
        OutcomeIndex: number;
    }
    
    export interface IViewModel {
        Task: ITaskModel;
        MoveItemsCommand: ICommandModel;
    }

    export interface ITaskModel extends ITask {
        Outcomes: ITaskOutcome[];
    }

    export interface ICommandModel {
        RequestId: string;
        Items: IItemModel[];
        HideOutcomes: boolean;
        ShowPrice: boolean;
        CanEdit: boolean;
        EditMode?: boolean;
        Message?: string;
        HasErrors?: boolean;
    }

    export interface IItemModel {
        Id: number;
        ItemExternalId: number;
        Title: string;
        Article: number;
        Price: number;
        Qty: number;
        SubItems: IItemModel[],
        SerialNumber: string;
        ReserveStatus: string;
        ArticleProductType: string;
        ItemAnswers: IItemAnswer[];
        ProductGroupName: string;
        ProductGroupNo: number;
        DepartmentNumber: number;
        IsReadOnly: boolean;
        HasShippedFromStock: boolean;
        BrandTitle: string;
        StorageCell: string;
    }

    export interface IItemAnswer {
        Disabled: boolean;
        Group: string;
        Selected: boolean;
        Text: string;
        Value: string;
    }

    export interface ITaskOutcome {
        Value: string;
        RequiredFields: ITaskField[];
    }

    export interface ITaskField {
        Name: string;
        StringType: string;
        Value: string;
        DefaultValue: string;
        ErrorMessage: string;
        PredefinedValues: string[];
    }

    export interface IResult {
        HasErrors: boolean;
        Message: string;
    }

    export interface ICompleteTask {
        taskId: string;
        requestId: string;
        outcome: ITaskOutcome;
        comments: string;
        items: IItemModel[];
    }

    export interface IUpdateOrder {
        taskId: string;
        requestId: string;
        items: IItemModel[];
    }

    export interface IFieldValue {
        index: number;
        value: string;
    }

    export declare var RootUrl: string;
    export declare var ViewModel: IViewModel;

    var data: ITaskStore = {
        Task: ViewModel.Task,
        Command: ViewModel.MoveItemsCommand,
        OutcomeIndex: -1
    };

    export var TaskStore = new Rx.BehaviorSubject<ITaskStore>(data);
}