/// <reference path="../typings/_references.d.ts" />
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var StoreFulfillment;
(function (StoreFulfillment) {
    var d = React.DOM;
    var OrderLinesTable = (function (_super) {
        __extends(OrderLinesTable, _super);
        function OrderLinesTable() {
            _super.apply(this, arguments);
        }
        OrderLinesTable.prototype.render = function () {
            var _this = this;
            var Line = React.createFactory(OrderLine);
            var showPrice = this.props.ShowPrice;
            var canEdit = this.props.CanEdit;
            var editMode = this.props.EditMode;
            var items = this.props.Items
                .filter(function (item) { return item.ArticleProductType === "Product" || item.ArticleProductType === "Set"; })
                .map(function (item, index) { return Line({ key: item.Id, item: item, index: index, showPrice: showPrice, editMode: editMode, canEdit: canEdit }); });
            var messageClass = React.addons.classSet({
                "alert": true,
                "alert-danger": this.props.HasErrors,
                "alert-success": !this.props.HasErrors
            });
            return (d.div({}, canEdit ? d.div({ className: "btn-group" }, this.props.EditMode ? d.button({
                className: "btn btn-success", onClick: function (e) {
                    e.preventDefault();
                    StoreFulfillment.OutcomeActions.updateOrder.onNext({});
                }
            }, "Сохранить") : null, d.button({
                className: "btn btn-primary", onClick: function (e) {
                    e.preventDefault();
                    StoreFulfillment.OutcomeActions.editOrder.onNext(!_this.props.EditMode);
                }
            }, this.props.EditMode ? "Отменить" : "Изменить заказ")) : null, this.props.Message ? d.div({ className: messageClass }, this.props.Message) : null, d.table({ className: "table table-bordered" }, d.thead({}, d.th({}, "Артикул"), showPrice ? d.th({}, "Цена") : null, d.th({}, "Кол-во")), items)));
        };
        return OrderLinesTable;
    })(React.Component);
    StoreFulfillment.OrderLinesTable = OrderLinesTable;
    var OrderLine = (function (_super) {
        __extends(OrderLine, _super);
        function OrderLine() {
            _super.apply(this, arguments);
        }
        OrderLine.prototype.render = function () {
            var ReserveStatus = React.createFactory(ItemReserveStatusSelect);
            var Form = React.createFactory(SerialForm);
            var item = this.props.item;
            var index = this.props.index;
            var showPrice = this.props.showPrice;
            var editMode = this.props.editMode;
            var canEdit = this.props.canEdit;
            return d.tbody({}, d.tr({}, d.td({ colSpan: 3 }, (item.BrandTitle || "") + " " + (item.Title || "[название не известно]"))), d.tr({}, d.td({}, item.Article), showPrice ? d.td({}, item.Price.toFixed(2)) : null, d.td({}, item.Qty)), d.tr({}, d.td({ colSpan: 3 }, "\u041E\u0442\u0434\u0435\u043B: " + item.DepartmentNumber + ", \u0422\u0413 " + (item.ProductGroupName || "") + "(" + item.ProductGroupNo + ")")), d.tr({}, d.td({ colSpan: 3 }, ReserveStatus({ index: index, item: item }))), canEdit ? Form({ index: index, item: item, editMode: editMode }) : null, d.tr({ className: "success" }, d.td({ colSpan: 3 }, "")));
        };
        return OrderLine;
    })(React.Component);
    StoreFulfillment.OrderLine = OrderLine;
    var ItemReserveStatusSelect = (function (_super) {
        __extends(ItemReserveStatusSelect, _super);
        function ItemReserveStatusSelect() {
            _super.apply(this, arguments);
        }
        ItemReserveStatusSelect.prototype.updateReserveStatus = function (e) {
            e.preventDefault();
            var item = this.props.item;
            item.ReserveStatus = e.target.value;
            StoreFulfillment.OutcomeActions.updateItem.onNext(item);
        };
        ItemReserveStatusSelect.prototype.updateStorageCell = function (e) {
            if (e.keyCode === 10 || e.keyCode === 13) {
                e.preventDefault();
            }
            var item = this.props.item;
            item.StorageCell = e.target.value;
            StoreFulfillment.OutcomeActions.updateItem.onNext(item);
        };
        ItemReserveStatusSelect.prototype.render = function () {
            var item = this.props.item;
            var currentAnswer = item.ItemAnswers.filter(function (a) { return a.Value === item.ReserveStatus; })[0];
            var options = item.ItemAnswers
                .filter(function (a) { return !a.Disabled; })
                .map(function (a) {
                return d.option({ key: a.Value, value: a.Value, disabled: a.Disabled }, a.Text);
            });
            options.splice(0, 0, d.option({ key: "NoValue", value: "" }, "Выберите ответ"));
            return (item.IsReadOnly || item.ItemAnswers.length === 0
                ? d.span({}, "C\u0442\u0430\u0442\u0443\u0441 \u0440\u0435\u0437\u0435\u0440\u0432\u0430: " + (currentAnswer ? currentAnswer.Text : item.ReserveStatus) + "; \u042F\u0447\u0435\u0439\u043A\u0430: " + (item.StorageCell || "[не задано]"))
                : d.div({}, d.div({ className: "form-group" }, d.label({}, "Статус резерва "), d.select({ className: "form-control required", onChange: this.updateReserveStatus.bind(this) }, options)), d.div({ className: "form-group" }, d.label({}, "Ячейка "), d.input({ type: "text", className: "form-control", onInput: this.updateStorageCell.bind(this), onChange: this.updateStorageCell.bind(this), onKeyDown: this.updateStorageCell.bind(this) }))));
        };
        return ItemReserveStatusSelect;
    })(React.Component);
    StoreFulfillment.ItemReserveStatusSelect = ItemReserveStatusSelect;
    var SerialForm = (function (_super) {
        __extends(SerialForm, _super);
        function SerialForm() {
            _super.apply(this, arguments);
        }
        SerialForm.prototype.updateSerialNumber = function (e) {
            var item = this.props.item;
            item.SerialNumber = e.target.value;
            StoreFulfillment.OutcomeActions.updateItem.onNext(item);
        };
        SerialForm.prototype.updateHasShippedFromStock = function (e) {
            var item = this.props.item;
            item.HasShippedFromStock = e.target.checked;
            StoreFulfillment.OutcomeActions.updateItem.onNext(item);
        };
        SerialForm.prototype.render = function () {
            var item = this.props.item;
            var editMode = this.props.editMode;
            return (d.tr({}, editMode && item.Qty === 1
                ? d.td({ colSpan: 3 }, d.div({ className: "form-group" }, d.label({}, "Серийный номер "), d.input({ type: "text", className: "form-control required", value: item.SerialNumber, onInput: this.updateSerialNumber.bind(this) })), d.div({ className: "checkbox" }, d.label({}, d.input({ type: "checkbox", className: "required", checked: item.HasShippedFromStock, onChange: this.updateHasShippedFromStock.bind(this) }), "Выдача со склада")))
                : d.td({ colSpan: 3 }, "\u0421\u0435\u0440\u0438\u0439\u043D\u044B\u0439 \u043D\u043E\u043C\u0435\u0440: " + (item.SerialNumber || "[не задано]") + "; \u0412\u044B\u0434\u0430\u0447\u0430 \u0441\u043E \u0441\u043A\u043B\u0430\u0434\u0430: " + (item.HasShippedFromStock ? "да" : "нет"))));
        };
        return SerialForm;
    })(React.Component);
    StoreFulfillment.SerialForm = SerialForm;
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=OrderComponents.js.map