/// <reference path="../typings/_references.d.ts" /> 
"use strict";
var StoreFulfillment;
(function (StoreFulfillment) {
    function CompleteTask(data) {
        return $.ajax({
            url: StoreFulfillment.RootUrl + "/ManagerTask/CompleteTask/",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        });
    }
    function UpdateOrder(data) {
        return $.ajax({
            url: StoreFulfillment.RootUrl + "/ManagerTask/UpdateOrder/",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        });
    }
    function updateItemsFormData(items) {
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            console.log("Item", item);
            var els = document.getElementsByName("[" + i + "].ItemAnswers");
            if (els.length > 0) {
                var el = els[0];
                console.log("el.value", el.value);
                item.ReserveStatus = el.value;
            }
        }
    }
    StoreFulfillment.updateItemsFormData = updateItemsFormData;
    var pleaseWaitDiv = $('<div class="modal js-loading-bar"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h3>Сохранение...</h3></div><div class="modal-body"><div class="progress progress-popup progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div></div></div></div>');
    function getFormComments() {
        var el = document.getElementById("comments");
        return el ? el.value : null;
    }
    StoreFulfillment.getFormComments = getFormComments;
    StoreFulfillment.OutcomeActions = {
        setOutcome: new Rx.Subject(),
        setFieldValue: new Rx.Subject(),
        completeTask: new Rx.Subject(),
        editOrder: new Rx.Subject(),
        updateOrder: new Rx.Subject(),
        updateItem: new Rx.Subject(),
        showResult: new Rx.Subject(),
    };
    function OutcomeActionsSubscribe(store) {
        StoreFulfillment.OutcomeActions.setOutcome
            .map(function (index) {
            var value = store.getValue();
            value.OutcomeIndex = index;
            return value;
        })
            .subscribe(store);
        StoreFulfillment.OutcomeActions.setFieldValue
            .map(function (v) {
            var value = store.getValue();
            var outcome = value.Task.Outcomes[value.OutcomeIndex];
            var field = outcome.RequiredFields[v.index];
            field.Value = v.value;
            return value;
        })
            .subscribe(store);
        StoreFulfillment.OutcomeActions.editOrder
            .map(function (b) {
            var value = store.getValue();
            value.Command.EditMode = b;
            return value;
        })
            .subscribe(store);
        StoreFulfillment.OutcomeActions.updateItem
            .map(function (v) {
            var value = store.getValue();
            var items = value.Command.Items;
            var item = items.filter(function (i) { return i.Id === v.Id; })[0];
            item.ReserveStatus = v.ReserveStatus;
            item.SerialNumber = v.SerialNumber;
            return value;
        })
            .subscribe(store);
        StoreFulfillment.OutcomeActions.showResult
            .map(function (r) {
            var value = store.getValue();
            value.Command.Message = r.Message;
            value.Command.HasErrors = r.HasErrors;
            return value;
        })
            .subscribe(store);
        StoreFulfillment.OutcomeActions.updateOrder
            .map(function () {
            var value = store.getValue();
            return {
                taskId: value.Task.TaskId.toString(),
                requestId: StoreFulfillment.ViewModel.MoveItemsCommand.RequestId,
                items: StoreFulfillment.ViewModel.MoveItemsCommand.Items
            };
        })
            .flatMapLatest(function (data) {
            console.log("OutcomeActions.updateOrder: ", data);
            pleaseWaitDiv.modal("show");
            return Rx.Observable.fromPromise(UpdateOrder(data));
        })
            .subscribe(function (result) {
            pleaseWaitDiv.modal("hide");
            console.log("OutcomeActions.updateOrder Result: ", result);
            StoreFulfillment.OutcomeActions.showResult.onNext(result);
            if (result != null && !result.HasErrors) {
                StoreFulfillment.OutcomeActions.editOrder.onNext(false);
            }
        }, function (e) {
            console.log(e);
            StoreFulfillment.OutcomeActions.showResult.onNext({ HasErrors: false, Message: e });
        });
        StoreFulfillment.OutcomeActions.completeTask
            .filter(function (outcome) {
            $('form#order-form').validate({ errorClass: "validation-error" });
            return $('form#order-form').valid();
        })
            .map(function (outcome) {
            var value = store.getValue();
            var comments = getFormComments();
            //updateItemsFormData(ViewModel.MoveItemsCommand.Items);
            return {
                taskId: value.Task.TaskId.toString(),
                requestId: StoreFulfillment.ViewModel.MoveItemsCommand.RequestId,
                outcome: outcome,
                comments: comments,
                items: StoreFulfillment.ViewModel.MoveItemsCommand.Items.filter(function (i) { return !i.IsReadOnly; })
            };
        })
            .flatMapLatest(function (data) {
            console.log("OutcomeActions.completeTask: ", data);
            pleaseWaitDiv.modal("show");
            return Rx.Observable.fromPromise(CompleteTask(data));
        })
            .subscribe(function (result) {
            console.log("OutcomeActions.completeTask Result: ", result);
            pleaseWaitDiv.modal("hide");
            if (result != null && result.HasErrors) {
                window.location.href = StoreFulfillment.RootUrl + "/Errors/ErrorWithMessage?message=" + result.Message;
            }
            else {
                window.location.href = StoreFulfillment.RootUrl + "/ManagerTask/Success";
            }
            //(<HTMLInputElement>document.getElementById("outcome")).value = JSON.stringify(o);
            //(<HTMLFormElement>document.getElementById("order-form")).submit();
        }, function (e) { return console.log(e); });
    }
    StoreFulfillment.OutcomeActionsSubscribe = OutcomeActionsSubscribe;
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=OutcomeActions.js.map