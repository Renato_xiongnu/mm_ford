/// <reference path="../typings/_references.d.ts" /> 
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var StoreFulfillment;
(function (StoreFulfillment) {
    var d = React.DOM;
    var App = (function (_super) {
        __extends(App, _super);
        function App() {
            _super.apply(this, arguments);
        }
        App.prototype.render = function () {
            var OrderLines = React.createFactory(StoreFulfillment.OrderLinesTable);
            var View = React.createFactory(StoreFulfillment.TaskView);
            return (d.div({ className: "col-sm-8" }, OrderLines(this.props.Command), this.props.Command.HideOutcomes ? null : View(this.props)));
        };
        return App;
    })(React.Component);
    StoreFulfillment.App = App;
    StoreFulfillment.OutcomeActionsSubscribe(StoreFulfillment.TaskStore);
    var app = React.createFactory(App);
    StoreFulfillment.TaskStore.subscribe(function (value) {
        React.render(app(value), document.getElementById('outcomes'));
    }, function (error) {
        console.log(error);
    });
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=OutcomeApp.js.map