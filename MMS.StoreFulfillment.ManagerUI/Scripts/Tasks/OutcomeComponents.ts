﻿/// <reference path="../typings/_references.d.ts" /> 
"use strict";

module StoreFulfillment {
    var d = React.DOM;

    export interface IOutcomeProps {
        index: number;
        outcome: ITaskOutcome;
        showFields: boolean;
    }

    export interface IFieldProps {
        index: number;
        field: ITaskField;
        outcome: ITaskOutcome;
    }

    export class TaskView extends React.Component<ITaskStore, any> {
        render() {
            const TaskButton = React.createFactory(TaskOutcomeButton);

            var task = this.props.Task;
            var outcomeIndex = this.props.OutcomeIndex;
            if (typeof task === "undefined" || task === null) {
                return null;
            }
            var buttons = task.Outcomes.map((o, index) => TaskButton({ key: index, index: index, outcome: o, showFields: index === outcomeIndex }));
            return (
                d.div({ className: "tt-task-view" },
                    buttons
                    )
                );
        }
    };

    export class TaskOutcomeButton extends React.Component<IOutcomeProps, any> {

        clickButton(e) {
            var index = this.props.index;
            var outcome = this.props.outcome;
            var hasFields = outcome.RequiredFields.length !== 0;

            return hasFields ? OutcomeActions.setOutcome.onNext(index) : OutcomeActions.completeTask.onNext(outcome);
        }

        render() {
            const Outcome = React.createFactory(TaskOutcome);

            var index = this.props.index;
            var outcome = this.props.outcome;
            var showFields = this.props.showFields;
            var hasFields = outcome.RequiredFields.length !== 0;
            var btnClass = React.addons.classSet({
                "btn": true,
                "btn-large": true,
                "btn-block": true,
                "btn-primary": !hasFields,
                "btn-info": hasFields,
            });
            var text = !hasFields ? outcome.Value : outcome.Value + " (заполнить полей: " + outcome.RequiredFields.length + ")";
            return (
                d.div({},
                    d.a({ className: btnClass, onClick: this.clickButton.bind(this) }, text),
                    showFields ? d.div({ className: "row", style: { padding: "15px;" } }, Outcome({ outcome: outcome })) : null
                    )
                );
        }
    };

    export class TaskOutcome extends React.Component<IOutcomeProps, any> {
        constructor(props) {
            super(props);
            moment.locale("ru");

            props.outcome.RequiredFields.forEach((field: ITaskField) => {
                field.Value = field.Value === null ? field.DefaultValue : field.Value;
            });
        }

        validate() {
            return this.props.outcome.RequiredFields.every(f => {
                if (f.StringType === "System.DateTime" && f.Value !== null && f.Value !== "") {
                    var m = moment(f.Value);
                    if (m.isValid() && m.isAfter(moment().format("YYYY-MM-DD"))) {
                        f.ErrorMessage = null;
                        return true;
                    }
                    f.ErrorMessage = "Дата должна быть больше или равна сегодняшней";
                    return false;
                }
                return (f.Value !== null && f.Value !== "");
            });
        }

        render() {
            var outcome = this.props.outcome;
            const TextField = React.createFactory(TaskTextField);
            const ChoiceField = React.createFactory(TaskChoiceField);
            const DateField = React.createFactory(TaskDateField);
            const BooleanField = React.createFactory(TaskBooleanField);
            const TimeSpanField = React.createFactory(TaskTimeSpanField);

            var fields = outcome.RequiredFields.map((field, index) => {
                field.Value = field.Value === null ? field.DefaultValue : field.Value;
                switch (field.StringType) {
                    case "Choice":
                        return ChoiceField({ key: index, index: index, field: field, outcome: outcome });
                    case "DateTime":
                    case "System.DateTime":
                        return DateField({ key: index, index: index, field: field, outcome: outcome });
                    case "Boolean":
                    case "System.Boolean":
                        return BooleanField({ key: index, index: index, field: field, outcome: outcome });
                    case "TimeSpan":
                    case "System.TimeSpan":
                        return TimeSpanField({ key: index, index: index, field: field, outcome: outcome });
                    case "System.String":
                    case "System.Int32":
                    case "System.Decimal":
                    case "System.Double":
                    default:
                        return TextField({ key: index, index: index, field: field, outcome: outcome });
                }
            });

            var btnClass = React.addons.classSet({
                "btn": true,
                "btn-large": true,
                "btn-block": true,
                "btn-primary": true
            });
            return (
                d.div({ className: "col-sm-12" },
                    d.div({ className: "form-horizontal" }, fields),
                    d.div({ className: "buttons" },
                        d.button({ className: btnClass, disabled: !this.validate(), onClick: (e) => {
                            e.preventDefault(); OutcomeActions.completeTask.onNext(outcome); } }, "Завершить")
                        )
                    )
                );
        }
    };

    export class TaskTextField extends React.Component<IFieldProps, any> {
        setValue(value: string) {
            var index = this.props.index;
            OutcomeActions.setFieldValue.onNext({ index: index, value: value });
        }

        render() {
            var field = this.props.field;

            return (
                d.div({ className: "form-group" },
                    d.label({ className: "col-sm-4 control-label" }, field.Name),
                    d.div({ className: "col-sm-8" },
                        d.input({ type: "text", className: "form-control", name: field.Name, value: field.Value, onChange: e => { this.setValue((<HTMLInputElement>e.target).value); } })
                        )
                    )
                );
        }
    };

    export class TaskChoiceField extends React.Component<IFieldProps, any> {
        setValue(value: string) {
            var index = this.props.index;
            OutcomeActions.setFieldValue.onNext({ index: index, value: value });
        }

        render() {
            var field = this.props.field;

            var values = field.PredefinedValues.map((s, index) => {
                return d.div({ key: index, className: "radio" },
                    d.label({},
                        d.input({ type: "radio", name: field.Name, checked: (s === field.Value), onChange: e => { if ((<HTMLInputElement>e.target).checked) { this.setValue(s); } } }),
                        s
                        )
                    );
            });
            return (
                d.div({ className: "form-group" },
                    d.label({ className: "col-sm-4 control-label" }, field.Name),
                    d.div({ className: "col-sm-8" },
                        values
                        )
                    )
                );
        }
    };

    export class TaskDateField extends React.Component<IFieldProps, any> {
        setValue(m: moment.Moment) {
            var index = this.props.index;
            var v = m && m.isValid() ? m.format("YYYY-MM-DDT00:00:00") + "Z" : null;
            OutcomeActions.setFieldValue.onNext({ index: index, value: v });
        }

        componentDidMount() {
            var el = React.findDOMNode(this.refs["datepicker"]);

            var field = this.props.field;
            var moment1 = moment(field.Value, "DD.MM.YYYY HH:mm:ss");
            var dateTime = moment1.isValid() ? moment1.toDate() : null;

            $(el).datetimepicker({ format: "DD.MM.YYYY", locale: "ru", defaultDate: dateTime });
            $(el).on("dp.change", (e) => {
                var m = (<DateTimePickerEvent>e).date;
                this.setValue(m);
            });
        }

        render() {
            var field = this.props.field;            

            return (
                d.div({ className: "form-group" },
                    d.label({ className: "col-sm-4 control-label" }, field.Name),
                    d.div({ className: "col-sm-8" },
                        d.div({ className: "input-group date", ref: "datepicker" },
                            d.input({ type: "text", className: "form-control datepicker", name: field.Name }),
                            d.span({ className: "input-group-addon" }, d.span({ className: "glyphicon glyphicon-calendar" }))
                            ),
                            field.ErrorMessage ? d.label({ className: "validation-error" }, field.ErrorMessage) : null
                        )
                    )
                );
        }

        componentWillUnmount() {
            var el = React.findDOMNode(this.refs["datepicker"]);
            $(el).data("DateTimePicker").destroy();
        }
    };

    export class TaskBooleanField extends React.Component<IFieldProps, any> {
        setValue(value: boolean) {
            var index = this.props.index;
            OutcomeActions.setFieldValue.onNext({ index: index, value: value.toString() });
        }

        render() {
            var field = this.props.field;
            return (
                d.div({ className: "form-group" },
                    d.div({ className: "col-sm-offset-4 col-sm-8" },
                        d.div({ className: "checkbox" },
                            d.label({},
                                d.input({ type: "checkbox", name: field.Name, checked: (field.Value || "").toLowerCase() === "true", onChange: e => { this.setValue((<HTMLInputElement>e.target).checked); } }),
                                field.Name
                                )
                            )
                        )
                    )
                );
        }
    };

    export class TaskTimeSpanField extends React.Component<IFieldProps, any> {
        setValue(momentInput: moment.MomentInput) {
            var m = moment.duration(momentInput);
            var v = m.asMilliseconds() > 0 ? m.days() + "." + moment.utc(m.asMilliseconds()).format("HH:mm:ss") : null;
            var index = this.props.index;
            OutcomeActions.setFieldValue.onNext({ index: index, value: v });
        }

        getValues(lowEnd: number, highEnd: number, step: number = 1) {
            var list = [];
            for (var i = lowEnd; i <= highEnd; i += step) {
                list.push(i);
            }
            return list;
        }

        getHourUnits(n: number) {
            switch (true) {
                case n === 1 || n === 21: return n + " час";
                case (n > 1 && n < 5) || (n > 21 && n < 25): return n + " часа";
                default: return n + " часов";
            }
        }

        getDayUnits(n: number) {
            switch (true) {
                case n === 1 || n === 21 || n === 31: return n + " день";
                case (n > 1 && n < 5) || (n > 21 && n < 25): return n + " дня";
                default: return n + " дней";
            }
        }

        render() {
            var field = this.props.field;

            var duration = moment.duration(field.Value || "00:00:00");
            var days = duration.days();
            var hours = duration.hours();
            var minutes = duration.minutes();

            return (
                d.div({ className: "form-group" },
                    d.label({ className: "col-sm-4 control-label" }, field.Name),
                    d.div({ className: "col-sm-8" },
                        d.select({ className: "form-control", style: { width: "30%", display: "inline-block" }, value: days.toString(), onChange: e => { this.setValue({ days: parseInt((<HTMLInputElement>e.target).value), hours: hours, minutes: minutes }); } },
                            this.getValues(0, 31).map(i => d.option({ key: i, value: i }, this.getDayUnits(i)))
                            ),
                        d.select({ className: "form-control", style: { width: "30%", display: "inline-block" }, value: hours.toString(), onChange: e => { this.setValue({ days: days, hours: parseInt((<HTMLInputElement>e.target).value), minutes: minutes }); } },
                            this.getValues(0, 23).map(i => d.option({ key: i, value: i }, this.getHourUnits(i)))
                            ),
                        d.select({ className: "form-control", style: { width: "30%", display: "inline-block" }, value: minutes.toString(), onChange: e => { this.setValue({ days: days, hours: hours, minutes: parseInt((<HTMLInputElement>e.target).value) }); } },
                            this.getValues(0, 55, 5).map(i => d.option({ key: i, value: i }, i + " минут"))
                            )
                        )
                    )
                );
        }
    };
} 