﻿/// <reference path="../typings/_references.d.ts" /> 
"use strict";

module StoreFulfillment {
    function CompleteTask(data: ICompleteTask) {
        return $.ajax({
            url: RootUrl + "/ManagerTask/CompleteTask/",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        });
    }

    function UpdateOrder(data: IUpdateOrder) {
        return $.ajax({
            url: RootUrl + "/ManagerTask/UpdateOrder/",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        });
    }

    export function updateItemsFormData(items: IItemModel[]) {
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            console.log("Item", item);
            var els = document.getElementsByName("[" + i + "].ItemAnswers");
            if (els.length > 0) {
                var el = <HTMLInputElement>els[0];
                console.log("el.value", el.value);
                item.ReserveStatus = el.value;
            }

            //var props = Object.getOwnPropertyNames(item);
            //for (var prop of props) {
            //    console.log("prop", prop);
            //    var els = document.getElementsByName("[" + i + "]." + prop);
            //    console.log("els.length", els.length);
            //    if (els.length > 0) {
            //        var el = <HTMLInputElement>els[0];
            //        console.log("el.value", el.value);
            //        item[prop] = el.value;
            //    }
            //}
        }
    }

    var pleaseWaitDiv = $('<div class="modal js-loading-bar"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h3>Сохранение...</h3></div><div class="modal-body"><div class="progress progress-popup progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div></div></div></div>');

    export function getFormComments() {
        var el = <HTMLInputElement>document.getElementById("comments");
        return el ? el.value : null;
    }

    export var OutcomeActions = {
        setOutcome: new Rx.Subject<number>(),
        setFieldValue: new Rx.Subject<IFieldValue>(),
        completeTask: new Rx.Subject<ITaskOutcome>(),
        editOrder: new Rx.Subject<boolean>(),
        updateOrder: new Rx.Subject<any>(),
        updateItem: new Rx.Subject<IItemModel>(),
        showResult: new Rx.Subject<IResult>(),
    }

    export function OutcomeActionsSubscribe(store: Rx.BehaviorSubject<ITaskStore>) {

        OutcomeActions.setOutcome
            .map((index) => {
                var value = store.getValue();
                value.OutcomeIndex = index;
                return value;
            })
            .subscribe(store);

        OutcomeActions.setFieldValue
            .map(v => {
                var value = store.getValue();
                var outcome = value.Task.Outcomes[value.OutcomeIndex];
                var field = outcome.RequiredFields[v.index];
                field.Value = v.value;
                return value;
            })
            .subscribe(store);

        OutcomeActions.editOrder
            .map(b => {
                var value = store.getValue();
                value.Command.EditMode = b;
                return value;
            })
            .subscribe(store);

        OutcomeActions.updateItem
            .map(v => {
                var value = store.getValue();
                var items = value.Command.Items;
                var item = items.filter(i => i.Id === v.Id)[0];
                item.ReserveStatus = v.ReserveStatus;
                item.SerialNumber = v.SerialNumber;
                return value;
            })
            .subscribe(store);

        OutcomeActions.showResult
            .map(r => {
                var value = store.getValue();
                value.Command.Message = r.Message;
                value.Command.HasErrors = r.HasErrors;
                return value;
            })
            .subscribe(store);

        OutcomeActions.updateOrder
            .map(() => {
                var value = store.getValue();
                return {
                    taskId: value.Task.TaskId.toString(),
                    requestId: ViewModel.MoveItemsCommand.RequestId,
                    items: ViewModel.MoveItemsCommand.Items
                };
            })
            .flatMapLatest((data) => {
                console.log("OutcomeActions.updateOrder: ", data);
                pleaseWaitDiv.modal("show");
                return Rx.Observable.fromPromise<IResult>(UpdateOrder(data));
            })
            .subscribe(result => {
                pleaseWaitDiv.modal("hide");
                console.log("OutcomeActions.updateOrder Result: ", result);
                OutcomeActions.showResult.onNext(result);
                if (result != null && !result.HasErrors) {
                    OutcomeActions.editOrder.onNext(false);
                }
            }, e => {
                console.log(e);
                OutcomeActions.showResult.onNext({ HasErrors: false, Message: e });
            });

        OutcomeActions.completeTask
            .filter(outcome => {
                $('form#order-form').validate({ errorClass: "validation-error" });
                return $('form#order-form').valid();
            })
            .map(outcome => {
                var value = store.getValue();
                var comments = getFormComments();
                //updateItemsFormData(ViewModel.MoveItemsCommand.Items);
                return {
                    taskId: value.Task.TaskId.toString(),
                    requestId: ViewModel.MoveItemsCommand.RequestId,
                    outcome: outcome,
                    comments: comments,
                    items: ViewModel.MoveItemsCommand.Items.filter(i => !i.IsReadOnly)
                };
            })
            .flatMapLatest((data) => {
                console.log("OutcomeActions.completeTask: ", data);
                pleaseWaitDiv.modal("show");
                return Rx.Observable.fromPromise<IResult>(CompleteTask(data));
            })
            .subscribe(result => {
                console.log("OutcomeActions.completeTask Result: ", result);
                pleaseWaitDiv.modal("hide");
                if (result != null && result.HasErrors) {
                    window.location.href = RootUrl + "/Errors/ErrorWithMessage?message=" + result.Message;
                }
                else {
                    window.location.href = RootUrl + "/ManagerTask/Success";
                }
                //(<HTMLInputElement>document.getElementById("outcome")).value = JSON.stringify(o);
                //(<HTMLFormElement>document.getElementById("order-form")).submit();
            }, e => console.log(e));
    }
}