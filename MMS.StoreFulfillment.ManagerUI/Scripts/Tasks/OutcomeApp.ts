﻿/// <reference path="../typings/_references.d.ts" /> 
"use strict";

module StoreFulfillment {
    var d = React.DOM;

    export class App extends React.Component<ITaskStore, any> {
        render() {
            const OrderLines = React.createFactory(OrderLinesTable);
            const View = React.createFactory(TaskView);

            return (
                d.div({ className: "col-sm-8" },
                    OrderLines(this.props.Command),
                    this.props.Command.HideOutcomes ? null : View(this.props)
                    )
                );
        }
    }

    OutcomeActionsSubscribe(TaskStore);

    var app = React.createFactory(App);

    TaskStore.subscribe(
        value => {
            React.render(app(value), document.getElementById('outcomes'));
        },
        error => {
            console.log(error);
        }
        );

} 