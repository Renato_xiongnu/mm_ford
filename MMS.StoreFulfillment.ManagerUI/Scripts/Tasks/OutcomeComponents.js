/// <reference path="../typings/_references.d.ts" /> 
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var StoreFulfillment;
(function (StoreFulfillment) {
    var d = React.DOM;
    var TaskView = (function (_super) {
        __extends(TaskView, _super);
        function TaskView() {
            _super.apply(this, arguments);
        }
        TaskView.prototype.render = function () {
            var TaskButton = React.createFactory(TaskOutcomeButton);
            var task = this.props.Task;
            var outcomeIndex = this.props.OutcomeIndex;
            if (typeof task === "undefined" || task === null) {
                return null;
            }
            var buttons = task.Outcomes.map(function (o, index) { return TaskButton({ key: index, index: index, outcome: o, showFields: index === outcomeIndex }); });
            return (d.div({ className: "tt-task-view" }, buttons));
        };
        return TaskView;
    })(React.Component);
    StoreFulfillment.TaskView = TaskView;
    ;
    var TaskOutcomeButton = (function (_super) {
        __extends(TaskOutcomeButton, _super);
        function TaskOutcomeButton() {
            _super.apply(this, arguments);
        }
        TaskOutcomeButton.prototype.clickButton = function (e) {
            var index = this.props.index;
            var outcome = this.props.outcome;
            var hasFields = outcome.RequiredFields.length !== 0;
            return hasFields ? StoreFulfillment.OutcomeActions.setOutcome.onNext(index) : StoreFulfillment.OutcomeActions.completeTask.onNext(outcome);
        };
        TaskOutcomeButton.prototype.render = function () {
            var Outcome = React.createFactory(TaskOutcome);
            var index = this.props.index;
            var outcome = this.props.outcome;
            var showFields = this.props.showFields;
            var hasFields = outcome.RequiredFields.length !== 0;
            var btnClass = React.addons.classSet({
                "btn": true,
                "btn-large": true,
                "btn-block": true,
                "btn-primary": !hasFields,
                "btn-info": hasFields,
            });
            var text = !hasFields ? outcome.Value : outcome.Value + " (заполнить полей: " + outcome.RequiredFields.length + ")";
            return (d.div({}, d.a({ className: btnClass, onClick: this.clickButton.bind(this) }, text), showFields ? d.div({ className: "row", style: { padding: "15px;" } }, Outcome({ outcome: outcome })) : null));
        };
        return TaskOutcomeButton;
    })(React.Component);
    StoreFulfillment.TaskOutcomeButton = TaskOutcomeButton;
    ;
    var TaskOutcome = (function (_super) {
        __extends(TaskOutcome, _super);
        function TaskOutcome(props) {
            _super.call(this, props);
            moment.locale("ru");
            props.outcome.RequiredFields.forEach(function (field) {
                field.Value = field.Value === null ? field.DefaultValue : field.Value;
            });
        }
        TaskOutcome.prototype.validate = function () {
            return this.props.outcome.RequiredFields.every(function (f) {
                if (f.StringType === "System.DateTime" && f.Value !== null && f.Value !== "") {
                    var m = moment(f.Value);
                    if (m.isValid() && m.isAfter(moment().format("YYYY-MM-DD"))) {
                        f.ErrorMessage = null;
                        return true;
                    }
                    f.ErrorMessage = "Дата должна быть больше или равна сегодняшней";
                    return false;
                }
                return (f.Value !== null && f.Value !== "");
            });
        };
        TaskOutcome.prototype.render = function () {
            var outcome = this.props.outcome;
            var TextField = React.createFactory(TaskTextField);
            var ChoiceField = React.createFactory(TaskChoiceField);
            var DateField = React.createFactory(TaskDateField);
            var BooleanField = React.createFactory(TaskBooleanField);
            var TimeSpanField = React.createFactory(TaskTimeSpanField);
            var fields = outcome.RequiredFields.map(function (field, index) {
                field.Value = field.Value === null ? field.DefaultValue : field.Value;
                switch (field.StringType) {
                    case "Choice":
                        return ChoiceField({ key: index, index: index, field: field, outcome: outcome });
                    case "DateTime":
                    case "System.DateTime":
                        return DateField({ key: index, index: index, field: field, outcome: outcome });
                    case "Boolean":
                    case "System.Boolean":
                        return BooleanField({ key: index, index: index, field: field, outcome: outcome });
                    case "TimeSpan":
                    case "System.TimeSpan":
                        return TimeSpanField({ key: index, index: index, field: field, outcome: outcome });
                    case "System.String":
                    case "System.Int32":
                    case "System.Decimal":
                    case "System.Double":
                    default:
                        return TextField({ key: index, index: index, field: field, outcome: outcome });
                }
            });
            var btnClass = React.addons.classSet({
                "btn": true,
                "btn-large": true,
                "btn-block": true,
                "btn-primary": true
            });
            return (d.div({ className: "col-sm-12" }, d.div({ className: "form-horizontal" }, fields), d.div({ className: "buttons" }, d.button({ className: btnClass, disabled: !this.validate(), onClick: function (e) {
                    e.preventDefault();
                    StoreFulfillment.OutcomeActions.completeTask.onNext(outcome);
                } }, "Завершить"))));
        };
        return TaskOutcome;
    })(React.Component);
    StoreFulfillment.TaskOutcome = TaskOutcome;
    ;
    var TaskTextField = (function (_super) {
        __extends(TaskTextField, _super);
        function TaskTextField() {
            _super.apply(this, arguments);
        }
        TaskTextField.prototype.setValue = function (value) {
            var index = this.props.index;
            StoreFulfillment.OutcomeActions.setFieldValue.onNext({ index: index, value: value });
        };
        TaskTextField.prototype.render = function () {
            var _this = this;
            var field = this.props.field;
            return (d.div({ className: "form-group" }, d.label({ className: "col-sm-4 control-label" }, field.Name), d.div({ className: "col-sm-8" }, d.input({ type: "text", className: "form-control", name: field.Name, value: field.Value, onChange: function (e) { _this.setValue(e.target.value); } }))));
        };
        return TaskTextField;
    })(React.Component);
    StoreFulfillment.TaskTextField = TaskTextField;
    ;
    var TaskChoiceField = (function (_super) {
        __extends(TaskChoiceField, _super);
        function TaskChoiceField() {
            _super.apply(this, arguments);
        }
        TaskChoiceField.prototype.setValue = function (value) {
            var index = this.props.index;
            StoreFulfillment.OutcomeActions.setFieldValue.onNext({ index: index, value: value });
        };
        TaskChoiceField.prototype.render = function () {
            var _this = this;
            var field = this.props.field;
            var values = field.PredefinedValues.map(function (s, index) {
                return d.div({ key: index, className: "radio" }, d.label({}, d.input({ type: "radio", name: field.Name, checked: (s === field.Value), onChange: function (e) { if (e.target.checked) {
                        _this.setValue(s);
                    } } }), s));
            });
            return (d.div({ className: "form-group" }, d.label({ className: "col-sm-4 control-label" }, field.Name), d.div({ className: "col-sm-8" }, values)));
        };
        return TaskChoiceField;
    })(React.Component);
    StoreFulfillment.TaskChoiceField = TaskChoiceField;
    ;
    var TaskDateField = (function (_super) {
        __extends(TaskDateField, _super);
        function TaskDateField() {
            _super.apply(this, arguments);
        }
        TaskDateField.prototype.setValue = function (m) {
            var index = this.props.index;
            var v = m && m.isValid() ? m.format("YYYY-MM-DDT00:00:00") + "Z" : null;
            StoreFulfillment.OutcomeActions.setFieldValue.onNext({ index: index, value: v });
        };
        TaskDateField.prototype.componentDidMount = function () {
            var _this = this;
            var el = React.findDOMNode(this.refs["datepicker"]);
            var field = this.props.field;
            var moment1 = moment(field.Value, "DD.MM.YYYY HH:mm:ss");
            var dateTime = moment1.isValid() ? moment1.toDate() : null;
            $(el).datetimepicker({ format: "DD.MM.YYYY", locale: "ru", defaultDate: dateTime });
            $(el).on("dp.change", function (e) {
                var m = e.date;
                _this.setValue(m);
            });
        };
        TaskDateField.prototype.render = function () {
            var field = this.props.field;
            return (d.div({ className: "form-group" }, d.label({ className: "col-sm-4 control-label" }, field.Name), d.div({ className: "col-sm-8" }, d.div({ className: "input-group date", ref: "datepicker" }, d.input({ type: "text", className: "form-control datepicker", name: field.Name }), d.span({ className: "input-group-addon" }, d.span({ className: "glyphicon glyphicon-calendar" }))), field.ErrorMessage ? d.label({ className: "validation-error" }, field.ErrorMessage) : null)));
        };
        TaskDateField.prototype.componentWillUnmount = function () {
            var el = React.findDOMNode(this.refs["datepicker"]);
            $(el).data("DateTimePicker").destroy();
        };
        return TaskDateField;
    })(React.Component);
    StoreFulfillment.TaskDateField = TaskDateField;
    ;
    var TaskBooleanField = (function (_super) {
        __extends(TaskBooleanField, _super);
        function TaskBooleanField() {
            _super.apply(this, arguments);
        }
        TaskBooleanField.prototype.setValue = function (value) {
            var index = this.props.index;
            StoreFulfillment.OutcomeActions.setFieldValue.onNext({ index: index, value: value.toString() });
        };
        TaskBooleanField.prototype.render = function () {
            var _this = this;
            var field = this.props.field;
            return (d.div({ className: "form-group" }, d.div({ className: "col-sm-offset-4 col-sm-8" }, d.div({ className: "checkbox" }, d.label({}, d.input({ type: "checkbox", name: field.Name, checked: (field.Value || "").toLowerCase() === "true", onChange: function (e) { _this.setValue(e.target.checked); } }), field.Name)))));
        };
        return TaskBooleanField;
    })(React.Component);
    StoreFulfillment.TaskBooleanField = TaskBooleanField;
    ;
    var TaskTimeSpanField = (function (_super) {
        __extends(TaskTimeSpanField, _super);
        function TaskTimeSpanField() {
            _super.apply(this, arguments);
        }
        TaskTimeSpanField.prototype.setValue = function (momentInput) {
            var m = moment.duration(momentInput);
            var v = m.asMilliseconds() > 0 ? m.days() + "." + moment.utc(m.asMilliseconds()).format("HH:mm:ss") : null;
            var index = this.props.index;
            StoreFulfillment.OutcomeActions.setFieldValue.onNext({ index: index, value: v });
        };
        TaskTimeSpanField.prototype.getValues = function (lowEnd, highEnd, step) {
            if (step === void 0) { step = 1; }
            var list = [];
            for (var i = lowEnd; i <= highEnd; i += step) {
                list.push(i);
            }
            return list;
        };
        TaskTimeSpanField.prototype.getHourUnits = function (n) {
            switch (true) {
                case n === 1 || n === 21: return n + " час";
                case (n > 1 && n < 5) || (n > 21 && n < 25): return n + " часа";
                default: return n + " часов";
            }
        };
        TaskTimeSpanField.prototype.getDayUnits = function (n) {
            switch (true) {
                case n === 1 || n === 21 || n === 31: return n + " день";
                case (n > 1 && n < 5) || (n > 21 && n < 25): return n + " дня";
                default: return n + " дней";
            }
        };
        TaskTimeSpanField.prototype.render = function () {
            var _this = this;
            var field = this.props.field;
            var duration = moment.duration(field.Value || "00:00:00");
            var days = duration.days();
            var hours = duration.hours();
            var minutes = duration.minutes();
            return (d.div({ className: "form-group" }, d.label({ className: "col-sm-4 control-label" }, field.Name), d.div({ className: "col-sm-8" }, d.select({ className: "form-control", style: { width: "30%", display: "inline-block" }, value: days.toString(), onChange: function (e) { _this.setValue({ days: parseInt(e.target.value), hours: hours, minutes: minutes }); } }, this.getValues(0, 31).map(function (i) { return d.option({ key: i, value: i }, _this.getDayUnits(i)); })), d.select({ className: "form-control", style: { width: "30%", display: "inline-block" }, value: hours.toString(), onChange: function (e) { _this.setValue({ days: days, hours: parseInt(e.target.value), minutes: minutes }); } }, this.getValues(0, 23).map(function (i) { return d.option({ key: i, value: i }, _this.getHourUnits(i)); })), d.select({ className: "form-control", style: { width: "30%", display: "inline-block" }, value: minutes.toString(), onChange: function (e) { _this.setValue({ days: days, hours: hours, minutes: parseInt(e.target.value) }); } }, this.getValues(0, 55, 5).map(function (i) { return d.option({ key: i, value: i }, i + " минут"); })))));
        };
        return TaskTimeSpanField;
    })(React.Component);
    StoreFulfillment.TaskTimeSpanField = TaskTimeSpanField;
    ;
})(StoreFulfillment || (StoreFulfillment = {}));
//# sourceMappingURL=OutcomeComponents.js.map