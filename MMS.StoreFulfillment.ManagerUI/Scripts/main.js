﻿var pricePattern = /^\d+([.,]\d{1,2})?$/;
var numberPattern = /^\d+$/;
var requiredMessage = 'Обязательное поле';
var numberMessage = 'Только цифры';

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

$('.accordion').on('show hide', function (n) {
    $(n.target).siblings('.accordion-heading').find('.accordion-toggle i').toggleClass('icon-chevron-up icon-chevron-down');
});

//---------------MODAL---------------//
function addResultData(name, data) {
    if (!data) return '';

    return String.format('.{0}:{1}', name, data);
};

function formPrevent(selector) {
    $(selector).submit(function (e) {
        if (!e.preventDefault()) e.preventDefault();
    });
};

function closePopup(selector) {
    $(selector).modal('hide');
};

function clearPostData() {
    var id = $('#itemId').val();
    $('#ReviewStatus_' + id).val('');
    var answersList = '#answersForItem_' + id;
    $(answersList).val($(answersList+" option:first").val());
};

function resetModal(modalName, formName, focusItem) {
    $(modalName).on('shown.bs.modal', function () {
        $(formName).bootstrapValidator('resetForm', true);
        $(formName).find('[name="' + focusItem + '"]').focus();
    });
};
//---------------END-MODAL---------------//

$(document).ready(function() {
    jQuery.extend(jQuery.validator.messages, {
        required: "Поле обязательно к заполнению"
    });
});