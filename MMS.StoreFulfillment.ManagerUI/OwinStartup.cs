﻿using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MMS.StoreFulfillment.ManagerUI.Startup))]

namespace MMS.StoreFulfillment.ManagerUI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("StoreFulfillmentContext");

            app.UseHangfireDashboard();
        }
    }
}