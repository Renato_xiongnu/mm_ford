﻿using System.Web.Optimization;

namespace MMS.StoreFulfillment.ManagerUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.full.js",
                "~/Scripts/baseDialog.js",
                "~/Scripts/authorize.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/react/react-with-addons.js",
                "~/Scripts/rx.lite.js",
                "~/Scripts/ts/TaskStore.js",
                "~/Scripts/ts/TaskActions.js",
                "~/Scripts/ts/TaskComponents.js",
                "~/Scripts/ts/App.js"));

            bundles.Add(new ScriptBundle("~/bundles/taskapp").Include(
                "~/Scripts/react/react-with-addons.js",
                "~/Scripts/rx.lite.js",
                "~/Scripts/Tasks/OutcomeStore.js",
                "~/Scripts/Tasks/OutcomeActions.js",
                "~/Scripts/Tasks/OutcomeComponents.js",
                "~/Scripts/Tasks/OrderComponents.js",
                "~/Scripts/Tasks/OutcomeApp.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/moment-with-locales.js",
                "~/Scripts/moment-timezone.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/respond.js",
                "~/Scripts/jquery.inputmask/jquery.inputmask.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            var mangerCssBundle = new StyleBundle("~/Content/managerCss").Include(
                "~/Content/bootstrap-theme.css",
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-datetimepicker.css",
                "~/Content/bootstrapValidator.min.css",
                "~/Content/main.css",
                "~/Content/jquery-ui-1.10.3.full.css");

            bundles.Add(mangerCssBundle);

            var managerJsBundle = new ScriptBundle("~/Scripts/managerScripts").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-1.10.3.full.min.js",
                "~/Scripts/moment-with-locales.js",
                "~/Scripts/moment-timezone.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/bootstrapValidator.min.js",
                "~/Scripts/main.js",
                "~/Scripts/validationModalRules.js",
                "~/Scripts/jquery.validate.js");

            bundles.Add(managerJsBundle);

            BundleTable.EnableOptimizations = false;
        }
    }
}
