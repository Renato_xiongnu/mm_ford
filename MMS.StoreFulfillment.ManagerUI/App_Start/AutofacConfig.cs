﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.ManagerUI.AppServices;
using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;
using MMS.StoreFulfillment.ManagerUI.Proxy.RecreateReserve;

namespace MMS.StoreFulfillment.ManagerUI
{
    public class AutofacConfig
    {
        public static void RegisterContainer()
        {
            var containerBuider = new ContainerBuilder();
            containerBuider.RegisterType<TaskStrategy>().As<ITaskStrategy>();
            containerBuider.RegisterType<AuthStrategy>().As<IAuthStrategy>();
            containerBuider.RegisterType<DataFacade>().As<IDataFacade>();
            containerBuider.RegisterType<RecreateReserveServiceClient>().As<IRecreateReserveService>();
            containerBuider.RegisterType<StoreFulfillmentStorage>().As<IStoreFulfillmentStorage>();
            containerBuider.RegisterType<StoreFulfillmentContext>();
            containerBuider.RegisterControllers(typeof(MvcApplication).Assembly);
            containerBuider.RegisterApiControllers(typeof(MvcApplication).Assembly);
            
            var container = containerBuider.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}