﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using NLog;

namespace MMS.StoreFulfillment.ManagerUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.Services.Add(typeof (IExceptionLogger), new WebApiExceptionLogger());
        }
    }

    public class WebApiExceptionLogger : IExceptionLogger
    {
        private ILogger _logger = LogManager.GetLogger("GlobalExceptionLogger");

        public Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            _logger.Error("Unhandler error {0}", context.Exception.ToString());

            return Task.FromResult(0);
        }
    }
}
