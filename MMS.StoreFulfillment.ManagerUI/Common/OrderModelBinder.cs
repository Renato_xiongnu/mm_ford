﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;

namespace MMS.StoreFulfillment.ManagerUI.Common
{
    public class OrderModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var form = controllerContext.HttpContext.Request.Form;

            var order = new ItemMovementViewModel
            {
                Task = new TaskViewModel
                {
                    TaskId = Convert.ToInt32(form["Task.TaskId"])
                },
                MoveItemsCommand = new ItemMovementRequestViewModel
                {
                    RequestId = form["MoveItemsCommand.RequestId"],
                    Additionalnfo = 
                        new ReserveProductsContent {Comments = form["MoveItemsCommand.RequestContent.Comments"]},
                    Outcome = form["MoveItemsCommand.Outcome"],
                    Items = GetOrderLines(form),
                    RequiredFields = GetRequiredFields(form)
                }
            };

            return order;
        }

        private ICollection<RequiredField> GetRequiredFields(NameValueCollection form)
        {
            var result = new List<RequiredField>();
            bool hasValue = form.AllKeys.Any(t=>t.Contains("rf__"));
            if (!hasValue) return result;

            var keys = form.AllKeys.Where(t => t.StartsWith("rf__"));
            foreach (var requiredFieldKey in keys)
            {
                result.Add(new RequiredField
                {
                    Name = requiredFieldKey.Substring("rf__".Length),
                    Value = form.Get(requiredFieldKey)
                });
            }

            return result;
        }

        private IList<ItemViewModel> GetOrderLines(NameValueCollection form)
        {
            var result = new List<ItemViewModel>();
            int i = 0;
            while (true)
            {
                bool hasFirstValue = form.Get(string.Format("[{0}].Id", i)) != null;
                if (!hasFirstValue) return result;

                var orderLine = new ItemViewModel
                {
                    Id = Convert.ToInt32(form.Get(string.Format("[{0}].Id", i))),
                    ReserveStatus = form.Get(string.Format("[{0}].ItemAnswers", i))
                };

                result.Add(orderLine);

                i++;
            }
        }
    }
}