﻿using MMS.StoreFulfillment.Application.Proxy.Auth;
using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;

namespace MMS.StoreFulfillment.ManagerUI.AppServices
{
    public class AuthStrategy : StrategyBase, IAuthStrategy
    {
        public AuthStrategy(IDataFacade dataFacade) : base(dataFacade)
        {
        }

        public bool Login(string userName, string password, bool remeberMe)
        {
            return new AuthenticationServiceClient().LogIn(userName, password, remeberMe);
        }

        public Models.ManagerTask.User GetCurrentUser()
        {
            return ToModelUser(new AuthenticationServiceClient().GetCurrentUser());
        }

        public void Logout()
        {
            new AuthenticationServiceClient().LogOut();
        }

        public bool LoginByToken(string userName, string userToken, string taskId, string taskToken)
        {
            return new AuthenticationServiceClient().LoginByToken(userName, userToken, taskId, taskToken);
        }

        private static Models.ManagerTask.User ToModelUser(UserInfo user)
        {
            return new Models.ManagerTask.User
            {
                Login = user.LoginName,
                Email = user.Email,
                Roles = user.Roles,
                Parameters = user.Parameters,
                Authorized = user.IsAuthtorized
            };
        }
    }
}