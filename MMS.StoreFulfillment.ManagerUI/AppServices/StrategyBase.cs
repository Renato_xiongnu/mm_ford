﻿namespace MMS.StoreFulfillment.ManagerUI.AppServices
{
    public abstract class StrategyBase
    {
        protected StrategyBase(IDataFacade dataFacade)
        {
            DataFacade = dataFacade;
        }

        protected IDataFacade DataFacade { get; set; }
    }
}