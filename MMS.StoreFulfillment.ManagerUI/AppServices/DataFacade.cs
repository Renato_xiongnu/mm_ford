﻿using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;

namespace MMS.StoreFulfillment.ManagerUI.AppServices
{
    public class DataFacade : IDataFacade
    {
        private ITaskStrategy _tasks;
        private IAuthStrategy _auth;        
        
        public ITaskStrategy Tasks
        {
            get { return _tasks ?? (_tasks = new TaskStrategy(this)); }
        }
        
        public IAuthStrategy Auth
        {
            get { return _auth ?? (_auth = new AuthStrategy(this)); }
        }
    }
}