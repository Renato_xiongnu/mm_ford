﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.DAL;
using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;
using RequiredField = MMS.StoreFulfillment.Application.TicketTool.RequiredField;

namespace MMS.StoreFulfillment.ManagerUI.AppServices
{
    public class TaskStrategy : StrategyBase, ITaskStrategy
    {
        public TaskStrategy(IDataFacade dataFacade)
            : base(dataFacade)
        {
        }

        public AssignTaskForMeResult AssignTaskForMe(string taskId)
        {
            return new TicketToolServiceClient().AssignTaskForMe(taskId);
        }

        public TicketTask GetTask(string taskId)
        {
            return new TicketToolServiceClient().GetTask(taskId);
        }

        public IEnumerable<TaskViewModel> GetTasksInProgress(string[] parameters)
        {
            var moscow = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

            var tasksResult = new TicketToolServiceClient().GetMyTasksInProgress(parameters, null);
            
            var storage = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var requests = tasksResult.Select(m => m.WorkitemId).ToArray();
            var requestResult =
                storage.Find(el => requests.Contains(el.RequestId)).ToList();

            var taskItems =
                tasksResult.Select(
                    t => new { Task = t, Order = requestResult.SingleOrDefault(z => z.RequestId == t.WorkitemId) })
                    .Where(x => x.Order != null)
                    .Select(x => new TaskViewModel
                    {
                        Name = x.Task.Name,
                        CreationDate = TimeZoneInfo.ConvertTimeFromUtc(x.Task.CreationDate, moscow),
                        DeadlineDate =
                            TimeZoneInfo.ConvertTimeFromUtc(
                                x.Task.CreationDate + (x.Task.MaxRunTime ?? new TimeSpan()), moscow),
                        Url = x.Task.Url,
                        IsAssignedOnMe = x.Task.IsAssignedOnMe,
                        AssignedOn = x.Task.AssignedOn,
                        Escaleted = x.Task.Escalated,
                        OrderId = x.Order.CustomerOrder.CustomerOrderId,
                        WWSOrderId = x.Order != null ? x.Order.WwsOrderId : String.Empty,
                        Store = x.Order != null
                            ? new StoreItem
                            {
                                SapCode = x.Order.SapCode
                            }
                            : new StoreItem()
                    })
                    .OrderBy(t => t.CreationDate)
                    .ToList();

            return taskItems;
        }

        public ParameterViewModel[] GetParameters()
        {
            return new TicketToolServiceClient()
                .GetMyParameters()
                .Select(p => new ParameterViewModel {Name = p.ParameterName, Title = p.Name})
                .ToArray();
        }

        public void SaveTaskOutcome(TaskExecutionResult saveTaskRequest, RequiredField[] reqFields=null)
        {
            var client = new TicketToolServiceClient();

            var requiredFieldsLength = reqFields != null ? reqFields.Length : 0;
            var reqFieldsConverted = new RequiredField[requiredFieldsLength];
            if (reqFields != null)
            {
                for (int i = 0; i < reqFields.Length; i++)
                {
                    reqFieldsConverted[i] = new RequiredField();

                    reqFieldsConverted[i].Value = reqFields[i].Value;
                    reqFieldsConverted[i].DefaultValue = reqFields[i].DefaultValue;
                    reqFieldsConverted[i].Name = reqFields[i].Name;
                    reqFieldsConverted[i].Type = reqFields[i].Type;
                }
            }

            var result = client.CompleteTask(saveTaskRequest.TaskId, saveTaskRequest.Outcome, reqFieldsConverted.Any() ? reqFieldsConverted : null);
            if (result.HasError) throw new Exception(result.MessageText);
        }

        public void FinishTaskLater(string taskId)
        {
            var result=new TicketToolServiceClient().PostponeTask(taskId, null);
            if (result.HasError)
            {
                throw new Exception(result.MessageText);
            }
        }
    }
}