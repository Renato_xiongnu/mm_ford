﻿namespace MMS.StoreFulfillment.ManagerUI.AppServices.Contracts
{
    public interface IAuthStrategy
    {
        bool Login(string userName, string password, bool remeberMe);
        Models.ManagerTask.User GetCurrentUser();
        void Logout();
        bool LoginByToken(string userName, string userToken, string taskId, string taskToken);
    }
}
