﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.ManagerUI.Models.ManagerTask;
using RequiredField = MMS.StoreFulfillment.Application.TicketTool.RequiredField;

namespace MMS.StoreFulfillment.ManagerUI.AppServices.Contracts
{
    public interface ITaskStrategy
    {
        IEnumerable<TaskViewModel> GetTasksInProgress(string[] parameters);

        ParameterViewModel[] GetParameters();

        void SaveTaskOutcome(TaskExecutionResult saveTaskRequest, RequiredField[] reqFields = null);

        void FinishTaskLater(string taskId);

        AssignTaskForMeResult AssignTaskForMe(string taskId);

        TicketTask GetTask(string taskId);
    }    
}
