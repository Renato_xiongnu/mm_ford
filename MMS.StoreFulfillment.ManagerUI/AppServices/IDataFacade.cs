﻿using MMS.StoreFulfillment.ManagerUI.AppServices.Contracts;

namespace MMS.StoreFulfillment.ManagerUI.AppServices
{
    public interface IDataFacade
    {
        ITaskStrategy Tasks { get; }
        IAuthStrategy Auth { get; }
    }
}