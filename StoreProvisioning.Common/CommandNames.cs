﻿namespace StoreProvisioning.Common
{
    public class CommandNames
    {
        public const string AssembleItems = "AssembleItems";

        public const string ReturnItems = "ReturnItems";

        public const string AssembleItemsContent = "AssembleItemsContent";

        public const string ReturnItemsContent = "ReturnItemsContent";
    }
}
