﻿using MMS.Cloud.Shared.Commands;

namespace StoreProvisioning.Common
{
    public class KeySerializer
    {
        public static string Serialize(Key key)
        {
            return string.Format("{0}/{1}", key.Id, key.CorrelationId);
        }

        public static Key Deserialize(string keySerialized)
        {
            var keyPart = keySerialized.Split('/');

            var key = Key.FromCorrelationId(keyPart[1]);
            key.Id = keyPart[0];

            return key;
        }
    }
}
