﻿using System;
using Autofac;
using Microsoft.Practices.ServiceLocation;
using MMS.StoreFulfillment.Application.SF.TaskHandlers;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Host.Configuration;
using NUnit.Framework;

namespace MMS.StoreFulfillment.Host.Tests
{
    [TestFixture]
    public class IocTests
    {
        [Test]
        public void AutofacConfig_Test()
        {
            AutofacConfig.RegisterContainer();

            var scope = ServiceLocator.Current.GetInstance<ILifetimeScope>();
            using (var s = scope.BeginLifetimeScope())
            {
                Assert.IsNotNull(s.Resolve<IDeliveryStoresProvider>());

                Assert.IsNotNull(s.Resolve<ITransferStoresDataProvider>());

                Assert.IsNotNull(s.Resolve<IProductsAvailabilityProvider>());

                Assert.IsNotNull(s.ResolveNamed<ITicketTaskCreator>("CreateTransfer"));

                Assert.IsNotNull(s.Resolve<ICommandHandler<ProcessItemsMovementCompletionCommand>>());

                Assert.IsNotNull(s.Resolve<ICommandHandler<ProcessFulfillmentOrderCompletionCommand>>());

                Assert.IsNotNull(s.Resolve<ICommandHandler<ProcessItemsReservedCommand>>());

            }
        }

    }
}
