﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class AssembleItemsContent : AdditionalContent
    {
        [DataMember]
        public string PaymentType { get; set; }
        [DataMember]
        public string CustomerFullName { get; set; }
        [DataMember]
        public string CustomerEmail { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string DeliveryAddress { get; set; }
        [DataMember]
        public string DeliveryCity { get; set; }
    }
}