﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class AssembleItemsResult:CommandExecutionResult
    {
        [DataMember]
        public ICollection<OrderLineReviewed> OrderLines { get; set; } 
    }
}