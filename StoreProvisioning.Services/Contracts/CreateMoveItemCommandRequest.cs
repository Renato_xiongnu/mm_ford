﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    [KnownType(typeof(ReturnItemsContent))]
    [KnownType(typeof(AssembleItemsContent))]
    public class CreateMoveItemCommandRequest
    {
        [DataMember]
        public string RequestId { get; set; }
        [DataMember]
        public string CustomerOrderId { get; set; }
        [DataMember]
        public string WwsOrderId { get; set; }
        [DataMember]
        public AdditionalContent AdditionalContent { get; set; }
        [DataMember]
        public ICollection<OrderLine> OrderLines { get; set; }
    }
}