using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class OrderLine
    {
        [DataMember]
        public long ArticleId { get; set; }
        [DataMember]
        public int Qty { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int WwsDepartmentNo { get; set; }
        [DataMember]
        public int WWSStockNumber { get; set; }
        [DataMember]
        public int WWSFreeQty { get; set; }
        [DataMember]
        public string WWSPriceOrig { get; set; }
        [DataMember]
        public string ProductGroup { get; set; }
        [DataMember]
        public int ProductGroupNo { get; set; }
        [DataMember]
        public string AppliedCampaigns { get; set; }
        [DataMember]
        public string LineComment { get; set; }
    }
}