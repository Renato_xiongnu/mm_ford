﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class CommandExecutionResult
    {
        [DataMember]
        public string Outcome { get; set; }

        [DataMember]
        public string WwsOrderId { get; set; }
    }
}