﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class GetMoveItemCommandRequest
    {
        [DataMember]
        public string RequestId { get; set; }
    }
}