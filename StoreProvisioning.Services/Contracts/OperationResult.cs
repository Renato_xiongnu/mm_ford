using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class OperationResult
    {
        [DataMember]
        public bool IsOk { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}