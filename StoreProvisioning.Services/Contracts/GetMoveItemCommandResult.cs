﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class GetMoveItemCommandResult : OperationResult
    {
        [DataMember]
        public string CommandName { get; set; }

        [DataMember]
        public string CommandParameter { get; set; }

        [DataMember]
        public string WwsOrderId { get; set; }
    }
}