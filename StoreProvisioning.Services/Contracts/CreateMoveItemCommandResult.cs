﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class CreateMoveItemCommandResult:OperationResult
    {
        [DataMember]
        public string MoveItemCommandId { get; set; }
    }
}