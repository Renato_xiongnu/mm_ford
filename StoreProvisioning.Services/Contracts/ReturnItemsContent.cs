﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class ReturnItemsContent : AdditionalContent
    {
        [DataMember]
        public string CustomerOrderId { get; set; }
        [DataMember]
        public string WwsOrderId { get; set; }
    }
}