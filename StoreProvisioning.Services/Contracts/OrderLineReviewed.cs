﻿using System.Runtime.Serialization;

namespace StoreProvisioning.Services.Contracts
{
    [DataContract]
    public class OrderLineReviewed:OrderLine
    {
        [DataMember]
        public bool SuccesfullyFulfilled { get; set; }
        [DataMember]
        public string ReviewStatus { get; set; }
        [DataMember]
        public string ItemStateDescription { get; set; }
    }
}