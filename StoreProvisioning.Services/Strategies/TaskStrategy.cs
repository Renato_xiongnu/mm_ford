﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using MMS.StoreFulfillment.DAL;
using StoreProvisioning.Services.Model;
using StoreProvisioning.Services.Proxy.TicketToolService;
using StoreProvisioning.Services.Strategies.Contracts;
using Outcome = StoreProvisioning.Services.Model.Outcome;
using Parameter = StoreProvisioning.Services.Model.Parameter;
using RequiredField = StoreProvisioning.Services.Model.RequiredField;

namespace StoreProvisioning.Services.Strategies
{
    public class TaskStrategy : StrategyBase, ITaskStrategy
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public TaskStrategy(IDataFacade dataFacade)
            : base(dataFacade)
        {
        }

        public OperationResult<AssignTaskForMeResult> AssignTaskForMe(string taskId)
        {
            return Try(() => new TicketToolServiceClient().AssignTaskForMe(taskId));
        }
        
        public OperationResult<IEnumerable<TaskItem>> GetTasksInProgress(string[] parameters)
        {
            var moscow = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

            var tasksResult = Try(() => new TicketToolServiceClient().GetMyTasksInProgress(parameters, null));
            if (tasksResult.HasErrors)
                return new OperationResult<IEnumerable<TaskItem>>(null, tasksResult.Message);

            var storage = new StoreFulfillmentStorage(new StoreFulfillmentContext());
            var requests = tasksResult.Item.Select(m => m.WorkitemId).ToArray();
            var requestResult =
                storage.Find(el => requests.Contains(el.RequestId));

            var taskItems =
                tasksResult.Item.Select(
                    t => new { Task = t, Order = requestResult.SingleOrDefault(z => z.RequestId == t.WorkitemId) })
                    .Where(x => x.Order != null)
                    .Select(x => new TaskItem
                    {
                        Name = x.Task.Name,
                        CreationDate = TimeZoneInfo.ConvertTimeFromUtc(x.Task.CreationDate, moscow),
                        DeadlineDate =
                            TimeZoneInfo.ConvertTimeFromUtc(
                                x.Task.CreationDate + (x.Task.MaxRunTime ?? new TimeSpan()), moscow),
                        Url = x.Task.Url,
                        IsAssignedOnMe = x.Task.IsAssignedOnMe,
                        AssignedOn = x.Task.AssignedOn,
                        Escaleted = x.Task.Escalated,
                        OrderId = x.Order.CustomerOrder.CustomerOrderId,
                        WWSOrderId = x.Order != null ? x.Order.WwsOrderId : String.Empty,
                        Store = x.Order != null
                            ? new StoreItem
                            {
                                SapCode = x.Order.SapCode
                            }
                            : new StoreItem()
                    })
                    .OrderBy(t => t.CreationDate)
                    .ToList();

            return new OperationResult<IEnumerable<TaskItem>>(taskItems);
        }

        public OperationResult<Parameter[]> GetParameters()
        {
            return Try(InternalGetMyParameters);
        }

        public OperationResult<TaskItem> GetTask(string taskId)
        {
            return Try(() => GetTaskItem(taskId));
        }

        public OperationResult<bool> SaveTaskOutcome(TaskExecutionResult saveTaskRequest)
        {
            return Try(() => CompleteTaskItem(saveTaskRequest.TaskId, saveTaskRequest.Outcome, null));
        }

        public OperationResult<bool> FinishTaskLater(string taskId)
        {
            return Try(() =>
            {
                new TicketToolServiceClient().PostponeTask(taskId, null);
                return true;
            });
        }
        
        private static Parameter[] InternalGetMyParameters()
        {
            return new TicketToolServiceClient()
                .GetMyParameters()
                .Select(p => new Parameter {Name = p.ParameterName, Title = p.Name})
                .ToArray();
        }

        private TaskItem GetTaskItem(string taskId)
        {
            var client = new TicketToolServiceClient();
            try
            {
                var response = client.GetTask(taskId);

                return new TaskItem
                {
                    TaskId = Convert.ToInt32(response.TaskId),
                    Name = response.Name,
                    CreationDate = response.Created,
                    Escaleted = response.Escalated,
                    Url = response.WorkitemPageUrl,
                    WorkItemId = response.WorkItemId,
                    Store = new StoreItem(),
                    RequiredFields = response.RequiredFields.Select(x => new RequiredField
                    {
                        Name = x.Name,
                        Value = x.Value.ToString(),
                        DefaultValue = x.DefaultValue.ToString(),
                        Type = (RequiredFieldType)Enum.Parse(typeof(RequiredFieldType),x.Type)
                    }).ToArray(),
                    Outcomes = response.Outcomes.Select(x => new Outcome
                    {
                        Value = x.Value,
                        RequiredFields = x.RequiredFields.Select(c => new RequiredField
                        {
                            Name = c.Name,
                            Value = c.Value.ToString(),
                            DefaultValue = c.DefaultValue.ToString(),
                            Type = (RequiredFieldType)Enum.Parse(typeof(RequiredFieldType), c.Type)
                        }).ToArray()
                    }).ToArray(),
                    Description = response.Description
                };
            }
            catch (Exception ex)
            {
                Log.Error("TicketToolServiceClient.GetTask", ex);
                throw;
            }
        }

        private bool CompleteTaskItem(string taskId, string outcome, RequiredField[] reqFields = null)
        {
            var client = new TicketToolServiceClient();

            var requiredFieldsLength = reqFields != null ? reqFields.Length : 0;
            var reqFieldsConverted = new Proxy.TicketToolService.RequiredField[requiredFieldsLength];
            if (reqFields != null)
            {
                for (int i = 0; i < reqFields.Length; i++)
                {
                    reqFieldsConverted[i].Value = reqFields[i].Value;
                    reqFieldsConverted[i].DefaultValue = reqFields[i].DefaultValue;
                    reqFieldsConverted[i].Name = reqFields[i].Name;
                    reqFieldsConverted[i].Type = reqFields[i].Type.ToString();
                }
            }

            var result = client.CompleteTask(taskId, outcome, reqFieldsConverted);
            if (result.HasError) throw new Exception(result.MessageText);

            return true;
        }
    }
}