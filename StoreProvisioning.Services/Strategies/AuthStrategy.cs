﻿using StoreProvisioning.Services.Model;
using StoreProvisioning.Services.Proxy.TicketToolAuth;
using StoreProvisioning.Services.Strategies.Contracts;

namespace StoreProvisioning.Services.Strategies
{
    public class AuthStrategy : StrategyBase, IAuthStrategy
    {
        public AuthStrategy(IDataFacade dataFacade) : base(dataFacade)
        {
        }

        public OperationResult<bool> Login(string userName, string password, bool remeberMe)
        {
            return Try(() => new AuthenticationServiceClient().LogIn(userName, password, remeberMe));
        }

        public OperationResult<User> GetCurrentUser()
        {
            return Try(() => new AuthenticationServiceClient().GetCurrentUser().ToModelUser());
        }

        public void Logout()
        {
            new AuthenticationServiceClient().LogOut();
        }

        public OperationResult<bool> LoginByToken(string userName, string userToken, string taskId, string taskToken)
        {
            return Try(() => new AuthenticationServiceClient().LoginByToken(userName, userToken, taskId, taskToken));
        }
    }
}