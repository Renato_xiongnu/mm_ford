﻿using System;
using System.Reflection;
using log4net;
using StoreProvisioning.Services.Model;

namespace StoreProvisioning.Services.Strategies
{
    public abstract class StrategyBase
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected StrategyBase(IDataFacade dataFacade)
        {
            DataFacade = dataFacade;
        }

        protected IDataFacade DataFacade { get; set; }

        protected static OperationResult<T> Try<T>(Func<T> func)
        {
            try
            {
                return new OperationResult<T>(func());
            }
            catch (Exception e)
            {
                Log.Error(e);
                return new OperationResult<T>(default(T), e.Message);
            }
        }
    }
}