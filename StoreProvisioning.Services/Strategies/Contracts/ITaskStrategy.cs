﻿using System.Collections.Generic;
using StoreProvisioning.Services.Model;
using StoreProvisioning.Services.Proxy.TicketToolService;
using Parameter = StoreProvisioning.Services.Model.Parameter;

namespace StoreProvisioning.Services.Strategies.Contracts
{
    public interface ITaskStrategy
    {
        OperationResult<IEnumerable<TaskItem>> GetTasksInProgress(string[] parameters);

        OperationResult<Parameter[]> GetParameters();

        OperationResult<TaskItem> GetTask(string taskId);

        OperationResult<bool> SaveTaskOutcome(TaskExecutionResult saveTaskRequest);

        OperationResult<bool> FinishTaskLater(string taskId);

        OperationResult<AssignTaskForMeResult> AssignTaskForMe(string taskId);
    }    
}
