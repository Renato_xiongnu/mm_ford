﻿using StoreProvisioning.Services.Model;

namespace StoreProvisioning.Services.Strategies.Contracts
{
    public interface IAuthStrategy
    {
        OperationResult<bool> Login(string userName, string password, bool remeberMe);
        OperationResult<User> GetCurrentUser();
        void Logout();
        OperationResult<bool> LoginByToken(string userName, string userToken, string taskId, string taskToken);
    }
}
