﻿using System;
using System.Collections.Generic;
using System.Management.Instrumentation;
using System.Web;
using StoreProvisioning.Services.Properties;

namespace StoreProvisioning.Services.Common
{
    public class HttpCache
    {
        private readonly Dictionary<string, object> _actions;
        private readonly Dictionary<string, DateTime> _cachedTime;

        public HttpCache()
        {
            _actions = new Dictionary<string, object>();
            _cachedTime = new Dictionary<string, DateTime>();
        }

        public void Cache<T>(Func<T> action, string key, int ttl)
        {
            _actions[key] = action;
            CacheInternal<T>(key, ttl);
        }

        private T CacheInternal<T>(string key, int ttl)
        {            
            _cachedTime[key] = DateTime.Now;
            var objectToCache = ((Func<T>)_actions[key])();
            HttpContext.Current.Cache[key] = new CachedObject<T> { TTL = ttl, Item = objectToCache }; 
            return objectToCache;
        }

        public T GetCachedObject<T>(string key)
        {
            var result = HttpContext.Current.Cache[key] as CachedObject<T>;
            if (result == null)
            {
                if (_actions.ContainsKey(key))
                {
                    return CacheInternal<T>(key, Settings.Default.TTL);
                }
                throw new InstanceNotFoundException();
            }
            return _cachedTime[key].AddSeconds(result.TTL) < DateTime.Now 
                ? CacheInternal<T>(key, result.TTL) 
                : result.Item;
        }
    }
}