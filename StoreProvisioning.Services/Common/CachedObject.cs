﻿namespace StoreProvisioning.Services.Common
{
    public class CachedObject<T>
    {
        public int TTL { get; set; }
        public T Item { get; set; }
    }
}