﻿namespace StoreProvisioning.Services.Model
{
    public enum RequiredFieldType
    {
        DateTime,
        TimeSpan,
        Boolean,
        String,
        Int32,
        Decimal,
        Double,
        Choice
    }
}