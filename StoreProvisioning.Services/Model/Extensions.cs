﻿namespace StoreProvisioning.Services.Model
{
    internal static class Extensions
    {
        public static User ToModelUser(this Proxy.TicketToolAuth.UserInfo user)
        {
            return new User
                {
                    Login = user.LoginName,
                    Email = user.Email,
                    Roles = user.Roles,
                    Parameters = user.Parameters,
                    Authorized = user.IsAuthtorized
                };
        }
    }
}
