namespace StoreProvisioning.Services.Model
{
    public class TaskExecutionResult
    {
        public string TaskId { get; set; }

        public string Outcome { get; set; }
        public string Comments { get; set; }
    }
}