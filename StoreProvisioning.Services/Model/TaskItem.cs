﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace StoreProvisioning.Services.Model
{
    public class TaskItem
    {
        public int TaskId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeadlineDate { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsAssignedOnMe { get; set; }
        public string AssignedOn { get; set; }
        public bool Escaleted { get; set; }
        public StoreItem Store { get; set; }
        public string Description { get; set; }

        public string WorkItemId { get; set; }

        public string OrderId { get; set; }
        public string WWSOrderId { get; set; }

        public ICollection<Outcome> Outcomes { get; set; }
        public ICollection<RequiredField> RequiredFields { get; set; }
    }

    public class Outcome
    {
        public string Value { get; set; }
        public ICollection<RequiredField> RequiredFields { get; set; }
    }

    public class RequiredField
    {
        public string DefaultValue { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string TypeName { get; set; }
        public RequiredFieldType Type { get; set; }

        public ICollection<SelectListItem> PredefinedValues { get; set; }
    }
}