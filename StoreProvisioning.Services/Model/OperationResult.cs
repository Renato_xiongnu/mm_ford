﻿namespace StoreProvisioning.Services.Model
{
    public class OperationResult<T>
    {
        public OperationResult(T item)
        {
            Item = item;
        }

        public OperationResult(T item, string message)
            : this(item)
        {
            HasErrors = true;
            Message = message;
        }

        public static OperationResult<T> Error(string message)
        {
            return new OperationResult<T>(default(T), message);
        }

        public bool HasErrors { get; private set; }
        public string Message { get; private set; }
        public T Item { get; private set; }
    }
}