﻿using StoreProvisioning.Services.Common;
using StoreProvisioning.Services.Proxy.ProductService;
using StoreProvisioning.Services.Strategies;
using StoreProvisioning.Services.Strategies.Contracts;

namespace StoreProvisioning.Services
{
    public class DataFacade : IDataFacade
    {
        private ITaskStrategy _tasks;
        private IAuthStrategy _auth;        
        
        public DataFacade(HttpCache cache)
        {            
            Cache = cache;
        }

        public HttpCache Cache { get; private set; }

        public ITaskStrategy Tasks
        {
            get { return _tasks ?? (_tasks = new TaskStrategy(this)); }
        }
        
        public IAuthStrategy Auth
        {
            get { return _auth ?? (_auth = new AuthStrategy(this)); }
        }
    }
}