﻿using StoreProvisioning.Services.Strategies.Contracts;

namespace StoreProvisioning.Services
{
    public interface IDataFacade
    {
        ITaskStrategy Tasks { get; }
        IAuthStrategy Auth { get; }
    }
}