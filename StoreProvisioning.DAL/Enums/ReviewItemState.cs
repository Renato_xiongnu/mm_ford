namespace StoreProvisioning.DAL.Enums
{
    public enum ReviewItemState
    {
        Empty,
        Reserved,
        ReservedPart,
        ReservedDisplayItem,
        NotFound,
        IncorrectPrice,
        Transfer,
        DisplayItem,
    }
}