﻿using StoreProvisioning.DAL.Enums;

namespace StoreProvisioning.DAL.Models.Content
{
    public class ReturnItemsContent : AdditionalContent
    {
        public string CustomerOrderId { get; set; }
        public string WwsOrderId { get; set; }
    }
}