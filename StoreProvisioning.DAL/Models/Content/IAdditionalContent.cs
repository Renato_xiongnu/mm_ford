﻿namespace StoreProvisioning.DAL.Models.Content
{
    public interface IAdditionalContent
    {
        int ContentId { get; set; }
    }
}