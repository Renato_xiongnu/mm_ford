﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreProvisioning.DAL.Models.Content
{
    public class AdditionalContent : IAdditionalContent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContentId { get; set; }

        public string Comments { get; set; }
    }
}
