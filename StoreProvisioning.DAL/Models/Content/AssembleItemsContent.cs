﻿namespace StoreProvisioning.DAL.Models.Content
{
    public class AssembleItemsContent : AdditionalContent
    {
        public string PaymentType { get; set; }
        public string CustomerFullName { get; set; }
        public string CustomerEmail { get; set; }
        public string DeliveryCity { get; set; }
        public string DeliveryAddress { get; set; }
    }
}