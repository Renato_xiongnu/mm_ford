﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreProvisioning.DAL.Models
{
    public class OrderLineResult
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderLineResultId { get; set; }

        public bool SuccesfullyFulfilled { get; set; }
        public string ReviewStatus { get; set; }
        public string ItemStateDescription { get; set; }
    }
}