﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StoreProvisioning.DAL.Models.Content;

namespace StoreProvisioning.DAL.Models
{
    public class MoveItemsCommand
    {
        [Key]
        public string RequestId { get; set; } //Comes from Cloud

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public string CustomerOrderId { get; set; }
        public string WwsOrderId { get; set; }
        
        public string Outcome { get; set; }

        public AdditionalContent AdditionalContent { get; set; }
        public IList<OrderLine> OrderLines { get; set; }

        [NotMapped]
        public string AdditionalContentType { get { return AdditionalContent.GetType().Name; } }
    }
}