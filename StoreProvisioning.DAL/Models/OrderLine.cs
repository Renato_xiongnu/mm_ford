﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace StoreProvisioning.DAL.Models
{
    public class OrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderLineId { get; set; }

        public long ArticleId { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public string Title { get; set; }
        public OrderLineResult OrderLineResult { get; set; }

        [NotMapped]
        public int WwsDepartmentNo { get; set; }
        [NotMapped]
        public int WWSStockNumber { get; set; }
        [NotMapped]
        public string ProductGroup { get; set; }
        [NotMapped]
        public int ProductGroupNo { get; set; }
        [NotMapped]
        public string AppliedCampaigns { get; set; }
        public string LineComment { get; set; }
        [NotMapped]
        public int Index { get; set; }
        [NotMapped]
        public string DepartmentTotal
        {
            get
            {
                return string.Format("Отдел {0}; ТГ {1} ({2})", WwsDepartmentNo, ProductGroupNo,
                    ProductGroup);
            }
        }
        
        [NotMapped]
        public IList<SelectListItem> OrderLineAnswers { get; set; }
    }
}