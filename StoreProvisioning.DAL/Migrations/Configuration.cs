namespace StoreProvisioning.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<StoreProvisioning.DAL.Context.MoveItemsCommandContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StoreProvisioning.DAL.Context.MoveItemsCommandContext context)
        {
        }
    }
}
