namespace StoreProvisioning.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommandItems",
                c => new
                    {
                        RequestId = c.String(nullable: false, maxLength: 128),
                        CustomerOrderId = c.String(),
                        WwsOrderId = c.String(),
                        MoveFrom = c.Int(nullable: false),
                        MoveTo = c.Int(nullable: false),
                        AdditionalContent_ContentId = c.Int(),
                    })
                .PrimaryKey(t => t.RequestId)
                .ForeignKey("dbo.AdditionalContents", t => t.AdditionalContent_ContentId)
                .Index(t => t.AdditionalContent_ContentId);
            
            CreateTable(
                "dbo.AdditionalContents",
                c => new
                    {
                        ContentId = c.Int(nullable: false, identity: true),
                        Outcome = c.String(),
                        Comments = c.String(),
                        PaymentType = c.String(),
                        CustomerFullName = c.String(),
                        CustomerEmail = c.String(),
                        DeliveryCity = c.String(),
                        DeliveryAddress = c.String(),
                        CustomerOrderId = c.String(),
                        WwsOrderId = c.String(),
                        ReturnFrom = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ContentId);
            
            CreateTable(
                "dbo.OrderLineRevieweds",
                c => new
                    {
                        OrderLineId = c.Int(nullable: false, identity: true),
                        SuccesfullyFulfilled = c.Boolean(nullable: false),
                        ReviewStatus = c.String(),
                        ItemStateDescription = c.String(),
                        ArticleId = c.Long(nullable: false),
                        Qty = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Title = c.String(),
                        WwsDepartmentNo = c.Int(nullable: false),
                        WWSStockNumber = c.Int(nullable: false),
                        WWSFreeQty = c.Int(nullable: false),
                        WWSPriceOrig = c.String(),
                        ProductGroup = c.String(),
                        ProductGroupNo = c.Int(nullable: false),
                        AppliedCampaigns = c.String(),
                        LineComment = c.String(),
                        CommandItem_RequestId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderLineId)
                .ForeignKey("dbo.CommandItems", t => t.CommandItem_RequestId)
                .Index(t => t.CommandItem_RequestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLineRevieweds", "CommandItem_RequestId", "dbo.CommandItems");
            DropForeignKey("dbo.CommandItems", "AdditionalContent_ContentId", "dbo.AdditionalContents");
            DropIndex("dbo.OrderLineRevieweds", new[] { "CommandItem_RequestId" });
            DropIndex("dbo.CommandItems", new[] { "AdditionalContent_ContentId" });
            DropTable("dbo.OrderLineRevieweds");
            DropTable("dbo.AdditionalContents");
            DropTable("dbo.CommandItems");
        }
    }
}
