namespace StoreProvisioning.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _100 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MoveItemsCommand",
                c => new
                    {
                        RequestId = c.String(nullable: false, maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CustomerOrderId = c.String(),
                        WwsOrderId = c.String(),
                        Outcome = c.String(),
                        StockLocationId = c.String(),
                        MoveFrom = c.Int(nullable: false),
                        MoveTo = c.Int(nullable: false),
                        AdditionalContent_ContentId = c.Int(),
                    })
                .PrimaryKey(t => t.RequestId)
                .ForeignKey("dbo.AdditionalContent", t => t.AdditionalContent_ContentId)
                .Index(t => t.AdditionalContent_ContentId);
            
            CreateTable(
                "dbo.AdditionalContent",
                c => new
                    {
                        ContentId = c.Int(nullable: false, identity: true),
                        Comments = c.String(),
                        PaymentType = c.String(),
                        CustomerFullName = c.String(),
                        CustomerEmail = c.String(),
                        DeliveryCity = c.String(),
                        DeliveryAddress = c.String(),
                        CustomerOrderId = c.String(),
                        WwsOrderId = c.String(),
                        ReturnFrom = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ContentId);
            
            CreateTable(
                "dbo.OrderLine",
                c => new
                    {
                        OrderLineId = c.Int(nullable: false, identity: true),
                        ArticleId = c.Long(nullable: false),
                        Qty = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Title = c.String(),
                        LineComment = c.String(),
                        OrderLineResult_OrderLineResultId = c.Int(),
                        MoveItemsCommand_RequestId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderLineId)
                .ForeignKey("dbo.OrderLineResult", t => t.OrderLineResult_OrderLineResultId)
                .ForeignKey("dbo.MoveItemsCommand", t => t.MoveItemsCommand_RequestId)
                .Index(t => t.OrderLineResult_OrderLineResultId)
                .Index(t => t.MoveItemsCommand_RequestId);
            
            CreateTable(
                "dbo.OrderLineResult",
                c => new
                    {
                        OrderLineResultId = c.Int(nullable: false, identity: true),
                        SuccesfullyFulfilled = c.Boolean(nullable: false),
                        ReviewStatus = c.String(),
                        ItemStateDescription = c.String(),
                    })
                .PrimaryKey(t => t.OrderLineResultId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLine", "MoveItemsCommand_RequestId", "dbo.MoveItemsCommand");
            DropForeignKey("dbo.OrderLine", "OrderLineResult_OrderLineResultId", "dbo.OrderLineResult");
            DropForeignKey("dbo.MoveItemsCommand", "AdditionalContent_ContentId", "dbo.AdditionalContent");
            DropIndex("dbo.OrderLine", new[] { "MoveItemsCommand_RequestId" });
            DropIndex("dbo.OrderLine", new[] { "OrderLineResult_OrderLineResultId" });
            DropIndex("dbo.MoveItemsCommand", new[] { "AdditionalContent_ContentId" });
            DropTable("dbo.OrderLineResult");
            DropTable("dbo.OrderLine");
            DropTable("dbo.AdditionalContent");
            DropTable("dbo.MoveItemsCommand");
        }
    }
}
