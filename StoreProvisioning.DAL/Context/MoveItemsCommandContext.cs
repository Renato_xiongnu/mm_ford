﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using StoreProvisioning.DAL.Models;

namespace StoreProvisioning.DAL.Context
{
    public class MoveItemsCommandContext : DbContext
    {
        public MoveItemsCommandContext()
            : base("CommandDBConnectionString")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MoveItemsCommandContext>());
        }

        public DbSet<MoveItemsCommand> MoveItemsCommands { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}