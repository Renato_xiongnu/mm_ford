﻿using System.ComponentModel;
using Hangfire;
using NLog;
using Autofac;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Application.SF.Hangfire
{
    [AutomaticRetry(Attempts = 100, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
    public class CreateTaskJob
    {
        private readonly ILifetimeScope _container;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CreateTaskJob(ILifetimeScope container)
        {
            _container = container;
        }

        [DisplayName("Create call back later task, planStepId {0}, ticketId {1}")]
        public void CreateTask(int planStepId, string ticketId)
        {
            using (var sc = _container.BeginLifetimeScope())
            {
                var storage = sc.Resolve<IStoreFulfillmentStorage>();
                var mb = sc.Resolve<IMessageBus>();

                var planStep = storage.GetPlanStepById(planStepId);

                var req = storage.GetByTicketId(ticketId);

                var currentPlanStep = req.CurrentPlanStep();
                if (req.Status == RequestStatus.Executing && currentPlanStep != null && currentPlanStep.Id == planStepId)
                {
                    var cmd = new CreateTaskCommand(planStep.RequestId, planStep.Id,
                        ((TaskPlanStep) planStep).TaskType);

                    mb.SendCommand(cmd);
                }
                else
                {
                    _logger.Warn(
                        "{0} {1} Callback Later task will not be created for planStep {2} - Request status is {3}, current plan step is {4}",
                        req.CustomerOrder.CustomerOrderId, req.RequestId, planStepId, req.Status,
                        currentPlanStep != null ? currentPlanStep.Id.ToString() : string.Empty);
                }

            }
        }
    }
}

