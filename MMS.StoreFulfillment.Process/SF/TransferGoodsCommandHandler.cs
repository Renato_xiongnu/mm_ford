﻿using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using Newtonsoft.Json;
using CustomerOrderInfo = MMS.StoreFulfillment.Entities.SF.Items.CustomerOrderInfo;
using Item = MMS.StoreFulfillment.Entities.SF.Items.Item;

namespace MMS.StoreFulfillment.Application.SF
{
    public class TransferGoodsCommandHandler:ICommandHandler<InternalCloudCommandWrapper<TransferGoods>>
    {
        private readonly IExecutionPlanner _executionPlanner;
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;

        public TransferGoodsCommandHandler(IExecutionPlanner executionPlanner, IStoreFulfillmentStorage storeFulfillmentStorage)
        {
            _executionPlanner = executionPlanner;
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public SendCommandResult Handle(InternalCloudCommandWrapper<TransferGoods> commandWrapped)
        {
            var command = commandWrapped.Command;
            var requestBuilder = new RequestBuilder(command.SourceSapCode, RequestType.TransferGoods, commandWrapped.FulfillmentOrderId);
            var contact = new CustomerContact
            {
                AdditionalPhone = command.Contact.AdditionalPhone,
                Email = command.Contact.Email,
                FirstName = command.Contact.FirstName,
                LastName = command.Contact.LastName,
                Phone = command.Contact.Phone
            };
            var customerOrderInfo = new CustomerOrderInfo
            {
                CustomerOrderId = command.CustomerOrderId
            };

            requestBuilder.WithCustomerContact(contact);
            requestBuilder.WithOrderInfo(customerOrderInfo);
            requestBuilder.WithItems(command.Items.Select(el => new Item(el.Article, el.Price, el.Qty, el.Title, el.ItemId) { ReserveStatus = ReserveStatuses.Empty, ArticleProductType = el.ArticleProductType }).ToArray());
            requestBuilder.WithBasedCommand(JsonConvert.SerializeObject(commandWrapped.CommandKey),
                commandWrapped.CommandName);

            var transferGoodsRequest = requestBuilder.Build();

            var plan = _executionPlanner.PlanExecution(transferGoodsRequest);
            transferGoodsRequest.AssignPlan(plan);

            _storeFulfillmentStorage.Save(transferGoodsRequest);

            return SendCommandResult.Completed;
        }
    }
}
