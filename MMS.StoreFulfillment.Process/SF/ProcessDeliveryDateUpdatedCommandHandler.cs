﻿using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.SF
{
    public class ProcessDeliveryDateUpdatedCommandHandler : ICommandHandler<ProcessDeliveryDateUpdatedCommand>
    {
        private readonly IClientCommandManager _clientCommandManager;

        public ProcessDeliveryDateUpdatedCommandHandler(IClientCommandManager clientCommandManager)
        {
            _clientCommandManager = clientCommandManager;
        }

        public SendCommandResult Handle(ProcessDeliveryDateUpdatedCommand command)
        {
            _clientCommandManager.CreateCommand("FulfillOrderProcessingInfo",
                new FulfillOrderProcessingInfo
                {
                    OrderId = command.OrderId,
                    Status = FulfillOrderProcessingInfoStatuses.NewDeliveryDateConfirmed,
                    RequestId = command.RequestId,
                    AdditionalInfo = new FulfillOrderAdditionalInfo
                    {
                        ExpectedDeliveryDate = command.NewDeliveryDate
                    }
                }, command.OrderId,
                JsonConvert.DeserializeObject<Key>(command.BasedOnCommandId));

            return SendCommandResult.Completed;
        }
    }
}
