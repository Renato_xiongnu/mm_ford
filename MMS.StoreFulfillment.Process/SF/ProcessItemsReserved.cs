﻿using System;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.SF
{
    public class ProcessItemsReservedCommandHandler : ICommandHandler<ProcessItemsReservedCommand>
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;
        private readonly IClientCommandManager _clientCommandManager;

        public ProcessItemsReservedCommandHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IClientCommandManager clientCommandManager)
        {
            _clientCommandManager = clientCommandManager;
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public SendCommandResult Handle(ProcessItemsReservedCommand commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetById(commandWrapped.RequestId);

            _clientCommandManager.CreateCommand("FulfillOrderProcessingInfo",
                new FulfillOrderProcessingInfo
                {
                    OrderId = request.CustomerOrder.CustomerOrderId,
                    Status = FulfillOrderProcessingInfoStatuses.AllResourcesReserved,
                    RequestId = request.RequestId,
                    AdditionalInfo = new FulfillOrderAdditionalInfo
                    {
                        ExpectedDeliveryDate =
                            (request.CustomerOrder.DeliveryType == DeliveryType.Delivery &&
                             request.Contact.RequestedDeliveryDate.HasValue)
                                ? request.Contact.RequestedDeliveryDate
                                : DateTime.UtcNow.Add(Configurations.DefaultReserveLifiteme),
                        Comments = request.Comments
                    }
                }, request.CustomerOrder.CustomerOrderId,
                JsonConvert.DeserializeObject<Key>(request.BasedOnCommandId));

            return SendCommandResult.Completed;
        }
    }
}
