﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.Planning;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;

namespace MMS.StoreFulfillment.Application.SF
{
    public class ProcessItemsMovementCompletionCommandHandler : ICommandHandler<ProcessItemsMovementCompletionCommand>
    {
        private readonly IClientCommandManager _clientCommandManager;
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;
        private readonly IFulfillmentOrderRepository _fulfillmentOrderStorage;
        private readonly FulfillmentPlanner _fulfillmentPlanner;

        public ProcessItemsMovementCompletionCommandHandler(IClientCommandManager clientCommandManager, IStoreFulfillmentStorage storeFulfillmentStorage, IFulfillmentOrderRepository fulfillmentOrderStorage, IFulfillmentOrderRepository fulfillmentOrderStorage1, FulfillmentPlanner fulfillmentPlanner)
        {
            _clientCommandManager = clientCommandManager;
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _fulfillmentOrderStorage = fulfillmentOrderStorage1;
            _fulfillmentPlanner = fulfillmentPlanner;
        }

        public SendCommandResult Handle(ProcessItemsMovementCompletionCommand commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.Items });
            if (!string.IsNullOrEmpty(request.BasedOnCommand))
            {
                switch (request.BasedOnCommand)
                {
                    //Вызывать SaveSaveLocationOutcome по аналогии с процессом
                    case "FulfillOrder":
                        {
                            if (commandWrapped.IsSuccesfull)
                            {
                                ProcessSucessfullCompletion(commandWrapped, request);
                            }
                            else
                            {
                                ProcessUnsucesfullCompletion(commandWrapped, request);
                            }
                            break;
                        }
                    case "TransferGoods":
                        {
                            if (commandWrapped.IsSuccesfull)
                            {
                                var fulfillmentOrder = _fulfillmentOrderStorage.GetById(request.FulfillmentOrderId);
                                foreach (var item in request.Items)
                                {
                                    var fulfillmentItem = fulfillmentOrder.FulfillmentItems.First(t => t.ItemExternalId == item.ItemExternalId);
                                    fulfillmentItem.IncomingTransfer = item.IncomingTransfer;
                                }
                                fulfillmentOrder.NotifyStepCompleted();
                                _fulfillmentOrderStorage.Save(fulfillmentOrder);
                            }
                            else
                            {
                                ProcessUnsucesfullCompletion(commandWrapped, request);
                            }
                            break;
                        }
                    case "CancelReserveProducts":
                        {
                            //It's sync command, so do nothing
                            break;
                        }
                    case "CancelFulfillOrder":
                        {
                            //It's sync command, so do nothing
                            break;
                        }
                    default:
                        {
                            throw new ArgumentOutOfRangeException("commandWrapped", "request.BasedOnCommand is out of range");
                        }
                }
            }

            return SendCommandResult.Completed;
        }

        private void ProcessUnsucesfullCompletion(ProcessItemsMovementCompletionCommand commandWrapped,
            ItemsMovementRequest request)
        {
            if (string.IsNullOrEmpty(request.FulfillmentOrderId))
            {
                _clientCommandManager.CreateResponse(
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Key>(request.BasedOnCommandId),
                    new FulfillOrderResponse
                    {
                        Result = Result.Failed,
                        ResultReason = commandWrapped.IncidentCode,
                        Comment = request.Comments,
                        Items = request.Items.Select(el => new ReservedItem
                        {
                            ItemId = el.ItemExternalId,
                            ReserveStatus = el.ReserveStatus,
                            ReservedSucesfully =
                                string.Equals(el.ReserveStatus, ReserveStatuses.Reserved)
                        }).ToArray(),
                        Incidents =
                            new List<Incident>
                            {
                                new Incident
                                {
                                    IncidentCode = commandWrapped.IncidentCode,
                                    IncidentDescription = commandWrapped.IncidentCode
                                }
                            }
                    }, request.CustomerOrder.CustomerOrderId);
            }
            else
            {
                var fulfillmentOrder = _fulfillmentOrderStorage.GetById(request.FulfillmentOrderId);
                fulfillmentOrder.NotifyStepFailed();

                switch(commandWrapped.IncidentCode)
                {
                    case IncidentCode.CreateTransferFail:
                    case IncidentCode.WwsReserveCreationFailed:
                        //case IncidentCode.NotAllItemsReserved:
                        fulfillmentOrder.Fatal(ResultReasons.NotAllItemsReserved, commandWrapped.CompletionReason);
                        break;
                    case IncidentCode.WwsReserveCanceled:
                        fulfillmentOrder.Fatal(IncidentCode.WwsReserveCanceled, commandWrapped.CompletionReason);
                        break;
                    default:
                        {
                            try
                            {
                                var fulfillmentPlan = _fulfillmentPlanner.PlanFulfillment(fulfillmentOrder);
                                fulfillmentOrder.AttachPlan(fulfillmentPlan);
                            }
                            catch (PlanGenerationException ex)
                            {
                                fulfillmentOrder.Fatal(ResultReasons.NotAllItemsReserved, ex.Message);
                            }
                        }
                        break;
                }
                _fulfillmentOrderStorage.Save(fulfillmentOrder);
            }
        }

        private void ProcessSucessfullCompletion(ProcessItemsMovementCompletionCommand commandWrapped,
            ItemsMovementRequest request)
        {
            if (string.IsNullOrEmpty(request.FulfillmentOrderId)) //TODO Only for backward compatibility for one deploy
            {
                _clientCommandManager.CreateResponse(
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Key>(request.BasedOnCommandId),
                    new FulfillOrderResponse
                    {
                        Result = Result.Done,
                        Comment = request.Comments,
                        ResultReason = commandWrapped.CompletionReason,
                    }, request.CustomerOrder.CustomerOrderId);
            }
            else
            {
                var fulfillmentOrder = _fulfillmentOrderStorage.GetById(request.FulfillmentOrderId);
                fulfillmentOrder.NotifyStepCompleted();
                _fulfillmentOrderStorage.Save(fulfillmentOrder);
            }
        }
    }
}
