﻿using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.SF
{
    public class FulfillOrderFromTransferCommandHandler : ICommandHandler<InternalCloudCommandWrapper<FulfillOrderFromTransfer>>
    {
        private readonly IStoreFulfillmentStorage _requestRepository;
        private readonly IExecutionPlanner _executionPlanner;

        public FulfillOrderFromTransferCommandHandler(IStoreFulfillmentStorage requestRepository, IExecutionPlanner executionPlanner)
        {
            _requestRepository = requestRepository;
            _executionPlanner = executionPlanner;
        }

        public SendCommandResult Handle(InternalCloudCommandWrapper<FulfillOrderFromTransfer> commandWrapped)
        {
            var command = commandWrapped.Command.SourceCommand;
            var requestBuilder = new RequestBuilder(command.SapCode, RequestType.ReserveProducts, commandWrapped.FulfillmentOrderId);
            var contact = new CustomerContact
            {
                AdditionalPhone = command.Contact.AdditionalPhone,
                Address = command.CustomerOrder.Address,
                City = command.CustomerOrder.City,
                Email = command.Contact.Email,
                FirstName = command.Contact.FirstName,
                LastName = command.Contact.LastName,
                Phone = command.Contact.Phone,
                ZIPCode = command.CustomerOrder.ZipCode,
                Floor = command.CustomerOrder.Floor,
                RequestedDeliveryServiceOption = command.CustomerOrder.DeliveryService,
                RequestedDeliveryTimeslot = command.CustomerOrder.DeliveryTimeslot,
                RequestedDeliveryDate = command.CustomerOrder.RequestedDeliveryDate
            };
            var customerOrderInfo = new CustomerOrderInfo
            {
                CustomerOrderId = command.CustomerOrderId,
                Downpayment = command.CustomerOrder.Downpayment,
                IsOnlineOrder = command.CustomerOrder.IsOnlineOrder,
                OrderSource = command.CustomerOrder.OrderSource,
                PaymentType = command.CustomerOrder.PaymentType,
                ShippingMethod = command.CustomerOrder.ShippingMethod,
                SocialCardNumber = command.CustomerOrder.SocialCardNumber
            };

            requestBuilder.WithCustomerContact(contact);
            requestBuilder.WithOrderInfo(customerOrderInfo);
            requestBuilder.WithItems(commandWrapped.Command.FulfillmentItems);
            requestBuilder.WithBasedCommand(JsonConvert.SerializeObject(commandWrapped.CommandKey),
                commandWrapped.CommandName);

            var reserveProductsRequest = requestBuilder.Build();

            var plan = _executionPlanner.PlanExecution(reserveProductsRequest);
            reserveProductsRequest.AssignPlan(plan);

            _requestRepository.Save(reserveProductsRequest);

            return SendCommandResult.Completed;
        }
    }
}
