﻿using MMS.Cloud.Shared.Commands;

namespace MMS.StoreFulfillment.Application.SF
{
    public class CloudCommandWrapper<T>
    {
        public T Command { get; private set; }

        public Key CommandKey { get; private set; }

        public string CommandName { get; private set; }
        
        public CloudCommandWrapper(T command, Key commandKey, string commandName)
        {
            CommandKey = commandKey;
            CommandName = commandName;
            Command = command;
        }
    }

    public class InternalCloudCommandWrapper<T>
    {
        public T Command { get; private set; }

        public Key CommandKey { get; private set; }

        public string CommandName { get; private set; }

        public string FulfillmentOrderId { get; private set; }

        public InternalCloudCommandWrapper(T command, Key commandKey, string commandName, string fulfillmentOrderId)
        {
            CommandKey = commandKey;
            CommandName = commandName;
            Command = command;
            FulfillmentOrderId = fulfillmentOrderId;
        }
    }
}
