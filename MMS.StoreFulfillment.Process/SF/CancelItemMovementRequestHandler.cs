﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MMS.Cloud.Shared.Commands;
using MMS.Json;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Compensation;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using NLog;

namespace MMS.StoreFulfillment.Application.SF
{
    public class CancelItemMovementRequestHandler:ICommandHandler<CloudCommandWrapper<CancelItemMovementRequest>>
    {
        private readonly IStoreFulfillmentStorage _storage;
        private readonly IMessageBus _messageBus;
        private readonly ICompensationEngine _compensationEngine;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CancelItemMovementRequestHandler(IStoreFulfillmentStorage storage, IMessageBus messageBus, ICompensationEngine compensationEngine)
        {
            _storage = storage;
            _messageBus = messageBus;
            _compensationEngine = compensationEngine;
        }

        public SendCommandResult Handle(CloudCommandWrapper<CancelItemMovementRequest> commandWrapped)
        {
            var command = commandWrapped.Command;

            var request = _storage.GetById(command.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[]
                {t => t.PlanSteps.Select(q => q.TicketTask), t => t.Items});

            if (request.CancellationAllowed)
            {
                CompensateRequest(request, commandWrapped.CommandKey, commandWrapped.CommandName);
                request.Cancel(_messageBus, IncidentCode.CanceledExtrernally, "Cancelled externally");

                _storage.Save(request);
            }
            else if (request.CompensationAllowed)
            {
                CompensateRequest(request, commandWrapped.CommandKey, commandWrapped.CommandName);
                request.MarkCompensated();

                _storage.Save(request);
            }
            else
            {
                _logger.Warn("{0} {1} Ignore cancelling request, because it's status is {2}",
                    request.CustomerOrder.CustomerOrderId, request.RequestId, request.Status);
            }

            return SendCommandResult.Completed;
        }

        private void CompensateRequest(ItemsMovementRequest request, Key commandKey, string commandName)
        {
            _compensationEngine.Compensate(request, JsonConvert.SerializeObject(commandKey), commandName);
        }
    }
}
