﻿using System;
using System.Linq;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.Cloud.Commands.SF;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class CreateTransferTaskHandler : TaskHandlerBase
    {
        private readonly ITransferStoresDataProvider _transferStoresProvider;
        public const string CreateTransferOutcome = "Сделал трансфер";
        public const string BadTransferOutcome = "Невозможно создать трансфер";
        public const string TransferNumberField = "Номер заказ-трансфера";
        public const string SapCodeField = "Магазин (SapCode)";

        public CreateTransferTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus, ITransferStoresDataProvider transferStoresProvider)
            : base(storeFulfillmentStorage, messageBus)
        {
            _transferStoresProvider = transferStoresProvider; 
        }

        //, PlanStep planStep
        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            // Получаем магазины для трансферов по request.SapCode - должен быть виртуальным
            var deliveryStores = _transferStoresProvider.GetAvailableStoresForTransfer(request.SapCode);

            return new TicketTaskDescription
            {
                Name = "Создай трансфер",
                Description = @"Выберете магазин откуда будет отправлен трансфер, узнайте среднее нетто (цену закупки) 
                              для товаров, которые будет отправлены трансфером из этого магазине. 
                              Создайте заказ-трансфер в магазине, в котором покупатель хочет получить заказ. 
                              Укажите САП номер магазина откуда был сделан трансфер 
                              и укажите номер заявки из WWS 
                              в поле номер заказ-трансфера на форме. Будьте внимательны при вводе САП-кода магазина",
                Type = TicketTaskTypes.CreateTransfer,
                Outcomes = new[]
                {
                    new OutcomeDescription(CreateTransferOutcome,
                        new[]
                        {
                            new RequiredFieldDescription(TransferNumberField, typeof (string)),
                            new RequiredFieldDescription(SapCodeField, typeof (string))
                            {
                                ExtendedValues = deliveryStores.Select(s => s.SapCode).ToList()
                            }
                        }),
                    new OutcomeDescription(BadTransferOutcome),
                    new OutcomeDescription("Перезвонить позже", new[]
                    {
                        new RequiredFieldDescription("Перезвоните мне через", typeof (TimeSpan))
                    })
                    //new OutcomeDescription("Отклонено покупателем", new[]
                    //{
                    //    new RequiredFieldDescription("Причина отказа", typeof (string))
                    //    {
                    //        ExtendedValues = new List<string>() {"Невозможно создать трансфер"}
                    //    }
                    //})
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == CreateTransferOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
                                
                var incomingTransfer = commandWrapped.RequiredFields
                    .First(t => t.Key == string.Format("{0};#{1}", CreateTransferOutcome, TransferNumberField)).Value.ToString();
                var storeSapCode = commandWrapped.RequiredFields
                    .First(t => t.Key == string.Format("{0};#{1}", CreateTransferOutcome, SapCodeField)).Value.ToString();

                //TODO: Hack! Update SapCode in request
                request.SapCode = storeSapCode;

                foreach (var item in request.Items)
                {
                    item.IncomingTransfer = incomingTransfer;
                }
            }
            else if (commandWrapped.Outcome == BadTransferOutcome)
            {
                request.MarkStepFailed(planStep.Id);
                request.Cancel(MessageBus, IncidentCode.CreateTransferFail, BadTransferOutcome);
            }
            else
            {
                throw new NotImplementedException();
            }

            StoreFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }
}
