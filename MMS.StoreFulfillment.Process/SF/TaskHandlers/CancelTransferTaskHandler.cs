﻿using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{    
    public class CancelTransferTaskHandler : TaskHandlerBase
    {
        private readonly ITransferStoresDataProvider _transferStoresProvider;
        public const string CancelTransferOutcome = "Отменил";


        public CancelTransferTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus, ITransferStoresDataProvider transferStoresProvider)
            : base(storeFulfillmentStorage, messageBus)
        {
            _transferStoresProvider = transferStoresProvider;
        }

        //, PlanStep planStep
        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            // Получаем магазины для трансферов по request.SapCode - должен быть виртуальным
            var deliveryStores = _transferStoresProvider.GetAvailableStoresForTransfer(request.SapCode);

            return new TicketTaskDescription
            {
                Name = "Отмени трансфер",
                Description = string.Format("Отмени трансфер, Sap Code {0}, Incomming Transfer: {1}", request.SapCode, request.Items[0].IncomingTransfer) , // @"Отмени трансфер",
                Type = TicketTaskTypes.CreateTransfer,
                Outcomes = new[]
                {
                    new OutcomeDescription(  CancelTransferOutcome)           
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == CancelTransferOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
            }

            StoreFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }


}
