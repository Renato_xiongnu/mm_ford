﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class DeliverOrderTaskHandler : TaskHandlerBase
    {
        public const string DoneOutcome = "Доставлено";

        public const string CancelOutcome = "Отменен";

        public DeliverOrderTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Доставь заказ по адресу",
                Description = "Передай в транспортную компанию",
                Type = TicketTaskTypes.DeliverOrder,
                Outcomes = new[]
                {
                    new OutcomeDescription(DoneOutcome),
                    new OutcomeDescription(CancelOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == DoneOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
            }
            else if (commandWrapped.Outcome == CancelOutcome)
            {
                request.MarkStepFailed(planStep.Id);
                request.Cancel(MessageBus, IncidentCode.WwsReserveCanceled, "Reserve cancelled in WWS");
            }
            else
            {
                throw new ArgumentOutOfRangeException("commandWrapped", commandWrapped.Outcome);
            }

            StoreFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }
}
