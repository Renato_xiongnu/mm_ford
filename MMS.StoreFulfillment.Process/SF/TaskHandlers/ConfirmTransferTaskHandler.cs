﻿using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class ConfirmTransferTaskHandler : TaskHandlerBase
    {
        public const string DoneOutcome = "Трансфер получен";
        public const string FailOutcome = "Трансфер не получен";

        public ConfirmTransferTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Прими трансфер",
                Description = @"Прими трансфер",
                Type = TicketTaskTypes.ConfirmTransfer,
                Outcomes = new[]
                {
                    new OutcomeDescription(DoneOutcome),
                    new OutcomeDescription(FailOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == DoneOutcome)
            {
                MessageBus.SendCommand(new ProcessItemsReservedCommand(request.RequestId));
                request.MarkStepCompleted(planStep.Id);
            }
            else
            {
                request.MarkStepFailed(planStep.Id);
                request.Cancel(MessageBus, IncidentCode.ConfirmTransferFail, FailOutcome);
            }
            StoreFulfillmentStorage.Save(request);
            return SendCommandResult.Completed;
        }
    }
}
