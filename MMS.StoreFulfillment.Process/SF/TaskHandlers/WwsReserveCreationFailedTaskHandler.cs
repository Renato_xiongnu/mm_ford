﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.WWS;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class WwsReserveCreationFailedTaskHandler : TaskHandlerBase
    {
        public const string FixedOutcome = "Исправлено";
        public const string FailedOutcome = "Резерв нельзя создать";

        public WwsReserveCreationFailedTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Исправь ошибку создания резерва",
                Description =
                    @"Резерв в ВВС не может быть создан по причине {ReserveCreationFailedReason}. Найди описание ошибки в Битриксе и исправь ее. ",
                Type = TicketTaskTypes.WwsReserveCreationFailed,
                Outcomes = new[] {
                    new OutcomeDescription(FixedOutcome),
                    new OutcomeDescription(FailedOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped, ItemsMovementRequest request, PlanStep planStep)
        {
            var sendCommandResult = SendCommandResult.Completed;

            if (string.Equals(commandWrapped.Outcome, FixedOutcome, StringComparison.InvariantCultureIgnoreCase))
            {
                sendCommandResult = MessageBus.SendCommand(new CreateReserveCommand(request.RequestId, planStep.Id));
            }
            else if (string.Equals(commandWrapped.Outcome, FailedOutcome, StringComparison.InvariantCultureIgnoreCase))
            {
                request.MarkStepFailed(planStep.Id);
                request.Cancel(MessageBus, IncidentCode.WwsReserveCreationFailed, "Impossible to create reserve");

                StoreFulfillmentStorage.Save(request);
            }
            return sendCommandResult;
        }
    }
}
