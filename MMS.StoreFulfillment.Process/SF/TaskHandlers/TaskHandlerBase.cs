﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using Hangfire;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Application.SF.Hangfire;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public abstract class TaskHandlerBase : ITicketTaskHandler, ITicketTaskCreator
    {
        protected readonly IStoreFulfillmentStorage StoreFulfillmentStorage;
        protected readonly IMessageBus MessageBus;

        private string _key_callBackLater = "Перезвонить позже";
        private string _key_CallMeIn = "Перезвоните мне через";
        
        protected TaskHandlerBase(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
        {
            StoreFulfillmentStorage = storeFulfillmentStorage;
            MessageBus = messageBus;
        }

        public SendCommandResult Handle(TaskCompletedEvent commandWrapped)
        {
            TimeSpan time;
            PlanStep planStep;

            planStep = StoreFulfillmentStorage.GetPlanStepByTaskId(commandWrapped.TaskId);
            SaveTask(planStep, commandWrapped);

            var request = StoreFulfillmentStorage.GetByTicketId(commandWrapped.TicketId);

            if (planStep.Status != PlanStepStatus.Processing || request.Status != RequestStatus.Executing)
            {
                return SendCommandResult.Completed;
            }

            if (commandWrapped.Outcome.Equals(_key_callBackLater, StringComparison.OrdinalIgnoreCase) &&
                !string.IsNullOrEmpty((string) commandWrapped.RequiredFields[_key_CallMeIn]))
            {
                time = TimeSpan.Parse((string) commandWrapped.RequiredFields[_key_CallMeIn]);
                var jobId = BackgroundJob.Schedule<CreateTaskJob>(
                    p => p.CreateTask(commandWrapped.PlanStepId, commandWrapped.TicketId), time);

                try
                {
                    SaveTask(planStep, commandWrapped);
                }
                catch (Exception)
                {
                    BackgroundJob.Delete(jobId);
                    throw;
                }

                return SendCommandResult.LongRunning;
            }


            return HandleInternal(commandWrapped, request, planStep);
        }

        private void SaveTask(PlanStep planStep, TaskCompletedEvent cmd)
        {
            var task = planStep.TicketTask;
            task.Outcome = cmd.Outcome;
            task.Completed = true;
            task.RequiredFields = JsonConvert.SerializeObject(cmd.RequiredFields);
            task.FinishedDate = DateTime.UtcNow;

            StoreFulfillmentStorage.Save(planStep);
        }
        
        public abstract TicketTaskDescription CreateDescription(ItemsMovementRequest request);

        protected abstract SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep);
    }
}
