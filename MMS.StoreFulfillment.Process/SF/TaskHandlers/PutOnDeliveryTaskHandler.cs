﻿using System;
using System.Linq;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class PutOnDeliveryTaskHandler : TaskHandlerBase
    {
        public const string PutOutcome = "Записал";
        public const string DeliveryDateChangedOutcome = "Новая дата доставки";
        public const string FailOutcome = "Доставка невозможна";

        public PutOnDeliveryTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Запиши на доставку",
                Description = @"Внеси заказ в реестр доставок на указанный день. Если доставка в выбранный день невозможна, то укажи ближайший возможный день",
                Type = TicketTaskTypes.PutOnDelivery,
                Outcomes =new[]
                {
                    new OutcomeDescription(PutOutcome),
                    new OutcomeDescription(DeliveryDateChangedOutcome, new[]
                    {
                        new RequiredFieldDescription("Новая дата доставки", typeof (DateTime))
                    }),
                    new OutcomeDescription(FailOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped, ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == PutOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
            }

            if (commandWrapped.Outcome == DeliveryDateChangedOutcome)
            {
                var deliveryDate = (DateTime)commandWrapped.RequiredFields.First(t => t.Key == "Новая дата доставки").Value;
                request.MarkStepCompleted(planStep.Id);
                request.Contact.RequestedDeliveryDate = deliveryDate;
                MessageBus.SendCommand(new ProcessDeliveryDateUpdatedCommand(request.RequestId, request.CustomerOrder.CustomerOrderId, deliveryDate, request.BasedOnCommandId));
            }

            if (commandWrapped.Outcome == FailOutcome)
            {
                request.MarkStepFailed(planStep.Id);
                request.Cancel(MessageBus, IncidentCode.CanceledByCustomer, "Reserve cancelled by Customer");
            }

            StoreFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }
}