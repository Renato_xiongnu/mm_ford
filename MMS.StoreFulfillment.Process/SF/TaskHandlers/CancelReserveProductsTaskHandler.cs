﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class CancelReserveProductsTaskHandler : TaskHandlerBase
    {
        public CancelReserveProductsTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Верни товар в зал",
                Description =
                    @"Заказ отменен. Верни товар в зал продаж",
                Type = TicketTaskTypes.CancelReserveProducts,
                Outcomes = new[] { new OutcomeDescription("Вернул"), }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == "Вернул")
            {
                request.MarkStepCompleted(planStep.Id);

                StoreFulfillmentStorage.Save(request);
            }

            return SendCommandResult.Completed;
        }
    }
}
