﻿using System;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class ReserveProductsTaskHandler : TaskHandlerBase
    {
        public static string ReserveCreatedOutcome = "Готово";

        public ReserveProductsTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Собери заказ",
                Description = @"Клиент подтвердил заказ. Найди все позиции и отнеси товар на стойку выдачи интернет-заказов.
                                Если товар витринный, опиши его состояние и укажи цену с учетом скидки.
                                По возможности, предложи альтернативы – особенно если заказанного товара нет в наличии.",
                Type = TicketTaskTypes.ReserveProducts,
                Outcomes = new[] {
                    new OutcomeDescription(ReserveCreatedOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == ReserveCreatedOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
                if (request.Items.Any(t => t.ReserveStatus != ReserveStatuses.Reserved))
                {
                    request.MarkStepFailed(planStep.Id);
                    request.Cancel(MessageBus, IncidentCode.NotAllItemsReserved, "NotAllItemsReserved");
                }
                else
                {
                    if (request.CustomerOrder.DeliveryType == DeliveryType.Pickup)
                    {
                        //TODO ??? Уточнить, на сколько продлять
                        request.PostponeExpirationDate(DateTime.UtcNow.AddDays(3));
                    }

                    MessageBus.SendCommand(new ProcessItemsReservedCommand(request.RequestId));
                    request.MarkStepCompleted(planStep.Id);
                }

                StoreFulfillmentStorage.Save(request);
            }

            return SendCommandResult.Completed;
        }
    }
}
