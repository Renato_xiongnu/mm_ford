﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class HandOverToCustomerTaskHandler : TaskHandlerBase
    {
        public const string DoneOutcome = "Выдан";
        public const string CancelOutcome = "Отменен";

        public HandOverToCustomerTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
            : base(storeFulfillmentStorage, messageBus)
        {
        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Выдай товар покупателю",
                Description = "Выдай товар покупателю на самовывоз",
                Type = TicketTaskTypes.HandOverToCustomer,
                Outcomes = new[]
                {
                    new OutcomeDescription(DoneOutcome),
                    new OutcomeDescription(CancelOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            var sendCommandResult = SendCommandResult.Completed;

            if (commandWrapped.Outcome == DoneOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
            }
            else if (commandWrapped.Outcome == CancelOutcome)
            {
                request.MarkStepFailed(planStep.Id);
                request.Cancel(MessageBus, IncidentCode.WwsReserveCanceled, "Резерв отменен в WWS");
            }
            else
            {
                throw new ArgumentOutOfRangeException("commandWrapped", commandWrapped.Outcome);
            }

            StoreFulfillmentStorage.Save(request);

            return sendCommandResult;
        }
    }
}
