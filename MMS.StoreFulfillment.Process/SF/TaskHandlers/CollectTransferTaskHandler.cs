﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.SF.Compensation;

namespace MMS.StoreFulfillment.Application.SF.TaskHandlers
{
    public class CollectTransferTaskHandler : TaskHandlerBase
    {
        public const string DoneOutcome = "Собрал";
        public const string FailOutcome = "Невозможно собрать трансфер";
        private readonly ICompensationEngine _compensationEngine;


        public CollectTransferTaskHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus, ICompensationEngine compensationEngine)
            : base(storeFulfillmentStorage, messageBus)
        {
            _compensationEngine = compensationEngine;

        }

        public override TicketTaskDescription CreateDescription(ItemsMovementRequest request)
        {
            return new TicketTaskDescription
            {
                Name = "Собери трансфер",
                Description = @"Собери трансфер",
                Type = TicketTaskTypes.CollectTransfer,
                Outcomes = new[]
                {
                    new OutcomeDescription(DoneOutcome),
                    new OutcomeDescription(FailOutcome)
                }
            };
        }

        protected override SendCommandResult HandleInternal(TaskCompletedEvent commandWrapped,
            ItemsMovementRequest request, PlanStep planStep)
        {
            if (commandWrapped.Outcome == DoneOutcome)
            {
                request.MarkStepCompleted(planStep.Id);
            }
            else
            {
                request.MarkStepFailed(planStep.Id);

                _compensationEngine.Compensate(request);

                request.Cancel(MessageBus, IncidentCode.CollectTransferFail, "Cant collect transfer");
            }
            StoreFulfillmentStorage.Save(request);
            return SendCommandResult.Completed;
        }
    }
}
