﻿using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Application.SF
{
    public class PostponeExpirationDateCommandHandler:ICommandHandler<CloudCommandWrapper<PostponeExpirationDate>>
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;

        public PostponeExpirationDateCommandHandler(IStoreFulfillmentStorage storeFulfillmentStorage)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public SendCommandResult Handle(CloudCommandWrapper<PostponeExpirationDate> commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetByCustomerOrderId(commandWrapped.Command.CustomerOrderId, null)
                .OrderByDescending(t => t.CreatedDate).Last();

            request.PostponeExpirationDate(commandWrapped.Command.NewExpirationDate);

            _storeFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }
}
