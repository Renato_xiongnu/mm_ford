﻿using Castle.DynamicProxy;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using Newtonsoft.Json;
using NLog;

namespace MMS.StoreFulfillment.Application.Common
{
    public class CommandHandlerLogger : IInterceptor
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.Name == "Handle")
            {
                var commandArguments = invocation.Arguments[0];

                var command = commandArguments as ICommand;
                if (command != null)
                {
                    var orderId = OrderByRequestLookup.Get(command.RequestId);

                    var serializedCommand = JsonConvert.SerializeObject(commandArguments);
                    _logger.Info("{0} {1} Command handler {2} process command {3}", orderId, command.RequestId,
                        invocation.TargetType.Name,
                        serializedCommand);

                    invocation.Proceed();

                    _logger.Info("{0} {1} Command handler {2} processed succesfully for {3}",
                        orderId,
                        command.RequestId, invocation.TargetType.Name,
                        serializedCommand);
                }
                else
                {
                    var serializedCommand = JsonConvert.SerializeObject(commandArguments);
                    _logger.Info("Command handler {0} process command {1}", invocation.TargetType.Name,
                        serializedCommand);

                    invocation.Proceed();

                    _logger.Info("Command handler {0} processed succesfully for {1}", invocation.TargetType.Name,
                        serializedCommand);
                }
            }
            else
            {
                invocation.Proceed();
            }
        }
    }
}
