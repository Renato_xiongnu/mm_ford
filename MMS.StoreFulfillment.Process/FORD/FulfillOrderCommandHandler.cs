﻿using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Application.SF;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.Planning;
using MMS.StoreFulfillment.Entities.Messaging;
using Newtonsoft.Json;
using CustomerOrderInfo = MMS.StoreFulfillment.Entities.SF.Items.CustomerOrderInfo;
using Item = MMS.StoreFulfillment.Entities.SF.Items.Item;

namespace MMS.StoreFulfillment.Application.FORD
{
    public class FulfillOrderCommandHandler:ICommandHandler<CloudCommandWrapper<FulfillOrder>>
    {
        private readonly IFulfillmentOrderRepository _fulfillmentOrderStorage;
        private readonly FulfillmentPlanner _fulfillmentPlanner;

        public FulfillOrderCommandHandler(IFulfillmentOrderRepository fulfillmentOrderStorage,FulfillmentPlanner fulfillmentPlanner)
        {
            _fulfillmentOrderStorage = fulfillmentOrderStorage;
            _fulfillmentPlanner = fulfillmentPlanner;
        }

        public SendCommandResult Handle(CloudCommandWrapper<FulfillOrder> commandWrapper)
        {
            var command = commandWrapper.Command;
            var fulfillmentOrder = new FulfillmentOrder
            {
                FulfillmentItems =
                    command.Items.Select(
                        el =>
                            new Item(el.Article, el.Price, el.Qty, el.Title, el.ItemId)
                            {
                                ReserveStatus = ReserveStatuses.Empty,
                                ArticleProductType = el.ArticleProductType
                            }).ToArray(),
                SapCode = command.SapCode,
                SaleLocationSapCode = "R" + command.CustomerOrderId.Substring(0, 3), //TODO
                SourceOrder = new CustomerOrderInfo
                {
                    CustomerOrderId = command.CustomerOrderId,
                    Downpayment = command.CustomerOrder.Downpayment,
                    IsOnlineOrder = command.CustomerOrder.IsOnlineOrder,
                    OrderSource = command.CustomerOrder.OrderSource,
                    PaymentType = command.CustomerOrder.PaymentType,
                    ShippingMethod = command.CustomerOrder.ShippingMethod,
                    SocialCardNumber = command.CustomerOrder.SocialCardNumber,
                    RequestedDeliveryDate = command.CustomerOrder.RequestedDeliveryDate
                },
                BasedOnCommand = JsonConvert.SerializeObject(commandWrapper.CommandKey)
            };
            _fulfillmentOrderStorage.Save(fulfillmentOrder);

            try
            {
                var fulfillmentPlan = _fulfillmentPlanner.PlanFulfillment(fulfillmentOrder);
                fulfillmentOrder.AttachPlan(fulfillmentPlan);
                _fulfillmentOrderStorage.Save(fulfillmentOrder);
            }
            catch (PlanGenerationException ex)
            {
                fulfillmentOrder.Fatal(ResultReasons.NotAllItemsReserved, ex.Message);
            }
            return SendCommandResult.Completed;
        }
    }
}
