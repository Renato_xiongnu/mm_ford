﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Application.FORD
{
    public class ProcessFulfillmentOrderCompletionCommandHandler : ICommandHandler<ProcessFulfillmentOrderCompletionCommand>
    {
        private readonly IClientCommandManager _clientCommandManager;
        private readonly IFulfillmentOrderRepository _fulfillmentOrderRepository;

        public ProcessFulfillmentOrderCompletionCommandHandler(IClientCommandManager clientCommandManager, IFulfillmentOrderRepository fulfillmentOrderRepository)
        {
            _clientCommandManager = clientCommandManager;
            _fulfillmentOrderRepository = fulfillmentOrderRepository;
        }

        public SendCommandResult Handle(ProcessFulfillmentOrderCompletionCommand commandWrapped)
        {
            var fulfillmentOrder = _fulfillmentOrderRepository.GetById(commandWrapped.FulfillmentOrderId);

            if (commandWrapped.IsSuccesfull)
            {
                _clientCommandManager.CreateResponse(
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Key>(fulfillmentOrder.BasedOnCommand),
                    new FulfillOrderResponse
                    {
                        Result = Result.Done,
                        ResultReason = commandWrapped.IncidentCode,
                        Comment = commandWrapped.CompletionReason, //Сейчас комменты не нужны ?
                        Items = fulfillmentOrder.FulfillmentItems.Select(el => new ReservedItem
                        {
                            ItemId = el.ItemExternalId,
                            ReserveStatus = ReserveStatuses.Reserved,
                            //Это поле уже неактуально, т.к. для ТТ заказ "исполнен".
                            //Но для обратной совместимости оставляю
                            ReservedSucesfully = true
                        }).ToArray(),
                        Incidents =
                            new List<Incident>
                            {
                            }
                    }, fulfillmentOrder.SourceOrder.CustomerOrderId);
            }
            else
            {
                _clientCommandManager.CreateResponse(
                    Newtonsoft.Json.JsonConvert.DeserializeObject<Key>(fulfillmentOrder.BasedOnCommand),
                    new FulfillOrderResponse
                    {
                        Result = Result.Failed,
                        ResultReason = commandWrapped.IncidentCode,
                        Comment = commandWrapped.CompletionReason,
                        Items = fulfillmentOrder.FulfillmentItems.Select(el => new ReservedItem
                        {
                            ItemId = el.ItemExternalId,
                            ReserveStatus = el.ReserveStatus,
                            ReservedSucesfully = string.Equals(el.ReserveStatus, ReserveStatuses.Reserved)
                        }).ToArray(),
                        Incidents = new List<Incident>
                        {
                        }
                    }, fulfillmentOrder.SourceOrder.CustomerOrderId);
            }

            return SendCommandResult.Completed;
        }
    }
}
