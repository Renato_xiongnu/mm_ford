﻿using System.Collections.Generic;
using System.Linq;
using MMS.StoreFulfillment.Application.Proxy.ProductBackend;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.SF.Items;
using Newtonsoft.Json;
using NLog;

namespace MMS.StoreFulfillment.Application.FORD
{
    public class ProductBackendAvailabilityProvider : IProductsAvailabilityProvider
    {
        private readonly IOrdersBackendService _ordersBackendService;

        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public ProductBackendAvailabilityProvider(IOrdersBackendService ordersBackendService)
        {
            _ordersBackendService = ordersBackendService;
        }

        public bool AvailableInStore(string sapCode, IEnumerable<Item> items)
        {
            //Логика следующая:
            /*
            Посмотрели наличие - вернули true - в магазине есть товар
            Если товара нет - вернули false
            На этом ответственность класса заканчивается, он просто обертка над бекендом
            Т.е. не ему решать, можно ли делать трансфер или нет (это ответственность планировщика FORD FulfillmentPlanner)
            В последнем может быть хитрая логика - например, делать трансфер, если осталось меньше дня до самовывоза, и т.д.
            Единственный минус - в некоторых кейсах, вероятно, будет N запросов - можно поправить контракт на IEnumerable<SapCode>
            */
            var products = items.Where(el => el.ArticleProductType == ArticleProductTypes.Set || el.ArticleProductType == ArticleProductTypes.Product);
            var request = new GetItemsRequest
            {
                Channel = "MM",
                SaleLocation = string.Concat("shop_", sapCode),
                Articles = products.Select(el => el.Article.ToString()).ToArray(),
            };
            var client = _ordersBackendService as OrdersBackendServiceClient;
            if (client != null)
            {
                _logger.Debug("Call AvailableInStore - Url: {0}, SapCode: {1}, Request: {2}", client.Endpoint.ListenUri, sapCode, JsonConvert.SerializeObject(request));
            }
            var result = _ordersBackendService.GetArticleStockStatus(request);
            _logger.Debug("Call AvailableInStore - Result: {0}", JsonConvert.SerializeObject(result));
            var availableInStock = products.All(pr => result.Items.Any(obi => obi.Article == pr.Article && obi.Qty >= pr.Qty && obi.Status == StockStatusEnum.InStock));
            return availableInStock;
        }
    }
}
