﻿using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Interfaces.Clients.Commands.Storage;
using MMS.Cloud.Shared.Commands;
using MMS.Cloud.Shared.Serialization;
using MMS.StoreFulfillment.Application.SF;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.Execution;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.FORD.Execution
{
    public class TransferGoodsStepHandler:IPlanStepHandler
    {
        private readonly IMessageBus _messageBus;
        private readonly IClientCommandStorage _clientCommandStorage;
        private readonly IFulfillmentOrderRepository _fulfillmentOrderStorage;

        public TransferGoodsStepHandler(IMessageBus messageBus, IClientCommandStorage clientCommandStorage, IFulfillmentOrderRepository fulfillmentOrderStorage)
        {
            _messageBus = messageBus;
            _clientCommandStorage = clientCommandStorage;
            _fulfillmentOrderStorage = fulfillmentOrderStorage;
        }

        public void Process(FulfillmentPlanStep fulfillmentPlanStep)
        {
            var fulfillmentOrder = _fulfillmentOrderStorage.GetById(fulfillmentPlanStep.FulfillmentOrderId);

            var externalCommands = _clientCommandStorage.GetExternalCommandsByRelatedEntityId(fulfillmentOrder.SourceOrder.CustomerOrderId); //TODO
            var command = externalCommands.First(t => Equals(t.CommandKey, JsonConvert.DeserializeObject<Key>(fulfillmentOrder.BasedOnCommand)));
            
            var fulfillOrderCommand = command.SerializedData.Deserialize<FulfillOrder>();
            var transferGoods = new TransferGoods
            {
                CustomerOrderId = fulfillmentOrder.SourceOrder.CustomerOrderId,
                Items = fulfillOrderCommand.Items.ToArray(),
                SourceSapCode = fulfillmentPlanStep.Responsible,
                TargetSapCode = fulfillOrderCommand.SapCode,
                Contact = fulfillOrderCommand.Contact
            };

            _messageBus.SendCommand(new InternalCloudCommandWrapper<TransferGoods>(transferGoods, command.CommandKey, "TransferGoods", fulfillmentOrder.Id));
        }
    }
}
