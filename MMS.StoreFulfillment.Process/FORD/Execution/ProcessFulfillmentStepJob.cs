﻿using System;
using Autofac;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.Execution;
using NLog;

namespace MMS.StoreFulfillment.Application.FORD.Execution
{
    public class ProcessFulfillmentStepJob
    {
        private readonly ILifetimeScope _lifetimeScope;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private static object _lockObject=new object();

        public ProcessFulfillmentStepJob(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public void Execute()
        {
            lock (_lockObject)
            {
                _logger.Info("Start fulfillment process iteration");
                using (var scope = _lifetimeScope.BeginLifetimeScope())
                {
                    var fulfillmentOrderStorage = scope.Resolve<IFulfillmentOrderRepository>();

                    var unprocessedPlanSteps = fulfillmentOrderStorage.GetUprocessedPlanSteps();

                    foreach (var unprocessedPlanStep in unprocessedPlanSteps)
                    {
                        try
                        {
                            var planStepHandler = scope.ResolveNamed<IPlanStepHandler>(unprocessedPlanStep.PlanStepType);
                            planStepHandler.Process(unprocessedPlanStep);

                            unprocessedPlanStep.FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Processing;
                            fulfillmentOrderStorage.Save(unprocessedPlanStep);
                        }
                        catch (Exception e)
                        {
                            _logger.Error(e, "Error during processing plan step {0}", unprocessedPlanStep.Id);
                        }
                    }
                }

                _logger.Info("Finish fulfillment process iteration");
            }
        }
    }
}
