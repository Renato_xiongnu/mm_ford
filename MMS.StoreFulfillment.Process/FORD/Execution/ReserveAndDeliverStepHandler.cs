﻿using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Interfaces.Clients.Commands.Storage;
using MMS.Cloud.Shared.Commands;
using MMS.Cloud.Shared.Serialization;
using MMS.StoreFulfillment.Application.SF;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.Execution;
using MMS.StoreFulfillment.Entities.Messaging;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.FORD.Execution
{
    public class ReserveAndDeliverStepHandler : IPlanStepHandler
    {
        private readonly IMessageBus _messageBus;
        private readonly IClientCommandStorage _clientCommandStorage;
        private readonly IFulfillmentOrderRepository _fulfillmentOrderStorage;

        public ReserveAndDeliverStepHandler(IMessageBus messageBus, IClientCommandStorage clientCommandStorage,
            IFulfillmentOrderRepository fulfillmentOrderStorage)
        {
            _messageBus = messageBus;
            _clientCommandStorage = clientCommandStorage;
            _fulfillmentOrderStorage = fulfillmentOrderStorage;
        }

        public void Process(FulfillmentPlanStep fulfillmentPlanStep)
        {
            var fulfillmentOrder = _fulfillmentOrderStorage.GetById(fulfillmentPlanStep.FulfillmentOrderId);

            var externalCommands =
                _clientCommandStorage.GetExternalCommandsByRelatedEntityId(fulfillmentOrder.SourceOrder.CustomerOrderId);
            var command =
                externalCommands.First(
                    t => Equals(t.CommandKey, JsonConvert.DeserializeObject<Key>(fulfillmentOrder.BasedOnCommand)));

            var fulfillOrder = command.SerializedData.Deserialize<FulfillOrder>();
            fulfillOrder.SapCode = fulfillmentPlanStep.Responsible; //Тут сап код должен быть другой

            _messageBus.SendCommand(
                new InternalCloudCommandWrapper<FulfillOrder>(fulfillOrder, command.CommandKey, command.CommandName, fulfillmentOrder.Id));
        }
    }
}
