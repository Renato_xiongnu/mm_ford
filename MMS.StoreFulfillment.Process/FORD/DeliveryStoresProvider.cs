﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Application.StockSelector;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.FORD.Planning;

namespace MMS.StoreFulfillment.Application.FORD
{
    public class DeliveryStoresProvider : IDeliveryStoresProvider, ITransferStoresDataProvider
    {
        private readonly ISaleLocationService _saleLocationService;

        public DeliveryStoresProvider(ISaleLocationService saleLocationService)
        {
            _saleLocationService = saleLocationService;
        }

        /// <summary>
        /// Return collection of Stocks with AvailableShippingMethods contains ShippingMethods.PickupShop
        /// </summary>
        /// <param name="sapCode">Virtulal stock SapCode</param>
        /// <returns></returns>
        public IOrderedEnumerable<Stock> GetAvailableStoresForTransfer(string sapCode)
        {
            var stocksResult = _saleLocationService.GetStockLocations(sapCode);
            if (stocksResult.ReturnCode == ReturnCode.Error)
            {
                throw new Exception(string.Format("Error during obtaining available stocks {0}", stocksResult.ErrorMessage));
            }
            var stores = stocksResult.Result
                .Where(t => t.AvailableShippingMethods.Contains(ShippingMethods.PickupShop))
                .Select(t => new Stock { SapCode = t.StockLocationSapCode, Rating = t.Rate })
                .OrderByDescending(t => t.Rating);
            return stores;
        }

        public IOrderedEnumerable<Stock> GetAvailableStores(FulfillmentOrder fulfillmentOrder)
        {
            var stocksResult =
                _saleLocationService.GetStockLocations(fulfillmentOrder.SaleLocationSapCode);
            if (stocksResult.ReturnCode == ReturnCode.Error)
                throw new Exception(string.Format("Error during obtaining available stocks {0}",
                    stocksResult.ErrorMessage));

            return
                stocksResult.Result.Where(t => t.AvailableShippingMethods.Contains(ShippingMethods.Delivery))
                    .Select(t => new Stock { SapCode = t.StockLocationSapCode, Rating = t.Rate })
                    .OrderByDescending(t => t.Rating);
        }
    }
}
