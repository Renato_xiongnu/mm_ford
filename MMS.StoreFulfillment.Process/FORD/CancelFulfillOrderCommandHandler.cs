﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Application.SF;
using MMS.StoreFulfillment.Entities.FORD;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using NLog;

namespace MMS.StoreFulfillment.Application.FORD
{
    public class CancelFulfillOrderCommandHandler : ICommandHandler<CloudCommandWrapper<CancelFulfillOrder>>
    {
        private readonly IFulfillmentOrderRepository _fulfillmentOrderRepository;
        private readonly IStoreFulfillmentStorage _storage;
        private readonly IMessageBus _messageBus;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CancelFulfillOrderCommandHandler(IStoreFulfillmentStorage storage, IMessageBus messageBus, IFulfillmentOrderRepository fulfillmentOrderRepository)
        {
            _storage = storage;
            _messageBus = messageBus;
            _fulfillmentOrderRepository = fulfillmentOrderRepository;
        }

        public SendCommandResult Handle(CloudCommandWrapper<CancelFulfillOrder> commandWrapped)
        {
            var command = commandWrapped.Command;

            var fulfillmentOrders = _fulfillmentOrderRepository.GetForCustomerOrderId(command.CustomerOrderId);
            var fulfullmentOrder = fulfillmentOrders.OrderByDescending(t => t.CreatedDate).First();
            //Исходим из инварианта "для одного заказа в ТТ может быть только один АКТИВНЫЙ FulfillmentOrder"
            //Нужно добавить проверки, был ли заказ уже отменен или не существует

            var requests = _storage.GetByCustomerOrderId(command.CustomerOrderId,
                new Expression<Func<ItemsMovementRequest, object>>[]
                {t => t.PlanSteps.Select(q => q.TicketTask), t => t.Items})
                .Where(el => el.FulfillmentOrderId == fulfullmentOrder.Id)
                .ToList();

            foreach (var request in requests)
            {
                _messageBus.SendCommand(new CloudCommandWrapper<CancelItemMovementRequest>(new CancelItemMovementRequest(request.RequestId), commandWrapped.CommandKey, commandWrapped.CommandName));
            }

            return SendCommandResult.Completed;
        }
    }
}
