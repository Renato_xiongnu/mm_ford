﻿using Autofac;
using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Application
{
    public class MessageBus:IMessageBus
    {
        private readonly ILifetimeScope _container;

        public MessageBus(ILifetimeScope container)
        {
            _container = container;
        }

        public SendCommandResult SendCommand<T>(T command)
        {
            using (var scope = _container.BeginLifetimeScope())
            {
                var commandHandler = scope.Resolve<ICommandHandler<T>>();
                return commandHandler.Handle(command);
            }
        }
    }
}
