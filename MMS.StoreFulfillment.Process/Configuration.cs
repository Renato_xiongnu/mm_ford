﻿using System;

namespace MMS.StoreFulfillment.Application
{
    public class Configurations
    {
        public static string SystemName
        {
            get { return "StoreFulfillment"; }
        }

        public static string TtLogin
        {
            get { return Application.Default.TtLogin; }
        }

        public static string TtPassword
        {
            get { return Application.Default.TtPassword; }
        }

        public static TimeSpan DefaultReserveLifiteme
        {
            get { return TimeSpan.FromDays(Application.Default.DefaultReserveLifiteme); }
        }

        public static TimeSpan DefaultReserveLifitemeAfterDelivery
        {
            get { return TimeSpan.FromDays(1); }
        }

        public static int HangfireWorkersCount
        {
            get { return Application.Default.HangfireWorkersCount; }
        }
    }
}
