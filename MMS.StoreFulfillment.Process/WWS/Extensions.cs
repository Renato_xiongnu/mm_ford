﻿using System.Linq;
using System.Text.RegularExpressions;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Application.WWS
{
    internal static class Extensions
    {
        public static OrderInfo ToOrderInfo(this ItemsMovementRequest itemsMovementRequest)
        {
            var orderInfo = new OrderInfo
            {
                SapCode = itemsMovementRequest.SapCode,
                Customer = new CustomerInfo
                {
                    Address = CreateShortAddress(itemsMovementRequest.Contact.Address),
                    City = itemsMovementRequest.Contact.City,
                    Email = itemsMovementRequest.Contact.Email,
                    FirstName = itemsMovementRequest.Contact.FirstName,
                    Surname = itemsMovementRequest.Contact.LastName,
                    Mobile = itemsMovementRequest.Contact.Phone,
                    Phone = itemsMovementRequest.Contact.Phone,
                    ZipCode = itemsMovementRequest.Contact.ZIPCode
                },
                IsFiscalInvoice = false,
                IsOnlineOrder = itemsMovementRequest.CustomerOrder.IsOnlineOrder,
                Comment = "Created from TicketTool",
                OnlineOrderId = itemsMovementRequest.CustomerOrder.CustomerOrderId,
                ShippingMethod =
                    itemsMovementRequest.CustomerOrder.DeliveryType == DeliveryType.Delivery
                        ? ShippingMethod.Delivery
                        : ShippingMethod.Pickup,
                OrderLines = itemsMovementRequest.Items.Select(ToOrderLine).ToArray(),
                SourceSystem = "MediaMarkt",
                PaymentType = PaymentType.Cash//request.Payment.ToWwsPaymentType()
            };

            if (itemsMovementRequest.CustomerOrder.DeliveryType == DeliveryType.Delivery)
            {
                orderInfo.OutletInfo = itemsMovementRequest.Contact.AddressWithRequestedService;
                orderInfo.PrintableInfo += itemsMovementRequest.Contact.AddressWithRequestedService;
            }

            return orderInfo;
        }


        private static OrderLine ToOrderLine(Item item)
        {
            return new OrderLine
            {
                ArticleNo = (int)item.Article, // :(
                Price = item.Price,
                Quantity = item.Qty,
                SerialNumber = item.SerialNumber,
                SubLines = item.SubItems != null ? item.SubItems.Select(el => ToOrderLine(item)).ToArray() : null,
                HasShippedFromStock = item.HasShippedFromStock
            };
        }

        private static string CreateShortAddress(string address)
        {
            if (string.IsNullOrEmpty(address)) return address;

            var houseRegex = new Regex("ДОМ", RegexOptions.IgnoreCase);
            var buildingRegex = new Regex("КОРПУС", RegexOptions.IgnoreCase);
            var apartmentRegex = new Regex("КВАРТИРА", RegexOptions.IgnoreCase);
            address = houseRegex.Replace(address, "д.");
            address = buildingRegex.Replace(address, "к.");
            address = apartmentRegex.Replace(address, "кв.");

            return address;
        }
    }
}
