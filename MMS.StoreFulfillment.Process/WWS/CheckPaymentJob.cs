﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Hangfire;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Commands.SF;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using Newtonsoft.Json;
using Autofac;
using MMS.StoreFulfillment.Application.TicketTool;
using MMS.StoreFulfillment.Entities.SF.Items;
using NLog;
namespace MMS.StoreFulfillment.Application.WWS
{
    [AutomaticRetry(Attempts = 0)]
    public class CheckPaymentJob
    {
        private readonly ILifetimeScope _lifeTimeScope;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CheckPaymentJob(ILifetimeScope scope)
        {
            _lifeTimeScope = scope;
        }

        public void CheckPayment(string requestId)
        {
            using (var scope = _lifeTimeScope.BeginLifetimeScope())
            {
                var storage = scope.Resolve<IStoreFulfillmentStorage>();
                var request = storage.GetById(requestId,
                    new Expression<Func<ItemsMovementRequest, object>>[]
                    {t => t.PlanSteps, t => t.PlanSteps.Select(task => task.TicketTask)});
                _logger.Info("{0} {1} start check payment", request.CustomerOrder.CustomerOrderId, requestId);

                var wwsCheckRequestId = WwsReserveHelper.ToWwsRequest(request.WwsOrderId, request.SapCode);

                if (!request.ProcessingAllowed)
                {
                    _logger.Info("{0} {1} request was processed, cancelling job", request.CustomerOrder.CustomerOrderId,
                        request.RequestId);
                    RecurringJob.RemoveIfExists(wwsCheckRequestId);
                    return;
                }

                var salesService = scope.Resolve<ISalesService>();
                var salesDoc =
                    salesService.GetSalesOrder(new OrderNumberInfo
                    {
                        OrderNumber = request.WwsOrderId,
                        SapCode = request.SapCode
                    });

                if (request.CustomerOrder.ShippingMethod == ShippingMethods.PickupShop)
                {
                    var taskService = _lifeTimeScope.Resolve<CompleteTaskService>();
                    ProcessHandover(salesDoc, request, wwsCheckRequestId, taskService);
                }
                else
                {
                    var taskService = _lifeTimeScope.Resolve<CompleteTaskService>();
                    var clientCommandManager = _lifeTimeScope.Resolve<IClientCommandManager>();
                    ProcessDelivery(salesDoc, request, wwsCheckRequestId, taskService, clientCommandManager, storage);
                }


                _logger.Info("{0} {1} finish check payment", request.CustomerOrder.CustomerOrderId, requestId);
            }
        }

        private void ProcessHandover(GetSalesOrderResult salesDoc, ItemsMovementRequest request, string wwsCheckRequestId, CompleteTaskService completeTaskService)
        {
            if (WwsReserveHelper.IsPaid(salesDoc.OrderHeader.OrderStatus, request.CustomerOrder.ShippingMethod))
            {
                completeTaskService.CompleteTask(request.CurrentPlanStep().TicketTask.TaskId, "Выдан");
                RecurringJob.RemoveIfExists(wwsCheckRequestId);
            }

            if (salesDoc.OrderHeader.OrderStatus == SalesOrderStatus.Canceled)
            {
                completeTaskService.CompleteTask(request.CurrentPlanStep().TicketTask.TaskId, "Отменен");
                RecurringJob.RemoveIfExists(wwsCheckRequestId);
            }
        }

        private void ProcessDelivery(GetSalesOrderResult salesDoc, ItemsMovementRequest request, string wwsCheckRequestId, CompleteTaskService completeTaskService, IClientCommandManager clientCommandManager, IStoreFulfillmentStorage storage)
        {
            if (WwsReserveHelper.IsPrePaid(salesDoc.OrderHeader.OrderStatus, request.CustomerOrder.ShippingMethod) &&
                !request.IsShipped)
            {
                clientCommandManager.CreateCommand("FulfillOrderProcessingInfo", new FulfillOrderProcessingInfo
                {
                    OrderId = request.CustomerOrder.CustomerOrderId,
                    Status = "Shipped",
                    RequestId = request.RequestId,
                    AdditionalInfo = new FulfillOrderAdditionalInfo {}
                }, request.CustomerOrder.CustomerOrderId,
                    JsonConvert.DeserializeObject<Key>(request.BasedOnCommandId));

                request.IsShipped = true;
                storage.Save(request);
            }

            if (WwsReserveHelper.IsPaid(salesDoc.OrderHeader.OrderStatus, request.CustomerOrder.ShippingMethod))
            {
                completeTaskService.CompleteTask(request.CurrentPlanStep().TicketTask.TaskId, "Доставлено");
                RecurringJob.RemoveIfExists(wwsCheckRequestId);
            }

            if (salesDoc.OrderHeader.OrderStatus == SalesOrderStatus.Canceled)
            {
                completeTaskService.CompleteTask(request.CurrentPlanStep().TicketTask.TaskId, "Отменен");
                RecurringJob.RemoveIfExists(wwsCheckRequestId);
            }
        }
    }
}
