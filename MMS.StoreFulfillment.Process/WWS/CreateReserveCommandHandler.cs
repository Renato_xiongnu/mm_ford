﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.WWS;
using Newtonsoft.Json;
using NLog;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class CreateReserveCommandHandler:ICommandHandler<CreateReserveCommand>
    {
        private readonly IStoreFulfillmentStorage _requestRepository;
        private readonly IMessageBus _messageBus;
        private readonly IClientCommandManager _clientCommandManager;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CreateReserveCommandHandler(IStoreFulfillmentStorage requestRepository, IMessageBus messageBus,
            IClientCommandManager clientCommandManager)
        {
            _requestRepository = requestRepository;
            _messageBus = messageBus;
            _clientCommandManager = clientCommandManager;
        }

        public SendCommandResult Handle(CreateReserveCommand commandWrapped)
        {
            var request = _requestRepository.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] {el => el.PlanSteps, el => el.Items});

            using (var client = new SalesServiceClient())
            {
                var orderInfo = request.ToOrderInfo();

                var salesOrder = client.CreateSalesOrder(orderInfo);

                if (salesOrder.OperationCode.Code != ReturnCode.Ok)
                    return
                        _messageBus.SendCommand(new CreateTaskCommand(commandWrapped.RequestId, commandWrapped.PlanStepId,
                            TicketTaskTypes.WwsReserveCreationFailed,
                            new Dictionary<string, string>
                            {
                                {
                                    "{ReserveCreationFailedReason}",
                                    salesOrder.OperationCode.MessageText
                                }
                            }));
                
                request.WwsOrderId = salesOrder.OrderHeader.OrderNumber;
                request.MarkStepCompleted(commandWrapped.PlanStepId);
                foreach(var item in request.Items)
                {
                    var item1 = item;
                    var line = salesOrder.OrderHeader.Lines.FirstOrDefault(t => t.ArticleNo == item1.Article && t.Price == item1.Price);
                    if (line == null)
                    {
                        _logger.Warn("{0} Item {1} (article {2}, price {3}) not found in document", request.RequestId,
                            item.Id, item.Article, item.Price);
                        continue;
                    }

                    item.BrandTitle = line.ManufacturerName;
                    item.ProductGroupName = line.ProductGroupName;
                    item.ProductGroupNo = line.ProductGroupNo;
                    item.DepartmentNumber = line.DepartmentNumber;
                }

                _requestRepository.Save(request);

                _clientCommandManager.CreateCommand("FulfillOrderProcessingInfo",
                    new FulfillOrderProcessingInfo //TODO
                    {
                        OrderId = request.CustomerOrder.CustomerOrderId,
                        Status = FulfillOrderProcessingInfoStatuses.WwsReserveCreated,
                        RequestId = request.RequestId,
                        AdditionalInfo = new FulfillOrderAdditionalInfo
                        {
                            SapCode = request.SapCode,
                            WwsReserveId = request.WwsOrderId
                        }
                    }, request.CustomerOrder.CustomerOrderId,
                    JsonConvert.DeserializeObject<Key>(request.BasedOnCommandId));

                return SendCommandResult.Completed;
            }
        }
    }
}
