﻿using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.WWS;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class ProcessReserveCreationFailedCommandHandler : ICommandHandler<ProcessReserveCreationFailedCommand>
    {
        private readonly IMessageBus _messageBus;

        public ProcessReserveCreationFailedCommandHandler(IMessageBus messageBus)
        {
            _messageBus = messageBus;
        }

        public SendCommandResult Handle(ProcessReserveCreationFailedCommand commandWrapped)
        {
            return
                _messageBus.SendCommand(new CreateTaskCommand(commandWrapped.RequestId, commandWrapped.PlanStepId,
                    TicketTaskTypes.WwsReserveCreationFailed));
        }
    }
}
