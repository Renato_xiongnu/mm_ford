﻿using System;
using Hangfire;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.WWS;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class CheckPaymentCommandHandler:ICommandHandler<CheckPaymentCommand>
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;

        private readonly IMessageBus _messageBus;

        public CheckPaymentCommandHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _messageBus = messageBus;
        }

        public SendCommandResult Handle(CheckPaymentCommand commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetById(commandWrapped.RequestId);

            SendFakeTask(commandWrapped, request);

            var requestId = WwsReserveHelper.ToWwsRequest(request.WwsOrderId, request.SapCode);

            RecurringJob.AddOrUpdate<CheckPaymentJob>(requestId,
                t => t.CheckPayment(commandWrapped.RequestId),
                Cron.Hourly(DateTime.Now.Minute));
            return SendCommandResult.LongRunning;
        }

        private void SendFakeTask(CheckPaymentCommand commandWrapped, ItemsMovementRequest request)
        {
            if (request.CustomerOrder.ShippingMethod == ShippingMethods.PickupShop)
            {
                _messageBus.SendCommand(new CreateTaskCommand(commandWrapped.RequestId, commandWrapped.PlanStepId,
                    TicketTaskTypes.HandOverToCustomer));
            }
            else
            {
                _messageBus.SendCommand(new CreateTaskCommand(commandWrapped.RequestId, commandWrapped.PlanStepId,
                    TicketTaskTypes.DeliverOrder));
            }
        }
    }
}
