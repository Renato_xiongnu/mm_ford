﻿using System;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.WWS;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class CancelReserveCommandHandler:ICommandHandler<CancelReserveCommand>
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;
        private readonly ISalesService _salesService;

        public CancelReserveCommandHandler(IStoreFulfillmentStorage storeFulfillmentStorage, ISalesService salesService)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _salesService = salesService;
        }

        public SendCommandResult Handle(CancelReserveCommand commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.PlanSteps});

            var result =
                _salesService.CancelSalesOrder(new OrderNumberInfo
                {
                    OrderNumber = request.WwsOrderId,
                    SapCode = request.SapCode
                });

            if (result.OperationCode.Code != ReturnCode.Ok) throw new Exception(result.OperationCode.MessageText);

            request.MarkStepCompleted(commandWrapped.PlanStepId);

            _storeFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }
}
