﻿using System.Collections.Generic;
using System.ServiceModel;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Application.WWS
{
    [ServiceContract]
    public interface IRecreateReserveService
    {
        [OperationContract]
        string RecreateReserve(string requestId, IEnumerable<Item> items);
    }
}