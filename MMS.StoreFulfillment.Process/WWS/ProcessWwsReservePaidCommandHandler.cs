﻿using System;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.WWS;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class ProcessWwsReservePaidCommandHandler:ICommandHandler<ProcessWwsReservePaidCommand>
    {
        private IStoreFulfillmentStorage _storeFulfillmentStorage;

        public ProcessWwsReservePaidCommandHandler(IStoreFulfillmentStorage storeFulfillmentStorage)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public SendCommandResult Handle(ProcessWwsReservePaidCommand commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] {t => t.PlanSteps});

            request.MarkStepCompleted(commandWrapped.PlanStepId);

            _storeFulfillmentStorage.Save(request);

            return SendCommandResult.Completed;
        }
    }
}
