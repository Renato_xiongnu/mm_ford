﻿using System;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Compensation;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.WWS;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class ProcessWwsReserveCanceledCommandHandler:ICommandHandler<ProcessWwsReserveCanceledCommand>
    {
        private IStoreFulfillmentStorage _storeFulfillmentStorage;
        private IMessageBus _messageBus;
        private ICompensationEngine _compensationEngine;

        public ProcessWwsReserveCanceledCommandHandler(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus, ICompensationEngine compensationEngine)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _messageBus = messageBus;
            _compensationEngine = compensationEngine;
        }

        public SendCommandResult Handle(ProcessWwsReserveCanceledCommand commandWrapped)
        {
            var request = _storeFulfillmentStorage.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[]
                {
                    t => t.PlanSteps,
                    t => t.Items
                });

            request.Cancel(_messageBus, IncidentCode.WwsReserveCanceled,  "Reserve was canceled in WWS");
            
            _compensationEngine.Compensate(request);

            return SendCommandResult.Completed;
        }
    }
}
