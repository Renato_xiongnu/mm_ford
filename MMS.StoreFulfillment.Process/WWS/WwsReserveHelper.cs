﻿using MMS.Cloud.Commands.SF;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class WwsReserveHelper
    {
        public static bool IsPaid(SalesOrderStatus salesDocStatus, string shippingMethod)
        {
            if (salesDocStatus == SalesOrderStatus.Paid)
            {
                return true;
            }

            if (salesDocStatus == SalesOrderStatus.Prepaid && shippingMethod == ShippingMethods.Delivery)
            {
                return false;
            }
            
            return false;
        }

        public static bool IsPrePaid(SalesOrderStatus salesDocStatus, string shippingMethod)
        {
            return salesDocStatus==SalesOrderStatus.Prepaid;
        }

        public static string ToWwsRequest(string wwsOrderId, string sapCode)
        {
            return string.Format("{0}-{1}", sapCode, wwsOrderId);
        }
    }
}
