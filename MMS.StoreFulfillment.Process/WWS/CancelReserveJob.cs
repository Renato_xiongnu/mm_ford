﻿using System;
using Hangfire;
using NLog;
using Autofac;

namespace MMS.StoreFulfillment.Application.WWS
{
    [AutomaticRetry(Attempts = 100, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
    public class CancelReserveJob
    {
        private readonly ILifetimeScope _container;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CancelReserveJob(ILifetimeScope container)
        {
            _container = container;
        }

        public void CancelReserve(string sapCode, string wwsOrderId)
        {
            using (var sc = _container.BeginLifetimeScope())
            {
                _logger.Info("Start cancelling reserve {0} in sapCode {1}", wwsOrderId, sapCode);
                var salesService = sc.Resolve<ISalesService>();

                var cancelServiceResult = salesService.CancelSalesOrder(new OrderNumberInfo
                {
                    OrderNumber = wwsOrderId,
                    SapCode = sapCode
                });

                if (cancelServiceResult.OperationCode.Code != ReturnCode.Ok)
                {
                    _logger.Error("Error during cancelling reserve {0} in sapCode {1}: {2}", wwsOrderId, sapCode,
                        cancelServiceResult.OperationCode.MessageText);
                    throw new Exception(cancelServiceResult.OperationCode.MessageText);
                }

                _logger.Info("Reserve {0} in sapCode {1} cancelled sucessfully", sapCode, wwsOrderId);
            }
        }
    }
}
