﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Hangfire;
using MMS.Cloud.Commands.FORD;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.Cloud.Shared.Commands;
using MMS.StoreFulfillment.Entities.SF.Items;
using Newtonsoft.Json;

namespace MMS.StoreFulfillment.Application.WWS
{
    public class RecreateReserveService : IRecreateReserveService
    {
        private IStoreFulfillmentStorage _storeFulfillmentStorage;
        private ISalesService _salesService;
        private IClientCommandManager _clientCommandManager;

        public RecreateReserveService(IStoreFulfillmentStorage storeFulfillmentStorage, ISalesService salesService, IClientCommandManager clientCommandManager)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _salesService = salesService;
            _clientCommandManager = clientCommandManager;
        }

        public string RecreateReserve(string requestId, IEnumerable<Item> items)
        {
            var itemMovementRequest = _storeFulfillmentStorage.GetById(requestId,
                new Expression<Func<ItemsMovementRequest, object>>[] {t => t.Items});

            BackgroundJob.Enqueue<CancelReserveJob>(
                t => t.CancelReserve(itemMovementRequest.SapCode, itemMovementRequest.WwsOrderId));

            foreach (var item in items)
            {
                var targetItem = itemMovementRequest.Items.FirstOrDefault(t => t.Id == item.Id);
                if (targetItem == null)
                    throw new Exception(string.Format("Item {0} not found in request {1}", item.Id,
                        itemMovementRequest.RequestId));

                targetItem.SerialNumber = item.SerialNumber;
                targetItem.HasShippedFromStock = item.HasShippedFromStock;
            }

            var orderInfo = itemMovementRequest.ToOrderInfo();

            var createSalesOrderResult = _salesService.CreateSalesOrder(orderInfo);

            if (createSalesOrderResult.OperationCode.Code != ReturnCode.Ok)
            {
                throw new Exception(string.Format("Reserve wasn't created sucessfully: {0}",
                    createSalesOrderResult.OperationCode.MessageText));
            }

            itemMovementRequest.WwsOrderId = createSalesOrderResult.OrderHeader.OrderNumber;

            _storeFulfillmentStorage.Save(itemMovementRequest);

            _clientCommandManager.CreateCommand("FulfillOrderProcessingInfo",
                new FulfillOrderProcessingInfo
                {
                    OrderId = itemMovementRequest.CustomerOrder.CustomerOrderId,
                    Status = "WwsReserveRecreated",
                    RequestId = itemMovementRequest.RequestId,
                    AdditionalInfo = new FulfillOrderAdditionalInfo
                    {
                        SapCode = itemMovementRequest.SapCode,
                        WwsReserveId = itemMovementRequest.WwsOrderId
                    }
                }, itemMovementRequest.CustomerOrder.CustomerOrderId,
                JsonConvert.DeserializeObject<Key>(itemMovementRequest.BasedOnCommandId));
            return itemMovementRequest.WwsOrderId;
        }
    }
}
