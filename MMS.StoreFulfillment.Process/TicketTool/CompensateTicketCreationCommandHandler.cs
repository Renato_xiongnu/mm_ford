﻿using System;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class CompensateTicketCreationCommandHandler : ICommandHandler<CompensateTicketCreationCommand>
    {
        private readonly IStoreFulfillmentStorage _requestRepository;
        private readonly ITicketToolService _ticketToolService;

        public CompensateTicketCreationCommandHandler(IStoreFulfillmentStorage requestRepository,
            ITicketToolService ticketToolService)
        {
            _requestRepository = requestRepository;
            _ticketToolService = ticketToolService;
        }

        public SendCommandResult Handle(CompensateTicketCreationCommand commandWrapped)
        {
            var request = _requestRepository.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] { t => t.PlanSteps, t => t.Items });

            _ticketToolService.CloseTicket(request.TicketId);

            return SendCommandResult.Completed;
        }
    }
}
