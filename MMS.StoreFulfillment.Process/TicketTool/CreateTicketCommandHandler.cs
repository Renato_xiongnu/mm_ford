﻿using System;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class CreateTicketCommandHandler : ICommandHandler<CreateTicketCommand>
    {
        private readonly ITicketToolService _ticketToolClient;
        private readonly IStoreFulfillmentStorage _requestRepository;

        public CreateTicketCommandHandler(IStoreFulfillmentStorage requestRepository, ITicketToolService ticketToolClient)
        {
            _requestRepository = requestRepository;
            _ticketToolClient = ticketToolClient;
        }

        public SendCommandResult Handle(CreateTicketCommand commandWrapped)
        {
            var request = _requestRepository.GetById(commandWrapped.RequestId, new Expression<Func<ItemsMovementRequest, object>>[] { el => el.PlanSteps });
            // Create Ticket with WorkitemId = CustomerOrderId-RequestId
            var ticket = _ticketToolClient.CreateTicket(Configurations.SystemName, new WorkitemId(request).ToString(), request.SapCode);
            if (string.IsNullOrEmpty(ticket))
            {
                throw new Exception("Ticket was not created for request");
            }

            request.TicketId = ticket;
            request.MarkStepCompleted(commandWrapped.PlanStepId);

            _requestRepository.Save(request);

            return SendCommandResult.Completed;
        }
    }
}
