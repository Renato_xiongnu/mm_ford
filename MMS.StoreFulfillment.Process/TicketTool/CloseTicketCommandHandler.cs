﻿using System;
using System.Linq.Expressions;
using MMS.Cloud.Interfaces.Clients.Commands;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class CloseTicketCommandHandler : ICommandHandler<CloseTicketCommand>
    {
        private readonly IStoreFulfillmentStorage _requestRepository;
        private readonly ITicketToolService _ticketToolService;

        public CloseTicketCommandHandler(IStoreFulfillmentStorage requestRepository, ITicketToolService ticketToolService, IClientCommandManager clientCommandManager)
        {
            _requestRepository = requestRepository;
            _ticketToolService = ticketToolService;
        }

        public SendCommandResult Handle(CloseTicketCommand commandWrapped)
        {
            var request = _requestRepository.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] {el => el.PlanSteps, el => el.Items});

            _ticketToolService.CloseTicket(request.TicketId);
            request.MarkStepCompleted(commandWrapped.PlanStepId);

            _requestRepository.Save(request);
            
            return SendCommandResult.Completed;
        }
    }
}
