﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Autofac;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class CreateTaskCommandHandler : ICommandHandler<CreateTaskCommand>
    {
        private readonly ITicketToolService _ticketToolClient;
        private readonly IStoreFulfillmentStorage _requestRepository;
        private readonly ILifetimeScope _lifetimeScope;

        public CreateTaskCommandHandler(IStoreFulfillmentStorage requestRepository, ITicketToolService ticketToolClient, ILifetimeScope lifetimeScope)
        {
            _requestRepository = requestRepository;
            _ticketToolClient = ticketToolClient;
            _lifetimeScope = lifetimeScope;
        }

        public SendCommandResult Handle(CreateTaskCommand commandWrapped)
        {
            var request = _requestRepository.GetById(commandWrapped.RequestId,
                new Expression<Func<ItemsMovementRequest, object>>[] {el => el.PlanSteps});
            var planStep = _requestRepository.GetPlanStepById(commandWrapped.PlanStepId);

            using (var sc = _lifetimeScope.BeginLifetimeScope())
            {
                var taskCreator = sc.ResolveNamed<ITicketTaskCreator>(commandWrapped.TaskType);
                
                var taskDescription = taskCreator.CreateDescription(request);
                if (commandWrapped.AdditionalInfo != null)
                {
                    foreach (var additional in commandWrapped.AdditionalInfo)
                    {
                        taskDescription.Description = taskDescription.Description.Replace(additional.Key, additional.Value);
                    }
                }

                var ticketTask = new TicketTask
                {
                    TicketId = request.TicketId,
                    Name = taskDescription.Name,
                    Description = taskDescription.Description,
                    Type = taskDescription.Type,
                    RequiredFields = new RequiredField[0],
                    WorkItemId = request.CustomerOrder.CustomerOrderId,
                    Outcomes =
                        taskDescription.Outcomes.Select(
                            el =>
                                new Outcome
                                {
                                    Value = el.OutcomeName,
                                    RequiredFields = GetRequiredFields(el)
                                }).ToArray()
                };
                var taskId = _ticketToolClient.CreateTask(ticketTask);
                if (string.IsNullOrWhiteSpace(taskId)) throw new Exception("Task creation failed");

                planStep.TicketTask = new Entities.SF.Tasks.TicketTask
                {
                    TaskId = taskId,
                    PlanStepId = commandWrapped.PlanStepId,
                    Completed = false,
                    TaskType = taskDescription.Type                             
                };

                _requestRepository.Save(planStep);

                return SendCommandResult.LongRunning;
            }
        }

        private RequiredField[] GetRequiredFields(OutcomeDescription outcome)
        {
            return
                outcome.RequiredFields != null
                    ? outcome.RequiredFields.Select(
                        field => field.ExtendedValues == null ? 
                            new RequiredField {Name = field.FieldName, Type = field.FieldType.FullName} : 
                            new RequiredFieldValueList{ Name = field.FieldName,
                                                        Type = "Choice",
                                                        PredefinedValuesList = field.ExtendedValues.ToArray() })
                        .ToArray()
                    : new RequiredField[0];
        }
    }
}
