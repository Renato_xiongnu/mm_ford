﻿using System.Collections.Generic;
using System.ServiceModel;
using MMS.StoreFulfillment.Entities.SF.Tasks;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    [ServiceContract]
    public interface ICallbackTtService
    {
        // ReSharper disable InconsistentNaming
        [OperationContract]
        TaskResponse ReserveProductsContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer); //Need dynamic contract generation

        [OperationContract]
        TaskResponse WwsReserveCreationFailedContinue(string TicketId, string outcome,
            Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse CancelReserveProductsContinue(string TicketId, string outcome,
            Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse HandOverToCustomerContinue(string TicketId, string outcome,
            Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse PutOnDeliveryContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse DeliverOrderContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse CreateTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse CollectTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse ConfirmTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer);

        [OperationContract]
        TaskResponse CancelTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer);


    }
}
