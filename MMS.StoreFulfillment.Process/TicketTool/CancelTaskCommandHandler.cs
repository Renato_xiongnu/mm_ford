﻿using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class CancelTaskCommandHandler:ICommandHandler<CancelTaskCommand>
    {
        private readonly ITicketToolService _ticketToolClient;

        public CancelTaskCommandHandler(ITicketToolService ticketToolClient)
        {
            _ticketToolClient = ticketToolClient;
        }

        public SendCommandResult Handle(CancelTaskCommand commandWrapped)
        {
            _ticketToolClient.CancelTask(commandWrapped.TaskId);

            return SendCommandResult.Completed;
        }
    }
}
