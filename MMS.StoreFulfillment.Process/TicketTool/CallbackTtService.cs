﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Autofac;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using NLog;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class CallbackTtService : ICallbackTtService
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private ILifetimeScope _scope;

        public CallbackTtService(IStoreFulfillmentStorage storeFulfillmentStorage, IMessageBus messageBus, ILifetimeScope scope)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
            _scope = scope;
        }

        // ReSharper disable  InconsistentNaming
        public TaskResponse ReserveProductsContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields,
            string performer)
        {
            return CompleteTask(TicketId, outcome);
        }

        public TaskResponse WwsReserveCreationFailedContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse CancelReserveProductsContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse HandOverToCustomerContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse PutOnDeliveryContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse DeliverOrderContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse CreateTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse CollectTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse ConfirmTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }

        public TaskResponse CancelTransferContinue(string TicketId, string outcome, Dictionary<string, object> requestedFields, string performer)
        {
            return CompleteTask(TicketId, outcome, requestedFields);
        }


        private TaskResponse CompleteTask(string ticketId, string outcome,
            IDictionary<string, object> requestedFields = null)
        {
            ItemsMovementRequest request = null;
            try
            {
                request = _storeFulfillmentStorage.GetByTicketId(ticketId);
                _logger.Info("{0} {1} start process outcome {2}", request.CustomerOrder.CustomerOrderId,
                    request.RequestId,
                    outcome);
                using (var scope = _scope.BeginLifetimeScope())
                {
                    var taskHandler =
                        scope.ResolveNamed<ITicketTaskHandler>(request.CurrentPlanStep().TicketTask.TaskType);
                    taskHandler.Handle(new TaskCompletedEvent(request.CurrentPlanStep().TicketTask.TaskId,
                        outcome, ticketId, request.CurrentPlanStep().Id, requestedFields));
                }
                _logger.Info("{0} {1} finish process outcome {2}", request.CustomerOrder.CustomerOrderId,
                    request.RequestId,
                    outcome);

                return new TaskResponse {CompleteSuccessful = true};
            }
            catch (Exception e)
            {
                if (request != null)
                {
                    _logger.Error("{0} {1} Task completion failed, exception {2}", request.CustomerOrder.CustomerOrderId,
                        request.RequestId, e);
                }
                _logger.Error("Exception for complete task, TicketId: {0}, Exception: {1}", ticketId, e);
                throw;
            }
        }
    }
}
