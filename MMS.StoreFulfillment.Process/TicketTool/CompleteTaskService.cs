﻿using System;
using MMS.StoreFulfillment.Application.Proxy.Auth;
using NLog;

namespace MMS.StoreFulfillment.Application.TicketTool
{
    public class CompleteTaskService
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public void CompleteTask(string taskId, string outcome)
        {
            var ticketToolService = new TicketToolServiceClient();
            var authService = new AuthenticationServiceClient();

            var authContext = new AuthContext();

            if (!authContext.Login(() => authService.LogIn(Configurations.TtLogin, Configurations.TtPassword, true),
                authService.InnerChannel))
            {
                throw new Exception("Can't login!");
            }

            var result = authContext.Execute(
                () =>
                {
                    var assignForMeForce = ticketToolService.ForceAssignTaskForMe(taskId);
                    if (assignForMeForce.HasError)
                    {
                        Logger.Error("Error during task {0} assigning: {1}", taskId, assignForMeForce.MessageText);
                        throw new Exception("Error on task completion");
                    }
                    return ticketToolService.CompleteTask(taskId, outcome, new RequiredField[0]);
                },
                ticketToolService.InnerChannel);
            if (result.HasError)
            {
                Logger.Error("Error during task {0} completion: {1}", taskId, result.MessageText);
                throw new Exception("Error on task completion");
            }
        }
    }
}
