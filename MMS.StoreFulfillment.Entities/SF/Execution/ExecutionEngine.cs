﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;
using MMS.StoreFulfillment.Entities.WWS;
using NLog;

namespace MMS.StoreFulfillment.Entities.SF.Execution
{
    public class ExecutionEngine
    {
        private readonly IMessageBus _messageBus;
        private readonly IStoreFulfillmentStorage _requestRepository;
        private static readonly object Lock = new object();
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public ExecutionEngine(IMessageBus messageBus, IStoreFulfillmentStorage requestRepository)
        {
            _messageBus = messageBus;
            _requestRepository = requestRepository;
        }

        public void MakeIteration()
        {
            lock (Lock)
            {
                _logger.Trace("Start new iteration");
                try
                {
                    var planSteps = _requestRepository.GetUnprocessedPlanSteps();
                    var sendCommandResult = SendCommandResult.Completed;
                    foreach (var planStep in planSteps)
                    {
                        var orderId = OrderByRequestLookup.Get(planStep.RequestId);
                        try
                        {
                            _logger.Info("{0} {1}: Start process plan step {2}", orderId, planStep.RequestId,
                                planStep.Id);
                            switch (planStep.PlanStepType)
                            {
                                case PlanStepType.CreateReserve:
                                    sendCommandResult =
                                        _messageBus.SendCommand(new CreateReserveCommand(planStep.RequestId, planStep.Id));
                                    break;
                                case PlanStepType.CancelReserve:
                                    sendCommandResult =
                                        _messageBus.SendCommand(new CancelReserveCommand(planStep.RequestId, planStep.Id));
                                    break;
                                case PlanStepType.CreateTask:
                                    sendCommandResult =
                                        _messageBus.SendCommand(new CreateTaskCommand(planStep.RequestId, planStep.Id,
                                            ((TaskPlanStep) planStep).TaskType));
                                    break;
                                case PlanStepType.CreateTicket:
                                    sendCommandResult =
                                        _messageBus.SendCommand(new CreateTicketCommand(planStep.RequestId, planStep.Id));
                                    break;
                                case PlanStepType.CloseTicket:
                                    sendCommandResult =
                                        _messageBus.SendCommand(new CloseTicketCommand(planStep.RequestId, planStep.Id));
                                    break;
                                case PlanStepType.CheckPayment:
                                    sendCommandResult =
                                        _messageBus.SendCommand(new CheckPaymentCommand(planStep.RequestId, planStep.Id));
                                    break;
                            }

                            switch (sendCommandResult)
                            {
                                case SendCommandResult.Completed:
                                    break;
                                case SendCommandResult.LongRunning:
                                    planStep.Status = PlanStepStatus.Processing;
                                    _requestRepository.Save(planStep);
                                    break;
                            }

                            _logger.Info("{0} {1}: Finish process plan step {2}", orderId, planStep.RequestId,
                                planStep.Id);
                        }
                        catch (Exception e)
                        {
                            _logger.Error("{0} {1}: error process plan step {2}: {3}", orderId, planStep.RequestId,
                                planStep.Id, e);
                            planStep.Status = PlanStepStatus.Pending;
                            _requestRepository.Save(planStep);
                        }
                    }

                    _logger.Trace("Finish iteration");

                }
                catch (Exception e)
                {
                    _logger.Error("Iteration failed: {0}", e.ToString());
                }
            }
        }
    }
}
