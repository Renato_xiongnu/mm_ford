﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public class WorkitemId
    {
        private static Regex GuidRegex = new Regex(@"[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}");
        private static Regex OrderIdRegex = new Regex(@"^[0-9]{3}-[0-9]{8,12}");


        public string RequestId { get; private set; }

        public string OrderId { get; private set; }

        public WorkitemId(ItemsMovementRequest request)
        {
            OrderId = request.CustomerOrder.CustomerOrderId;
            RequestId = request.RequestId;            
        }

        public WorkitemId(string workitemId)
        {
            OrderId = OrderIdRegex.IsMatch(workitemId) ? OrderIdRegex.Match(workitemId).Value : null;
            RequestId = GuidRegex.IsMatch(workitemId) ? GuidRegex.Match(workitemId).Value : null;
        }

        public override string ToString()
        {
            return string.Join("-", new[] { OrderId, RequestId }.Where(s => !string.IsNullOrEmpty(s)));
        }
    }
}
