﻿namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public class TaskResponse
    {
        public string Message { get; set; }
        public bool CompleteSuccessful { get; set; }
    }
}
