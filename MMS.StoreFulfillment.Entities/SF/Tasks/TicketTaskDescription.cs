﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public class TicketTaskDescription
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public OutcomeDescription[] Outcomes { get; set; }
    }

    public class OutcomeDescription
    {
        public string OutcomeName { get; private set; }

        public RequiredFieldDescription[] RequiredFields { get; private set; }

        public OutcomeDescription(string outcome)
        {
            OutcomeName = outcome;
        }

        public OutcomeDescription(string outcome, RequiredFieldDescription[] requiredFields)
        {
            OutcomeName = outcome;
            RequiredFields = requiredFields;
        }
    }

    public class RequiredFieldDescription
    {
        public string FieldName { get; private set; }

        public Type FieldType { get; private set; }

        public List<string> ExtendedValues { get;  set; }

        public RequiredFieldDescription(string fieldName, Type fieldType)
        {
            if (!IsAllowedType(fieldType)) throw new TaskConstructionException(string.Format("Type {0} is not allowed", fieldType));

            FieldName = fieldName;
            FieldType = fieldType;
        }

        private bool IsAllowedType(Type t)
        {
            var types = new[] { typeof(DateTime), typeof(decimal), typeof(string), typeof(TimeSpan) };
            return t.IsPrimitive || types.Contains(t);
        }
    }

    public class TaskConstructionException : Exception
    {
        public TaskConstructionException(string text) : base(text)
        {
        }
    }
}
