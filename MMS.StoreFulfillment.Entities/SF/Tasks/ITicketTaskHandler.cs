﻿using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public interface ITicketTaskHandler : ICommandHandler<TaskCompletedEvent>
    {
    }
}
