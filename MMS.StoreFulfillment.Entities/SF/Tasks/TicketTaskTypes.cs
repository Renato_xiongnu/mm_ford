﻿using System.Collections.Generic;
using System.Linq;

namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public class TicketTaskTypes
    {
        public const string ReserveProducts = "ReserveProducts";
        public const string WwsReserveCreationFailed = "WwsReserveCreationFailed";
        public const string CancelReserveProducts = "CancelReserveProducts";
        public const string HandOverToCustomer = "HandOverToCustomer";
        public const string PutOnDelivery = "PutOnDelivery";
        public const string DeliverOrder = "DeliverOrder";
        public const string CreateTransfer = "CreateTransfer";
        public const string CancelTransfer = "CancelTransfer";
        public const string CollectTransfer = "CollectTransfer";
        public const string ConfirmTransfer = "ConfirmTransfer";

        public static IEnumerable<string> GetAllTaskTypes()
        {
            return typeof (TicketTaskTypes).GetFields().Select(t => t.GetRawConstantValue().ToString());
        }
    }
}
