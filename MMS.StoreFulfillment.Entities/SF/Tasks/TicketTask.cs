﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public class TicketTask
    {
        public TicketTask() {
            this.CreatedDate = DateTime.UtcNow;
        }

        public int Id { get; private set; }

        public string TaskId { get; set; }

        public int PlanStepId { get; set; }

        public string Outcome { get; set; }

        public bool Completed { get; set; }

        public string TaskType { get; set; }


        public  DateTime? CreatedDate { get; set; }

        public  DateTime? UpdatedDate { get; set; }

        public  DateTime? FinishedDate { get; set; }

        [Column(TypeName = "nvarchar(MAX)")]
        public string RequiredFields { get; set; }

    }
}
