﻿using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Entities.SF.Tasks
{
    public interface ITicketTaskCreator
    {
        TicketTaskDescription CreateDescription(ItemsMovementRequest request);
    }
}
