﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Tasks.Commands
{
    public class CompensateTicketCreationCommand : ICommand
    {
        public string RequestId { get; private set; }

        public CompensateTicketCreationCommand(string requestId)
        {
            RequestId = requestId;
        }
    }
}
