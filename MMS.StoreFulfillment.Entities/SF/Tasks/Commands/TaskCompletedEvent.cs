﻿using System.Collections.Generic;

namespace MMS.StoreFulfillment.Entities.SF.Tasks.Commands
{
    public class TaskCompletedEvent
    {
        public string TicketId { get; private set; }
        public int PlanStepId { get; private set; }
        public string TaskId { get; private set; }
        public string Outcome { get; private set; }
        public IDictionary<string,object> RequiredFields { get; private set; }

        public TaskCompletedEvent(string taskId, string outcome, string ticketId, int planStepId, IDictionary<string, object> requiredFields = null)
        {
            Outcome = outcome;
            TicketId = ticketId;
            PlanStepId = planStepId;
            TaskId = taskId;
            RequiredFields = requiredFields;
        }
    }
}
