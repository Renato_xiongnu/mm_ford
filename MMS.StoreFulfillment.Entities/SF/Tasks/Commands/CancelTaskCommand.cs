﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Tasks.Commands
{
    public class CancelTaskCommand : ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }
        public string TaskId { get; private set; }

        public CancelTaskCommand(string requestId, int planStepId, string taskId)
        {
            TaskId = taskId;
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
