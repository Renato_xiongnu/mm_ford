﻿using System.Collections.Generic;
using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Tasks.Commands
{
    public class CreateTaskCommand : ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }
        public string TaskType { get; private set; }
        public Dictionary<string, string> AdditionalInfo { get; private set; }

        public CreateTaskCommand(string requestId, int planStepId, string taskType,
            Dictionary<string, string> additionalInfo = null, Dictionary<string, object> additionalFields = null)
        {
            AdditionalInfo = additionalInfo;
            TaskType = taskType;
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
