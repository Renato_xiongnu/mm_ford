﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Tasks.Commands
{
    public class CreateTicketCommand : ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }

        public CreateTicketCommand(string requestId, int planStepId)
        {
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
