﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Tasks.Commands
{
    public class CloseTicketCommand : ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }

        public CloseTicketCommand(string requestId, int planStepId)
        {
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
