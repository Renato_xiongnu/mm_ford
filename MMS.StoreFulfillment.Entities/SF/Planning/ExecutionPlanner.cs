using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.SF.Items;
using MMS.StoreFulfillment.Entities.SF.Tasks;

namespace MMS.StoreFulfillment.Entities.SF.Planning
{
    public class ExecutionPlanner : IExecutionPlanner
    {
        public IList<PlanStep> PlanExecution(ItemsMovementRequest request)
        {
            switch (request.RequestType)
            {
                case RequestType.ReserveProducts:
                {
                    if (request.Items.Any(t => !string.IsNullOrEmpty(t.IncomingTransfer)))
                    {
                        return PlanForFulfillmentAfterTransfer(request.RequestId);
                    }
                    else
                    {
                        return PlanForFulfillment(request.RequestId, request.CustomerOrder.ShippingMethod);
                    }
                }
                case RequestType.TransferGoods:
                    return PlanForTransfer(request.RequestId);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IList<PlanStep> PlanForFulfillmentAfterTransfer(string requestId)
        {
            var plan = new List<PlanStep>
            {
                new PlanStep(PlanStepType.CreateTicket)
                {
                    Status = PlanStepStatus.Pending,
                    RequestId = requestId
                },
                new PlanStep(PlanStepType.CreateReserve) {RequestId = requestId},
                new TaskPlanStep(TicketTaskTypes.ConfirmTransfer) {RequestId = requestId},
                new PlanStep(PlanStepType.CheckPayment),
                new PlanStep(PlanStepType.CloseTicket)
            };
            
            return plan;
        }

        private static IList<PlanStep> PlanForTransfer(string requestId)
        {
            var plan = new List<PlanStep>
            {
                new PlanStep(PlanStepType.CreateTicket)
                {
                    Status = PlanStepStatus.Pending,
                    RequestId = requestId
                },
                new TaskPlanStep(TicketTaskTypes.CreateTransfer) {RequestId = requestId},
                new TaskPlanStep(TicketTaskTypes.CollectTransfer) {RequestId = requestId},
                new PlanStep(PlanStepType.CloseTicket)
            };
            
            return plan;
        }

        private static IList<PlanStep> PlanForFulfillment(string requestId, string shippingMethod)
        {
            var plan = new List<PlanStep>
            {
                new PlanStep(PlanStepType.CreateTicket)
                {
                    Status = PlanStepStatus.Pending,
                    RequestId = requestId
                },
                new PlanStep(PlanStepType.CreateReserve) {RequestId = requestId},
                new TaskPlanStep(TicketTaskTypes.ReserveProducts) {RequestId = requestId}
            };

            if (shippingMethod == ShippingMethods.PickupShop)
            {
                plan.Add(new PlanStep(PlanStepType.CheckPayment));
            }
            else if (shippingMethod == ShippingMethods.Delivery)
            {
                plan.Add(new TaskPlanStep(TicketTaskTypes.PutOnDelivery));
                plan.Add(new PlanStep(PlanStepType.CheckPayment));
            }
            else
            {
                throw new NotSupportedException(string.Format("Shipping method {0} currently not supported",
                    shippingMethod));
            }

            plan.Add(new PlanStep(PlanStepType.CloseTicket));

            return plan;
        }
    }
}