﻿using MMS.StoreFulfillment.Entities.SF.Tasks;
using System;

namespace MMS.StoreFulfillment.Entities.SF.Planning
{
    public class PlanStep
    {
        public int Id { get; private set; }

        public PlanStepType PlanStepType { get; private set; }
        public TicketTask TicketTask { get; set; }
        
        internal PlanStep(){}

        public PlanStep(PlanStepType planStepType)
        {
            PlanStepType = planStepType;
            CreatedDate = DateTime.UtcNow;
        }

        private PlanStepStatus _status;

        public PlanStepStatus Status {
            get {
                return _status;
            }
            set
            {

                if (value == PlanStepStatus.Finished || value == PlanStepStatus.Failed ||
                    value == PlanStepStatus.Canceled)
                {
                    FinishedDate = DateTime.UtcNow;
                }
                else if(value == PlanStepStatus.Processing || value == PlanStepStatus.Pending || value == PlanStepStatus.Compensated)
                {
                    UpdatedDate = DateTime.UtcNow;
                }

                _status = value;
            }
        }

        public string RequestId { get; set; }


        public DateTime? CreatedDate { get; private set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? FinishedDate { get; set; }
    }

    public enum PlanStepStatus
    {
        None,
        Pending,
        Processing,
        Finished,
        Failed,
        Canceled,
        Compensated
    }

    public class TaskPlanStep : PlanStep
    {
        public string TaskType { get; private set; }

        internal TaskPlanStep() { }
        
        public TaskPlanStep(string type) : base(PlanStepType.CreateTask)
        {
            TaskType = type;
        }
    }

    public class SendCommandPlanStep : PlanStep
    {
        public string CommandType { get; set; }

        internal SendCommandPlanStep(){}

        public SendCommandPlanStep(string type)
            : base(PlanStepType.SendCommand)
        {
            CommandType = type;
        }
    }

    public enum PlanStepType
    {
        CreateTicket,
        CreateTask,
        CancelTask,
        CloseTicket,
        CreateReserve,
        CancelReserve,
        CheckPayment,
        SendCommand,
        SendResponse
    }
}
