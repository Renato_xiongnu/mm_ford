using System.Collections.Generic;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Entities.SF.Planning
{
    public interface IExecutionPlanner
    {
        IList<PlanStep> PlanExecution(ItemsMovementRequest request);
    }
}