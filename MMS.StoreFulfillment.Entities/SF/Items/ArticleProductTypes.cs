﻿namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public class ArticleProductTypes
    {
        public const string Product = "Product";
        public const string Set = "Set";
    }
}
