﻿using System;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public class CustomerContact
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string AdditionalPhone { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string ZIPCode { get; set; }

        public int Floor { get; set; }

        public string RequestedDeliveryServiceOption { get; set; }

        public string RequestedDeliveryTimeslot { get; set; }

        public DateTime? RequestedDeliveryDate { get; set; }

        public string AddressWithRequestedService
        {
            get
            {
                string translatedServiceOption;
                if (RequestedDeliveryServiceOption == null)
                {
                    RequestedDeliveryServiceOption = string.Empty;
                }
                switch (RequestedDeliveryServiceOption.ToLower())
                {
                    case "curbside":
                        {
                            translatedServiceOption = "до обочины";
                            break;
                        }
                    case "doorstepembatted":
                        {
                            translatedServiceOption = "до двери с возможностью подъезда";
                            break;
                        }
                    case "doorstepunfixed":
                        {
                            translatedServiceOption = "до двери БЕЗ возможности подъезда";
                            break;
                        }
                    case "apartmentwithoutlift":
                        {
                            translatedServiceOption = "до квартиры БЕЗ лифта";
                            break;
                        }
                    case "apartmentwithlift":
                        {
                            translatedServiceOption = "до квартиры на лифте";
                            break;
                        }
                    default:
                        {
                            translatedServiceOption = RequestedDeliveryServiceOption;
                            break;
                        }
                }
                return string.Format("{0}, эт.{1}, {2}, {3}", Address, Floor, translatedServiceOption,
                    string.IsNullOrEmpty(RequestedDeliveryTimeslot)
                        ? "9-23"
                        : RequestedDeliveryTimeslot);
            }
        }
    }

    public enum DeliveryType
    {
        Pickup,
        Delivery
    }
}