namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public enum RequestType : short
    {
        ReserveProducts,
        TransferGoods
    }
}