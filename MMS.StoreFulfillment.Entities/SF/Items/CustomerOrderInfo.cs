﻿using System;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public class CustomerOrderInfo
    {
        public string CustomerOrderId { get; set; }

        public string OrderSource { get; set; }

        public bool IsOnlineOrder { get; set; }

        public string PaymentType { get; set; }

        public decimal Downpayment { get; set; }

        public string SocialCardNumber { get; set; }

        public string ShippingMethod { get; set; }


        public DateTime? RequestedDeliveryDate { get; set; }

        public DeliveryType DeliveryType
        {
            get
            {
                return string.Equals(ShippingMethod, "PickupShop", StringComparison.InvariantCultureIgnoreCase)
                    ? DeliveryType.Pickup
                    : DeliveryType.Delivery;
            }
        }
    }
}