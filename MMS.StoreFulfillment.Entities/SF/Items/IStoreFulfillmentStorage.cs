﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MMS.StoreFulfillment.Entities.SF.Planning;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public interface IStoreFulfillmentStorage
    {
        ItemsMovementRequest GetById(string requestId,
            Expression<Func<ItemsMovementRequest, object>>[] includes = null);

        PlanStep GetPlanStepByTaskId(string taskId);
        PlanStep GetPlanStepById(int planStepId);
        ItemsMovementRequest GetByTicketId(string ticketId);
        IList<ItemsMovementRequest> GetByCustomerOrderId(string customerOrderId, Expression<Func<ItemsMovementRequest, object>>[] includes);

        IList<PlanStep> GetUnprocessedPlanSteps(int count = 100);

        void Save(ItemsMovementRequest request);
        void Save(PlanStep requestBase);
    }
}
