﻿using System.Collections.Generic;
using MMS.StoreFulfillment.Entities.SF.Planning;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public class RequestBuilder
    {
        private ItemsMovementRequest _itemsMovementRequest;

        public RequestBuilder(string sapCode, RequestType requestType, string fulfillmentOrderId)
        {
            _itemsMovementRequest = new ItemsMovementRequest(sapCode, requestType, fulfillmentOrderId);
        }

        public void WithPlan(IList<PlanStep> plan)
        {
            _itemsMovementRequest.PlanSteps = plan;
        }

        public void WithItems(Item[] items)
        {
            _itemsMovementRequest.Items = items;
        }

        public void WithOrderInfo(CustomerOrderInfo orderInfo)
        {
            _itemsMovementRequest.CustomerOrder = orderInfo;
        }

        public void WithCustomerContact(CustomerContact customerContact)
        {
            _itemsMovementRequest.Contact = customerContact;
        }

        public ItemsMovementRequest Build()
        {
            return _itemsMovementRequest;
        }

        public void WithBasedRequest(ItemsMovementRequest basedOnRequest)
        {
            _itemsMovementRequest.WwsOrderId = basedOnRequest.WwsOrderId;
            _itemsMovementRequest.BasedOnRequestId = basedOnRequest.RequestId;
        }
        
        public void WithBasedCommand(string serializeObject, string commandName)
        {
            _itemsMovementRequest.BasedOnCommandId = serializeObject;
            _itemsMovementRequest.BasedOnCommand = commandName;
        }
        
        public string CurrentRequestId
        {
            get { return _itemsMovementRequest.RequestId; }
        }
    }
}
