﻿using System;
using System.Runtime.Caching;
using NLog;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public static class OrderByRequestLookup
    {
        private static readonly MemoryCache Cache = new MemoryCache("OrderByRequestCache");

        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private static IStoreFulfillmentStorage _storeFulfillmentStorage;
        
        public static void Initialize(IStoreFulfillmentStorage storeFulfillmentStorage)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public static string Get(string requestId)
        {
            var orderId = Cache.Get(requestId) as string;
            if (!string.IsNullOrEmpty(orderId)) return orderId;

            Logger.Warn("Cache miss for request {0}", requestId);
            orderId = _storeFulfillmentStorage.GetById(requestId).CustomerOrder.CustomerOrderId;
            Cache.Add(requestId, orderId, new CacheItemPolicy {AbsoluteExpiration = DateTimeOffset.Now.AddDays(1)});
            Logger.Warn("Current cache size: {0}", Cache.GetCount());

            return orderId;
        }
    }
}
