﻿using System;
using System.Collections.Generic;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public class Item
    {
        public int Id { get; set; }

        public long ItemExternalId { get; private set; }
        public string Title { get; private set; }
        public long Article { get; private set; }
        public decimal Price { get; private set; }
        public int Qty { get; private set; }
        public IList<Item> SubItems { get; set; }
        public string SerialNumber { get; set; }
        public string ReserveStatus { get; set; }
        public string ArticleProductType { get; set; }
        public string ProductGroupName { get; set; }
        public int ProductGroupNo { get; set; }
        public int DepartmentNumber { get; set; }
        public bool HasShippedFromStock { get; set; }
        public string BrandTitle { get; set; }
        public string StorageCell { get; set; }
        public string IncomingTransfer { get; set; } //Может быть несколько, если товар с разных складов

        public Item(long article, decimal price, int qty, string title, long itemExternalId)
        {
            Article = article;
            Price = price;
            Qty = qty;
            Title = title;
            ItemExternalId = itemExternalId;
        }

        public Item(Item item)
        {
            Article = item.Article;
            Price = item.Price;
            Qty = item.Qty;
            Title = item.Title;
            ItemExternalId = item.ItemExternalId;
            ArticleProductType = item.ArticleProductType;
            StorageCell = item.StorageCell;
            ReserveStatus = item.ReserveStatus;
        }

        [Obsolete("Only for auto code", true)]
        private Item()
        {
        }
    }
}