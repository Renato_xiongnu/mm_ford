﻿using MMS.Cloud.Commands.SF;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public class FulfillOrderFromTransfer
    {
        public FulfillOrder SourceCommand { get; private set; }

        public Item[] FulfillmentItems { get; private set; }

        public FulfillOrderFromTransfer(FulfillOrder sourceCommand, Item[] fulfillmentItems)
        {
            SourceCommand = sourceCommand;
            FulfillmentItems = fulfillmentItems;
        }
    }
}
