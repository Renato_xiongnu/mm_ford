﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public class ProcessItemsReservedCommand : ICommand
    {
        public string RequestId { get; private set; }

        public ProcessItemsReservedCommand(string requestId)
        {
            RequestId = requestId;
        }
    }
}
