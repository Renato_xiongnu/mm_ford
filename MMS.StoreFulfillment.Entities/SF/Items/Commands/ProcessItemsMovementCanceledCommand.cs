﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public class ProcessItemsMovementCanceledCommand:ICommand
    {
        public string RequestId { get; private set; }

        public ProcessItemsMovementCanceledCommand(string requestId)
        {
            RequestId = requestId;
        }
    }
}
