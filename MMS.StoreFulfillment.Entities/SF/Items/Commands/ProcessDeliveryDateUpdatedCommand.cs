﻿using System;
using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public class ProcessDeliveryDateUpdatedCommand : ICommand
    {
        public string RequestId { get; private set; }
        public string OrderId { get; private set; }
        public DateTime NewDeliveryDate { get; private set; }
        public string BasedOnCommandId { get; private set; }

        public ProcessDeliveryDateUpdatedCommand(string requestId, string orderId, DateTime newDeliveryDate, string basedOnCommandId)
        {
            RequestId = requestId;
            OrderId = orderId;
            NewDeliveryDate = newDeliveryDate;
            BasedOnCommandId = basedOnCommandId;
        }
    }
}
