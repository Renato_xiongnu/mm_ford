﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public  class CancelItemMovementRequest : ICommand
    {
        public string RequestId { get; private set; }

        public CancelItemMovementRequest(string requestId)
        {
            RequestId = requestId;
        }
    }
}
