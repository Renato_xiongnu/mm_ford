﻿using System.Collections.Generic;
using MMS.Cloud.Commands.SF;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public class TransferGoods
    {
        public string CustomerOrderId { get; set; }

        public IList<Cloud.Commands.SF.Item> Items { get; set; }

        public string TargetSapCode { get; set; }

        public string SourceSapCode { get; set; }

        public Customer Contact { get; set; }
    }
}
