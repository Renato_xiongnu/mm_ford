﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.SF.Items.Commands
{
    public class ProcessItemsMovementCompletionCommand : ICommand
    {
        public string RequestId { get; private set; }
        public string CompletionReason { get; private set; }
        public bool IsSuccesfull { get; private set; }
        public string IncidentCode { get; private set; }

        public ProcessItemsMovementCompletionCommand(string requestId, string completionReason)
        {
            RequestId = requestId;
            CompletionReason = completionReason;
            IsSuccesfull = true;
        }

        public ProcessItemsMovementCompletionCommand(string requestId, string completionReason, string incidentCode)
        {
            RequestId = requestId;
            CompletionReason = completionReason;
            IsSuccesfull = false;
            IncidentCode = incidentCode;
        }
    }
}
