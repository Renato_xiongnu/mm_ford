namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public enum RequestStatus : short
    {
        Created = 10,
        Executing = 20,
        Finished = 100,
        Canceled = 110,
        Compensated = 120
    }
}