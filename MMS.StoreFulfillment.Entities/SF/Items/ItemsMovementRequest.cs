﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items.Commands;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using MMS.StoreFulfillment.Entities.SF.Tasks.Commands;

namespace MMS.StoreFulfillment.Entities.SF.Items
{
    public class ItemsMovementRequest
    {
        #region Header
        private string _wwsOrderId;
        public string RequestId { get; private set; }
        public string BasedOnRequestId { get; internal set; }
        public string BasedOnCommandId { get; internal set; }
        public string BasedOnCommand { get; internal set; }
        public string FulfillmentOrderId { get; internal set; }
        public string SapCode { get; set; }
        public RequestStatus Status { get; private set; }
        public string LastStatusReason { get; private set; }
        public RequestType RequestType { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime? FinishedDate { get; set; }
        public DateTime? ExpirationDate { get; private set; }
        public bool IsShipped { get; set; } //TODO !!!
        public byte[] TimeStamp { get; set; }
        public string WwsOrderId
        {
            get { return _wwsOrderId; }
            set
            {
                if (string.IsNullOrEmpty(_wwsOrderId))
                    _wwsOrderId = value;
                else
                {
                    //DomainEvents.Publish
                    _wwsOrderId = value;
                }
            }
        }
        public string Comments { get; set; }
        private string _ticketId;

        public string TicketId
        {
            get { return _ticketId; }
            set
            {
                if (string.IsNullOrEmpty(_ticketId))
                    _ticketId = value;
                else
                    throw new InvalidOperationException("Ticket was already set");
            }
        }
        #endregion

        #region Customer order
        public CustomerOrderInfo CustomerOrder { get; internal set; }
        public CustomerContact Contact { get; internal set; }
        public virtual IList<Item> Items { get; set; }
        #endregion

        #region Execution
        public virtual IList<PlanStep> PlanSteps { get; internal set; }
        #endregion    
        
        public bool CancellationAllowed
        {
            get { return Status == RequestStatus.Created || Status == RequestStatus.Executing; }
        }

        public bool ProcessingAllowed
        {
            get { return Status == RequestStatus.Created || Status == RequestStatus.Executing; }
        }

        public bool CompensationAllowed
        {
            get { return Status != RequestStatus.Compensated; }
        }

        internal ItemsMovementRequest(string sapCode, RequestType requestType, string fulfillmentOrderId)
        {
            RequestId = Guid.NewGuid().ToString();
            var currentDateTime = DateTime.UtcNow;
            CreatedDate = currentDateTime;
            UpdatedDate = currentDateTime;
            SapCode = sapCode;
            RequestType = requestType;
            Status = RequestStatus.Created;
            FulfillmentOrderId = fulfillmentOrderId;
        }

        [Obsolete("Only for automatic construction", true)]
        internal ItemsMovementRequest()
        {
        }

        public void MarkStepCompleted(int planStepId)
        {
            if (!ProcessingAllowed)
            {
                throw new Exception(string.Format("Can't complete step for request in status {0}", Status));
            }

            var step = PlanSteps.First(el => el.Id == planStepId);
            step.Status = PlanStepStatus.Finished;
            var nextplanStep = NextPlanStep(planStepId);
            if (nextplanStep != null)
            {
                Status = RequestStatus.Executing;
                nextplanStep.Status = PlanStepStatus.Pending;
            }
            else
            {
                LastStatusReason = "Last step completed";
                Status = RequestStatus.Finished;
                FinishedDate = DateTime.UtcNow;
                DomainEvents.Publish(new ProcessItemsMovementCompletionCommand(RequestId, LastStatusReason));
            }
        }

        public void MarkStepFailed(int planStepId)
        {
            if (!ProcessingAllowed)
            {
                throw new Exception(string.Format("Can't complete step for request in status {0}", Status));
            }

            var step = PlanSteps.First(el => el.Id == planStepId);
            step.Status = PlanStepStatus.Failed;
        }

        public void MarkCompensated()
        {
            Status = RequestStatus.Compensated;
        }

        public void Cancel(IMessageBus messageBus, string incidentCode, string cancelReason)
        {
            if (!CancellationAllowed)
            {
                throw new InvalidOperationException(string.Format("Cannot cancel request: in status {0} cansellation not allowed", Status));
            }

            var pendingPlanSteps = PlanSteps.Where(t => t.Status == PlanStepStatus.Pending || t.Status == PlanStepStatus.None);
            foreach (var pendingPlanStep in pendingPlanSteps)
            {
                pendingPlanStep.Status = PlanStepStatus.Canceled;
            }

            var processingPlanSteps = PlanSteps.Where(t => t.Status == PlanStepStatus.Processing);

            foreach (var processingPlanStep in processingPlanSteps)
            {
                if (processingPlanStep.PlanStepType == PlanStepType.CreateTask && TicketTaskTypes.GetAllTaskTypes().Contains(processingPlanStep.TicketTask.TaskType))
                {
                    messageBus.SendCommand(new CancelTaskCommand(RequestId, processingPlanStep.Id, processingPlanStep.TicketTask.TaskId)); //Fire and forget
                }
                else
                {
                    processingPlanStep.Status = PlanStepStatus.Canceled;
                }
            }

            messageBus.SendCommand(new CompensateTicketCreationCommand(RequestId));

            LastStatusReason = cancelReason;
            Status = RequestStatus.Canceled;
            FinishedDate = DateTime.UtcNow;

            DomainEvents.Publish(new ProcessItemsMovementCompletionCommand(RequestId, LastStatusReason, incidentCode));
        }

        public PlanStep CurrentPlanStep()
        {
            return PlanSteps.FirstOrDefault(el => el.Status == PlanStepStatus.Processing);
        }

        private PlanStep NextPlanStep(int planStepId)
        {
            return PlanSteps.FirstOrDefault(t => t.Id > planStepId);
        }

        public void PostponeExpirationDate(DateTime newExpirationDate)
        {
            ExpirationDate = newExpirationDate;
        }

        public void AssignPlan(IList<PlanStep> plan)
        {
            PlanSteps = plan;
        }
    }
}