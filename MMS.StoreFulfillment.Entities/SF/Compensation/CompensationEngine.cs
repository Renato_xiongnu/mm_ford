﻿using System.Linq;
using MMS.StoreFulfillment.Entities.SF.Items;
using Newtonsoft.Json;
using NLog;
using Item = MMS.StoreFulfillment.Entities.SF.Items.Item;

namespace MMS.StoreFulfillment.Entities.SF.Compensation
{
    public class CompensationEngine : ICompensationEngine
    {
        private readonly IStoreFulfillmentStorage _storeFulfillmentStorage;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public CompensationEngine(IStoreFulfillmentStorage storeFulfillmentStorage)
        {
            _storeFulfillmentStorage = storeFulfillmentStorage;
        }

        public void Compensate(ItemsMovementRequest request)
        {
            Compensate(request, string.Empty, string.Empty);
        }

        public void Compensate(ItemsMovementRequest request, string commandKey, string commandName)
        {
            _logger.Info("{0}: Start compensate request {1}", request.CustomerOrder.CustomerOrderId, request.RequestId);
            if (!request.CompensationAllowed)
            {
                _logger.Info("{0}: Ignore request compenastion for request {1}, because it's status is {2}",
                    request.CustomerOrder.CustomerOrderId, request.RequestId, request.Status);
                return;
            }

            var requestBuilder = new RequestBuilder(request.SapCode, request.RequestType, request.FulfillmentOrderId);

            requestBuilder.WithCustomerContact(request.Contact);
            requestBuilder.WithOrderInfo(request.CustomerOrder);
            requestBuilder.WithItems(
                request.Items.Select(
                    el =>
                        new Item(el)).ToArray());
            requestBuilder.WithBasedRequest(request);
            if (!(string.IsNullOrEmpty(commandKey) || string.IsNullOrEmpty(commandName)))
            {
                requestBuilder.WithBasedCommand(JsonConvert.SerializeObject(commandKey), commandName);
            }

            var plan = new CompensationPlanner().PlanCompensation(requestBuilder.CurrentRequestId, request.PlanSteps, request.Items);

            requestBuilder.WithPlan(plan);
            var compensationRequest = requestBuilder.Build();

            _storeFulfillmentStorage.Save(compensationRequest);

            _logger.Info("{0}: Finish compensate request {1}", request.CustomerOrder.CustomerOrderId, request.RequestId);
            _logger.Info("{0}: Compensation request {1}", request.CustomerOrder.CustomerOrderId,
                compensationRequest.RequestId);
        }
    }
}
