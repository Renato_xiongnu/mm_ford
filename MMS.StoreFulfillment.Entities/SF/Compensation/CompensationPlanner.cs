using System;
using System.Collections.Generic;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.SF.Planning;
using MMS.StoreFulfillment.Entities.SF.Tasks;
using Item = MMS.StoreFulfillment.Entities.SF.Items.Item;

namespace MMS.StoreFulfillment.Entities.SF.Compensation
{
    public class CompensationPlanner
    {
        public IList<PlanStep> PlanCompensation(string requestId, IEnumerable<PlanStep> planSteps,
            IList<Item> requestItems)
        {
            if (planSteps == null) throw new ArgumentNullException("planSteps");
            if (requestItems == null) throw new ArgumentNullException("requestItems");

            var plan = new List<PlanStep>();

            var finishedPlanSteps =
                planSteps.Where(el => el.Status == PlanStepStatus.Finished);

            foreach (var planStep in finishedPlanSteps)
            {
                TaskPlanStep taskPlanStep = null;

                switch (planStep.PlanStepType)
                {
                    case PlanStepType.CreateReserve:
                        plan.Add(new PlanStep(PlanStepType.CancelReserve) {RequestId = requestId});
                        break;
                    case PlanStepType.CreateTask:
                        taskPlanStep = (TaskPlanStep) planStep;

                        if (taskPlanStep.TaskType == TicketTaskTypes.ReserveProducts &&
                            requestItems.All(el => el.ReserveStatus == ReserveStatuses.Reserved))
                        {
                            plan.Add(new TaskPlanStep(TicketTaskTypes.CancelReserveProducts) {RequestId = requestId});
                        }
                    
                        if (taskPlanStep.TaskType == TicketTaskTypes.CreateTransfer)
                        {
                            plan.Add(new TaskPlanStep(TicketTaskTypes.CancelTransfer) { RequestId = requestId });
                        }
                        break;



                    default:
                        continue;
                }
            }

            if (plan.Any(el => el.PlanStepType == PlanStepType.CreateTask))
            {
                plan.Insert(0, new PlanStep(PlanStepType.CreateTicket) {RequestId = requestId});
                plan.Add(new PlanStep(PlanStepType.CloseTicket) {RequestId = requestId});
            }

            if (plan.Any())
            {
                plan.First().Status = PlanStepStatus.Pending;
            }

            return plan;
        }
    }
}