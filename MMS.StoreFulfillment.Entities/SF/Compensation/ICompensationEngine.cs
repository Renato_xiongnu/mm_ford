﻿using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Entities.SF.Compensation
{
    public interface ICompensationEngine
    {
        void Compensate(ItemsMovementRequest request);
        void Compensate(ItemsMovementRequest request, string commandKey, string commandName);
    }
}