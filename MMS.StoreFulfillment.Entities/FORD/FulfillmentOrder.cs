﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMS.StoreFulfillment.Entities.FORD.Planning;
using MMS.StoreFulfillment.Entities.Messaging;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Entities.FORD
{
    public class FulfillmentOrder
    {
        public string Id { get; set; }

        public FulfillmentPlan CurrentPlan()
        {
            return FulfillmentPlans.FirstOrDefault(t => t.CurrentFulfillmentPlanStatus() == FulfillmentPlanStatus.Created);
        }

        public CustomerOrderInfo SourceOrder { get; set; }
        public FulfillmentOrderStatus FulfillmentStatus { get; internal set; }
        public IList<Item> FulfillmentItems { get; set; }
        public string SapCode { get; set; }
        public string SaleLocationSapCode { get; set; }
        public string BasedOnCommand { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime? FinishedDate { get; set; }

        public IList<FulfillmentPlan> FailedFulfillmentPlans()
        {
            return FulfillmentPlans != null
                ? FulfillmentPlans.Where(t => t.CurrentFulfillmentPlanStatus() != FulfillmentPlanStatus.Created).ToList()
                : new List<FulfillmentPlan>();
        }

        public IList<FulfillmentPlan> FulfillmentPlans { get; set; }
        
        public FulfillmentOrder()
        {
            Id = Guid.NewGuid().ToString();
            FulfillmentPlans = new List<FulfillmentPlan>();
            CreatedDate = DateTime.UtcNow;
            FulfillmentStatus = FulfillmentOrderStatus.Created;
        }

        public void NotifyStepCompleted()
        {
            var plan = CurrentPlan();
            if (plan == null)
            {
                return;
            }
            var step = plan.CurrentProcessingStep();
            if (step != null)
            {
                step.FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Finished;
            }
            var nextStep = plan.NextUnprocessed();
            if (nextStep == null)
            {
                FulfillmentStatus = FulfillmentOrderStatus.Done;
                FinishedDate = DateTime.UtcNow;
                DomainEvents.Publish(new ProcessFulfillmentOrderCompletionCommand(Id,
                    "FulfillmentOrder sucessfully fulfilled", true));
            }
            else
            {
                nextStep.FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Pending;
            }
        }

        public void NotifyStepFailed()
        {
            var plan = CurrentPlan();
            if (plan != null)
            {
                plan.CurrentProcessingStep().FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Failed;
            }            
        }

        public void AttachPlan(FulfillmentPlan fulfillmentPlan)
        {
            FulfillmentPlans.Add(fulfillmentPlan);
            FulfillmentStatus = FulfillmentOrderStatus.Executing; //TODO
        }

        public void Fatal(string incidentCode, string reason)
        {
            FinishedDate = DateTime.UtcNow;
            FulfillmentStatus = FulfillmentOrderStatus.FatalException;
            DomainEvents.Publish(new ProcessFulfillmentOrderCompletionCommand(Id, reason, false, incidentCode));
        }
    }

    public enum FulfillmentOrderStatus : short
    {
        Created = 10,
        PlanGenerated = 20,
        ReservesApproved = 30,
        Executing = 50,
        Done = 100,
        FatalException = 110,
        Canceled = 120
    }
}
