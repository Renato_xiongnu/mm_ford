﻿namespace MMS.StoreFulfillment.Entities.FORD
{
    public class ProcessFulfillmentOrderCompletionCommand
    {
        public string FulfillmentOrderId { get; private set; }
        public string CompletionReason { get; private set; }
        public bool IsSuccesfull { get; private set; }
        public string IncidentCode { get; private set; }

        public ProcessFulfillmentOrderCompletionCommand(string fulfillmentOrderId, string completionReason, bool isSuccesfull)
        {
            FulfillmentOrderId = fulfillmentOrderId;
            CompletionReason = completionReason;
            IsSuccesfull = isSuccesfull;
        }

        public ProcessFulfillmentOrderCompletionCommand(string fulfillmentOrderId, string completionReason, bool isSuccesfull, string incidentCode)
        {
            FulfillmentOrderId = fulfillmentOrderId;
            CompletionReason = completionReason;
            IsSuccesfull = isSuccesfull;
            IncidentCode = incidentCode;
        }
    }
}
