﻿namespace MMS.StoreFulfillment.Entities.FORD.Execution
{
    public interface IPlanStepHandler
    {
        void Process(FulfillmentPlanStep fulfillmentPlanStep);
    }
}
