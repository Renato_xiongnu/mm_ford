﻿using System;
using MMS.Json;

namespace MMS.StoreFulfillment.Entities.FORD
{
    public class FulfillmentPlanStep
    {
        public int Id { get; private set; }

        public string PlanStepType { get; private set; }

        public string FulfillmentOrderId { get; private set; }

        private FulfillmentPlanStepStatus _fulfillmentPlanStepStatus;

        public FulfillmentPlanStepStatus FulfillmentPlanStepStatus
        {
            get { return _fulfillmentPlanStepStatus; }
            set
            {
                if (value == FulfillmentPlanStepStatus.Finished || value == FulfillmentPlanStepStatus.Failed ||
                    value == FulfillmentPlanStepStatus.Canceled)
                {
                    FinishedDate = DateTime.UtcNow;
                }
                _fulfillmentPlanStepStatus = value;
            }
        }

        public string PlanStepContext { get; private set; }

        public string PlanStepContextTypeName { get; private set; }

        public string Responsible { get; private set; }

        public DateTime CreatedDate { get; private set; }
        
        public DateTime? FinishedDate { get; private set; }

        public FulfillmentPlanStep(string planStepType, string fulfillmentOrderId, string responsible)
        {
            PlanStepType = planStepType;
            FulfillmentOrderId = fulfillmentOrderId;
            Responsible = responsible;
            CreatedDate = DateTime.UtcNow;
        }

        public FulfillmentPlanStep(string planStepType, string fulfillmentOrderId, object context, string responsible)
        {
            PlanStepType = planStepType;
            FulfillmentOrderId = fulfillmentOrderId;
            PlanStepContext = JsonConvert.SerializeObject(context);
            PlanStepContextTypeName = context.GetType().Name;
            Responsible = responsible;
            CreatedDate = DateTime.UtcNow;
        }
        
        //For EF
        // ReSharper disable once UnusedMember.Local
        private FulfillmentPlanStep()
        {
        }
    }

    public enum FulfillmentPlanStepType
    {
        ReserveAndPickup,
        ReserveAndDeliver,
        TransferGoods,
        ConfirmTransferAndPickup
    }

    public enum FulfillmentPlanStepStatus
    {
        None,
        Pending,
        Processing,
        Finished,
        Failed,
        Canceled,
        Compensated
    }
}