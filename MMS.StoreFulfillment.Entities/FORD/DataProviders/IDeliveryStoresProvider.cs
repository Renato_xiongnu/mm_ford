﻿using System.Linq;
using MMS.StoreFulfillment.Entities.FORD.Planning;

namespace MMS.StoreFulfillment.Entities.FORD.DataProviders
{
    public interface IDeliveryStoresProvider
    {
        IOrderedEnumerable<Stock> GetAvailableStores(FulfillmentOrder fulfillmentOrder);
    }
}
