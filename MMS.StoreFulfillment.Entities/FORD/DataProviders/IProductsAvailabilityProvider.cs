﻿using System.Collections.Generic;
using MMS.StoreFulfillment.Entities.SF.Items;

namespace MMS.StoreFulfillment.Entities.FORD.DataProviders
{
    public interface IProductsAvailabilityProvider
    {
        bool AvailableInStore(string sapCode, IEnumerable<Item> items);
    }
}
