﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMS.StoreFulfillment.Entities.FORD.Planning;

namespace MMS.StoreFulfillment.Entities.FORD.DataProviders
{
    public interface ITransferStoresDataProvider
    {
        IOrderedEnumerable<Stock> GetAvailableStoresForTransfer(string sapCode);
    }
}
