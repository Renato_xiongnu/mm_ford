﻿using System.Collections.Generic;
using System.Linq;

namespace MMS.StoreFulfillment.Entities.FORD.Planning
{
    public class FulfillmentPlan
    {
        public int Id { get; set; }

        public string BasedOnTemplate { get; set; }

        public FulfillmentOrder FulfillmentOrder { get; set; }
        
        public IList<FulfillmentPlanStep> FulfillmentPlanSteps { get; private set; }

        public FulfillmentPlan()
        {
            FulfillmentPlanSteps = new List<FulfillmentPlanStep>();
        }

        public FulfillmentPlanStatus CurrentFulfillmentPlanStatus()
        {
            if (FulfillmentPlanSteps.All(t => t.FulfillmentPlanStepStatus == FulfillmentPlanStepStatus.Finished))
            {
                return FulfillmentPlanStatus.Completed;
            }
            if (FulfillmentPlanSteps.Any(t => t.FulfillmentPlanStepStatus == FulfillmentPlanStepStatus.Failed))
            {
                return FulfillmentPlanStatus.Failed;
            }
            return FulfillmentPlanStatus.Created;
        }

        public FulfillmentPlanStep CurrentProcessingStep()
        {
            return FulfillmentPlanSteps.FirstOrDefault(t => t.FulfillmentPlanStepStatus == FulfillmentPlanStepStatus.Processing);
        }

        public FulfillmentPlanStep NextUnprocessed()
        {
            return FulfillmentPlanSteps.FirstOrDefault(t => t.FulfillmentPlanStepStatus == FulfillmentPlanStepStatus.None);
        }
    }

    public enum FulfillmentPlanStatus
    {
        Created = 10,
        Completed = 100,
        Failed = 110
    }
}
