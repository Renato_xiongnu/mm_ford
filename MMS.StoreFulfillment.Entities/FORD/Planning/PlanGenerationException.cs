﻿using System;

namespace MMS.StoreFulfillment.Entities.FORD.Planning
{
    [Serializable]
    public class PlanGenerationException:Exception
    {
        public PlanGenerationException(string message):base(message)
        {
        }
    }
}
