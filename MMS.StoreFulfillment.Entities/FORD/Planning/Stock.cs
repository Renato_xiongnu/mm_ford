﻿namespace MMS.StoreFulfillment.Entities.FORD.Planning
{
    public class Stock
    {
        public string SapCode { get; set; }

        public int Rating { get; set; }
    }
}
