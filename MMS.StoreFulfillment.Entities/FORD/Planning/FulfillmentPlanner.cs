﻿using System;
using System.Linq;
using MMS.Cloud.Commands.SF;
using MMS.StoreFulfillment.Entities.FORD.DataProviders;
using MMS.StoreFulfillment.Entities.FORD.Execution;
using NLog;
using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.FORD.Planning
{
    public class FulfillmentPlanner
    {
        private readonly IProductsAvailabilityProvider _productsAvailabilityProvider;
        private readonly IDeliveryStoresProvider _availableStoresProvider;
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public FulfillmentPlanner(IProductsAvailabilityProvider productsAvailabilityProvider, IDeliveryStoresProvider availableStoresProvider)
        {
            _productsAvailabilityProvider = productsAvailabilityProvider;
            _availableStoresProvider = availableStoresProvider;
        }

        public FulfillmentPlan PlanFulfillment(FulfillmentOrder fulfillmentOrder)
        {
            switch (fulfillmentOrder.SourceOrder.ShippingMethod)
            {
                case ShippingMethods.Delivery:
                    return PlanForDelivery(fulfillmentOrder);
                case ShippingMethods.PickupShop:
                    return PlanForPickup(fulfillmentOrder);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private FulfillmentPlan PlanForPickup(FulfillmentOrder fulfillmentOrder)
        {
            var availableInStore = _productsAvailabilityProvider.AvailableInStore(fulfillmentOrder.SapCode, fulfillmentOrder.FulfillmentItems);
            _logger.Info("{0}: PlanForPickup - AvailableInStore: {1}", fulfillmentOrder.SourceOrder.CustomerOrderId, availableInStore);

            var plan = new FulfillmentPlan();

            if (!availableInStore)
            {
                if (fulfillmentOrder.SourceOrder.RequestedDeliveryDate.HasValue && fulfillmentOrder.SourceOrder.RequestedDeliveryDate.Value.Date <= DateTime.Today)
                {
                    throw new PlanGenerationException("Plan with Transfer cannot be generated: RequestedDeliveryDate <= DateTime.UtcNow");
                }

                plan.FulfillmentPlanSteps.Add(new FulfillmentPlanStep(FulfillmentPlanStepType.TransferGoods.ToString(),
                    fulfillmentOrder.Id, fulfillmentOrder.SaleLocationSapCode) // Используем виртуальный магазин для полседующего вызова сервиса SaleLocationSelector
                //Мы не знаем ответственного на этапе планирования, нужен какой-то признак
                {
                    FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Pending
                });



                plan.FulfillmentPlanSteps.Add(
                    new FulfillmentPlanStep(FulfillmentPlanStepType.ConfirmTransferAndPickup.ToString(),
                        fulfillmentOrder.Id, fulfillmentOrder.SapCode));
            }
            else
            {
                plan.FulfillmentPlanSteps.Add(
                    new FulfillmentPlanStep(FulfillmentPlanStepType.ReserveAndPickup.ToString(), fulfillmentOrder.Id,
                        fulfillmentOrder.SapCode)
                    {
                        FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Pending
                    });
            }

            return plan;
        }

        private FulfillmentPlan PlanForDelivery(FulfillmentOrder fulfillmentOrder)
        {
            var deliveryStores = _availableStoresProvider.GetAvailableStores(fulfillmentOrder);
            var previousUnsucsessfullOrders = fulfillmentOrder.FailedFulfillmentPlans();
            var previousUnsucssfullStores = previousUnsucsessfullOrders.SelectMany(
                t =>
                    t.FulfillmentPlanSteps.Where(
                        store => store.PlanStepType == FulfillmentPlanStepType.ReserveAndDeliver.ToString())
                        .Select(el => el.Responsible));


            //var availableInStore =
            //    _productsAvailabilityProvider
            //        .AvailableInStore(fulfillmentOrder.SapCode, fulfillmentOrder.FulfillmentItems);


            var targetStore =
                deliveryStores
                    .Where(t => !previousUnsucssfullStores.Contains(t.SapCode))
                    .OrderByDescending(t => t.Rating)
                    .Where(t => _productsAvailabilityProvider.AvailableInStore(t.SapCode, fulfillmentOrder.FulfillmentItems))
                    .FirstOrDefault();

            if (targetStore == null)
            {
                throw new PlanGenerationException("Plan cannot be generated");
            }

            var fulfillmentPlan = new FulfillmentPlan();
            fulfillmentPlan.FulfillmentPlanSteps.Add(
                new FulfillmentPlanStep(FulfillmentPlanStepType.ReserveAndDeliver.ToString(), fulfillmentOrder.Id,
                    targetStore.SapCode) {FulfillmentPlanStepStatus = FulfillmentPlanStepStatus.Pending});

            return fulfillmentPlan;
        }
    }
}
