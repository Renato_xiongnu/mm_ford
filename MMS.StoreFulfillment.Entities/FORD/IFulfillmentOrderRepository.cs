﻿using System.Collections.Generic;

namespace MMS.StoreFulfillment.Entities.FORD
{
    public interface IFulfillmentOrderRepository
    {
        void Save(FulfillmentOrder fulfillmentOrder);
        FulfillmentOrder GetById(string fulfillmentOrderId);
        IList<FulfillmentOrder> GetForCustomerOrderId(string customerOrderId);
        IList<FulfillmentPlanStep> GetUprocessedPlanSteps();
        void Save(FulfillmentPlanStep unprocessedPlanStep);
    }
}
