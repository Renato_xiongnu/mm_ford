﻿namespace MMS.StoreFulfillment.Entities.Messaging
{
    public interface ICommand
    {
        string RequestId { get; }
    }
}
