﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS.StoreFulfillment.Entities.Messaging
{
    public static class IncidentCode
    {
        public const string WwsReserveCanceled = "WwsReserveCanceled";
        public const string WwsReserveCreationFailed = "WwsReserveCreationFailed";
        public const string CanceledExtrernally = "CanceledExtrernally";
        public const string CanceledByCustomer = "CanceledByCustomer";
        public const string CollectTransferFail = "CollectTransferFail";
        public const string CreateTransferFail = "CreateTransferFail";
        public const string ConfirmTransferFail = "ConfirmTransferFail";
        public const string NotAllItemsReserved = "NotAllItemsReserved";
    }
}
