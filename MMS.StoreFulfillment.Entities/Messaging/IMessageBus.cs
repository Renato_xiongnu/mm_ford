﻿namespace MMS.StoreFulfillment.Entities.Messaging
{
    public interface IMessageBus
    {
        SendCommandResult SendCommand<T>(T command);
    }

    public enum SendCommandResult
    {
        Completed,
        LongRunning
    }
}
