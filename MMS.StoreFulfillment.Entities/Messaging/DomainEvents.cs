﻿using System;

namespace MMS.StoreFulfillment.Entities.Messaging
{
    public static class DomainEvents
    {
        private static IMessageBus _messageBus;

        public static void Initialize(IMessageBus messageBus)
        {
            _messageBus = messageBus;
        }

        public static void Publish<T>(T command)
        {
            if (_messageBus == null)
            {
                throw new Exception("Domain events should be initialized!");
            }

            var commandResult = _messageBus.SendCommand(command);
            if (commandResult == SendCommandResult.LongRunning)
            {
                throw new ArgumentException("Can't use domain events for long-running commands!");
            }
        }
    }
}
