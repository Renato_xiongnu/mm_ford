﻿namespace MMS.StoreFulfillment.Entities.Messaging
{
    public interface ICommandHandler<in T>
    {
        SendCommandResult Handle(T commandWrapped);
    }
}
