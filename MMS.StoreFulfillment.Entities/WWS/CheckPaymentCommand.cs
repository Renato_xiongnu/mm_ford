﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.WWS
{
    public class CheckPaymentCommand:ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }

        public CheckPaymentCommand(string requestId, int planStepId)
        {
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
