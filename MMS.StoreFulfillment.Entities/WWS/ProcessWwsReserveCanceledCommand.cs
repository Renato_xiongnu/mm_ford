﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.WWS
{
    public class ProcessWwsReserveCanceledCommand:ICommand
    {
        public string RequestId { get; private set; }

        public ProcessWwsReserveCanceledCommand(string requestId)
        {
            RequestId = requestId;
        }
    }
}
