﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.WWS
{
    public class CreateReserveCommand : ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }
        
        public CreateReserveCommand(string requestId, int planStepId)
        {
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
