﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.WWS
{
    public class CancelReserveCommand : ICommand
    {
        public int PlanStepId { get; private set; }
        public string RequestId { get; private set; }

        public CancelReserveCommand(string requestId, int planStepId)
        {
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}