﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.WWS
{
    public class ProcessReserveCreationFailedCommand : ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }

        public ProcessReserveCreationFailedCommand(string requestId, int planStepId)
        {
            RequestId = requestId;
            PlanStepId = planStepId;
        }
    }
}
