﻿using MMS.StoreFulfillment.Entities.Messaging;

namespace MMS.StoreFulfillment.Entities.WWS
{
    public class ProcessWwsReservePaidCommand:ICommand
    {
        public string RequestId { get; private set; }
        public int PlanStepId { get; private set; }

        public ProcessWwsReservePaidCommand(string requestId, int planStepId)
        {
            PlanStepId = planStepId;
            RequestId = requestId;
        }
    }
}
